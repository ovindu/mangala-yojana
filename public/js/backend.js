
/*
 * Complete the order from backend
 */
$(document).on('click', '.complete-order', function () {
    var payMethod = $(this).attr('data-order-type');
    var payment_id = $(this).attr('data-paymentid');
    var pay_user = $(this).attr('data-userid');

    $('[name="payment_type"]').val(payMethod);
    $('[name="payment_id"]').val(payment_id);
    $('[name="payment_user"]').val(pay_user);

    if (payMethod == 'hnb') {
	if (confirm("This payment will be marked as completed and You cannot undo this operation. Continue?")) {
	    $('[name="completePayment"]').submit();
	}
    }
});

$(document).on('change', '[name="filterPayments"]', function () {
    /*
     *  0 : All
     *  1 : Completed
     *  2 : Not Completed.
     */
    $('.allrow').each(function () {
	$(this).fadeOut();
    });

    if ($(this).val() == '0') {
	$('.allrow').each(function () {
	    $(this).fadeIn();
	});
    } else if ($(this).val() == '1') {
	$('.completed').each(function () {
	    $(this).fadeIn();
	});
    } else if ($(this).val() == '2') {
	$('.notcompleted').each(function () {
	    $(this).fadeIn();
	});
    }

});
/*
 * New user verification
 */
$(function () {
    $('.verify-user').click(function () {
	var userId = $(this).data('userid');
	$.ajax({
	    url: userVerifyUrl,
	    data: {id: userId},
	    success: function (data, textStatus, jqXHR) {
		$('tr[data-userid="' + userId + '"]').slideUp();
	    }
	});
    });

    $('.verify-cancel').click(function () {
	var userId = $(this).data('userid');
	$.ajax({
	    url: userCancelVerifyUrl,
	    data: {id: userId},
	    success: function (data, textStatus, jqXHR) {
		$('tr[data-userid="' + userId + '"]').slideUp();
	    }
	});
    });

});

/*
 * End of New user verification
 */


/*
 * User Profile Changes verification Page
 */
$(function () {

    //Verify edit
    $('.verify-edit').click(function () {
	var editId = $(this).data('edit-id');
	$.ajax({
	    url: editVerifyUrl + '/' + editId,
	    success: function (data, textStatus, jqXHR) {
		$('tr[data-edit-id="' + editId + '"]').slideUp();
	    }
	})
    });

    $('.verify-edit-cancel').click(function () {
	var editId = $(this).data('edit-id');
	$.ajax({
	    url: editVerifyIgnoreUrl + '/' + editId,
	    success: function (data, textStatus, jqXHR) {
		$('tr[data-edit-id="' + editId + '"]').slideUp();
	    }
	});
    });
});