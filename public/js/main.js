/*
 *  
 *  Update the last visit time 
 *  
 */
var MOBVER_ALERT_ERROR = 'error';
var MOBVER_ALERT_SUCCESS = 'success';
var ajaxUrlGroup = '/AJAX';

$(document).ready(function () {
    setTimeout(function () {
        $.ajax({
            url: ajaxUrlGroup + '/IMONLINE',
            type: 'POST',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            contentType: false,
            processData: false,
        }).done(function () {
        });
    }, 3000);
});

$(document).on('click', '[data-target="#plz-upgrade-popup"]', function () {
    var title = $(this).attr('data-title');
    var body = $(this).attr('data-body');
    $('#plz-upgrade-popup_title').html(title);
    $('#plz-upgrade-popup_msg_body').html(body);
});

/*
 * 
 * Only accept numeric values
 * 
 */
var specialKeys = new Array();
specialKeys.push(8); //Backspace
function IsNumeric(e) {
    var keyCode = e.which ? e.which : e.keyCode
    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
    document.getElementById("number").right = ret ? "1000" : "10";
    return ret;
}

/*
 * 
 * Refine Search - Filter by profile-pic , Salary - marital status.. etc
 * 
 */

$(document).on('click', '.imageSettings', function () {
    var thisID = $(this).attr('id');
    if (thisID === "all") {
        $(".allImage").stop(true, true).fadeOut({
            duration: 600,
            queue: false
        });
        setTimeout(function () {
            $(document).find(".allImage").fadeIn({ duration: 400, queue: false });
        }, 650);

    } else if (thisID === "userImage") {
        $(".noImage").stop(true, true).fadeOut({
            duration: 600,
            queue: false
        });
        setTimeout(function () {
            $(document).find(".UserImage").fadeIn({ duration: 400, queue: false });
        }, 650);
    } else if (thisID === "noImage") {
        $(".allImage").stop(true, true).fadeOut({
            duration: 600,
            queue: false
        });
        setTimeout(function () {
            $(document).find(".noImage").fadeIn({ duration: 400, queue: false });
        }, 650);
    }
});
// Filter by joined date.
$(document).on('click', '.resJoined', function () {
    var count = 1;
    var joinedVal = $(this).attr('maker');
    $('.results').stop(true, true).fadeOut({
        duration: 200,
        queue: false
    });
    $(".results").each(function () {
        var joind = $(this).attr("joined");
        var id = $(this).attr("id");

        if (joinedVal === joind) {
            setTimeout(function () {
                $(document).find("#" + id).fadeIn({ duration: 200, queue: false });
            }, 650);
        } else if (joinedVal === 'all') {
            if (count === 1) {
                count++;
                setTimeout(function () {
                    $(document).find(".results").fadeIn({ duration: 200, queue: false });
                }, 650);
            }
        }
    });

});

//filter by last active dates
$(document).on('click', '.resActive', function () {
    var count = 1;
    var joinedVal = $(this).attr('maker');
    $('.results').stop(true, true).fadeOut({
        duration: 200,
        queue: false
    });
    $(".results").each(function () {
        var joind = $(this).attr("activeIn");
        var id = $(this).attr("id");

        if (joinedVal === joind) {
            setTimeout(function () {
                $(document).find("#" + id).fadeIn({ duration: 200, queue: false });
            }, 650);
        } else if (joinedVal === 'all') {
            if (count === 1) {
                count++;
                setTimeout(function () {
                    $(document).find(".results").fadeIn({ duration: 200, queue: false });
                }, 650);
            }
        }
    });

});

//filter by annual income
$(document).on('click', '.resIncome', function () {
    var count = 1;
    var joinedVal = $(this).attr('maker');

    $('.results').stop(true, true).fadeOut({
        duration: 200,
        queue: false
    });

    $(".results").each(function () {
        var joind = $(this).attr("income");
        var id = $(this).attr("id");

        if (joinedVal === joind) {
            setTimeout(function () {
                $(document).find("#" + id).fadeIn({ duration: 200, queue: false });
            }, 650);
        } else if (joinedVal === 'all') {
            if (count === 1) {
                count++;
                setTimeout(function () {
                    $(document).find(".results").fadeIn({ duration: 200, queue: false });
                }, 650);

            }
        }
    });

});

//filter by marital status
$(document).on('click', '.marital', function () {
    var count = 1;
    var joinedVal = $(this).attr('maker');

    $('.results').stop(true, true).fadeOut({
        duration: 200,
        queue: false
    });

    $(".results").each(function () {
        var joind = $(this).attr("marital");
        var id = $(this).attr("id");

        if (joinedVal === joind) {
            setTimeout(function () {
                $(document).find("#" + id).fadeIn({ duration: 200, queue: false });
            }, 650);
        } else if (joinedVal === 'all') {
            if (count === 1) {
                count++;
                setTimeout(function () {
                    $(document).find(".results").fadeIn({ duration: 200, queue: false });
                }, 650);

            }
        }
    });

});

//filter by Interested persons
$(document).on('click', '.favorite', function () {
    var bool = $(this).attr('data-bool');
    if (bool == 0) {
        $('.icon-heart').addClass('interested');
        $(this).attr('data-bool', '1');
        $(".allImage").stop(true, true).fadeOut({
            duration: 600,
            queue: false
        });

        setTimeout(function () {
            $(document).find(".liked-profile").fadeIn({ duration: 400, queue: false });
        }, 650);
    } else {
        $(this).attr('data-bool', '0');
        $('.icon-heart').removeClass('interested');
        $(".results").stop(true, true).fadeOut({
            duration: 600,
            queue: false
        });
        setTimeout(function () {
            $(document).find(".results").fadeIn({ duration: 400, queue: false });
        }, 650);
    }
});

// End of refine search page java scripts.


// Like and unlike
$(document).ready(function () {
    $(document).on('click', '.like', function () {
        var likeID = $(this).attr('data-like-user-id');
        $.ajax({
            url: ajaxUrlGroup + "/LIKE/",
            type: 'POST',
            data: { id: likeID },
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            contentType: false,
            processData: false,

        }).done(function (e) {
            $('#btn-' + likeID).attr('liked_id', e);
            $('#btn-' + likeID).addClass("liked");
            $('#btn-' + likeID).removeClass("like");
            $('#' + likeID).addClass("liked-profile");
            $(".friend-" + likeID).addClass('liked-profile');
        });
    });

    $(document).on('click', '.liked', function () {
        var likeID = $(this).attr('data-like-user-id');
        var unlikeID = $(this).attr('liked_id');
        $.ajax({
            url: ajaxUrlGroup + "/UNLIKE",
            type: 'POST',
            data: { id: unlikeID },
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            contentType: false,
            processData: false,
        }).done(function (e) {
            $('#btn-' + likeID).addClass("like");
            $('#btn-' + likeID).removeClass("liked");
            $('#' + likeID).removeClass("liked-profile");
            $(".friend-" + likeID).removeClass('liked-profile');
        });
    });
});


/*
 * Loading notifications
 */

var templateNotification = '{{#each data}}' +
    '<li class="container-fluid notification_li" id="the-notification-{{id}}" role="presentation">' +
    '<button type="button" data-notif="{{id}}" class="notification-close-btn" aria-label="Close">' +
    '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>' +
    '</button>' +
    '<a href="{{url}}">' +
    '<div class="row">' +
    '<div class="col-sm-12">' +
    '<img src="{{img}}" class="img-responsive">' +
    '<div class="notiText">{{{text}}}</div>' +
    '<div class="notification_time">{{ago}}</div>' +
    '</div>' +
    '</div>' +
    '<div class="clearfix"></div>' +
    '</a>' +
    '</li>' +
    '{{/each}}';


var templateNoNewNotifications = '<li class="container-fluid notification_li" id="the-notification-empty" role="presentation">' +
    '<div class="row">' +
    '<div class="col-sm-12">' +
    '<div class="notiText text-center">No new notifications!</div>' +
    '<div class="notification_time"></div>' +
    '</div>' +
    '</div>' +
    '</li>';

var templateNoNewNotificationsSinhala = '<li class="container-fluid notification_li" id="the-notification-empty" role="presentation">' +
    '<div class="row">' +
    '<div class="col-sm-12">' +
    '<div class="notiText text-center">නව දැනුම්දීම් නොමැත.</div>' +
    '<div class="notification_time"></div>' +
    '</div>' +
    '</div>' +
    '</li>';

var numOfNotifications = 0;
var numOfNewNotifications = 0;

$(document).ready(function () {
    retrieveAndShowNotifications();
    setInterval(function () {
        retrieveAndShowNotifications();
    }, 10000);
});

function retrieveAndShowNotifications() {
    $.ajax({
        url: ajaxUrlGroup + "/GETNOTIFICATIONS",
        dataType: 'json',
        type: 'GET',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        contentType: false,
        processData: false,

    }).done(function (data) {
        numOfNotifications = data['notifications'].length;
        numOfNewNotifications = data['unread_count'];
        var template = Handlebars.compile(templateNotification);
        $('#loadNotifications').html(template({ data: data['notifications'] }));
        updateNumberOfNotifications();
    });
}

function updateNumberOfNotifications() {
    if (numOfNotifications == 0) {
        if ($('#get_locale').val() === 'si') {
            $('#loadNotifications').html("" + templateNoNewNotificationsSinhala);
        } else {
            $('#loadNotifications').html("" + templateNoNewNotifications);
        }
    }

    if (numOfNewNotifications > 0) {
        $('#notifictionIcon').addClass('menu-icon-active');
        $('#notif-count').show();
        $('#notif-count').html("" + numOfNewNotifications);
    } else {
        $('#notifictionIcon').removeClass('menu-icon-active');
        $('#notif-count').hide();
    }

}

$(document).on('click', '.notification-close-btn', function () {
    var theID = $(this).attr('data-notif');
    $.ajax({
        url: ajaxUrlGroup + "/CLOSENOTIFICATION",
        dataType: 'json',
        type: 'POST',
        data: { not_id: theID },
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        contentType: false,
        processData: false,
    }).done(function (data) {
        if (data) {
            numOfNotifications = numOfNotifications - 1;
            $('#the-notification-' + theID).slideUp();
            setTimeout(function () {
                updateNumberOfNotifications();
            }, 400);
        }
    });
});

$(document).on('click', '#notificationPanelMenu', function () {
    var $parent = $(this).parent();
    var notifIds = [];
    if ($parent.hasClass('open')) {
        var num = 0;
        $parent.find('.notification-close-btn').each(function () {
            notifIds[num] = $(this).attr('data-notif');
            num++;
        });
        makeNotificationsRead(notifIds);
    }
});

function makeNotificationsRead(ids_) {
    $.ajax({
        url: ajaxUrlGroup + "/READNOTIFICATIONS",
        dataType: 'json',
        type: 'POST',
        data: { ids: ids_ },
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        contentType: false,
        processData: false,
    }).done(function (data) {
        if (data) {
            numOfNewNotifications = 0;
            updateNumberOfNotifications();
        }
    });
}

/*
 * End of loading notifications
 */

// stop panels from closing when clicking on them.
$(document).on('click', '.dropdown-menu.top_menu_notice_panel', function (e) {
    $(this).parent().addClass('open');
});

//Open dropdown menu when clicking user name at profile
$(document).on('click', '.open-dropdown', function () {
    $('.header-settings').addClass('open');
});

//Reporting

$(document).on('click', '.radio-btn', function () {
    $('.report-continue').removeClass('disabled');
});

$(document).on('click', '.report-continue', function () {
    var selectorID = $('input[name=fake-prof-item]:radio:checked').attr('id');
    var radioValue = $('.' + selectorID).html();
    var text = $('#additional_text').val();
    var userid = $('#additional_text').attr('data-user');
    var verifi = $('#userid').val();
    if (text.length < 1) {
        text = "Not Given";
    }
    var text_pass = radioValue + " , " + text;
    if (userid == verifi) {
        $.ajax({
            url: ajaxUrlGroup + "/REPORT",
            type: 'POST',
            data: { fid: userid, message: text_pass },
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            contentType: false,
            processData: false,
        }).done(function (e) {
            $('#user-report-confirm').modal('show');
        });
    } else {

    }

});

$(document).on('click', '.submit-block', function () {
    $('#block-form').submit();
});
$(document).on('click', '.submit-unfriend', function () {
    $('#unfriend-form').submit();
});

/*
 * Select SmallCity by District
 */
$(document).on('change', '#district', function () {
    var count = 0;
    $('#reg-small-city option[value!=""]').remove();
    $.each(arrayFromPHP, function (i) {
        if (arrayFromPHP[i]['dist'] === $('#district').val()) {
            count++;
            $('#reg-small-city').append($("<option></option>").attr("value", [i]).text(arrayFromPHP[i]['city']));
        }
    });
    if (count <= 0) {
        $('#city_living').fadeOut();
    } else {
        $('#city_living').fadeIn();
    }
});
// fill data @ Block button confirmation box. 
$(document).on('click', '.btn-block-stng-drpwn', function () {
    var userID = $(this).attr('data-uid');
    var headingText = "Block " + $(this).attr('data-user-name');
    $('#hidden-block-id').val(userID);
    $('#block-confirmation-heading').html(headingText);
});

/*
 * Search form Validation
 */
$(document).on('click', '#formBtn', function () {
    $("#formBtn").attr('type', 'button');

    var bool = false;
    if ('Select...' === $('#motherTongue').val()) {
        var oldClasses = $("#motherTongue").attr('class');
        $("#motherTongue").attr('class', 'validateError ' + oldClasses);
        bool = true;
    } else {
        $("#motherTongue").removeClass('validateError');
        bool = false;
        var mtErrors;
        var cntryErrors;
        var religErrors;
        var ageErrors;
        if ('Select...' === $('#motherTongue').val()) {
            var oldClasses = $("#motherTongue").attr('class');
            $("#motherTongue").attr('class', 'validateError ' + oldClasses);
            mtErrors = true;
        } else {
            $("#motherTongue").removeClass('validateError');
            mtErrors = false;
        }


        if ('Select...' === $('#country').val()) {
            var oldClasses = $("#country").attr('class');
            $("#country").attr('class', 'validateError ' + oldClasses);
            cntryErrors = true;
        } else {
            cntryErrors = false;
            $("#country").removeClass('validateError');
        }

        if ('Select...' === $('#religion').val()) {
            var oldClasses = $("#religion").attr('class');
            $("#religion").attr('class', 'validateError ' + oldClasses);
            religErrors = true;
        } else {
            religErrors = false;
            $("#religion").removeClass('validateError');
        }

        var agefrom = $("#agefrom").val();
        var ageto = $("#ageto").val();
        if (agefrom > ageto) {
            ageErrors = true;
            var oldAgefrom = $("#agefrom").attr('class');
            $("#agefrom").attr('class', 'validateError ' + oldAgefrom);
            var oldClasses = $("#ageto").attr('class');
            $("#ageto").attr('class', ' validateError ' + oldClasses);
        } else {
            $("#agefrom").removeClass('validateError');
            $("#ageto").removeClass('validateError');
        }

        if (!mtErrors & !cntryErrors & !religErrors & !ageErrors) {
            $("#formBtn").attr('type', 'submit')
        }
    }
});

//Search validate

$(document).on('click', '.formSub', function () {
    var ageErrors;
    var agefrom = $(".agefrom").val();
    var ageto = $(".ageto").val();
    if (agefrom >= ageto) {
        ageErrors = true;
        var oldAgefrom = $(".agefrom").attr('class');
        $(".agefrom").attr('class', 'validateError ' + oldAgefrom);
        var oldClasses = $(".ageto").attr('class');
        $(".ageto").attr('class', 'validateError ' + oldClasses);
    } else {
        ageErrors = false;
        $(".agefrom").removeClass('validateError');
        $(".ageto").removeClass('validateError');
    }
    if (!ageErrors) {
        $(".formSub").attr('type', 'submit');
    }
});

/*
 * Set country code when user selects a country.
 */
$(document).on('change', "select[name='country']", function () {
    var code = $("select[name='country'] option:selected").attr('data-d_code');
    $("input[name='mobilenumber']").val(code);
});

/*
 * Resend confirmation email.
 */
$(document).on('click', "#resend_conf_btn", function () {
    $('#join').css('min-height', '39px');
    $(this).addClass("unclickable");
    var request_URL = $(this).attr('data-access-url');
    if (request_URL.length > 10) {
        $.ajax({
            url: request_URL,
            type: 'POST',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            contentType: false,
            processData: false,
        }).done(function (e) {
            $('#resend_conf_btn').fadeOut();
            $('#notverified_msg').fadeOut();
            $(this).removeClass("unclickable");
            setTimeout(function () {
                $('#resend_conf_success').fadeIn();
            }, 500);

        });
    }
});

/*
 * FAQ page
 */
$(document).on('click', '.custom-panel-group', function () {
    var unique_id = $(this).attr('data-id');
    if ($("a[child-id='" + unique_id + "']").hasClass('collapsed')) {
        $("div[data-id='" + unique_id + "']").removeClass('border-blue'); //data-id='c1'
        $("div[data-id='" + unique_id + "']").addClass('border-brown');
    } else {
        $("div[data-id='" + unique_id + "']").addClass('border-blue');
    }
});

/*
 * Send mobile verification code.
 */

$(document).on('click', '#SENDVERIF', function () {
    $.ajax({
        url: $(this).attr('data-url'),
        data: { mail: 'sadsahan@gmail.com' },
        type: "POST",
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        contentType: false,
        processData: false,
        success: function (obs) {
            alert(obs);

        },
        error: function (a, b, c) {
            alert(c);
        }
    });
});

/*
 * Verify number.
 */
var still_not_verified = true;
$(document).on('click', '#CONFNUMBER', function () {
    var mobno = $("input[name='mob_no']").val();
    if (validatePhoneNumber(mobno)) {
        showVerificationMessage($('#messageBox').attr('data-sendingcode'), MOBVER_ALERT_SUCCESS, false);
        if (still_not_verified) {
            // console.log($("input[name='mob_no']").val());
            $.ajax({
                url: $(this).attr('data-url') + '?mob_num=' + $("input[name='mob_no']").val(),
                // data: {mob_num: $("input[name='mob_no']").val()},
                type: "POST",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                contentType: false,
                processData: false,
                success: function (obs) {
                    // console.log(obs);
                    if (!obs['error']) {
                        // NO errors
                        $("input[name='mob_no']").prop('disabled', true);
                        $("#CONFNUMBER").addClass("disabled");
                        $("#CONFNUMBER").fadeOut('slow');
                        showVerificationMessage($('#messageBox').attr('data-codesent'), MOBVER_ALERT_SUCCESS, true);
                        still_not_verified = false;
                    } else {
                        showVerificationMessage($('#messageBox').attr('data-cannotsendcode'), MOBVER_ALERT_ERROR, false);
                    }

                },
                error: function (a, b, c) {
                    // console.log(c);
                }
            });
        }
    }
});

$(document).on('click', '#VERIFYBYCODE', function () {
    var verif_code = $("input[name='conf_code']").val();
    var redirect_to = $(this).attr('redirect-url');
    $.ajax({
        url: $(this).attr('data-url') + '?conf_code=' + verif_code,
        // data: {conf_code: verif_code},
        type: "POST",
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        contentType: false,
        processData: false,
        success: function (obs) {
            var returned = JSON.parse(obs);
            if (returned['error'] == true) {
                var error = returned['data'];
                showVerificationMessage(error, MOBVER_ALERT_ERROR, true);
            } else if (returned['error'] == false) {
                var success = returned['data'];
                showVerificationMessage(success, MOBVER_ALERT_SUCCESS, true);
                setTimeout(function () {
                    location.href = redirect_to;
                }, 3000);
            }
        },
        error: function (a, b, c) {
            alert(c);
        }
    });
});

$(document).on('keyup', '.mob_vb_conf', function () {
    this.value = this.value.replace(/[^0-9.]/g, '');
    var len = $(this).val().length;
    if (len >= 6) {
        $(this).val($(this).val().substring(0, 6));
    }
});


$(document).on('click', '.friend-favorite', function () {
    var bool = $(this).attr('data-bool');
    if (bool == 0) {
        $('.icon-heart').addClass('interested');
        $(this).attr('data-bool', '1');

        $(".allImage").stop(true, true).fadeOut({
            duration: 600,
            queue: false
        });
        setTimeout(function () {
            $(document).find(".liked-profile").fadeIn({ duration: 400, queue: false });
        }, 650);
    } else {
        $(this).attr('data-bool', '0');
        $('.icon-heart').removeClass('interested');

        $(".results").stop(true, true).fadeOut({
            duration: 600,
            queue: false
        });
        setTimeout(function () {
            $(document).find(".results").fadeIn({ duration: 400, queue: false });
        }, 650);
    }
});

/*
 * Show Upgrade pop up HNB image.
 */
var clickCount = 0;
$(document).on('click', '.hnbimg', function () {
    var det = clickCount % 2;
    if (det == 0) {
        $('#hnbSlipImage').slideDown();
    } else {
        $('#hnbSlipImage').slideUp();
    }
    clickCount++;
});

/*
 * HNB checkout validation
 */

$(document).on('click', '[name="checkoutButton"]', function () {
    //hnb
    var payMethod = $('[name="payment-method"]').val();
    if (payMethod == 'hnb') {
        var txnCode = $('[name="HnbTransactionCode"]').val();
        if (txnCode.length < 6) {
            alert("Transaction code length is invalid. Please Enter correct code on your slip.");
        } else {
            $('[name="type"]').val(payMethod);
            $('[name="transaction_id"]').val(txnCode);
            $('[name="submitPayment"]').submit();
            // Save the invoice
        }
    }
});

function showVerificationMessage(message, type, hide) {
    $('.mobilever-alert').removeClass('mobilever-alert-success mobilever-alert-error');
    if (type == MOBVER_ALERT_SUCCESS) {
        $('.mobilever-alert p').html(message);
        $('.mobilever-alert').addClass('mobilever-alert-success');
        $('.mobilever-alert').slideDown();

    } else if (type == MOBVER_ALERT_ERROR) {
        $('.mobilever-alert p').html(message);
        $('.mobilever-alert').addClass('mobilever-alert-error');
        $('.mobilever-alert').slideDown();
    }

    if (hide) {
        setTimeout(function () {
            $('.mobilever-alert').slideUp();
        }, 5000);
    }
}

$(document).on('keydown', '[name="mob_no"]', function (e) {
    if (e.which == 13) {
        $('#CONFNUMBER').trigger('click');
    }
});
$(document).on('keydown', '[name="conf_code"]', function (e) {
    if (e.which == 13) {
        $('#VERIFYBYCODE').trigger('click');
    }
});

function validatePhoneNumber(validate) {
    var validated = false;
    if (validate.length >= 10) {
        if (validate.length == 10) {
            if (validate.substr(0, 1) == '0') {
                showVerificationMessage($('#messageBox').attr('data-094needed'), MOBVER_ALERT_ERROR, true);
            } else {
                showVerificationMessage($('#messageBox').attr('data-invalidnbr'), MOBVER_ALERT_ERROR, true);
            }
        } else if (validate.length == 11) {
            if (validate.substr(0, 1) == '0') {
                showVerificationMessage($('#messageBox').attr('data-invalidnbr'), MOBVER_ALERT_ERROR, true);
            } else if (validate.substr(0, 2) == '94') {
                validated = true;
            }
        } else if (validate.length == 12) {
            if (validate.substr(0, 2) == '+9') {
                showVerificationMessage($('#messageBox').attr('data-invalidnbr'), MOBVER_ALERT_ERROR, true);
            }
        } else {
            validated = true;
            //showVerificationMessage($('#messageBox').attr('data-invalidnbr'), MOBVER_ALERT_ERROR, true);
        }
    }
    return validated;
}
$(document).on('click', '#resendcode-anc', function () {
    $('#CONFNUMBER').trigger('click');
});
