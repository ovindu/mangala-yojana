<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\Library\Common;

Route::group(['middleware' => 'guest'], function () {
    // Route::group(['middleware' => 'auth'], function () {
    Route::post('/account/login', ['as' => 'doLogin', 'uses' => 'AccountController@doLogin']);
    Route::post('/account/create', ['as' => 'doRegister', 'uses' => 'AccountController@doRegister']);
    // });
});

Route::group(['prefix' => 'my-admin'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::group(['middleware' => 'is_admin'], function () {
            Route::group(['before' => 'has_permisstion_to_access_backend'], function () {
                Route::any('/logout', ['as' => 'Backend_Logout', 'uses' => 'BackendController@doLogout']);

                Route::any('/dashboard', ['as' => 'Backend_Dashboard', function () {
                    return view('backend.dashboard');
                }]);

                Route::any('/user-reports', ['as' => 'Reports_page', 'uses' => 'BackendController@showReports']);

                Route::any('/neweset-users', ['as' => 'newestUsers', 'uses' => 'BackendController@getNewestUsers']);

                Route::get('/manage-users', ['as' => 'users_Dashboard', 'uses' => 'BackendController@showUsersPage']);

                Route::any('/edit-user/{id}', ['as' => 'editUser', 'uses' => 'BackendController@editUser']);

                Route::any('/mark-report/{id}', ['as' => 'markAsReadReport', 'uses' => 'BackendController@markAsRead']);

                Route::any('/update-data', ['as' => 'UpdateFunc', 'uses' => 'BackendController@updateUser']);

                Route::post('/update-user-profile-pic', ['as' => 'updateUserProPic', 'uses' => 'BackendController@updateUserProPic']);

                Route::any('/delete-Pro-pic', ['as' => 'deleteProPic', 'uses' => 'BackendController@deletePic']);

                Route::any('/user-state', ['as' => 'Avalability', 'uses' => 'BackendController@setAvailability']);

                Route::any('/search-by-name', ['as' => 'searchByName', 'uses' => 'BackendController@searchUsersByName']);

                Route::any('/advertising', ['as' => 'AdsPage', 'uses' => 'BackendController@showAdsPage']);

                Route::post('/update-settings', ['as' => 'updateSettings', 'uses' => 'BackendController@updateSettings']);

                Route::any('/settings', ['as' => 'settingsPage', 'uses' => function () {
                    return view::make('backend.settings');
                }]);
                Route::post('/complete-payment', ['as' => 'completePayment', 'uses' => 'PaymentsController@completePayment']);

                Route::get('/payments', ['as' => 'backendPaymentsPage', 'uses' => 'BackendController@showPayments']);


                Route::get('/verify-users', ['as' => 'backendVerifyPage', 'uses' => 'BackendController@showVerify']);


                Route::get('/verify-users/verify', ['as' => 'backendVerifyUser', 'uses' => 'BackendController@setVerify']);


                Route::get('/verify-users/cancel-verify', ['as' => 'backendCancelVerifyUser', 'uses' => 'BackendController@cancelVerify']);

                Route::get('/verify-edits', ['as' => 'backendVerifyEditsPage', 'uses' => 'BackendController@showUserProfileEdits']);

                Route::get('/verify-edits/verify/{id}', ['as' => 'backendVerifyEdit', 'uses' => 'BackendController@approveUserProfileEdit']);
                Route::get('/verify-edits/verify/cancel/{id}', ['as' => 'backendVerifyEditIgnore', 'uses' => 'BackendController@ignoreUserProfileEdit']);
            });
        });
    });

    Route::any('/login', ['as' => 'Backend_Login', function () {
        return view('backend.login');
    }]);
    Route::post('/doLogin', ['as' => 'Backend_DoLogin', 'uses' => 'BackendController@doLogin']);
});

Route::group(['prefix' => 'AJAX'], function () {
    Route::group(['middleware' => 'auth'], function () {
        Route::post('IMONLINE', ['as' => 'imonline', 'uses' => 'AjaxController@hitTheLastOnlineRecord']);
        Route::get('/unblock.php', ['as' => 'unblock', 'uses' => 'AjaxController@doUnblock']);
        Route::get('/GETNOTIFICATIONS', ['as' => 'ajaxGetNotifications', 'uses' => 'AjaxController@getNotifications']);
        Route::post('/CLOSENOTIFICATION', ['as' => 'ajaxCloseNotifications', 'uses' => 'AjaxController@closeNotifications']);
        Route::post('/READNOTIFICATIONS', ['as' => 'ajaxReadNotifications', 'uses' => 'AjaxController@readNotifications']);
        Route::post('/LIKE', ['as' => 'like', 'uses' => 'AjaxController@hitLike']);
        Route::post('/UNLIKE', ['as' => 'unlike', 'uses' => 'AjaxController@removeLike']);
        Route::post('/UNFRIEND', ['as' => 'unfriend', 'uses' => 'AjaxController@unfriend_user']);
        Route::post('/REPORT', ['as' => 'report', 'uses' => 'AjaxController@reportUser']);
        Route::any('/SETPRIVACY', ['as' => 'setPrivacy', 'uses' => 'PrivacyController@setPrivacy']);
        Route::post('/RESENDCONFMAIL', ['as' => 'resendConfirmationEmail', 'uses' => 'AjaxController@resend_confirmation_email']);
        Route::post('/SENDMOBILEVERIFIC', ['as' => 'sendMobileVerificationCode', 'uses' => 'AjaxController@sendMobileVerificationCode']);
        Route::any('/CONFIRMOBILENUMBER', ['as' => 'ConfirmMobileNumber', 'uses' => 'AjaxController@updateMobileNumber']);
        Route::any('/VERIFICATIONFUNC', ['as' => 'verifyUserByCode', 'uses' => 'AjaxController@verifyMobileNumber']);
    });
});

Route::group(['middleware' => ['auth','skip_suspends','mobile_verified']], function () {
    // Route::group(['middleware' => 'csrf'], function () {

        /*
         * Profile Data Edit
         */
        Route::post('/account/settings/edit/email', ['as' => 'emailSettings', 'uses' => 'SettingsController@emailSettings']);
        Route::post('/account/settings/{tab}', ['as' => 'doSettings', 'uses' => 'SettingsController@doSettings']);
        Route::post('/account/edit/do', ['as' => 'editAccountInfo', 'uses' => 'AccountController@editAccountInfo']);
        Route::post('/account/done', ['as' => 'completeRegistration', 'uses' => 'AccountController@completeRegistration']);
        Route::post('/account/profile/family', ['as' => 'familyDetailsPage', 'uses' => 'AccountController@doFamilyDetailsRegistration']);
        Route::post('/account/profile/partner-details', ['as' => 'PartnerDetailsPage', 'uses' => 'AccountController@doAskPartnerDetails']);
        Route::post('/account/profile/upload', ['as' => 'uploadPagePost', 'uses' => 'ProfileController@profilePictureUpload']);
        Route::post('/account/profile/basic/edit', ['as' => 'basicInfoPage', 'uses' => 'AccountController@doBasicInfoEdit']);
        Route::post('/account/profile/family-details/edit', ['as' => 'familyDetailsEdit', 'uses' => 'AccountController@dofamilyDetailsEdit']);
        Route::post('/account/profile/education-proffesion-details/edit', ['as' => 'eduProfEdit', 'uses' => 'AccountController@doeduProfEdit']);
        Route::post('/account/profile/location/edit', ['as' => 'locationEdit', 'uses' => 'AccountController@doLocationEdit']);
        Route::post('/account/profile/partner-preference/edit', ['as' => 'partnerPreferencesEdit', 'uses' => 'AccountController@doPartnerPreferencesEdit']);
        Route::post('/account/profile/general/edit', ['as' => 'generalInfoPage', 'uses' => 'AccountController@showgeneralInfoPage']);
        Route::post('/account/profile/about-you/edit', ['as' => 'editAboutMe', 'uses' => 'AccountController@doEditAboutMe']);
        Route::post('/account/profile/contact-us/edit', ['as' => 'editContactInfo', 'uses' => 'AccountController@editContactInfo']);
    // });


    /*
     * Mobile Verification.
     */

    Route::group(['middleware' => 'mobile_verified'], function () {
        Route::get('/mobile/verification', ['as' => 'mobileVerifPage', function () {
            return view('home/registration/mobile_verification');
        }]);

        Route::get('mobile/verification/{rand}/{id}/{code}', ['as' => 'Verify_from_mail', function () {
            return view('home/registration/mobile_verification')->with('show_notif', true);
        }]);
    });


    /*
     * Registration
     */
    // Route::filter('newbies', function () {
    //     if (!AccountController::created_today()) {
    //         return Redirect::route('homePage');
    //     }
    // });

    // Route::filter('messaging', function () {
    //     if (!Common::hasPermissions(Auth::id(), Common::$permissions_messaging)) {
    //         return Redirect::route("profilePage");
    //     }
    // });

    // Route::filter('mobile_verified', function () {
    //     $profile_age = Common::getProfileAge(Auth::id());
    //     if (!Common::isMobileVerified(Auth::id()) && $profile_age >= 14) {
    //         // Rediect to registration page.
    //         $url = Request::url();
    //         if (URL::route("mobileVerifPage") != $url) {
    //             return Redirect::route("mobileVerifPage");
    //         }
    //     }
    // });

    // Route::filter('premium_user', function () {
    //     if (!Common::isPremiumUser()) {
    //         return Redirect::route("profilePage");
    //     }
    // });

    // Route::filter('has_permisstion_to_access_backend', function () {
    //     if (!Common::isPremiumUser()) {
    //         return Redirect::route("4");
    //     }
    // });


    Route::group(['before' => 'newbies'], function () {
        Route::get('/registration', ['as' => 'registrationPage', function () {
            return view('home/registration/registration');
        }]);
        Route::get('/registration/matrimonial', ['as' => 'matrimonialPage', function () {
            return view('home/registration/matrimonial');
        }]);

        Route::get('/registration/thankyou', ['as' => 'thankyouPage', function () {
            return view('home/registration/thankyou');
        }]);

        Route::get('/registration/partner', ['as' => 'partnerPage', function () {
            return view('home/registration/partner');
        }]);
    });

    /*
     * Profile
     */
    Route::get('/account/profile/edit', ['as' => 'basiciInfoPage', function () {
        return view('home/profile/basicinfo');
    }]);

    Route::get('/account/profile', ['as' => 'profilePage', 'uses' => 'ProfileController@ShowSelfProfileInfo']);
    Route::post('/block', ['as' => 'block', 'uses' => 'BlockController@doBlock']);
    Route::get('/account/profile/upload', ['as' => 'uploadPage', function () {
        return view('home/profile/uploadphoto');
    }]);

    /*
     * Other Main Pages
     */
    Route::get('/horoscope', ['as' => 'horoscopePage', function () {
        return view('home/horoscope');
    }]);
    /*
     * Inbox
     */

    Route::get('/messages', ['as' => 'inboxPage', 'uses' => 'InboxController@showInbox']);

    Route::group(['before' => 'messaging'], function () {
    });

    Route::get('/messages/conversation/{userid}', ['as' => 'messagePage', 'uses' => 'InboxController@showMsg']);
    Route::get('/chk_messages.php', ['as' => 'chkmessagesPage', 'uses' => 'InboxController@chkMsgs']);
    Route::get('/messages/fetch.php', ['as' => 'fetchMessages', 'uses' => 'InboxController@fetchMsgList']);
    Route::get('/messages/compose', ['as' => 'sendMessage', 'uses' => 'InboxController@sendMsg']);
    Route::get('/messages/delete', ['as' => 'deleteMessage', 'uses' => 'InboxController@doReceiver_delete']);
    Route::get('/messages/markAsRead', ['as' => 'markAsRead', 'uses' => 'InboxController@doMarkAsRead']);
    Route::get('/messages/markAsUnread', ['as' => 'markAsUnread', 'uses' => 'InboxController@doMarkAsUnread']);

    /*
     * Friend Requests
     */
    Route::get('/profile/friend.php', ['as' => 'makeFriends', 'uses' => 'FriendController@make_friends']);
    Route::get('/profile/friend/request.php', ['as' => 'beFriends', 'uses' => 'FriendController@send_request']);
    Route::get('/profile/friend/request/cancel.php', ['as' => 'cancelFRequest', 'uses' => 'FriendController@del_request']);
    Route::get('/profile/friend/request/ignore.php', ['as' => 'ignoreFRequest', 'uses' => 'FriendController@ignore_request']);
    Route::get('/profile/friend/request/remind.php', ['as' => 'remindFRequest', 'uses' => 'FriendController@remind_request']);
    Route::get('profile/friend/request/chk_frquests.php', ['as' => 'chkFrequestsPage', 'uses' => 'FriendController@chkFrequests']);
    Route::get('profile/friend/fetch.php', ['as' => 'fetchFrequests', 'uses' => 'FriendController@fetchFrqs']);

    /*
     *  Friend Controll
     */
    Route::any('/unfriend', ['as' => 'unfriendFrd', 'uses' => 'FriendController@unfriendUser']);


    /*
     * Profile Edit Pages
     */
    Route::get('/account/edit', ['as' => 'accountInfoEditPage', function () {
        return view('home/profile/account');
    }]);

    Route::get('/account/profile/general/edit', ['as' => 'generalInfoPage', function () {
        return view('home/profile/generalinfo');
    }]);

    /*
     * Account Settings
     */
    Route::get('/account/settings/', ['as' => 'showGeneralSettings', 'uses' => 'SettingsController@showGeneralSettings']);
    Route::get('/account/settings/{tab}', ['as' => 'showSettings', 'uses' => 'SettingsController@showSettings']);
    Route::get('/account/settings/general', ['as' => 'showGenral']);
    // Deactivate account

    Route::post('/account/deactivate', ['as' => 'deactivator', 'uses' => 'AccountController@doDeactivation']);
    /*
     * Main Pages
     */
    Route::get('/horoscope', ['as' => 'horoscopePage', function () {
        return view('home/horoscope');
    }]);

    Route::group(['before' => 'premium_user'], function () {
        // Only premium users can visit this pages.
        Route::get('/who-is-online', ['as' => 'onlineUsers', 'uses' => 'SearchController@get_onlineUsers']);
    });

    Route::get('daily-10', ['as' => 'dailytenPage', 'uses' => 'SearchController@showDailyten']);
    Route::get('matches', ['as' => 'matchesPage', 'uses' => 'SearchController@getMatches']);
    Route::get('friends', ['as' => 'friendsPage', 'uses' => 'FriendController@showFriends']);
    Route::get('favourites', ['as' => 'favPage', 'uses' => 'SearchController@showFavorites']);

    /*
     *  Upgrade Page
     */
    Route::any('upgrade', ['as' => 'upgradePage', 'uses' => 'UpgradeController@showUpgrade']);

    /*
     * Search Page
     */
    Route::post('refine', ['as' => 'refinesearch', 'uses' => 'SearchController@refineSearch']);
    Route::get('refine', ['as' => 'refinesearch2', 'uses' => 'SearchController@advanceSearch']);
    Route::post('goto', ['as' => 'gotoProfile', 'uses' => 'SearchController@goToProfile']);
    Route::any('searchonlines', ['as' => 'searchonlineusers', 'uses' => 'SearchController@get_onlineUsers']);

    Route::get('/search', ['as' => 'searchPage', function () {
        return view('home/search/search');
    }]);
    Route::get('/search/results', ['as' => 'searchResults', 'uses' => 'SearchController@showResults']);

    Route::any('submit-report', ['as' => 'submitUserReport', 'uses' => 'BlockController@submitUserComplain']);
    Route::any('payment-error', ['as' => 'payErrorPage', function () {
        return view('payments.paymentError');
    }]);
});

/*
 * End of Auth
 */

Route::get('/', ['as' => 'homePage', function () {
    Common::createDaily10();
    if (!Auth::check()) {
        return view('home/index');
    } else {
        $message = Session::get('message');
        $alert_type = Session::get('alert-type');
        return Redirect::route('profilePage')->with(['message' => $message, 'alert-type' => $alert_type]);
    }
}]);

Route::get('/login', ['as' => 'loginPage', function () {
    return view('home/index');
}]);

Route::group(['before' => 'check_admin_approval'], function () {
    Route::get('/profile.php', ['as' => 'publicProfile', 'uses' => 'ProfileController@showPublicProfile']);
});

/*
 * Forgot-Password Control
 */
Route::post('/forgot-password.php', ['as' => 'forgotPassword', 'uses' => 'PasswordController@handleForgotPassword']);
Route::post('/reset-password.php', ['as' => 'resetPassword', 'uses' => 'PasswordController@resetPassword']);

/*
 * Incoming routes from Email
 */
Route::get('/confirm.php', ['as' => 'Mail_confirmUser', 'uses' => 'AccountController@confirmUserRegistration']);
Route::get('/reset-password.php', ['as' => 'checkPasswordResetToken', 'uses' => 'PasswordController@checkToken']);


//Public search
Route::any('searchmatches', ['as' => 'mainsearch', 'uses' => 'SearchController@ShowSearchResults']);

Route::get('lang/{lang}', ['as' => 'langChange', function ($lang) {
    Session::put('my.locale', $lang);
    return Redirect::back();
}]);

/*
 * Seach and deactivate inactive profiles between one week.
 */
Route::any('check-profiles', ['as' => 'checkProfiles', 'uses' => 'AccountController@checkInactiveAccounts']);

/*
 * Redirect for 404
 */
Route::any('404', ['as' => '404page', function () {
    return view('errors.404');
}]);
Route::get('unpaid-invoices', ['middleware' => 'auth', 'uses' => 'AccountController@showInvoicesForSuspends']);
Route::any('payments.php', ['as' => 'PaymentActorController', 'uses' => 'PaymentsController@doPayment']);

Route::get('logout', ['middleware' => 'auth', 'as' => 'doLogout', 'uses' => 'AccountController@doLogout']);

Route::any('faq', ['as' => 'faqPage', function () {
    return view('static-pages.faq');
}]);
Route::any('about-us', ['as' => 'aboutUsPage', function () {
    return view('static-pages.aboutus');
}]);
