<div class="col-sm-12 right-icon">
    <div class="dropdown">
        <button id="" class="btn-settings pull-right dropdown_button" type="button" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false" data-field="{!!$field!!}">
            <span id="icon-{!!$field!!}" class="glyphicon {!!PrivacyController::displayPrivacyIcon($field)!!}"></span>
            <span class="privacy-name" id="name-{!!$field!!}">{!!PrivacyController::getPrivacyName($field)!!}</span>
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu" aria-labelledby="">

            <li class="{!!$field!!} cursor-pointer" data-field="{!!$field!!}" data-step="0">
                <a>
                    <span id="setting-{!!$field!!}-0" class="glyphicon glyphicon-resize-small" data-name='{!!trans('translator.RegisteredUsersOnly')!!}'></span>
                    {!!trans('translator.RegisteredUsersOnly')!!}
                </a>
            </li>
            <li class="{!!$field!!} cursor-pointer" data-field="{!!$field!!}" data-step="1">
                <a>
                    <span id="setting-{!!$field!!}-1" class="glyphicon glyphicon-resize-horizontal" data-name='{!!trans('translator.FriendsOnly')!!}'></span>
                    {!!trans('translator.FriendsOnly')!!}
                </a>
            </li>
            @if(Common::isPremiumUser() | $field == 'profile_picture')
            <li class="{!!$field!!} cursor-pointer" data-field="{!!$field!!}" data-step="2">
                <a>
                    <span id="setting-{!!$field!!}-2" class="glyphicon glyphicon-globe" data-name="{!!trans('translator.premiumPublic')!!}"></span>
                    {!!trans('translator.premiumPublic')!!}
                </a>
            </li>
            @endif
            
            @if($field == Common::$PRIV_COL_CONTACT_INFO)
            <li class="{!!$field!!} cursor-pointer" data-field="{!!$field!!}" data-step="3">
                <a>
                    <span id="setting-{!!$field!!}-3" class="glyphicon glyphicon-lock" data-name="{!!trans('translator.OnlyMe')!!}"></span>
                    {!!trans('translator.OnlyMe')!!}
                </a>
            </li>
            @endif
        </ul>
    </div>
</div>    
<script type="text/javascript">
    $(function () {
        $('.{!!$field!!}').click(function () {
            field = $(this).data('field');
            step = $(this).data('step');
            $.ajax({
                url: "{!!URL::route('setPrivacy')!!}",
                data: {field: field, 'privacy-step': step},
                dataType: 'html',
                type: 'POST',
                error: function (jqXHR, textStatus, errorThrown) {

                }
            });

            $('.alert-success').slideDown().delay(500).slideUp();
            $('#icon-' + field).attr('class', $('#setting-' + field + "-" + step).attr('class'));
            $('#name-' + field).html($('#setting-' + field + "-" + step).attr('data-name'));
        });
    });
</script>