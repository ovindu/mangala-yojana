<div class="col-xs-1 right-icon">
    <div class="dropdown">
        <button id="" class="btn-settings" type="button" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
            <span class="glyphicon glyphicon-cog"></span>
            <span class="sr-only">More...</span>
        </button>
        <ul class="dropdown-menu" role="menu" aria-labelledby="">
            <span class="upTriangle">
                <span class="upTriangle inner"></span>
            </span>

            @if(!Common::myGender($userid))
            <li>
                @if(!FriendController::are_friends($userid) && !FriendController::has_requested($userid))
                <a href="#">
                    <form action="{!!URL::route('beFriends')!!}" method="get">
                        <input name="be-id" type="hidden" value="{!!$userid!!}"/>
                        <input name="be-path" type="hidden" value="{!!Request::url()!!}"/>
                        <button submit="submit" class="btn-transparent">{!!trans('translator.sendProposal')!!}</button>
                    </form>
                </a>
                @elseif(!FriendController::are_friends($userid) && FriendController::has_requested($userid) && FriendController::if_sender($userid))
                <a href="#">
                    <form action="{!!URL::route('cancelFRequest')!!}" method="get">
                        <input name="cancel-id" type="hidden" value="{!!$userid!!}"/>
                        <input name="cancel-path" type="hidden" value="{!!Request::url()!!}"/>
                        <button submit="submit" class="btn-transparent">{!!trans('translator.CancelProposal')!!}</button>
                    </form>
                </a>
                @elseif(!FriendController::are_friends($userid) && FriendController::has_requested($userid) && !FriendController::if_sender($userid))
                <a href="#">
                    <form action="{!!URL::route('makeFriends')!!}" method="get">
                        <input name="id" type="hidden" value="{!!$userid!!}"/>
                        <input name="public-prof-path" type="hidden" value="{!!Request::url()!!}"/>
                        <button submit="submit" class="btn-transparent">{!!trans('translator.AcceptProposal')!!}</button>
                    </form>
                </a>
                @else
                <a href="#">
                    <form action="{!!URL::route('cancelFRequest')!!}" method="get">
                        <input name="cancel-id" type="hidden" value="{!!$userid!!}"/>
                        <input name="cancel-path" type="hidden" value="{!!Request::url()!!}"/>
                        <button submit="submit" class="btn-transparent unfriend-btn" data-friend-id='{!!$userid!!}' data-row-frd='{!!$userid!!}'>{!!trans('translator.RemoveProposal')!!}</button>
                    </form>
                </a> 
                @endif
            </li>
            @endif
            @if(isset($userid))
            <li>
                <a href="#" data-toggle="modal" data-target="#block-confirmation">
                    <input type="hidden" name="id" value="{!!$userid!!}" data-user-name='@if(isset($user->fname)){!!$user->fname!!}@elseif(isset($name)){!!$name!!}@endif' class="btn-block-user" />
                    <input type="submit" value="{!!trans('translator.Block')!!}" data-uid='{!!$userid!!}' data-user-name='@if(isset($user->fname)){!!$user->fname!!}@elseif(isset($name)){!!$name!!}@endif' class="btn-transparent btn-block-stng-drpwn"/>
                </a>
            </li>
            @endif
        </ul>
    </div>
</div>    
