@if(isset($friends) & isset($isSelf))
<div class="report-conf modal fade" id="unfriend-confirmation" tabindex="-1" role="dialog" aria-labelledby="basic-info-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="profilePicUploadModalLabel">
                    {!!Lang::get('translator.missYou', array('fname' => $theUser->fname))!!}
                </h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <span>{!!Lang::get('translator.unfriendMessage', array('fname' => $theUser->fname))!!}</span><br>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-12">
                            {!!Form::open(array('url'=>URL::route('unfriendFrd'),'method'=>'get'))!!} {!!Form::token()!!}
                            <input type="hidden" name="id" value="{!!Input::get('id')!!}">
                            <section class="button-demo">
                                <button type="submit" class="btn-saveChanges btn ladda-button pull-right " id='showoffset' data-style="expand-right" 
                                        value="">{!!trans('translator.Confirm')!!}</button>
                            </section>
                            {!!Form::close()!!}
                            <button data-dismiss="modal" class="btn btn-close position-pop-save">{!!trans('translator.Cancel')!!}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endif