<div class="report-conf modal fade" id="block-confirmation" tabindex="-1" role="dialog" aria-labelledby="basic-info-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">{!!trans('translator.Close')!!}</span></button>
                <h4 class="modal-title" id="block-confirmation-heading">
                    @if(isset($theUser))
                    {!!trans('translator.Block')!!} {!!$theUser->fname.' '.$theUser->lname!!}
                    @else
                    {!!trans('translator.Block')!!}
                    @endif
                </h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <span>{!!trans('translator.BlockMessage')!!}.</span><br>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-12">
                            {!!Form::open(array('url'=>URL::route('block'),'method'=>'post'))!!} {!!Form::token()!!}
                            <input type="hidden" name="id" id="hidden-block-id" value="@if(Input::has('id')){!!Input::get('id')!!}@endif">
                            <section class="button-demo">
                                <button type="submit" class="btn-saveChanges btn ladda-button pull-right report-continue" id='showoffset' data-style="expand-right" 
                                        value="">{!!trans('translator.Block')!!}</button>
                            </section>
                            {!!Form::close()!!}
                            <button data-dismiss="modal" class="btn btn-close position-pop-save">{!!trans('translator.Cancel')!!}</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
