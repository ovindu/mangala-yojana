<?php
use \App\Library\FormDetails;    
use \App\Library\Common;    
use \App\Library\Salutation;    
use \App\Models\User;    
use \App\Models\Searchuser;    
use \App\Http\Controllers\AccountController;    
use \App\Http\Controllers\ProfileController;
use \App\Http\Controllers\FriendController;
?>
<div class="modal fade" id="user-report-pop" tabindex="-1" role="dialog" aria-labelledby="basic-info-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="profilePicUploadModalLabel">{!!trans('translator.wrongwiththisprofile')!!}</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <input name="fake-prof-item" type="radio" class="radio-btn cursor-pointer" id="fake-name">
                            <label for="fake-name" class="cursor-pointer fake-name">{!!trans('translator.Thisprofileisusingafakename')!!}</label><br>

                            <input name="fake-prof-item" type="radio" class="radio-btn cursor-pointer" id="fake-profiles">
                            <label for="fake-profiles" class="cursor-pointer fake-profiles">{!!trans('translator.Thisisafakeprofile')!!}</label><br>

                            <input name="fake-prof-item" type="radio" class="radio-btn cursor-pointer" id="fake-pretending">
                            <label for="fake-pretending" class="cursor-pointer fake-pretending">{!!trans('translator.Thisprofileispretending')!!}</label><br>

                            <input name="fake-prof-item" type="radio" class="radio-btn cursor-pointer" id="fake-annoying">
                            <label for="fake-annoying" class="cursor-pointer fake-annoying">{!!trans('translator.Thispersonisannoying')!!}</label><br>

                            <textarea placeholder="{!!trans('translator.Tellussomethingmore')!!}" class="top-margin-5 form-control resize-none " cols="4" rows="3" id="additional_text" name="desc" data-user='{!!Input::get('id')!!}'></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <section class="button-demo">
                            <button data-dismiss="modal" class="btn-saveChanges disabled btn ladda-button pull-right report-continue" id='showoffset' data-style="expand-right" value="">{!!trans('translator.Continue')!!}</button>
                        </section>
                        <button data-dismiss="modal" class="btn btn-close position-pop-save">{!!trans('translator.Cancel')!!}</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="report-conf modal fade" id="user-report-confirm" tabindex="-1" role="dialog" aria-labelledby="basic-info-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="profilePicUploadModalLabel">{!!trans('translator.receivedFeedback')!!}</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <span>{!!trans('translator.ActionReportText')!!}</span><br>
                            @if(FriendController::are_friends(Input::get('id')))
                            {!!Form::open(array('url'=>URL::route('unfriendFrd',array('id'=>'124565')),'method'=>'get','id'=>'unfriend-form'))!!} {!!Form::token()!!}
                            <input type="hidden" name="id" value="@if(Input::has('id')){!!Input::get('id')!!}@endif" />
                            <div class="position-report-conf-item cursor-pointer submit-unfriend">
                                <span class="icon-dislike cursor-pointer"></span>
                                <label for="icon-dislike" class="cursor-pointer">@if(isset($theUser->fname)){!!'Unfriend '.$theUser->fname!!}@endif</label>
                                <span class="help-block helper cursor-pointer">{!!trans('translator.Removeanddisposeyourproposals')!!}</span>
                            </div>
                            {!!Form::close()!!}
                            <hr class="opaque">
                            @endif
                            {!!Form::open(array('url'=>URL::route('block'),'id'=>'block-form'))!!}
                            <input type="hidden" name="id" value="@if(Input::has('id')){!!Input::get('id')!!}@endif" />
                            <a type="submit">
                                <div class="position-report-conf-item cursor-pointer submit-block">
                                    <span class="glyphicon glyphicon-flag cursor-pointer submit-block"></span>
                                    <label for="icon-dislike" class="cursor-pointer submit-block" >@if(isset($theUser->fname)){!!'Block '.$theUser->fname!!}@endif</label>
                                    <span class="help-block helper cursor-pointer submit-block">{!!trans('translator.NotAbleToContact')!!}</span>
                                </div>
                            </a>
                            {!!Form::close()!!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <button data-dismiss="modal" class="btn btn-close position-pop-save">{!!trans('translator.Cancel')!!}</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
