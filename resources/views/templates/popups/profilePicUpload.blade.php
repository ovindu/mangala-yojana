<!-- Modal -->
<div class="modal fade" id="profilePicUploadModal" tabindex="-1" role="dialog"
    aria-labelledby="profilePicUploadModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            {!!Form::open(array('files' => TRUE, 'enctype' => 'multipart/form-data', 'form', 'id' => 'form'))!!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">{!!trans('translator.Close')!!}</span></button>
                <h4 class="modal-title" id="profilePicUploadModalLabel">{!!trans('translator.Uploadyourprofilephoto')!!}
                </h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <div class="image-editor text-center" id="image-cropper">
                            <div class="select-image-btn mobileUpload btn-upload">
                                {!!trans('translator.Selectanimage')!!}</div>
                            <span class="help-block pic-upload-tip"
                                id="helpblock-green">{!!trans('translator.maxUploadSize')!!}</span>
                            <span class="help-block pic-upload-tip" id="helpblock-danger"
                                style="display: none;   color: red;">You could not able to upload this image, Image size
                                are too large.</span>
                            <input type="hidden" value="" name="cropPosition" id="cropPosition" value="0,0,0">
                            <input type="file" id="imgfile" class="cropit-image-input" name="theProfilePic">
                            <input type="hidden" id="imgfileString" name="theProfilePicString">
                            <!-- .cropit-image-preview-container is needed for background image to work -->
                            <div class="cropit-preview img-responsive" id="cropit-preview"></div>
                            <div class="image-size-label">
                                {!!trans('translator.Resizeimage')!!}
                            </div>

                            {{-- <input type="range" class="cropit-image-zoom-input"> --}}
                            {{-- <div class="controls-wrapper">
                                <div class="slider-wrapper"><span class="icon icon-image small-image"></span><input
                                        type="range" class="cropit-image-zoom-input" min="-1" max="1" step="0.01"><span
                                        class="icon icon-image large-image"></span></div>
                            </div> --}}
                        </div>
                    </div>
                </div>
                <hr>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3 fmlyDetails_bodyText">
                            <div class="fmlyDetails_bodyText">{!!trans('translator.ProfilePictureSettings')!!}
                            </div>
                        </div>
                        <div class="col-sm-3 fmlyDetails_bodyText">
                            <?php $field="profile_picture" ?>
                            @include('templates.dropdowns.privacySettings')
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">

                <section class="button-demo">
                    <input type="button" class="btn-close btn btn-default" data-dismiss="modal"
                        value="{!!trans('translator.Close')!!}">
                    <button type="submit" class="btn-saveChanges ladda-button " disabled="disabled" id='showoffset'
                        data-style="expand-right" value="">{!!trans('translator.SaveChanges')!!}</button>
                </section>

            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
<script>
    $('.select-image-btn').click(function () {
                        $('.cropit-image-input').click();
                    });

                    $(document).on('change', '.cropit-image-input', function () {
                        var imgpath = document.getElementById('imgfile');
                        if (!imgpath.value == "") {
                            var img = imgpath.files[0].size;
                            var imgsize = img / 4096; // Size from kilobytes.
                            if (imgsize < 4096) {
                                // Let user upload the image
                                $('#showoffset').removeAttr('disabled');
                                $('#helpblock-danger').slideUp();
                            } else {
                                // Empty the image and show tip. image is too large
                                $('#showoffset').attr('disabled', 'disabled');
                                $('#helpblock-green').slideUp('slow','linear',function (){
                                    $('#helpblock-danger').slideDown();
                                });
                            }
                        }
                    });

                    var resize = $('#cropit-preview').croppie({
                        enableExif: true,
                        enableOrientation: true,    
                        viewport: { // Default { width: 100, height: 100, type: 'square' } 
                            width: 280,
                            height: 280,
                            type: 'square' //square
                        },
                        boundary: {
                            width: 300,
                            height: 300
                        }
                    });
                    $('#imgfile').on('change', function () { 
                    var reader = new FileReader();
                        reader.onload = function (e) {
                        resize.croppie('bind',{
                            url: e.target.result
                        }).then(function(){
                            console.log('jQuery bind complete');                            
                            $('#showoffset').removeAttr('disabled');
                        });
                        }
                        reader.readAsDataURL(this.files[0]);
                    });
                    // $(function () {
                    //     $('#image-cropper').cropit({
                    //         // originalSize: false,
                    //         width: 263,
                    //         height: 263,
                    //         export: {
                    //             originalSize: false
                    //             }
                    //     });

                        

                    // });

                    // $(document).on('mousemove', ".cropit-preview", function () {
                    //     //alert('');
                    //     var offs = $('#image-cropper').cropit('offset');
                    //     if(offs){
                    //         console.log(offs);
                    //         console.log($('#image-cropper').cropit('zoom'));
                    //         $("#cropPosition").val(offs.x + ',' + offs.y + ',' + $('#image-cropper').cropit('zoom'));                        
                    //     }
                        
                    // });

                    $("#form").on('submit', function (e) {         
                        //e.preventDefault();
                        resize.croppie('result', {
                            type: 'canvas',
                            size: 'viewport'
                        }).then(function (img) {
                            $("#imgfileString").val(img);
                        });
                        // var offs = $('#image-cropper').cropit('offset');
                        // console.log(offs.x + ',' + offs.y + ',' + $('#image-cropper').cropit('zoom'));
                        // $("#cropPosition").val(offs.x + ',' + offs.y + ',' + $('#image-cropper').cropit('zoom'));
                    });


</script>
<script>
    // Bind normal buttons
    Ladda.bind('.button-demo button', {timeout: 5000});

    // Bind progress buttons and simulate loading progress
    Ladda.bind('.progress-demo button', {
        callback: function (instance) {
            var progress = 0;
            var interval = setInterval(function () {
                progress = Math.min(progress + Math.random() * 0.1, 1);
                instance.setProgress(progress);

                if (progress === 1) {
                    instance.stop();
                    clearInterval(interval);
                }
            }, 200);
        }
    });

    // You can control loading explicitly using the JavaScript API
    // as outlined below:

    // var l = Ladda.create( document.querySelector( 'button' ) );
    // l.start();
    // l.stop();
    // l.toggle();
    // l.isLoading();
    // l.setProgress( 0-1 );

</script>

<style>
    input.cropit-image-input {
        visibility: hidden;
    }
</style>