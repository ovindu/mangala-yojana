<!-- Modal -->
<div class="modal fade" id="contactinfo-modal" tabindex="-1" role="dialog" aria-labelledby="contactinfo-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="location-modal-label">{!!trans('translator.EditContactInfo')!!}</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <form method="post" action="{!!URL::route('editContactInfo')!!}">
                        {!!Form::token()!!}
			<input type="hidden" name="userId" value="{!!$theUser->id!!}">
                        <div class="container-fluid">
                            <div class="row frmMat_row">
                                <div class="col-lg-3 col-md-3 fmlyDetails_bodyText">{!!trans('translator.MobileNumber')!!}</div>
                                <div class="col-lg-7 col-md-7">
				    <?php $tp = ((!empty($theUser->mobilenumber)) ? $theUser->mobilenumber : ""); ?>
                                    <input value="{!!$tp!!}" name="mobilenumber" type="tel" class="fmlyDetails_txtbox" required> 

                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-3 col-md-3 fmlyDetails_bodyText">{!!trans('translator.email')!!}</div>
                                <div class="col-lg-7 col-md-7">
				    <?php $email = ((!empty($theUser->email)) ? $theUser->email : ""); ?>
                                    <input value="{!!$email!!}" name="emailedit" type="email" class="fmlyDetails_txtbox">
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-3 col-md-3 fmlyDetails_bodyText">{!!trans('translator.facebook')!!}</div>
                                <div class="col-lg-7 col-md-7 col-sm-4">
				    <?php $facebook = ((!empty($theUser->facebook)) ? $theUser->facebook : ""); ?>
                                    <input value="{!!$facebook!!}" name="facebookedit" type="text" class="fmlyDetails_txtbox">
                                    <span class="help-block">Go to your facebook profile and copy profile link then paste it.</span>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-12">
                            <section class="button-demo">
                                <button type="submit" class=" btn-saveChanges btn ladda-button pull-right " id='showoffset' data-style="expand-right" value="">{!!trans('translator.SaveChanges')!!}</button>
                                {!!Form::close()!!}  
                            </section>
                            <button data-dismiss="modal" class="btn btn-close position-pop-save">{!!trans('translator.Cancel')!!}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    // Bind normal buttons
    Ladda.bind('.button-demo button', {timeout: 5000});
    // Bind progress buttons and simulate loading progress
    Ladda.bind('.progress-demo button', {
	callback: function (instance) {
	    var progress = 0;
	    var interval = setInterval(function () {
		progress = Math.min(progress + Math.random() * 0.1, 1);
		instance.setProgress(progress);
		if (progress === 1) {
		    instance.stop();
		    clearInterval(interval);
		}
	    }, 200);
	}
    });

</script>
