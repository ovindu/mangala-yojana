<!-- Modal -->
<div class="modal fade" id="forgot-password-modal" tabindex="-1" role="dialog" aria-labelledby="forgot-password-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="forgot-password-modal-label">{!!trans('translator.RecoverYourPassword')!!}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="container-fluid">
                        <div class="row frmMat_row">
                            <div class="col-lg-3 col-md-3 fmlyDetails_bodyText">{!!trans('translator.EmailAddress')!!}</div>
                            <div class="col-lg-7 col-md-7">
                                <input name="forgot-email" type="text" class="fmlyDetails_bodyText email" style="width:100%">
                            </div>
                        </div>
                        <div class="message" style="display:none;"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <section class="button-demo">
                        <button type="submit" class="btn-saveChanges btn ladda-button" id="showoffset" data-style="expand-right" style="color:white;">{!!trans('translator.SendInstructions')!!}</button>
                    </section>

                </div>
            </div>
        </div>
    </div>
</div>
<script>

    // Bind normal buttons
    Ladda.bind('.button-demo button', {timeout: 10000});

    // Bind progress buttons and simulate loading progress
    Ladda.bind('.progress-demo button', {
        callback: function (instance) {
            var progress = 0;
            var interval = setInterval(function () {
                progress = Math.min(progress + Math.random() * 0.1, 1);
                instance.setProgress(progress);

                if (progress === 1) {
                    instance.stop();
                    clearInterval(interval);
                }
            }, 200);
        }
    });

    $('.btn-saveChanges').click(function () {
        checkEmail();
    });

    $('.email').keyup(function (e) {
        if (e.which === 13) {
            checkEmail();
        }
    });

    function checkEmail() {
        var email = $('.email').val();
        $.ajax({
            url: "{!!URL::route('forgotPassword')!!}?email=" + email,
            dataType: 'html',
            type: 'POST'
        }).done(function (response) {
            if (response.length === 0) {
                $('.message').addClass('bg-danger');
                $('.message').fadeIn();
                $('.message').html("{!!trans('translator.NoUserWithGivenMail')!!}");
            } else {
                $('.email').prop('disabled', true);
                $('.message').removeClass('bg-danger');
                $('.message').addClass('bg-success');
                $('.message').fadeIn();
                $('.message').html("{!!trans('translator.ForgotPasswordMailSent')!!}");
                $('.btn-saveChanges').html('{!!trans('translator.Resend')!!}');
            }
        });
    }
</script>
