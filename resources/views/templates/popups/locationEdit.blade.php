<?php
use \App\Library\Common;
use \App\Library\FormDetails;
?>
<!-- Modal -->
<div class="modal fade" id="location-modal" tabindex="-1" role="dialog" aria-labelledby="location-modal-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                <h4 class="modal-title" id="location-modal-label">{!!trans('translator.EditLocation')!!}</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="{!!URL::route('locationEdit')!!}">
                    <div class="row">

                        {!!Form::token()!!}

                        <div class="container-fluid">
                            <div class="row frmMat_row">
                                <div class="col-lg-3 col-md-3 fmlyDetails_bodyText">{!!trans('translator.DistrictLiving')!!}</div>
                                <div class="col-lg-7 col-md-7">
                                    <select id="district"
                                            class='fmlyDetails_txtbox @if ($errors->has('city')) validateError @endif'
                                            name='city'>
                                        <option value="">{!!trans('translator.Select')!!}</option>
                                        @foreach (\App\Library\Common::getCitiesForCountry() as $city)
                                            <option class="districts" value='{!!$city->id!!}'
                                                    data-prov='{!!$city->id!!}' <?php echo(Input::old('city') == $city->id ? 'selected' : '') ?>>@if($city->country_code == 'LK') {!!trans('translator.'.$city->city_name)!!} @else {!!$city->city_name!!}  @endif</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-3 col-md-3 fmlyDetails_bodyText">{!!trans('translator.City')!!}</div>
                                <div class="col-lg-7 col-md-7">
                                    <?php $regForm = new \App\Library\FormDetails; $dstarray = $regForm->get_cities(); ?>
                                    <select id="reg-small-city" name="small-city"
                                            class='fmlyDetails_txtbox @if ($errors->has('small-city')) validateError @endif'>
                                        <option value="">{!!trans('translator.Select')!!}</option>
                                        <script> var arrayFromPHP = JSON.parse('{!!json_encode(SmallCities::getSmallCities())!!}');</script>
                                    </select>
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-3 col-md-3 fmlyDetails_bodyText">{!!trans('translator.ResidencyStatus')!!}</div>
                                <div class="col-lg-7 col-md-7 col-sm-4">
                                    {!!Form::select('residency', \App\Library\FormDetails::get_residency(),$formData->profiles->residency, array('class' => 'fmlyDetails_txtbox'))!!}
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-xs-12">
                                <section class="button-demo">
                                    <button type="submit" class=" btn-saveChanges btn ladda-button pull-right "
                                            id='showoffset' data-style="expand-right"
                                            value="">{!!trans('translator.SaveChanges')!!}</button>

                                </section>
                                <button data-dismiss="modal"
                                        class="btn btn-close position-pop-save">{!!trans('translator.Cancel')!!}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>

    // Bind normal buttons
    Ladda.bind('.button-demo button', {timeout: 5000});
    // Bind progress buttons and simulate loading progress
    Ladda.bind('.progress-demo button', {
        callback: function (instance) {
            var progress = 0;
            var interval = setInterval(function () {
                progress = Math.min(progress + Math.random() * 0.1, 1);
                instance.setProgress(progress);
                if (progress === 1) {
                    instance.stop();
                    clearInterval(interval);
                }
            }, 200);
        }
    });

</script>
