<?php
use \App\Library\FormMatrimonial; 
use \App\Library\FormDetails; 

$matForm = new FormMatrimonial;
$regForm = new FormDetails;
?>
<!-- Modal -->
<div class="modal fade" id="basic-info-modal" tabindex="-1" role="dialog" aria-labelledby="basic-info-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="profilePicUploadModalLabel">{!!trans('translator.EditBasicInformation')!!}</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <form method="post" action="{!!URL::route('basicInfoPage')!!}">
                        {!!Form::token()!!}
			<input type="hidden" name="userId" value="{!!$theUser->id!!}">
                        <div class="container-fluid">
                            <div class="row frmMat_row">
                                <div class="col-md-3 fmlyDetails_bodyText ">{!!trans('translator.Height')!!}</div>
                                <div class="col-lg-4 col-md-4">
                                    {!!Form::select('height',$regForm->get_heightList(), $formData->profiles->height, array('class' => 'fmlyDetails_txtbox')); !!}
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-md-3 fmlyDetails_bodyText ">{!!trans('translator.MaritalStatus')!!}</div>
                                <div class="col-lg-4 col-md-4">
                                    {!! Form::select('marital_status', $regForm->get_maritalStatus(), $formData->profiles->marital_status, array('class' => 'fmlyDetails_txtbox')); !!}
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-md-3 fmlyDetails_bodyText ">{!!trans('translator.SkinTone')!!}</div>
                                <div class="col-lg-4 col-md-4">
                                    {!! Form::select('skin_tone', $regForm->get_skin_tone(), $formData->profiles->skin_tone, array('class' => 'fmlyDetails_txtbox')); !!}
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-md-3 fmlyDetails_bodyText ">{!!trans('translator.Bodytype')!!}</div>
                                <div class="col-lg-4 col-md-4">
                                    {!! Form::select('body_type', $regForm->get_body_type(), $formData->profiles->body_type, array('class' => 'fmlyDetails_txtbox')); !!}
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-md-3 fmlyDetails_bodyText ">{!!trans('translator.Diet')!!}</div>
                                <div class="col-lg-4 col-md-4">
                                    {!!Form::select('diet', $regForm->get_dietInfo(), $formData->profiles->diet, array('class' => 'fmlyDetails_txtbox'))!!}
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-md-3 fmlyDetails_bodyText ">{!!trans('translator.Drink')!!}</div>
                                <div class="col-lg-8">
                                    <?php $Drinkval = $formData->profiles->drink ?>
                                    <div class="col-md-3 col-xs-6 fmlyDetails_bodyText">
                                        <label for="yes" class="radio-text radio-inline radio-form"><input type="radio" name="drink" value="1" id='yes' <?php echo ($Drinkval == '1' ? 'checked' : '') ?>> {!!trans('translator.Yes')!!}</label>
                                    </div>
                                    <div class="col-md-3 col-xs-6 fmlyDetails_bodyText">
                                        <label for="no" class="radio-text radio-inline radio-form"><input type="radio" name="drink" value="2" id='no' <?php echo ($Drinkval == '2' ? 'checked' : '') ?>>{!!trans('translator.No')!!}</label>
                                    </div>
                                    <div class="col-md-3 col-xs-6 fmlyDetails_bodyText"> 
                                        <label for="occasionally" class="radio-text radio-inline radio-form"><input type="radio" name="drink" value="3" id='occasionally' <?php echo ($Drinkval == '3' ? 'checked' : '') ?>>{!!trans('translator.Occasionally')!!}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-md-3 col-xs-12 fmlyDetails_bodyText ">{!!trans('translator.Smoke')!!}</div>
                                <div class="col-lg-8">
                                    <div class="col-md-3 col-xs-6 fmlyDetails_bodyText">
                                        <?php $Smokeval = $formData->profiles->smoke ?>
                                        <label for="smo-yes" class="radio-text radio-inline radio-form"><input type="radio" name="smoke" value="1" id='smo-yes' <?php echo ($Smokeval == '1' ? 'checked' : '') ?>>  {!!trans('translator.Yes')!!}</label>
                                    </div>
                                    <div class="col-md-3 col-xs-6 fmlyDetails_bodyText">
                                        <label for="smo-no" class="radio-text radio-inline radio-form"><input type="radio" name="smoke" value="2" id='smo-no' <?php echo ($Smokeval == '2' ? 'checked' : '') ?>>  {!!trans('translator.No')!!}</label>
                                    </div>
                                    <div class="col-md-3 col-xs-6 fmlyDetails_bodyText"> 
                                        <label for="smo-occasionally" class="radio-text radio-inline radio-form"><input type="radio" name="smoke" value="3" id='smo-occasionally' <?php echo ($Smokeval == '3' ? 'checked' : '') ?>>{!!trans('translator.Occasionally')!!}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <section class="button-demo">
                            <button type="submit" class=" btn-saveChanges btn ladda-button pull-right " id='showoffset' data-style="expand-right" value="">{!!trans('translator.SaveChanges')!!}</button>
                            {!!Form::close()!!}  
                        </section>
                        <button data-dismiss="modal" class="btn btn-close position-pop-save">{!!trans('translator.Cancel')!!}</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>

    // Bind normal buttons
    Ladda.bind('.button-demo button', {timeout: 5000});

    // Bind progress buttons and simulate loading progress
    Ladda.bind('.progress-demo button', {
        callback: function(instance) {
            var progress = 0;
            var interval = setInterval(function() {
                progress = Math.min(progress + Math.random() * 0.1, 1);
                instance.setProgress(progress);

                if (progress === 1) {
                    instance.stop();
                    clearInterval(interval);
                }
            }, 200);
        }
    });

</script>
