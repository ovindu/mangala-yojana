<div class="report-conf modal fade" id="plz-upgrade-popup" tabindex="-1" role="dialog" aria-labelledby="basic-info-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content custom_model_content">
            <div class="modal-header custom_header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="plz-upgrade-popup_title">
                    
                    <!--Ooops! This feature is only enabled for premium users!-->
                </h4>
            </div>
            <div class="modal-body custom_model_body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <span id="plz-upgrade-popup_msg_body_"></span><br>
                            <!--Upgrade now and get many features and find your future partner easily!-->
							<div id='upgrade_pop_body'>
								<ul>
									<li>{!!trans('translator.SendUnlimitedProposals')!!}</li>
									<li>{!!trans('translator.InstantMessaging')!!}</li>
									<li>{!!trans('translator.ImprovedPrivacy')!!}</li>
									<li>{!!trans('translator.DiscountsfromOurPartnersmore')!!}</li>
								</ul>
								{!!HTML::image(asset('img/ajudshajovhflsmdojda.png'), null, array('class' => 'img-responsive','draggable'=>'false'))!!}
							</div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer custom_footer">
                    <div class="row">
                        <div class="col-xs-12">
						
							
							<a href='{!!URL::route('upgradePage')!!}'>
                            <button type="submit" class="btn-saveChanges  upg-cust-btn" id='showoffset' >{!!trans('translator.Upgrade')!!}</button>
                            </a>
							<button data-dismiss="modal" class=" btn-close  upg-cust-btn">{!!trans('translator.Cancel')!!}</button>
                            
                            
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>