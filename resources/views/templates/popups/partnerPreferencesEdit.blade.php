
<?php
use \App\Library\FormMatrimonial; 
use \App\Library\FormDetails; 

for ($i = 20; $i <= 75; $i++):
    $ageStart[$i] = $i;
endfor;
for ($j = 75; $j >= 20; $j--):
    $ageEnd[$j] = $j;
endfor;
try {
    $age = explode(',', $formData->matches->age);
    $height = explode(',', $formData->matches->height);
    $marital_status = explode(',', $formData->matches->marital_status);
    $religion = explode(',', $formData->matches->religion);
    $language = explode(',', $formData->matches->language);
    $edu_level = explode(',', $formData->matches->edu_level);
    $diet = explode(',', $formData->matches->diet);
    $smoke = explode(',', $formData->matches->smoke);
    $drink = explode(',', $formData->matches->drink);
    $body_type = explode(',', $formData->matches->body_type);
    $skin_tone = explode(',', $formData->matches->skin_tone);
} catch (Exception $ex) {
    
}
?>
<!-- Modal -->
<div class="modal fade" id="partner-pref-modal" tabindex="-1" role="dialog" aria-labelledby="partner-pref-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="location-modal-label">
                    @if($theUser->gender == 'm')
                    {!!Lang::get('translator.PartnerPreferenceEdit', array('gen' => 'සහකාරියගේ'))!!}
                    @else
                    {!!Lang::get('translator.PartnerPreferenceEdit', array('gen' => 'සහකරුගේ'))!!}
                    @endif
                </h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <form method="post" action="{!!URL::route('partnerPreferencesEdit')!!}">
                        {!!Form::token()!!}
			<input type="hidden" name="userId" value="{!!$theUser->id!!}">
                        <div class="container-fluid">
                            <div class="row frmMat_row">
                                <div class="col-lg-3 col-md-3 fmlyDetails_bodyText">{!!trans('translator.Age')!!}</div>
                                <div class="col-lg-3 col-md-3">
                                    {!!Form::select('ageStart', $ageStart,$age[0], array('class' => 'fmlyDetails_txtbox ageStart'))!!}
                                </div>
                                <div class="col-lg-1 col-md-1 fmlyDetails_bodyText">{!!trans('translator.To')!!} </div>
                                <div class="col-lg-3 col-md-3">
                                    {!!Form::select('ageEnd', $ageEnd,$age[1], array('class' => 'fmlyDetails_txtbox ageEnd'))!!}
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-3 col-md-3 fmlyDetails_bodyText">{!!trans('translator.Height')!!}</div>
                                <div class="col-lg-3 col-md-3">
                                    {!!Form::select('heightStart', FormDetails::get_heightList(), $height[0], array('class' => 'fmlyDetails_txtbox heightEnd'))!!}
                                </div>
                                <div class="col-lg-1 col-md-1 fmlyDetails_bodyText"> {!!trans('translator.To')!!} </div>
                                <div class="col-lg-3 col-md-3">
                                    {!!Form::select('heightEnd', FormDetails::get_heightList(), $height[1], array('class' => 'fmlyDetails_txtbox heightEnd'))!!}
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-3 col-md-3 fmlyDetails_bodyText">{!!trans('translator.MaritalStatus')!!}</div>
                                <div class="col-lg-7 col-md-7 col-sm-4">
                                    {!!Form::select('marital_status[]', FormDetails::get_maritalStatus(NULL,TRUE),$marital_status, array('class'=>'fmlyDetails_txtbox chosen-select','data-placeholder'=>'Doesn\'t Matter','multiple'=>'multiple','style'=>'width:350px !important;'))!!}
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-3 col-md-3 fmlyDetails_bodyText">{!!trans('translator.Religion')!!}</div>
                                <div class="col-lg-7 col-md-7 col-sm-4">
                                    {!!Form::select('religion[]', FormDetails::get_religion(),$religion, array('class'=>'fmlyDetails_txtbox chosen-select','data-placeholder'=>'Doesn\'t Matter','multiple'=>'multiple','style'=>'width:350px !important;'))!!}
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-3 col-md-3 fmlyDetails_bodyText">{!!trans('translator.MotherTongue')!!}</div>
                                <div class="col-lg-7 col-md-7 col-sm-4">
                                    {!!Form::select('language[]', FormDetails::get_language(),$language, array('class'=>'fmlyDetails_txtbox chosen-select','data-placeholder'=>'Doesn\'t Matter','multiple'=>'multiple','style'=>'width:350px !important;'))!!}
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-3 col-md-3 fmlyDetails_bodyText">{!!trans('translator.Education')!!}</div>
                                <div class="col-lg-7 col-md-7 col-sm-4">
                                    {!!Form::select('edu_level[]', FormDetails::get_edu_level('partner'),$edu_level, array('class'=>'fmlyDetails_txtbox chosen-select','data-placeholder'=>'Doesn\'t Matter','multiple'=>'multiple','style'=>'width:350px !important;'))!!}
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-3 col-md-3 fmlyDetails_bodyText">{!!trans('translator.Diet')!!}</div>
                                <div class="col-lg-7 col-md-7 col-sm-4">
                                    {!!Form::select('diet[]', FormDetails::get_dietInfo('partner'),$diet, array('class'=>'fmlyDetails_txtbox chosen-select','data-placeholder'=>'Doesn\'t Matter','multiple'=>'multiple','style'=>'width:350px !important;'))!!}
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-3 col-md-3 fmlyDetails_bodyText">{!!trans('translator.Smoke')!!}</div>
                                <div class="col-lg-7 col-md-7 col-sm-4">
                                    {!!Form::select('smoke[]', FormDetails::get_smokeInfo(),$smoke, array('class'=>'fmlyDetails_txtbox chosen-select','data-placeholder'=>'Doesn\'t Matter','multiple'=>'multiple','style'=>'width:350px !important;'))!!}
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-3 col-md-3 fmlyDetails_bodyText">{!!trans('translator.Drink')!!}</div>
                                <div class="col-lg-7 col-md-7 col-sm-4">
                                    {!!Form::select('drink[]', FormDetails::get_drinkInfo(),$drink, array('class'=>'fmlyDetails_txtbox chosen-select','data-placeholder'=>'Doesn\'t Matter','multiple'=>'multiple','style'=>'width:350px !important;'))!!}
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-3 col-md-3 fmlyDetails_bodyText">{!!trans('translator.Bodytype')!!}</div>
                                <div class="col-lg-7 col-md-7 col-sm-4">
                                    {!!Form::select('body_type[]', FormDetails::get_bodyType(),$body_type, array('class'=>'fmlyDetails_txtbox chosen-select','data-placeholder'=>'Doesn\'t Matter','multiple'=>'multiple','style'=>'width:350px !important;'))!!}
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-3 col-md-3 fmlyDetails_bodyText">{!!trans('translator.SkinTone')!!}</div>
                                <div class="col-lg-7 col-md-7 col-sm-4">
                                    {!!Form::select('skin_tone[]', FormDetails::get_skinTone(),$skin_tone, array('class'=>'fmlyDetails_txtbox chosen-select','data-placeholder'=>'Doesn\'t Matter','multiple'=>'multiple','style'=>'width:350px !important;'))!!}
                                </div>
                            </div>
                        </div>
                </div>

                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-12">
                            <section class="button-demo">
                                <button type="submit" class=" btn-saveChanges btn ladda-button pull-right " id='showoffset' data-style="expand-right" value="">{!!trans('translator.SaveChanges')!!}</button>
                                {!!Form::close()!!}  
                            </section>
                            <button data-dismiss="modal" class="btn btn-close position-pop-save">{!!trans('translator.Cancel')!!}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    // Bind normal buttons
    Ladda.bind('.button-demo button', {timeout: 5000});

    // Bind progress buttons and simulate loading progress
    Ladda.bind('.progress-demo button', {
	callback: function (instance) {
	    var progress = 0;
	    var interval = setInterval(function () {
		progress = Math.min(progress + Math.random() * 0.1, 1);
		instance.setProgress(progress);

		if (progress === 1) {
		    instance.stop();
		    clearInterval(interval);
		}
	    }, 200);
	}
    });

    var config = {
	'.chosen-select': {},
	'.chosen-select-deselect': {allow_single_deselect: true},
	'.chosen-select-no-single': {disable_search_threshold: 10},
	'.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
	'.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
	$(selector).chosen(config[selector]);
    }

    $('.ageStart').change(function () {
	if ($(this).val() > $('.ageEnd').val()) {
	    $(this).val($('.ageEnd').val());
	    $('.ageEnd').focus();
	}
    });
    $('.ageEnd').change(function () {
	if ($(this).val() < $('.ageStart').val()) {
	    $(this).val($('.ageStart').val());
	    $('.ageStart').focus();
	}
    });
</script>
