<div class="modal fade" id="upgrade-model" tabindex="-1" role="dialog" aria-labelledby="basic-info-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="profilePicUploadModalLabel">{!!trans('translator.UpgradePackage')!!}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <span>{!!trans('translator.Selectyourpaymentmethod')!!}</span>
                        <select class="form-control cursor-pointer form-top " name="payment-method" id="payment-meth">
<!--                            <option value="ezcash">Ez-Cash</option>
                            <option value="mcash">M-Cash</option>
-->
                            <option value="hnb">HNB</option>
                            <option value="paypal">Paypal</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="contentbox">

                            <div class="ezcash-cont data-all display-none">
                                <div class="row">
                                    <div class="col-lg-8 col-md-8 col-xs-12 col-sm-8 padding-none">
                                        <span class="upg-content upg-title">{!!Lang::get('translator.PayWith', array('brand' => 'Ez - cash'))!!}</span><br>
                                        <span class="upg-content help-block upg-tagline">Powered by Dialog, Most trusted</span>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-4 padding-none">
                                        {!!HTML::image("img/payment-methods/1.png",null,array('class'=>'float-right ','draggable'=>'false'))!!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <hr>
                                        <div>
                                            <ol>
                                                <li class="margin-btm-5"><span>#111#</span></li>
                                                <li class="margin-btm-5"><span>Make Payment</span></li>
                                                <li class="margin-btm-5"><span>Blah blah</span></li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 content-input">
                                        <input type="text" class="form-control" placeholder="{!!trans('translator.referencenumber')!!}">
                                    </div>
                                </div>
                            </div> 

                            <div class="mcash-cont data-all display-none">
                                <div class="row">
                                    <div class="col-lg-8 col-md-8 col-xs-12 col-sm-8 padding-none">
                                        <span class="upg-content upg-title">{!!Lang::get('translator.PayWith', array('brand' => 'M - cash'))!!}</span><br>
                                        <span class="upg-content help-block upg-tagline">Powered by Mobitel, Most trusted</span>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-4 padding-none">
                                        {!!HTML::image("img/payment-methods/2.png",null,array('class'=>'float-right mcash','draggable'=>'false'))!!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <hr>
                                        <div>
                                            <ol>
                                                <li class="margin-btm-5"><span>Dial</span></li>
                                                <li class="margin-btm-5"><span>Hangup</span></li>
                                                <li class="margin-btm-5"><span>Blah blah</span></li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 content-input">
                                        <input type="text" class="form-control" placeholder="{!!trans('translator.referencenumber')!!}">
                                    </div>
                                </div>
                            </div>

                            <div class="hnb-cont data-all">
                                <div class="row">
                                    <div class="col-lg-8 col-md-8 col-xs-12 col-sm-8 padding-none">
                                        <span class="upg-content upg-title">{!!Lang::get('translator.PayWith', array('brand' => 'HNB'))!!}</span><br>
                                        <span class="upg-content help-block upg-tagline">{!!trans('translator.hnbtagline')!!}</span>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-4 padding-none">
                                        {!!HTML::image("img/payment-methods/3.png",null,array('class'=>'float-right mcash','draggable'=>'false'))!!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <hr>
                                        <div>
                                            <span class="upg-tagline">{!!trans('translator.hnbFillDipositSlipHead')!!}</span>
                                            <ol>
                                                <li class="margin-btm-5">{!!trans('translator.AccName')!!} : {!!Common::getCommonData('HNB_account_name')!!}</li>
                                                <li class="margin-btm-5">{!!trans('translator.AccNo')!!} : {!!Common::getCommonData('HNB_account_no')!!}</li>
                                                <li class="margin-btm-5">{!!trans('translator.WriteRef')!!} : {!!Auth::id()!!}</li>
                                            </ol>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <label>{!!trans('translator.transactionCode')!!} <a class="hnbimg link cursor-pointer">(<em>{!!trans('translator.howtoFind')!!}</em>)</a></label>
                                            <input type="text" name="HnbTransactionCode" class="form-control">
                                        </div>
                                        <div id="hnbSlipImage" style="display: none;">
                                            <hr>
                                            <p>{!!trans('translator.enterHighlitedFieldNo')!!}</p>
                                            <img src="{!!asset('/img/hnbslip.jpg')!!}" draggable="false" class="img-responsive">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 content-input">
                                    </div>
                                </div>
                            </div>

                            <div class="paypal-cont data-all display-none">
                                <div class="row">
                                    <div class="col-lg-8 col-md-8 col-xs-12 col-sm-8 padding-none">
                                        <span class="upg-content upg-title">{!!Lang::get('translator.PayWith', array('brand' => 'Paypal'))!!}</span><br>
                                        <span class="upg-content help-block upg-tagline">Powered by Ceylon Systems</span>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-xs-12 col-sm-4 padding-none ">
                                        {!!HTML::image("img/payment-methods/paypal_verified.png",null,array('class'=>'float-right mcash','draggable'=>'false'))!!}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <hr>
                                        <div class="center">
                                            <button type="submit" class="btn btn-default ">
                                                <script async="async" src="{!!Common::getCommonData('merchant_button_link')!!}" 
                                                        data-button="buynow" 
                                                        data-name="{!!Common::getCommonData('paypal_package_name')!!}" 
                                                        data-quantity="1" 
                                                        data-amount="{!!Common::getCommonData('premium_package_price_USD')!!}" 
                                                        data-currency="USD" 
                                                        data-userid="{!!Auth::id()!!}"
                                                        data-image_url="{!!asset('img/logo.png')!!}"
                                                        data-callback="{!!URL::route('PaymentActorController',array("type"=>'paypal','userid'=>Auth::id()))!!}" 
                                                        data-return="{!!URL::route('PaymentActorController',array("type"=>'paypal','userid'=>Auth::id()))!!}" 
                                                        data-env="sandbox"
                                                ></script>
                                            </button>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 content-input">
                                        {!!HTML::image("img/payment-methods/4.png",null,array('class'=>' img-responsive flag-icons pull-right','draggable'=>'false'))!!}
                                        {!!HTML::image("img/payment-methods/5.png",null,array('class'=>' img-responsive flag-icons pull-right','draggable'=>'false'))!!}
                                        {!!HTML::image("img/payment-methods/6.png",null,array('class'=>' img-responsive flag-icons pull-right','draggable'=>'false'))!!}
                                    </div> 
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <section class="button-demo">
                            <button type="submit" class=" btn-saveChanges btn ladda-button pull-right " id='showoffset' name="checkoutButton" data-style="expand-right" value="">Checkout</button>
                        </section>
                        <button data-dismiss="modal" class="btn btn-close position-pop-save">Cancel</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div> 
<form name="submitPayment" action="{!!URL::route('PaymentActorController')!!}" method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="type" value="">
    <input type="hidden" name="transaction_id" value="">
</form>
<script>
    $('#payment-meth').change(function() {
        var value = $(this).val(); //alert(value);
        $(".data-all").stop(true, true).fadeOut({
            duration: 600,
            queue: false
        });
        setTimeout(function() {
            $(document).find("." + value + "-cont").slideDown({duration: 600, queue: false});
        }, 650);

    });
    

</script>