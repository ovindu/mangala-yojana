<div class="modal fade" id="deactivation-popup" tabindex="-1" role="dialog" aria-labelledby="basic-info-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="profilePicUploadModalLabel">{!!trans('translator.DeactivateTitle')!!}</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <input name="deactivate" type="radio" class="radio-btn cursor-pointer" id="myojana-user">
                            <label for="myojana-user" class="cursor-pointer myojana-user-found">{!!trans('translator.partnerfrommangalayojana')!!}</label><br>

                            <div class="border-deac-flist display-none myojana-user-pop all">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-7">
                                            <span class="center-block padding-top-7">{!!trans('translator.CongratsTellUsWhom')!!}</span>
                                        </div>
                                        <div class="col-lg-5">
                                            <select class="form-control" id="married-uid">
                                                @for($i=0; $i < count(FriendController::getFriendsList()[0]); $i++)
                                                <option value="{!!FriendController::getFriendsList()[1][$i]!!}">{!!FriendController::getFriendsList()[0][$i]!!}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input name="deactivate" type="radio" class="radio-btn cursor-pointer" id="external-user">
                            <label for="external-user" class="cursor-pointer external-user-found">{!!trans('translator.partnerfromExternal')!!}</label><br>
                            <div class="border-deac-flist display-none all external-user-pop">
                                <span class="center-block padding-7">{!!trans('translator.Congratsforyourbrightfuture')!!}</span>
                            </div>

                            <input name="deactivate" type="radio" class="radio-btn cursor-pointer" id="privacy-user">
                            <label for="privacy-user" class="cursor-pointer privacy-user-found">{!!trans('translator.NoEnaughPryvacy')!!}</label><br>
                            <div class="border-deac-flist display-none all privacy-user-pop">
                                <span class="center-block padding-7">{!!trans('translator.Wehavetoomuchprivacy')!!}</span>
                            </div>

                            <input name="deactivate" type="radio" class="radio-btn cursor-pointer" id="annoying-user">
                            <label for="annoying-user" class="cursor-pointer annoying-user annoying-user-found">{!!trans('translator.annoyingme')!!}</label><br>
                            <div class="border-deac-flist display-none all annoying-user-pop">
                                <span class="center-block padding-7">{!!trans('translator.Youcanblockthem')!!}</span>
                            </div>

                            <input name="deactivate" type="radio" class="radio-btn cursor-pointer" id="temp-user">
                            <label for="temp-user" class="cursor-pointer temp-user temp-user-found">{!!trans('translator.Thisistemporary')!!}</label><br>
                            <div class="border-deac-flist display-none all temp-user-pop">
                                <span class="center-block padding-7">{!!trans('translator.partnerlookingforyou')!!}</span>
                            </div>

                            <input name="deactivate" type="radio" class="radio-btn cursor-pointer" id="other-user">
                            <label for="other-user" class="cursor-pointer other-user-found">{!!trans('translator.Other')!!}</label><br>
                            <div class="border-deac-flist display-none all other-user-pop">
                                <textarea placeholder="{!!trans('translator.Tellussomethingmore')!!}" class="top-margin-5 form-control resize-none " cols="4" rows="3" id="additional_text" name="desc" data-user='{!!Input::get('id')!!}'></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <section class="button-demo">
                            <button data-dismiss="modal" class="disabled btn-saveChanges  btn ladda-button pull-right deactivate-continue" id='showoffset' data-style="expand-right" value="">{!!trans('translator.Continue')!!}</button>
                        </section>
                        <button data-dismiss="modal" class="btn btn-close position-pop-save">{!!trans('translator.Cancel')!!}</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="report-conf modal fade" id="deactivate-confirm" tabindex="-1" role="dialog" aria-labelledby="basic-info-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="profilePicUploadModalLabel">{!!trans('translator.enterMangalayojanaPwd')!!}</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <span>{!!trans('translator.deactivateMessage')!!}</span><br>
                            <hr class="opaque">
                                {!!Form::open(array('url'=>URL::route('deactivator'),'id'=>'deactive-form','method'=>'post'))!!}
                            <input type="hidden" name="reason" id="reason" value="" />
                            <input type="hidden" name="partner" id="partner" value="" />
                            <div class="position-report-conf-item cursor-pointer ">
                                <span class="glyphicon  cursor-pointer ">
                                    <span class="help-block helper cursor-pointer "></span>
                                </span>
                                <input class="form-control" id="password" name="password" type="password" >
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <section class="button-demo">
                            <button type="submit" data-dismiss="modal" class="btn-saveChanges disabled btn ladda-button pull-right deactivate-confirm" id='showoffset' data-style="expand-right" value="">{!!trans('translator.ContinueDeactivate')!!}</button>
                        </section>

                        <button data-dismiss="modal" class="btn btn-close position-pop-save">{!!trans('translator.Cancel')!!}</button>
                    </div>
                </div>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>

<script>

    $(document).on('click', '.myojana-user-found', function() {
        $('.all').css('display', 'none');
        $('.myojana-user-pop').fadeIn(500);
    });
    $(document).on('click', '.external-user-found', function() {
        $('.all').css('display', 'none');
        $('.external-user-pop').fadeIn(500);
    });
    $(document).on('click', '.privacy-user-found', function() {
        $('.all').css('display', 'none');
        $('.privacy-user-pop').fadeIn(500);
    });
    $(document).on('click', '.annoying-user-found', function() {
        $('.all').css('display', 'none');
        $('.annoying-user-pop').fadeIn(500);
    });
    $(document).on('click', '.temp-user-found', function() {
        $('.all').css('display', 'none');
        $('.temp-user-pop').fadeIn(500);
    });
    $(document).on('click', '.other-user-found', function() {
        $('.all').css('display', 'none');
        $('.other-user-pop').fadeIn(500);
    });

    $(document).on('click', '.deactivate-continue', function() {
        var select = $('input[name=deactivate]:radio:checked').attr('id');
        var selectedText = $('label[for=' + select + ']').html();

        var value = $('#' + select).val();
        var userid = "0";
        if (select === 'myojana-user') {
            userid = $('#married-uid').val();
        }
        var passValue = selectedText;
        if (selectedText === 'Other') {
            var txtVal = $('#additional_text').val();
            var passValue = selectedText + " : " + txtVal;
        }
        $('#reason').val(passValue);
        $('#partner').val(userid);
        $('#deactivate-confirm').modal('show');
    });

    $(document).on('click', '.radio-btn', function() {
        $('.deactivate-continue').removeClass('disabled');
    });

    $('#password').keyup(function() {
        var count = $('#password').val().length + 1;
        if (count > 4) {
            $('.deactivate-confirm').removeClass('disabled');
        } else {
            $('.deactivate-confirm').addClass('disabled');
        }
    });
    
    $('.deactivate-confirm').click(function (){
       $('#deactive-form').submit() 
    });

</script>