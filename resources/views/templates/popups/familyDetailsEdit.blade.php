<?php
use \App\Library\FormMatrimonial; 
use \App\Library\FormDetails; 

$matForm = new FormMatrimonial;
$regForm = new FormDetails;
?>
<!-- Modal -->
<div class="modal fade" id="family-details-modal" tabindex="-1" role="dialog" aria-labelledby="family-details-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="profilePicUploadModalLabel">{!!trans('translator.EditFamilyDetails')!!}</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <form method="post" action="{!!URL::route('familyDetailsEdit')!!}">
                        {!!Form::token()!!}
			<input type="hidden" name="userId" value="{!!$theUser->id!!}">
                        <div class="container-fluid">
                            <div class="row frmMat_row">
                                <div class="col-lg-2 col-md-2 fmlyDetails_bodyText ">{!!trans('translator.Father')!!}</div>
                                <div class="col-lg-4 col-md-4">
                                    {!!Form::select('fatherStatus', $matForm->get_fatherStatus(), $formData->profiles->fatherStatus, array('class' => 'fmlyDetails_txtbox'));!!}
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-2 col-md-2 fmlyDetails_bodyText ">{!!trans('translator.Mother')!!}</div>
                                <div class="col-lg-4 col-md-4">
                                    {!!Form::select('motherStatus', $matForm->get_motherStatus(), $formData->profiles->motherStatus, array('class' => 'fmlyDetails_txtbox'));!!}
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-2 col-md-2 col-sm-2 fmlyDetails_bodyText ">{!!trans('translator.Brothers')!!}</div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <input name="brotherCount" class="fmlyDetails_txtbox brother-count" value="{!!$formData->profiles->brotherCount!!}" type="number" min="0" max="50"/>
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 fmlyDetails_bodyText">{!!trans('translator.married')!!}</div>
                                <div class="col-lg-2 col-md-2 col-sm-4">
                                    <input name="brotherCountMarried" class="fmlyDetails_txtbox brother-married-count" value="{!!$formData->profiles->brotherCountMarried!!}" type="number" min="0" max="50" />
                                </div>
                                <script type="text/javascript">
				    $(function () {
					$('.brother-count').change(function () {
					    if ($('.brother-count').val() === "0") {
						$('.brother-married-count').val("0");
					    }
					    if ($('.brother-count').val() < $('.brother-married-count').val()) {
						$('.brother-married-count').val($('.brother-count').val());
					    }
					});
					$('.brother-married-count').change(function () {
					    if ($('.brother-married-count').val() > $('.brother-count').val()) {
						$('.brother-married-count').val($('.brother-count').val());
					    }
					});
				    });
                                </script>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-2 col-md-2  col-sm-2 fmlyDetails_bodyText ">{!!trans('translator.Sisters')!!}</div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <input name="sisterCount" class="sister-count fmlyDetails_txtbox" value="{!!$formData->profiles->sisterCount!!}" type="number" min="0" max="50"/>
                                </div>
                                <div class="col-lg-2 col-md-2  col-sm-2 fmlyDetails_bodyText">{!!trans('translator.married')!!}</div>
                                <div class="col-lg-2 col-md-2 col-sm-4">
                                    <input name="sisterCountMarried" class="sister-married-count fmlyDetails_txtbox" value="{!!$formData->profiles->sisterCountMarried!!}"  type="number" min="0" max="50" />
                                </div>
                                <script type="text/javascript">
				    $(function () {
					$('.sister-count').change(function () {
					    if ($('.sister-count').val() === "0") {
						$('.sister-married-count').val("0");
					    }
					    if ($('.sister-count').val() < $('.sister-married-count').val()) {
						$('.sister-married-count').val($('.sister-count').val());
					    }
					});
					$('.sister-married-count').change(function () {
					    if ($('.sister-married-count').val() > $('.sister-count').val()) {
						$('.sister-married-count').val($('.sister-count').val());
					    }
					});
				    });
                                </script>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 fmlyDetails_bodyText ">{!!trans('translator.Value')!!} @if ($errors->has('familyValue')) <span class="errorIcon icon-alert" data-toggle="tooltip" data-placement="right"></span> @endif</div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 fmlyDetails_bodyText">
				    <?php $familyval = $formData->profiles->familyValue; ?>
                                    <label for="familyvalues-Traditional" class="radio-text radio-inline"><input type="radio" name="familyValue" id="familyvalues-Traditional" value="1" class="radio-btn" onBlur="validate_family();" <?php echo ($familyval == '1' ? 'checked' : '') ?> /> {!!trans('translator.Traditional')!!}</label>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 fmlyDetails_bodyText">
                                    <label for="familyvalues-Moderate" class="radio-text radio-inline"><input type="radio" name="familyValue" id="familyvalues-Moderate" value="2" class="radio-btn" onBlur="validate_family();" <?php echo ($familyval == '2' ? 'checked' : '') ?> />{!!trans('translator.Moderate')!!} </label> 
                                </div>
                                <div class="col-lg-4 col-md-3 col-sm-3 col-xs-12 fmlyDetails_bodyText">
                                    <label for="familyvalues-Liberal" class="radio-text radio-inline"><input type="radio" name="familyValue" id="familyvalues-Liberal" value="3" class="radio-btn" onBlur="validate_family();" <?php echo ($familyval == '3' ? 'checked' : '') ?> />{!!trans('translator.Liberal')!!} </label>            
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 fmlyDetails_bodyText ">{!!trans('translator.Affluence')!!}  @if ($errors->has('affluenceLevel')) <span class="errorIcon icon-alert" data-toggle="tooltip" data-placement="right"></span> @endif</div>
                                <div class="col-lg-4">
                                    {!! Form::select('affluenceLevel', FormMatrimonial::get_affluenceLevel(), $formData->profiles->affluenceLevel, array('class' => 'fmlyDetails_txtbox')) !!}
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-2 col-md-2 fmlyDetails_bodyText ">{!!trans('translator.Origin')!!}</div>
                                <div class="col-lg-4 col-md-4">

                                    <input value="{!!$formData->profiles->ancestralOrigin!!}" name="ancestralOrigin" type="text" class="fmlyDetails_txtbox"/>

                                </div>
                                <div class="col-md-5"><div class="small-text">{!!trans('translator.SpecifOrigin')!!}</div></div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <section class="button-demo">
                            <button type="submit" class=" btn-saveChanges btn ladda-button pull-right " id='showoffset' data-style="expand-right" value="">{!!trans('translator.SaveChanges')!!}</button>
                            {!!Form::close()!!}  
                        </section>
                        <button data-dismiss="modal" class="btn btn-close position-pop-save">{!!trans('translator.Cancel')!!}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    // Bind normal buttons
    Ladda.bind('.button-demo button', {timeout: 5000});

    // Bind progress buttons and simulate loading progress
    Ladda.bind('.progress-demo button', {
	callback: function (instance) {
	    var progress = 0;
	    var interval = setInterval(function () {
		progress = Math.min(progress + Math.random() * 0.1, 1);
		instance.setProgress(progress);

		if (progress === 1) {
		    instance.stop();
		    clearInterval(interval);
		}
	    }, 200);
	}
    });

</script>
