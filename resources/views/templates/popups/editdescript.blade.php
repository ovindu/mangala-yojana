<div class="modal fade" id="user-description" tabindex="-1" role="dialog" aria-labelledby="basic-info-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="profilePicUploadModalLabel">{!!trans('translator.Saysomethingaboutyou')!!}</h4>
            </div>
            <div class="modal-body">
                {!!Form::open(array('url'=>URL::route('editAboutMe') , 'id'=>'edit-about-form'))!!}
                {!!Form::token()!!}
                @if(!empty($theUser->profiles->description))
                <textarea class="form-control description" cols="4" rows="5" name="desc">{!!$theUser->profiles->description!!}</textarea>
                @else
                <textarea class="form-control description" cols="4" rows="5" name="desc" placeholder="{!!trans('translator.smallBioAbout')!!}"></textarea>
                @endif
                <div class="position-char-count">
                    <span class="count style-char-count" style="margin-left: 3px;">Character Count <span id="count" class="color-char-count" style="color:red;">0</span>
                        min 50. max 4000
                    </span>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-xs-12">
                        <section class="button-demo">
                            <button type="submit" class="disabled btn-saveChanges btn ladda-button pull-right " id='showoffset' data-style="expand-right" value="">{!!trans('translator.SaveChanges')!!}</button>
                            {!!Form::close()!!}  
                        </section>
                        <button data-dismiss="modal" class="btn btn-close position-pop-save">{!!trans('translator.Cancel')!!}</button>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        var count = $('.description').val().length;
        setCount(count);
        $('.description').keyup(function() {
            setCount(count);
        });
    });

    function setCount (count){
        count = $('.description').val().length;
        $('#count').text(count);
        if (count < 50) {
            $('#count').css('color', 'red');
            $('#showoffset').addClass('disabled');
        }
        else if (count < 4000) {
            $('#count').css('color', 'black');
            $('#showoffset').removeClass('disabled');
        }
        else if (count > 4000) {
            $('#count').css('color', 'red');
            $('#showoffset').addClass('disabled');
        }
    }
</script>

