<?php
use \App\Library\FormDetails; 
$regForm = new FormDetails;
?>
<!-- Modal -->
<div class="modal fade" id="edu-prof-modal" tabindex="-1" role="dialog" aria-labelledby="edu-prof-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="profilePicUploadModalLabel">{!!trans('translator.EditEducationProfessionalDetails')!!}</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <form method="post" action="{!!URL::route('eduProfEdit')!!}">
                        {!!Form::token()!!}
			<input type="hidden" name="userId" value="{!!$theUser->id!!}">
                        <div class="container-fluid">
                            <div class="row frmMat_row">
                                <div class="col-lg-3 col-md-3 fmlyDetails_bodyText">{!!trans('translator.EducationProf')!!}</div>
                                <div class="col-lg-7 col-md-7">
                                    {!!Form::select('edu_level', $regForm->get_edu_level(), $theUser->profiles->edu_level, array('class' => 'fmlyDetails_txtbox'));!!}
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-3 col-md-3 fmlyDetails_bodyText">{!!trans('translator.Profession')!!}</div>
                                <div class="col-lg-7 col-md-7">
                                    {!!Form::select('working_on', $regForm->get_workingAs_option(), $formData->profiles->working_on, array('class' => 'fmlyDetails_txtbox'));!!}
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-3 col-md-3 fmlyDetails_bodyText">{!!trans('translator.AnnualIncome')!!}</div>
                                <div class="col-lg-7 col-md-7 col-sm-4">
                                    {!!Form::select('an_income', $regForm->get_an_income(), $formData->profiles->an_income, array('class' => 'fmlyDetails_txtbox'));!!}
                                </div>
                            </div>
                            <div class="row frmMat_row">
                                <div class="col-lg-3 col-md-3 fmlyDetails_bodyText">{!!trans('translator.Workingwith')!!}</div>
                                <div class="col-lg-7 col-md-7 col-sm-4">
                                    {!!Form::select('working_at', $regForm->get_workingAt(), $formData->profiles->working_at, array('class' => 'fmlyDetails_txtbox'));!!}
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-12">
                            <section class="button-demo">
                                <button type="submit" class=" btn-saveChanges btn ladda-button pull-right " id='showoffset' data-style="expand-right" value="">{!!trans('translator.SaveChanges')!!}</button>
                                {!!Form::close()!!}  
                            </section>
                            <button data-dismiss="modal" class="btn btn-close position-pop-save">{!!trans('translator.Cancel')!!}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    // Bind normal buttons
    Ladda.bind('.button-demo button', {timeout: 5000});

    // Bind progress buttons and simulate loading progress
    Ladda.bind('.progress-demo button', {
	callback: function (instance) {
	    var progress = 0;
	    var interval = setInterval(function () {
		progress = Math.min(progress + Math.random() * 0.1, 1);
		instance.setProgress(progress);

		if (progress === 1) {
		    instance.stop();
		    clearInterval(interval);
		}
	    }, 200);
	}
    });

</script>
