<?php 
use \App\Library\FormMatrimonial; 
use \App\Library\FormDetails; 
?>
<div class="col-lg-12">
    <div class="profile-info-body infoBox partner profile-edit-container">
        <div class="row">
            <div class="col-lg-7 col-md-5 col-sm-5 col-xs-8">
                <h3 id="infoBox-Title">
                    @if($theUser->gender == 'm')
                    {!!Lang::get('translator.PartnerPreference', array('gen' => 'සහකාරියගේ'))!!}
                    @else
                    {!!Lang::get('translator.PartnerPreference', array('gen' => 'සහකරුගේ'))!!}
                    @endif
                </h3>
            </div>
            <div class="col-lg-3 col-lg-offset-2 col-md-7 col-sm-7 col-xs-4">
                @if($isSelf)
                @include('templates.popups.partnerPreferencesEdit')
                <a id="profile-partner-edit-btn" class="pull-right profile-edit-catch" href="#" data-toggle="modal" data-target="#partner-pref-modal"><span class="glyphicon glyphicon-pencil"></span><span class="hidden-xs">{!!trans('translator.Edit')!!}</span></a>
                @endif
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                        {!!trans('translator.Age')!!}  </span>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                    @if(!empty($theUser->matches->age) && $theUser->matches->age <> [0,0])
                    : {!!implode(' '.trans('translator.To').' ', explode(',', $theUser->matches->age))!!}
                    @else
                    : {!!$noMatter!!}
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                        {!!trans('translator.Height')!!}  </span>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                    <?php $heightArray = (isset($theUser->matches->height) ? explode(',', $theUser->matches->height) : ''); ?>
                    @if(!empty($theUser->matches->height) && $theUser->matches->height <> [0,0])
                    : {!!FormDetails::get_heightList($noMatter)[$heightArray[0]] . ' '.trans('translator.To').' <br> &nbsp;&nbsp;' . FormDetails::get_heightList($noMatter)[$heightArray[1]];!!}
                    @else
                    : {!!$noMatter!!}
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                        {!!trans('translator.MaritalStatus')!!}  </span>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                    @if(!empty($theUser->matches->marital_status))
                    : 
                    <?php $maritalStatus = explode(',', $theUser->matches->marital_status); ?>
                    @foreach($maritalStatus as $maritalState)
                    {!!FormDetails::get_maritalStatus('Doesn\'t Matter')[$maritalState]!!}
                    @endforeach
                    @else
                    : {!!$noMatter!!}
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                        {!!trans('translator.Religion')!!}  </span>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                    @if(!empty($theUser->matches->religion))
                    :
                    <?php $religions = explode(',', $theUser->matches->religion); ?>
                    @foreach($religions as $religion)
                    {!!FormDetails::get_religion('Doesn\'t Matter')[$religion]!!}
                    @endforeach
                    @else
                    : {!!$noMatter!!}
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                        {!!trans('translator.MotherTongue')!!}     </span>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                    @if(!empty($theUser->matches->language))
                    : <?php $languages = explode(',', $theUser->matches->language) ?>
                    @foreach($languages as $language)
                    {!!FormDetails::get_language('Doesn\'t Matter')[$language]!!}
                    @endforeach
                    @else
                    : {!!$noMatter!!}
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                        {!!trans('translator.Education')!!}   </span>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                    @if(!empty($theUser->matches->edu_level))
                    :
                    <?php $eduLevels = explode(',', $theUser->matches->edu_level) ?>
                    @foreach($eduLevels as $eduLevel)
                    {!!FormDetails::get_edu_level('partner','Doesn\'t Matter')[$eduLevel]!!}
                    @endforeach
                    @else
                    : {!!$noMatter!!}
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                        {!!trans('translator.Diet')!!}   </span>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                    @if(!empty($theUser->matches->diet))
                    :
                    <?php $diets = explode(',', $theUser->matches->diet); ?>
                    @foreach($diets as $diet)
                    {!!FormDetails::get_dietInfo('partner','Doesn\'t Matter')[$diet]!!}
                    @endforeach
                    @else
                    : {!!$noMatter!!}
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                        {!!trans('translator.Smoke')!!}  </span>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                    @if(!empty($theUser->matches->smoke))
                    : 
                    <?php $smoker = explode(',', $theUser->matches->smoke); ?>
                    @foreach($smoker as $smoke)
                    {!!FormDetails::get_smokeInfo('Doesn\'t Matter')[$smoke]!!}
                    @endforeach
                    @else
                    : {!!$noMatter!!}
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                        {!!trans('translator.Drink')!!}  </span>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                    @if(!empty($theUser->matches->drink))
                    : 
                    <?php $drinks = explode(',', $theUser->matches->drink); ?>
                    @foreach($drinks as $drink)
                    {!!FormDetails::get_drinkInfo('Doesn\'t Matter')[$drink]!!}
                    @endforeach
                    @else
                    : {!!$noMatter!!}
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                        {!!trans('translator.Bodytype')!!} </span>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                    @if(!empty($theUser->matches->body_type))                                  
                    : 
                    <?php $bodies = explode(',', $theUser->matches->body_type); ?>
                    @foreach($bodies as $body)
                    {!!FormDetails::get_bodyType('Doesn\'t Matter')[$body]!!}
                    @endforeach
                    @else
                    : {!!$noMatter!!}
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                        {!!trans('translator.SkinTone')!!}  </span>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                    @if(!empty($theUser->matches->skin_tone))
                    : 
                    <?php $skinTones = explode(',', $theUser->matches->skin_tone) ?>
                    @foreach($skinTones as $skinTone)
                    {!!FormDetails::get_skinTone('Doesn\'t Matter')[$skinTone]!!}
                    @endforeach
                    @else
                    : {!!$noMatter!!}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>