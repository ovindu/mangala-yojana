<div class="searchResultBody" >
    <div class="container-fluid ">
        <div class="row">
            <div class="col-sm-12">
                <a href="{!!URL::route("publicProfile",array('id'=>$userid))!!}">
                   <h4 id="daily10Name">{!!$dataArray['name']!!} 
                        <span class="@if($searchCtrl->isOnline($userid)){!!"online-icon online-icon-sty"!!}@else{!!"offline-icon online-icon-sty"!!}@endif"></span> 
                   </h4>
                </a>
                        
                        <div id="searchResultBody">
                        @include('templates.dropdowns.settingsDropdown',array('name'=>$dataArray['name']))
                        </div>
            </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div id="searchResultThumbnail">
                            <a href="{!!URL::route("publicProfile",array('id'=>$userid))!!}">
                               {!!HTML::image($ProfilePicture,null,array('class'=>'img-responsive'))!!}
                        </a>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8">
                    <div id="searchResultContent">
                        <div class="container-fluid">

                            <div class="row" class="searchcontentRow">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">

                                    <div class="row"><span class="myLabel">{!!trans('translator.Age')!!}</span></div>
                                    <div class="row"><span class="myLabel">{!!trans('translator.Height')!!}</span></div>
                                    <div class="row"> <span class="myLabel">{!!trans('translator.Religion')!!}</span></div>
                                    <div class="row"><span class="myLabel">{!!trans('translator.CurrentCity')!!} </span></div>

                                </div>

                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-6">
                                    <div class="row">: {!!$dataArray['age']!!}yrs</div>
                                    <div class="row">: {!!$dataArray['height']!!}</div>
                                    <div class="row">: {!!$dataArray['relig']!!}</div>
                                    <div class="row">: {!!$dataArray['city']!!}</div> 
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="container-fluid">
                            <div class="col-lg-12" >
                                <div id="buttonArea">
                                    <h4>@if($dataArray['gender'] == 'm')
                                        {!!trans('translator.LikeHisProfile')!!}
                                        @else
                                        {!!trans('translator.LikeHerProfile')!!}
                                        @endif
                                        </h4>
                                    <button  id="btn-{!!$userid!!}" class="@if($friendController->isLiked($userid)){!!'liked'!!}@else{!!'like'!!}@endif searchResult-btn yes 
                                             " data-like-user-id='{!!$userid!!}' liked_id='{!!$friendController->isLiked($userid)!!}'></button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>