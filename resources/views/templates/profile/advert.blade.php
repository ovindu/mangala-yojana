<div data-toggle="modal" class=" infoMain profile-info-body advert_body" data-target="#plz-upgrade-popup" data-title="{!!trans('translator.UpgradePackage')!!}" data-body="" submit="0">
    <div class="row">
        <div class="col-lg-12" style="margin-top: -24px;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="container-fluid">
                            <div class="row">
                                <div id='upgrade_pop_body' style="  margin-bottom: -15px;">
                                    <h4>{!!trans('translator.upgradenow')!!}</h4>
                                    <ul>
                                        <li>{!!trans('translator.SendUnlimitedProposals')!!}</li>
                                        <li>{!!trans('translator.InstantMessaging')!!}</li>
                                        <li>{!!trans('translator.ImprovedPrivacy')!!}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs">
                        <div class="container-fluid">
                            <div class="row advert_img" style="display: block !important;">
                                {!!HTML::image(asset('img/ajudshajovhflsmdojda.png'), null, array('class' => 'img-responsive','draggable'=>'false'))!!}
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>