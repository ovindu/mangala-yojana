<?php
use \App\Library\FormDetails;    
use \App\Library\Common;    
use \App\Library\Salutation;    
use \App\Models\User;    
use \App\Models\Searchuser;    
use \App\Http\Controllers\AccountController;    
use \App\Http\Controllers\ProfileController;
?>
<div class=" infoMain profile-info-body">
    <div class="row">
        <div class="col-lg-12">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">  <span class="myLabel">{!!trans('translator.Age/Height')!!}</span>  </div>
                                <?php $searchUser = Searchuser::find($theUser->id); ?>
                                <div class="col-lg-8  col-md-8  col-sm-8 col-xs-8">: @if(!empty($searchUser->age)) {!!$searchUser->age!!} @else {!!$notGiven!!} @endif / {!!(!empty($searchUser->profile->height) ? FormDetails::get_heightList()[$searchUser->profile->height] : $notGiven )!!}</div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">  <span class="myLabel">{!!trans('translator.Status')!!} </span></div>
                                <div class="col-lg-8  col-md-8  col-sm-8 col-xs-8">: @if(!empty($theUser->profiles->marital_status)) {!!FormDetails::get_maritalStatus()[$theUser->profiles->marital_status]!!} @else {!!$notGiven!!} @endif</div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">  <span class="myLabel">{!!trans('translator.Education')!!} </span> </div>
                                <div class="col-lg-8  col-md-8  col-sm-8 col-xs-8">: @if(!empty($theUser->profiles->edu_level)) {!!FormDetails::get_edu_level()[$theUser->profiles->edu_level]!!} @else {!!$notGiven!!} @endif</div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">  <span class="myLabel">{!!trans('translator.Profession')!!} </span>  </div>
                                <div class="col-lg-8  col-md-8  col-sm-8 col-xs-8">: @if(!empty($theUser->profiles->working_on)) <?php $works = $UserDetails->get_workingAs_option(TRUE); ?> {!!$works[$theUser->profiles->working_on]!!} @else {!!$notGiven!!} @endif</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">  <span class="myLabel">{!!trans('translator.Religion')!!}</span> </div>
                                <div class="col-lg-8  col-md-8  col-sm-8 col-xs-8">: @if(!empty($theUser->religion)) {!!FormDetails::get_religion()[$theUser->religion]!!} @else {!!$notGiven!!} @endif</div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">  <span class="myLabel">{!!trans('translator.Vernacular')!!} </span></div>
                                <div class="col-lg-8  col-md-8  col-sm-8 col-xs-8">: @if(!empty($theUser->profiles->language)) {!!FormDetails::get_language()[$theUser->profiles->language]!!} @else {!!$notGiven!!} @endif</div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">  <span class="myLabel">{!!trans('translator.Diet')!!}  </span></div>
                                <div class="col-lg-8  col-md-8  col-sm-8 col-xs-8">: @if(!empty($theUser->profiles->diet)) {!!FormDetails::get_dietInfo('self')[$theUser->profiles->diet]!!} @else {!!$notGiven!!} @endif</div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">  <span class="myLabel">{!!trans('translator.Income')!!}</span></div>
                                <div class="col-lg-8  col-md-8  col-sm-8 col-xs-8">: @if(!empty($theUser->profiles->an_income)) {!!FormDetails::get_an_income()[$theUser->profiles->an_income]!!} @else {!!$notGiven!!} @endif</div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>