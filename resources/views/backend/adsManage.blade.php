@extends('backend.layout.backendMaster')
@section('content')
<div class="page-header">
    <div class="row">
        <div class="col-lg-12">
            <div class="top-leading">
                <a href="#" data-toggle="modal" data-target="#new-ad-pop">
                    <button class="btn btn-default margin-btm-15">Add new advertisement</button>
                </a>
                <span class="help-block">Double click to edit title or content fields & Click <i class="glyphicon glyphicon-ok"></i> to save changes.</span>
                <span class="help-block"><i class="glyphicon glyphicon-thumbs-up"></i> indicates activated advertisements, Click it for deactivate & You can reactivate advertisement by clicking <i class="glyphicon glyphicon-thumbs-down"></i>. </span>
                @if($errors->has('edit_title') | $errors->has('edit_content'))
                <div class="help-block">
                    <span>{!!$errors->first('edit_title')!!}</span> <br>
                    <span>{!!$errors->first('edit_content')!!}</span>
                </div>
                @endif
                @if($errors->has('title') | $errors->has('content'))
                <div class="help-block ">
                    <span>{!!"Cannot be saved,"!!}</span> <br>
                    <span>{!!$errors->first('title')!!}</span> <br>
                    <span>{!!$errors->first('content')!!}</span>
                </div>
                @endif
                <div class="bs-example" data-example-id="bordered-table">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th class="hidden-xs " >Content</th>
                                <th></th>

                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($ads) | Session::has('ads'))
                            @foreach($ads as $ad)

                            <tr>
                        <form action="{!!URL::Route('AdsPage')!!}" method="get">
                            <td style="width: 60px;">
                                <span class="pull-left" style=" margin-right: 10px;">{!!$ad->id!!}</span>
                                <span class="editspan " data-id="{!!$ad->id!!}"><i class=" cursor-pointer  hidden-xs glyphicon glyphicon-pencil"></i></span>

                            </td>
                            <td class="width-td-ad" style="width: 80px;">
                                <textarea readonly name="edit_title" class="{!!$ad->id!!} full-width border-none non-resize contentArea" style="resize: none; width: 100px;" rows="2">{!!$ad->title!!}{{Input::old('edit_title')}}</textarea>
                            </td>

                            <td class="hidden-xs "  > 
                                <textarea name="edit_content" readonly class="{!!$ad->id!!} full-width border-none non-resize contentArea" style="resize: none;" rows="2">{!!$ad->content!!}{{Input::old('edit_content')}}</textarea>
                            </td>

                            <td class="" style="width: 105px;">
                                <input type="hidden" name="adid" value="{!!$ad->id!!}">
                                <button type="submit" class="cursor-pointer btn btn-default pull-right display-inline" style="margin-top: 7px; ">
                                    <i class="glyphicon glyphicon-ok"></i>
                                </button>
                        </form>

                        <form action="{!!URL::Route('AdsPage')!!}" method="get">
                            <input type="hidden" name="delete_id" value="{!!$ad->id!!}">
                            @if($ad->active == 1)

                            <button type="submit" data-toggle="tooltip" data-placement="left" title="" data-original-title="Ad is active, Click to deactivate." class="cursor-pointer btn btn-success pull-right display-inline" style="margin-top: 7px; margin-right: 5px;">
                                <i class="glyphicon glyphicon-thumbs-up"></i>
                            </button>
                            @else 
                            <button type="submit" data-toggle="tooltip" data-placement="left" title="" data-original-title="Ad is inactive, Click to Activate." class="cursor-pointer btn btn-danger pull-right display-inline" style="margin-top: 7px; margin-right: 5px;">
                                <i class="glyphicon glyphicon-thumbs-down"></i>
                            </button>
                            @endif
                        </form>
                        </td>


                        </tr>

                        @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            @if(isset($ads))

            @endif
        </div>
    </div>
</div>
@include('backend.pop-ups.newAd')
<script>
    $(".contentArea").dblclick(function() {
        $(this).removeAttr('readonly');
        $(this).addClass('form-control');
        $(this).removeClass('border-none');
    });

    $(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });

    $('.editspan').click(function() {
        var id = $(this).attr('data-id');
        $('.' + id).removeAttr('readonly');
        $('.' + id).addClass('form-control');
        $('.' + id).removeClass('border-none');
    });

</script>
@stop