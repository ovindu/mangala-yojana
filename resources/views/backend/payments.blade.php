@extends('backend.layout.backendMaster')
@section('content')

<div class="page-header">
    <div class="row">
        <div class="col-lg-12">
            <div class="top-leading">
                <div class="col-lg-3 filter-slice ">
                    <label for="choose">Filter Payments : </label>
                    <select name="filterPayments" class="form-control cursor-pointer">
                        <option value="0">All</option>
                        <option value="1">Completed</option>
                        <option value="2">Not Completed</option>
                    </select>
                </div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Invoice ID</th>
                            <th>User ID</th>
                            <th>Payment Method</th>
                            <th>Transaction ID</th>
                            <td>Amount</td>
                            <td>Due Date</td>
                            <th>Payment Cleared</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($payments as $payment)
                        <tr class="{!!($payment['cleared'] == 0 ? 'notcompleted' : 'completed')!!} allrow ">
                            <td>{!!$payment['invoice_id']!!}</td>
                            <td>{!!$payment['user_id']!!}</td>
                            <td>{!!$payment['pay_method']!!}</td>
                            <td>{!!$payment['txn_id']!!}</td>
                            <td>{!!$payment['amount']!!}</td>
                            <td>{!!$payment['due_date']!!}</td>
                            <td>
                                {!!($payment['cleared'] == 0 ? 'No' : 'Yes')!!}
                                @if($payment['cleared'] == 0)
                                <button class="btn btn-default pull-right complete-order" data-userid="{!!$payment['user_id']!!}" data-paymentid="{!!$payment['payment_id']!!}" data-order-type="{!!$payment['pay_method']!!}">
                                    Complete
                                </button>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="pagination center">
                {!!$data->appends(Input::all())->render()!!}
            </div>
        </div>
    </div>
</div>
<form action="{!!URL::route("completePayment")!!}" name="completePayment" method="POST">
    <input type="hidden" name="payment_type" value="">
    <input type="hidden" name="payment_id" value="">
    <input type="hidden" name="payment_user" value="">
</form>
@stop