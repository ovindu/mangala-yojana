@extends('backend.layout.backendMaster')
@section('content')
<?php 

$yourProfile = 'data-toggle="tooltip" data-placement="right" title="" data-original-title="You" '; ?>
<div class="page-header">
    <div class="row">
        <div class="col-lg-12">
            <div class="top-leading">

                <div class="four position-toggle">
                    <div class="button-wrap" data-val="1" id="toggle-body">
                        <div class="button-bg height-toggle">
                            <div class="button-out"></div>
                            <div class="button-in"></div>
                            <div class="button-switch height-switch">
                                <i id="icon-btn" class="glyphicon glyphicon-eye-open"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <span class="position-span-toggle help-block" id="toggle-btn-helper"></span>

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Report By</th>
                            <th>Reported</th>
                            <th class="hidden-xs hidden-sm" >Description</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($reports) | Session::has('reports'))
                        @if(Session::has('reports'))
                        <?php $reports = Session::get('reports');
                        $state; ?>
                        @endif
                        @foreach($reports as $report)
                        <?php $state = $report; ?>
                        <tr>
                            <td>
                                {!!Form::open(array('url'=>URL::route('editUser',array('id'=>$report->reporter_id))))!!}
                                <a href="@if(Auth::id() == $report->reporter_id) {!!'#'.'"'!!} {!!$yourProfile!!} @else {!!ProfileController::getProfileUrl($report->reporter_id).'"'!!} {!!'target="_blank"'!!} @endif"  class="cursor-pointer link">{!!BackendController::getUserName($report->reporter_id)!!}</a>
                                <button class="pull-right cursor-pointer remove-btn-mask">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                </button>
                                {!!Form::close()!!}
                            </td>

                            <td>
                                {!!Form::open(array('url'=>URL::route('editUser',array('id'=>$report->reported_id))))!!}
                                <a href="@if(Auth::id() == $report->reported_id) {!!'#'.'"'!!} {!!$yourProfile!!} @else {!!ProfileController::getProfileUrl($report->reported_id).'"'!!}  {!!'target="_blank"'!!} @endif" class="cursor-pointer link">{!!BackendController::getUserName($report->reported_id)!!}</a>
                                <button class="pull-right cursor-pointer remove-btn-mask">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                </button>
                                {!!Form::close()!!}
                            </td>
                            <td class="hidden-xs hidden-sm" >{!!$report->description!!}</td>
                            <td>
                                {!!Form::open(array('url'=>URL::route('markAsReadReport',array('id'=>$report->id))))!!}
                                @if($report->read == 1)
                                <button data-toggle="tooltip" data-placement="left" title="" data-original-title="Report marked as read, Click to set it as unread." class="center cursor-pointer btn btn-danger">
                                    <i class="glyphicon glyphicon-remove"></i>
                                </button>
                                @else
                                <button data-toggle="tooltip" data-placement="left" title="" data-original-title="Click to mark as read." class="center cursor-pointer btn btn-warning">
                                    <i class="glyphicon glyphicon-ok"></i>
                                </button>
                                @endif
                                {!!Form::close()!!}
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-12">
            {!!$reports->appends(Input::all())->render()!!}
        </div>
    </div>
</div>
<form id="form-toggle" action="{!!URL::route('Reports_page')!!}">
    @if(isset($state->read)) 
    <input name="read_not" type="hidden" value="{!!$state->read!!}" id="readNot">
    @else
    <input name="read_not" type="hidden" value="0" id="readNot">
    @endif
</form>
<script>

    $(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
    
    $('.button-wrap').on("click", function() {
        $(this).toggleClass('button-active');
        var val = $(this).attr('data-val');
        if (val == 1) {
            $('#icon-btn').addClass('red');
            $('#toggle-btn-helper').html("Showing results for unread reports");
            $(this).attr('data-val', 0);
            $('#form-toggle').submit();
        } else {
            $('#icon-btn').addClass('green');
            $('#toggle-btn-helper').html("Showing results for read reports");
            $(this).attr('data-val', 1);
            $('#form-toggle').submit();
        }
    });
    $(document).ready(function() {
        var val = $('#readNot').val();
        if (val == 1) {
            $('#toggle-btn-helper').html("Showing results for read reports");
            $('#icon-btn').addClass('green glyphicon glyphicon-eye-open');
            $('#toggle-body').attr('data-val', 0);
            $('.button-wrap').addClass('button-active')
        } else {
            $('#toggle-btn-helper').html("Showing results for unread reports");
            $('#toggle-body').attr('data-val', 1);
            $('#icon-btn').addClass('red glyphicon glyphicon-eye-close');
            $('.button-wrap').removeClass('button-active')
        }
    });

</script>
@stop