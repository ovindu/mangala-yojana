<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>My-Admin | Login</title>
        {!!HTML::style('css/bootstrap.min.css')!!}
        {!!HTML::style('css/style.css')!!}

    </head>

    <body>

        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default @if(Session::has('loginError')) {!!'validateError'!!} @endif">
                        <div class="panel-heading">
                            {!!HTML::image('img/logo.png','mangalayojana-logo',array('class' => 'center'))!!}
                        </div>
                        <div class="panel-body">
                            {!!Form::open(array('url'=>URL::Route('Backend_DoLogin')))!!}
                            <fieldset>
                                <div class="form-group">
                                    <input name="login-email" class="form-control" placeholder="E-mail"  type="email" autofocus value="{!!Input::old('login-email')!!}">
                                    <span class="help-block">{!!$errors->first('login-email')!!}</span>
                                </div>
                                <div class="form-group">
                                    <input name="login-password" class="form-control" placeholder="Password"  type="password" value="{!!Input::old('login-password')!!}">
                                    <span class="help-block">{!!$errors->first('login-email')!!}</span>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>
                                <button class="btn btn-lg btn-default btn-block-login " type="submit">Login</button>
                            </fieldset>
                            <span class="help-block text-center">{!!Session::get('loginError')!!}</span>
                            {!!Form::close()!!}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

</html>
