<div class="report-conf modal fade" id="new-ad-pop" tabindex="-1" role="dialog" aria-labelledby="basic-info-modal-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="block-confirmation-heading">
                    Add new advertisement
                </h4>
            </div>
            {!!Form::open(array('url'=>URL::route('AdsPage') , 'method' => 'post' ))!!}
            <input type="hidden" name="newAdd" value="TRUE">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <span>Title</span>
                            @if(isset($nameError))
                            <span class="help-block">{!!$errors->first('author_name')!!}</span>
                            @endif
                            <input name="title" type="text" class="form-control bottom-m-5 " placeholder="Ad Title" value="{!!Input::old('title')!!}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <span>Content</span><br>
                            <textarea name="content" class="non-resize-ta form-control" placeholder="Ad Content" rows="5"></textarea>
                        </div>
                    </div>
                    
                </div>
            </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-12">

                            <input type="hidden" name="aid" id="author-id" value="">
                            <button type="submit" class="btn btn-default" id='showoffset' data-style="expand-right" 
                                        value="">Save</button>
                            <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                        </div>
                    </div>
                </div>
                {!!Form::close()!!}
            </div>
        </div>
    </div>

