@extends('backend.layout.backendMaster')
@section('content')
<div class="page-header">
    <div class="row">
        <div class="col-lg-12">
            <div class="top-leading">
                @if(Session::has('message'))
                <?php $message = Session::get('message');
                $icon = ($message['type'] == 'info' ? 'glyphicon-thumbs-up' : 'glyphicon-thumbs-down');
                ?>
                <span class="help-block"><i class="glyphicon {!!$icon!!}"></i> {!!$message['message']!!} </span>
                @endif
                <div class="slice">
                    <form action="{!!URL::route('updateSettings')!!}" method="post">

                        <table class="table table-bordered">
                            <thead>
                                <tr >
                                    <th style="  width: 25%;">Option</th>
                                    <th>Value</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <p class="settings-para">
                                            HNB Account Number 
                                        </p>
                                    </td>
                                    <td>
                                        <input class="full-width form-control" type="text"  name="HNB_account_no" value="{!!Common::getCommonData('HNB_account_no')!!}" >
                                    </td>
                                </tr>


                                <tr>
                                    <td>
                                        <p class="settings-para">
                                            Account Name
                                        </p>
                                    </td>
                                    <td>
                                        <input class="full-width form-control" type="text" placeholder="MangalaYojana.lk" name="HNB_account_name" value="{!!Common::getCommonData('HNB_account_name')!!}">
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <p class="settings-para">
                                            Package price (LKR)
                                        </p>
                                    </td>
                                    <td>
                                        <input class="full-width form-control" type="text" placeholder="$2.5" name="premium_package_price_LKR" value="{!!Common::getCommonData('premium_package_price_LKR')!!}">
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <p class="settings-para">
                                            Package price (USD)
                                        </p>
                                    </td>
                                    <td>
                                        <input class="full-width form-control" type="text" placeholder="$2.5" name="premium_package_price_USD" value="{!!Common::getCommonData('premium_package_price_USD')!!}">
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <p class="settings-para">
                                            Paypal package name
                                        </p>
                                    </td>
                                    <td>
                                        <input class="full-width form-control" type="text" placeholder="Mangalayoja.lk PREMIUM Package" name="paypal_package_name" value="{!!Common::getCommonData('paypal_package_name')!!}">
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <p class="settings-para">
                                            Paypal Merchant button Link
                                        </p>
                                    </td>
                                    <td>
                                        <input class="full-width form-control" type="text" placeholder="https://www.paypalobjects.com/js/external/paypal-button.min.js?merchant=example@example.com" name="merchant_button_link" value="{!!Common::getCommonData('merchant_button_link')!!}">
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        <p class="settings-para">
                                            Package valid until (Days)
                                        </p>
                                    </td>
                                    <td>
                                        <input class="full-width form-control" type="number" name="package_expire_after" value="{!!Common::getCommonData('package_expire_after')!!}">
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                        {!! Form::token() !!}
                        <button type="submit" class="btn btn-default pull-right" >Update Settings</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop