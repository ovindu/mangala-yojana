@extends('backend.layout.backendMaster')
@section('content')
<input type="hidden" id="searchVal" value="{!!Input::old('query')!!}">
<div class="page-header">


    <div class="row">
        <div class="col-lg-12">
            <div class=" panel-top">
                <div class="bs-example" data-example-id="bordered-table">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>User</th>
                                <th class="col-lg-2">Category</th>
                                <th>Old Value</th>
                                <th>New Value</th>
                                <th class="col-lg-1">Verify</th>
                                <th class="col-lg-1">Ignore</th>
                            </tr>
                        </thead>
                        <tbody>
			    @foreach($changes as $change)
			    <tr data-edit-id="{!!$change->id!!}">
				<td><a href="{!!ProfileController::getProfileUrl($change->user_id)!!}" target="_blank" class="cursor-pointer link">
					<span><i class="glyphicon glyphicon-link"></i></span> {!!$change->user_id!!}
                                    </a></td>
				<td>{!!ucfirst($change->key)!!}</td>
				<td>{!!Common::getUserVerifyOldValue($change->id)!!}</td>
				<td>
				    <?php
				    switch ($change->key):
					case "profile picture":
					    echo '<img src="' . asset(ProfileController::getProfileImageUrl(true, $change->user_id, 'main', 'pending')) . '" height="100">';
					    break;
					default :
					    echo $change->value;
				    endswitch;
				    ?>
				</td>
				<td>
                                    <button data-edit-id="{!!$change->id!!}" class="glyphicon glyphicon-ok center-block link verify-edit"></button>
                                </td>
				<td>
                                    <button data-edit-id="{!!$change->id!!}" class="glyphicon glyphicon-remove center-block cancel-link verify-edit-cancel"></button>
                                </td>
			    </tr>
			    @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
	    @if(count($changes) <= 0)
	    <div class="row">
		<div class="col-lg-12">
		    <div class="alert alert-warning alert-dismissible fade in" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			No changes pending approval.
		    </div>
		</div>
	    </div>
	    @endif

	    <div class="row">
		<div class="col-lg-12">
		    <div class="pagination center">
			{!!$changes->appends(Input::all())->render()!!}
		    </div>
		</div>
	    </div>
        </div>
    </div>
</div>
<script>
    var editVerifyUrl = "{!!URL::Route('backendVerifyEdit',['id'=>''])!!}",
	    editVerifyIgnoreUrl = "{!!URL::Route('backendVerifyEditIgnore',['id'=>''])!!}";
</script>
@stop
