@extends('backend.layout.backendMaster')
@section('content')
{{-- {{dd(Input::all())}} --}}
{{-- {{dd($users->links())}} --}}
<input type="hidden" id="searchVal" value="{!!Input::old('query')!!}">
<div class="page-header">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default panel-top">
                <div class="panel-body">
                    {!!Form::open(array('url'=>URL::Route('users_Dashboard') , 'method' => 'get'))!!}
                    <div class="row">
                        <div class="col-lg-3">
                            <label>Search by profiles created</label>
                            <select name="profile_created" class="cursor-pointer form-control" >
                                <option value="0" @if(Input::has('profile_created') && $inputsOLD['profile_created'] == '0') {!!'selected'!!} @endif>Any</option>
                                <option value="1" @if(Input::has('profile_created') && $inputsOLD['profile_created'] == 1) {!!'selected'!!} @endif >Between Week</option>
                                <option value="30" @if(Input::has('profile_created') && $inputsOLD['profile_created'] == 30) {!!'selected'!!} @endif>Between Month</option>
                                <option value="90" @if(Input::has('profile_created') && $inputsOLD['profile_created'] == 90) {!!'selected'!!} @endif>3 Months</option>
                            </select>
                        </div>

                        <div class="col-lg-3">
                            <label>Gender</label>
                            <select name="gender" class="cursor-pointer form-control">

                                <option value="0" @if(Input::has('gender') && $inputsOLD['gender'] == '0') {!!'selected'!!} @endif >Any</option>
                                <option value="m" @if(Input::has('gender') && $inputsOLD['gender'] == 'm') {!!'selected'!!} @endif >Male</option>
                                <option value="f" @if(Input::has('gender') && $inputsOLD['gender'] == 'f') {!!'selected'!!} @endif >Female</option>
                            </select>
                        </div>

                        <div class="col-lg-3">
                            <label>Active Users</label>
                            <select name="user_state" class="cursor-pointer form-control">
                                <option value="a" @if(Input::has('user_state') && $inputsOLD['user_state'] == 'a') {!!'selected'!!} @endif >Any</option>
                                <option value="1" @if(Input::has('user_state') && $inputsOLD['user_state'] == '1') {!!'selected'!!} @endif >Active</option>
                                <option value="0" @if(Input::has('user_state') && $inputsOLD['user_state'] == '0') {!!'selected'!!} @endif >Inactive</option>
                            </select>
                        </div>

                        <div class="col-lg-3">
                            <label>User Package</label>
                            <select name="user_package" class="cursor-pointer form-control">
                                <option value="a" @if(Input::has('user_package') && $inputsOLD['user_package'] == 'a') {!!'selected'!!} @endif >Any</option>
                                <option value="1" @if(Input::has('user_package') && $inputsOLD['user_package'] == '1') {!!'selected'!!} @endif >Basic</option>
                                <option value="3" @if(Input::has('user_package') && $inputsOLD['user_package'] == '3') {!!'selected'!!} @endif >Premium</option>
                            </select>
                        </div>

                        <div class="col-lg-3">
                            <button class="cursor-pointer btn btn-default pull-right center-block full-width search-btn" type="submit"><i class="glyphicon glyphicon-search btn-icon"></i>Search</button>
                        </div>
                    </div>
                    {!!Form::close()!!}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div>
                <div class="bs-example" data-example-id="bordered-table">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Package</th>
                                <th>First Name</th>
                                <th class="hidden-xs" >Last Name</th>
                                <th class="hidden-xs hidden-sm" >Username</th>
                                <th class="hidden-xs">Gender</th>
                                <th>State</th>
                                <th>Edit</th>
                                <th>Reject</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($users) | Session::has('users'))
                            @if(Session::has('users'))
                            <?php $users = Session::get('users'); ?>
                            @endif
                            @foreach($users as $user)
                            <tr data-userid="{!!$user->id!!}">
                                <th scope="row">
                                    <a href="{!!ProfileController::getProfileUrl($user->id)!!}" target="_blank" class="cursor-pointer link">
                                       <span><i class="glyphicon glyphicon-link"></i></span> {!!$user->id!!}
                                   </a>
                               </th>
                               <th>
                                <?php echo UserRoles::find($user->role_id)->title; ?>
                            </th>
                            <td>{!!$user->fname!!}</td>
                            <td class="hidden-xs" >{!!$user->lname!!}</td>
                            <td class="hidden-xs hidden-sm" >{!!$user->email!!}</td>
                            <td class="hidden-xs">@if($user->gender == 'f')
                                {!!'Female'!!}
                                @else
                                {!!'Male'!!} @endif
                            </td>

                            <td>@if($user->active == 1)
                                {!!'Active'!!}
                                @else
                                {!!'Inactive'!!}
                                @endif
                            </td>
                            <td>
                                <?php echo Form::open(array('url' => URL::route('editUser', array('id' => $user->id)))); ?>
                                <button type="submit" class="center cursor-pointer remove-btn-mask">
                                    <i class="center glyphicon glyphicon-edit"></i>
                                </button>
                                {!!Form::close()!!}
                            </td>
                            <td>
                                <button data-userid="{!!$user->id!!}" class="glyphicon glyphicon-remove center-block cancel-link verify-cancel"></button>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@if(count($users) <= 0)
<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-warning alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            No results found for your search criteria.
        </div>
    </div>
</div>
@endif

<div class="row">
    <div class="col-lg-12">
        <div class="pagination center">
            {{ $users->appends(request()->except('page'))->links() }}
        </div>
    </div>
</div>

</div>
<script>
    document.getElementById('searchBox').value = document.getElementById('searchVal').value;
    var userCancelVerifyUrl="{!!URL::Route('backendCancelVerifyUser')!!}";
</script>
@stop
