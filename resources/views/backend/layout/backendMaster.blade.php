<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Mangalayojana.lk | My-Admin</title>

        {!!HTML::style('css/bootstrap.min.css')!!}
        {!!HTML::style('css/sb-admin-2.css')!!}
        {!!HTML::style('css/style.css')!!}
        {!!HTML::style('css/toglbtn.css')!!}

        {!!HTML::Script('js/jquery-1.11.0.js')!!}
        {!!HTML::Script('js/bootstrap.min.js')!!}
        {!!HTML::Script('js/backend.js')!!}
        {!!HTML::script('js/jquery.cropit.min.js'); !!}
    </head>

    <body>

        <div id="wrapper">

            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{!!URL::route('Backend_Dashboard')!!}">
                        {!!HTML::image('img/logo.png','mangalayojana-logo',array('class' => 'center backend-logo '))!!}
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown pull-right">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i>  <i class="glyphicon glyphicon-tree-deciduous"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#"><i class="glyphicon glyphicon-info-sign"></i> {!!Auth::user()->fname!!} (You)</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="{!!URL::route('Backend_Logout')!!}"><i class="glyphicon glyphicon-log-out"></i> Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            {!!Form::open(array('url'=>URL::Route('searchByName'), 'method' => 'GET'))!!}
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <input type="text" id="searchBox" class="@if(Session::has('searchError')) {!!'error-shadow'!!} @endif form-control " name="query" placeholder="Search users...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit">
                                            <i class="glyphicon glyphicon-search"></i>
                                        </button>
                                    </span>
                                </div>
                            </li>
                            {!!Form::close()!!}
                            <li>
                                <a class="@if(Request::url() == URL::Route('Backend_Dashboard')) {!!'active'!!} @endif" href="{!!URL::Route('Backend_Dashboard')!!}">Dashboard
                                    <i class="glyphicon glyphicon-briefcase pull-right">
                                    </i>
                                </a>
                            </li>
                            <li>
                                <a class="@if(Request::url() == URL::Route('users_Dashboard')) {!!'active'!!} @endif" href="{!!URL::Route('users_Dashboard')!!}">Users
                                    <i class="glyphicon glyphicon-user pull-right">
                                    </i>
                                </a>
                            </li>
                            <li>
                                <a class="@if(Request::url() == URL::Route('Reports_page')) {!!'active'!!} @endif" href="{!!URL::Route('Reports_page')!!}">User Reports
                                    <i class="glyphicon glyphicon-flag pull-right">
                                    </i>
                                </a>
                            </li>
                            <li>
                                <a class="@if(Request::url() == URL::Route('AdsPage')) {!!'active'!!} @endif" href="{!!URL::Route('AdsPage')!!}">Advertising
                                    <i class="glyphicon glyphicon-credit-card pull-right">
                                    </i>
                                </a>
                            </li>
                            <li>
                                <a class="@if(Request::url() == URL::Route('settingsPage')) {!!'active'!!} @endif" href="{!!URL::Route('settingsPage')!!}">Settings
                                    <i class="glyphicon glyphicon-cog pull-right">
                                    </i>
                                </a>
                            </li>
                            <li>
                                <a class="@if(Request::url() == URL::Route('backendPaymentsPage')) {!!'active'!!} @endif" href="{!!URL::Route('backendPaymentsPage')!!}">Payments
                                    <i class="glyphicon glyphicon-list pull-right">
                                    </i>
                                </a>
                            </li>
                            <li>
                                <a class="@if(Request::url() == URL::Route('backendVerifyPage')) {!!'active'!!} @endif" href="{!!URL::Route('backendVerifyPage')!!}">Verify Users
                                    <i class="glyphicon glyphicon-ok pull-right">
                                    </i>
                                </a>
                            </li>


                            <li>
                                <a class="@if(Request::url() == URL::Route('backendVerifyEditsPage')) {!!'active'!!} @endif" href="{!!URL::Route('backendVerifyEditsPage')!!}">Verify Edited Details
                                    <i class="glyphicon glyphicon-edit pull-right">
                                    </i>
                                </a>
                            </li>


                        </ul>
                    </div>
                </div>
            </nav>

            <div id="page-wrapper">
                @yield('content')
                <!--        end    Dashboard banner -->          
            </div>
        </div>


    </body>
</html>
