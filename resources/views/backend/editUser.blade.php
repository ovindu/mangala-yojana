@extends('backend.layout.backendMaster')
@section('content')
<?php
$UserDetails = new FormDetails;
$profileCtrl = new ProfileController;
$friendController = new FriendController();
$notGiven = '<em>Not Given</em>';
$noMatter = '<em>No Matter</em>';
if (!isset($user)) {
    if (Session::has('user')) {
     $user = Session::get('user');
 }
}
$theUser = $user;
if (isset($user)) {
    $profiles = BackendController::getProfile($user->id);
}
?>
<div class="page-header">

    <div class="row">
        <div class="col-lg-12">
            @if(Session::has('success'))
            <div class="top-leading alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {!!Session::get('success')!!}
            </div>
            @endif
            @if(Session::has('error'))
            <div class="top-leading alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {!!Session::get('error')!!}
            </div>
            @endif
            @if(!isset($user))
            <div class="top-leading alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {!!"something went wrong while displaying this page"!!}
            </div>
            @elseif(isset($user) && Auth::id() == $user->id)
            <div class="top-leading alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {!!"You are going to edit your details self. Its not recommended on system. To change your profile info go to your profile."!!}
            </div>
            @endif

            @if(empty($profiles))
            <div class="top-leading alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {!!"this user doesn't have a profile"!!}
            </div>
            @endif
        </div>
    </div>

    @if(isset($user))
    <div class="row">
        <div class="col-lg-6">
            @if(!empty($profiles)) 
            <table class="table" style="margin-top: 20px;">
                <tbody>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                        <td>{!!$user->email!!}</td>
                    </tr>
                    <tr>
                        <td>Age</td>
                        <td>:</td>
                        <td>{!!BackendController::getAge($user->id)!!}</td>
                    </tr>
                    <tr>
                        <td>Height</td>
                        <td>:</td>
                        <td>
                            {!!$profiles->height!!}
                            @if(!empty($profiles->height)) {!!FormDetails::get_heightList()[$profiles->height]!!} @else {!!$notGiven!!} @endif</td>
                        </tr>
                        <tr>
                            <td>Marital Status</td>
                            <td>:</td>
                            <td>
                                @if(!empty($profiles->marital_status))
                                {!!FormDetails::get_maritalStatus()[$profiles->marital_status]!!}
                                @else
                                {!!$notGiven!!}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Skin Tone</td>
                            <td>:</td>
                            <td>
                                @if(!empty($profiles->skin_tone))
                                {!!FormDetails::get_skinTone()[$profiles->skin_tone]!!}
                                @else
                                {!!$notGiven!!}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-lg-6">
                <table class="table" style="margin-top: 20px;">
                    <tbody>
                      <tr>
                        <td>Phone Number</td>
                        <td>:</td>
                        <td>{!!$user->mobilenumber!!}</td>
                    </tr>
                    <tr>
                        <td>District Living</td>
                        <td>:</td>
                        <td>
                            @if(!empty($profiles->city))
                            {!!WorldCities::find($theUser->profiles->city)->city_name!!}
                            @else
                            {!!$notGiven!!}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>City/State</td>
                        <td>:</td>
                        <td>
                            @if(!empty($profiles->small_city))
                            {!!SmallCities::getSmallCities()[$profiles->small_city]['city']!!}
                            @else
                            {!!$notGiven!!}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Residency Status</td>
                        <td>:</td>
                        <td>
                            @if(!empty($profiles->residency))
                            {!!FormDetails::get_residency()[$profiles->residency]!!}
                            @else
                            {!!$notGiven!!}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Ancestral Origin</td>
                        <td>:</td>
                        <td>
                            @if(!empty($profiles->ancestralOrigin))
                            {!!htmlspecialchars($profiles->ancestralOrigin)!!}
                            @else
                            {!!$notGiven!!}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
        @endif
    </div>
    @endif

    <div class="row">
     <?php
     $email_confirmed = ($user->verified == 1);
     $mobile_confirmed = ($user->mobile_verified == 1);
     $active = ($user->active == 1);
     $inactive_status = $user->inactive_status;
     $inactive_reason = 'Account Deactivated';
     if ($inactive_status == AccountController::$ACCOUNT_SYSTEM_DEACTIVATION) {
         $inactive_reason = 'User deactivated by cron';
     } elseif ($inactive_status == AccountController::$ACCOUNT_USER_DEACTIVATION) {
         $inactive_reason = 'Deactivated by user';
     } elseif ($inactive_status == AccountController::$ACCOUNT_SITE_ADMIN_DEACTIVATION) {
         $inactive_reason = 'Deactivated by Site Admin';
     }
     ?>
     <div class="col-lg-2 col-md-4 col-sm-6 col-xs-6 user-st-batch">
        <div class="user-status-label @if($email_confirmed) user-status-confirmed @else user-status-notconf @endif">
            <span class="glyphicon glyphicon-envelope"></span>
            <strong>Email Confirmed</strong>
        </div>
    </div>

    <div class="col-lg-2 col-md-4 col-sm-6 col-xs-6 user-st-batch">
        <div class="user-status-label @if($mobile_confirmed) user-status-confirmed @else user-status-notconf @endif" >
            <span class="glyphicon glyphicon-phone"></span>
            <strong>SMS Confirmed</strong>
        </div>
    </div>

    <div class="@if($active) col-lg-2 @else col-lg-4 @endif col-md-4 col-sm-6 col-xs-6 user-st-batch">
        <div class="user-status-label @if($active) user-status-confirmed @else user-status-notconf @endif" >
            <span class="glyphicon glyphicon-phone"></span>
            <strong>@if($active) Active user @else Inactive user {!!'- '.$inactive_reason!!} @endif</strong>
        </div>
    </div>
    <hr>
</div>

@if(isset($user))
<div class="row">
    {!!Form::Open(array('url' => URL::Route('UpdateFunc')))!!}
    <input type="hidden" value="{!!$user->id!!}" name="userid">
    <div class=" col-lg-8 ">
        <section class="padding-7">
            <div class="row">
                <h4>First Name</h4>
                <input type="text" name="first_name" class="form-control" placeholder="First name" value="{!!$user->fname!!}">
            </div>
            <div class="row">
                <h4>Last Name</h4>
                <input type="text" name="last_name" class="form-control" placeholder="Last name" value="{!!$user->lname!!}">
            </div>
            <div class="row">
                <h4>Description</h4>
                <textarea name="description" class="form-control resize-none margin-btm-5" placeholder="User Description" rows="4">{!!BackendController::getUserDescription($user->id)!!}</textarea>
            </div>
            <div class="row">
                <h4>Current Package</h4>
                <?php
                $user_role = $user->role_id;
                ?>
                <select class="form-control" name="user_package">
                    <option value="{!!Common::$basic_user!!}" @if($user_role == Common::$basic_user) {!!"selected='true'"!!} @endif >Primary Package</option>
                    <option value="{!!Common::$premium_user!!}" @if($user_role == Common::$premium_user) {!!"selected='true'"!!} @endif>Premium Package</option>
                </select>
            </div>
            <div class="row top-margin-5">
              <button class="btn btn-success margin-btm-5">Submit</button>
              <hr>
          </div>
          <div class="row">                    
              <h4 class="">User Availability</h4>
              <div class="col-lg-6">
                @if($user->active==1)
                <a href="{!!URL::Route('Avalability').'?id='.$user->id!!}">
                    <button type="button" class="btn btn-danger margin-btm-5 full-width">Active - Click to Deactivate</button>
                </a>
                @else
                <a href="{!!URL::Route('Avalability').'?id='.$user->id!!}">
                    <button type="button" class="btn btn-success margin-btm-5 full-width">Inactive - Click to Reactivate</button>
                </a>
                @endif
            </div>
        </div>
    </section>
</div>

{!!Form::Close()!!}

<style>
.cropit-image-preview {
    background-color: #f8f8f8;
    background-size: cover;
    border: 5px solid #ccc;
    border-radius: 3px;
    margin-top: 7px;
    width: 175px;
    height: 263px;
    cursor: move;
}

.cropit-image-background {
    opacity: .2;
    cursor: auto;
}

input.cropit-image-input {
    visibility: hidden;
}

.image-size-label {
    margin-top: 0.6rem;
}

input {
    /* Use relative position to prevent from being covered by image background */
    position: relative;
    z-index: 10;
}
</style>

<div class="col-lg-4">
    <section class="padding-7 pull-right">
        <div class="">
            {!!HTML::image(ProfileController::getProfileImageUrl(TRUE,$user->id), null, array('class' => 'img-responsive img-thumbnail img-user-be','draggable'=>'false'))!!}
        </div>
        @if(!strpos(ProfileController::getProfileImageUrl(TRUE,$user->id),'default-avatar'))
        <a href="{!!URL::Route('deleteProPic').'?id='.$user->id!!}" class="">
            <button class="btn btn-default top-margin-5 pull-right" type="button">Delete Image</button>
        </a>
        @endif
        {{ Form::open(array('url' => URL::Route('updateUserProPic'), 'files' => TRUE, 'enctype' => 'multipart/form-data')) }}
        {{-- <br>
            <input type="file" id="imgfile" class="cropit-image-input" name="theProfilePic" required> --}}
            {{-- <input type="file" id="imgfile" class="cropit-image-input" name="theProfilePic"> --}}
        {{-- <div class="select-image-btn mobileUpload btn-upload">{!!trans('translator.Selectanimage')!!}</div>
        <input type="hidden" name="user" value="$user->id">

        <div class="cropit-image-preview-container">
            <div class="cropit-image-preview"></div>
        </div>
        <div class="image-size-label">
            {!!trans('translator.Resizeimage')!!}
        </div>
        <input type="range" class="cropit-image-zoom-input"> --}}

        <div class="row">
            
            <div class="col-md-6 col-md-offset-3 text-center">
                <hr>
                <div class="image-editor text-center">
                    <br>
                    <button type="button" class="select-image-btn btn btn-info">Select</button>
                    {{-- <span class="help-block pic-upload-tip" id="helpblock-green">{!!trans('translator.maxUploadSize')!!}</span> --}}
                    {{-- <span class="help-block pic-upload-tip" id="helpblock-danger" style="display: none;   color: red;">You could not able to upload this image, Image size are too large.</span> --}}
                    <input type="hidden" value="" name="cropPosition" id="cropPosition">
                    <input type="hidden" name="user" value="{{$user->id}}">
                    <input type="file" id="imgfile" class="cropit-image-input" name="theProfilePic">
                    <!-- .cropit-image-preview-container is needed for background image to work -->
                    <div class="cropit-image-preview-container">
                        <div class="cropit-image-preview"></div>
                    </div>
                    <div class="image-size-label">
                        {!!trans('translator.Resizeimage')!!}
                    </div>
                    <input type="range" class="cropit-image-zoom-input">
                    <br>
                    <button type="submit" class="btn btn-info ladda-button " disabled="disabled" id='showoffset' data-style="expand-right" value="">{!!trans('translator.SaveChanges')!!}</button>
                </div>
            </div>                        
        </div>
        {{ Form::close() }}
    </section>
</div>
</div>

</div>

@endif

<script>

    $('.select-image-btn').click(function () {
        $('.cropit-image-input').click();
    });

    $(document).on('change', '.cropit-image-input', function () {
        var imgpath = document.getElementById('imgfile');
        if (!imgpath.value == "") {
            var img = imgpath.files[0].size;
            var imgsize = img / 4096; 
            if (imgsize < 4096) {

                $('#showoffset').removeAttr('disabled');
                $('#helpblock-danger').slideUp();
            } else {

                $('#showoffset').attr('disabled', 'disabled');
                $('#helpblock-green').slideUp('slow','linear',function (){
                    $('#helpblock-danger').slideDown();
                });
            }
        }
    });
    $(function () {
        $('.image-editor').cropit({
            exportZoom: 12,
            imageBackground: true,
            imageBackgroundBorderWidth: 10,
            width: 175,
            height: 263
        });

    });

    $(document).on('mousemove', ".cropit-image-preview", function () {
        var offs = $('.image-editor').cropit('offset');

        $("#cropPosition").val(offs.x + ',' + offs.y + ',' + $('.image-editor').cropit('zoom'));
    });


</script>

@stop
