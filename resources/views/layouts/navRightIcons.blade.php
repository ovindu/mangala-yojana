<li>
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        <span id="freq-icon" class="icon-user-add"></span>
        <div id="freq-notif-holder"></div>
    </a>

    <ul class="icon-dropdown dropdown-menu top_menu_notice_panel scroller" role="menu">
        <span class="upTriangle"></span>
        <span id="frq-list-holder"></span>
    </ul>
</li>
<li>
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        <span id="msg-icon" class="glyphicon glyphicon-comment"></span>
        <div id="msg-notif-holder"></div>
    </a>
    <div class="icon-dropdown dropdown-menu top_menu_notice_panel scroller" role="menu">
        <ul class="msg-list-dropdown">
            <span class="upTriangle"></span>
            <li id="msg-list-holder"></li>
        </ul>
    </div>
</li>
<li id="topmenu_notification_menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true"
       id="notificationPanelMenu">
        <span class="glyphicon glyphicon-bell {!! $numOfNotifications > 0 ? 'menu-icon-active' : ''!!}"
              id="notifictionIcon"></span>
        <div id="notif-count" class=""
             style="{!! $numOfNotifications < 1 ? 'display:none' : ''!!}">{!!$numOfNotifications!!}</div>
    </a>
    <ul class="icon-dropdown dropdown-menu top_menu_notice_panel scroller" role="menu"
        id="topmenu_notification_menu_panel" aria-labelledby="notificationPanelMenu">
        <span class="upTriangle"></span>
        <span id="loadNotifications"></span>
    </ul>
</li>
<li id="topmenu_settings_menu">
    <a href="{!!URL::route('doSettings' , 'general')!!}">
        <span class="glyphicon glyphicon-cog"></span>
    </a>
</li>
<input type="hidden" id="get_locale" value="{!!App::getLocale()!!}">


<script>
    $(function () {
        checkNotifications();
        fetchMessages();
        fetchRequests();
    });
    setInterval(function () {
        checkNotifications();
        fetchMessages();
        fetchRequests();
    }, 30000);

    function checkNotifications() {
        $.ajax({
            url: "{!!URL::route('chkFrequestsPage',array(),false)!!}",
            dataType: 'html',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        }).done(function (NotifyFrqs) {
            $('#freq-notif-holder').html(NotifyFrqs);
            var notf_count = parseInt($('#freq-count').html());
            if (notf_count == 0) {
                $('#freq-notif-holder').html(' ');
            }
            if (NotifyFrqs.length !== 0) {
                $('#freq-icon').addClass('menu-icon-active');
            } else {
                $('#freq-icon').removeClass('menu-icon-active');
            }
        });
        $.ajax({
            url: "{!!URL::route('chkmessagesPage',array(),false)!!}",
            dataType: 'html',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        }).done(function (NotifyMsgs) {
            $('#msg-notif-holder').html(NotifyMsgs);
            if (NotifyMsgs.length !== 0) {
                $('#msg-icon').addClass('menu-icon-active');
            } else {
                $('#msg-icon').removeClass('menu-icon-active');
            }
        });
    }

    function fetchMessages() {
        $.ajax({
            url: "{!!URL::route('fetchMessages',array(),false)!!}",
            dataType: 'html',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        }).done(function (FetchMsgs) {
            $('#msg-list-holder').html(FetchMsgs);
        });
    }

    function fetchRequests() {
        $.ajax({
            url: "{!!URL::route('fetchFrequests',array('path'=>Request::url()),false)!!}",
            dataType: 'html',
            data: {path: "{!!Request::url()!!}"},
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        }).done(function (FetchFreqs) {
            $('#frq-list-holder').html(FetchFreqs);
        });
    }
</script>