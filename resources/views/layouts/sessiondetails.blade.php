<?php 
use \App\Library\FormDetails;    
use \App\Library\Common;    
use \App\Library\Salutation;    
use \App\Models\User;    
use \App\Models\Searchuser;    
use \App\Http\Controllers\FriendController;
use \App\Http\Controllers\ProfileController;
?>
@section('sessiondetails')
<!-- xs Login -->

@if(!Auth::check())
<div class="visible-xs-block">
    <div id="login-xs">

        <div class="col-xs-12 login-incentive">

            {!!trans('translator.fullViewReq')!!}
            <button id='login_btn' class="btnLogin-xs text-center" data-toggle="collapse" data-target="#showLogin">
                {!!trans('translator.Login')!!}
            </button>
        </div>
        <script>
            $(function() {
                $('#login_btn').on('click', function() {
                    $('.login-incentive').fadeOut();
                });
            });
            $(function() {
                $('#hideloginForm').on('click', function() {
                    $('.login-incentive').fadeIn();
                });
            });
        </script>

        <div class="collapse navbar-collapse" id="showLogin">
            @include('layouts.login')
            <a href="/" style="margin-top: 10px; font-size: 11px;">{!!trans('NoAccountMsg')!!}</a>
        </div>
    </div>
</div>
<!-- End of xs Login -->
@else
<?php
$theUser = User::with(array('profiles', 'matches'))->find(Auth::user()->id);
$ProfilePicture = ProfileController::getProfileImageUrl(FALSE, NULL, 'thumb');
?>
<div id='accountInfo' class="navbar-right hidden-xs">
    <div class="row">

        <div class="col-md-12" id='headerProfileInfo'>
            <div class="col-sm-3">
                <div class="navbar-text text-right">
                    @if(Auth::user()->role_id == 1)
                    <a href="{!!URL::route('upgradePage')!!}"
                        class="btn btn-upgrade">{!!trans('translator.Upgrade')!!}</a>
                    @endif
                </div>
            </div>
            <div class="col-sm-5">
                <div class="subicons" style="padding-top: 25px; width: 0px;">
                    <ul class="lang_div" style="display: inline-block; width:170px;">
                        {{-- <li class="offered_in_text">{!!trans('translator.SiteLang')!!}</li> --}}
                        <li>
                            <a class="link" href="{!!URL::route('langChange','si')!!}">සිංහල</a> |

                            <a class="link" href="{!!URL::route('langChange','en')!!}">English</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="open-dropdown cursor-pointer" id="account-details" data-toggle="dropdown"
                    aria-haspopup="true" role="button" aria-expanded="false">

                    <div class="profileheaderNameText navbar-text" style="float: right;">
                        <span class="open-dropdown cursor-pointer">{!!$theUser->fname!!}</span>
                        <div class="dropdown header-settings" style="float:right;">
                            <span id="dLabel" data-toggle="dropdown" aria-haspopup="true" role="button"
                                aria-expanded="false" class="caret"></span>
                            <ul class="dropdown-menu masterDropdown" role="menu" aria-labelledby="dLabel">
                                <span class="masterTriangle-border"></span>
                                <span class="masterTriangle"></span>
                                <li>
                                    <form action="{!!URL::route('profilePage')!!}">
                                        <a href="{!!URL::route('profilePage')!!}"><span
                                                class="glyphicon glyphicon-user"></span>{!!trans('translator.Profile')!!}</a>
                                    </form>
                                </li>
                                <li>
                                    <form action="{!!URL::route('friendsPage')!!}">
                                        <a href="{!!URL::route('friendsPage')!!}"><span
                                                class="glyphicon icon-users"></span>{!!trans('translator.Friends')!!}</a>
                                    </form>
                                </li>
                                <li>
                                    <form action="{!!URL::route('favPage')!!}">
                                        <a href="{!!URL::route('favPage')!!}"><span
                                                class="glyphicon glyphicon-heart"></span>{!!trans('translator.Favorites')!!}</a>
                                    </form>
                                </li>
                                <li>
                                    <form action="{!!URL::route('showGeneralSettings')!!}">
                                        <a href="{!!URL::route('showGeneralSettings')!!}"><span
                                                class="glyphicon glyphicon-cog"></span>{!!trans('translator.Settings')!!}</a>
                                    </form>
                                </li>
                                <li>
                                    <form action="{!!URL::route('doLogout')!!}">
                                        {!!Form::token()!!}
                                        <a class="" href="{!!URL::route('doLogout')!!}" type="submit"><span
                                                class="glyphicon glyphicon-off"></span>{!!trans('translator.Logout')!!}</a>
                                    </form>
                                </li>

                            </ul>
                        </div>
                    </div>
                    <div class="propic-small">
                        {!!HTML::image($ProfilePicture, null, array('class' => 'img-responsive'))!!}
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@stop