<div class="container-fluid mncontainer">
                <div class="col-xs-12 visible-xs-block">
                    <form role="form" id="loginform-xs" name='vhmbh' method="post" action="{!!URL::route('doLogin')!!}">
                        {!!Form::token()!!}
                        <div class="form-group">
                            <h4> Member login</h4>
                            <input name="login-email" type="email" value="{!!Input::old('email')!!}" class="frmtexts" placeholder="">
                        </div>
                        <div class="form-group">
                            <h4> Password</h4>
                            <input name="login-password" type="password" class="frmtexts" placeholder="">
                        </div>
                        <div id='' style="margin-bottom: 10px; ">
                            <button type="submit" class="btn_login-xs">Login</button>
                            <a class="btn_login-xs" style="background: #ccc; color: #fff !important; padding-top: 2px;" id='hideloginForm'  data-toggle="collapse" data-target="#showLogin">Not Now</a>
                            <div class="row">
                                <div id="navFormErrors">  
                                    @if(Session::has('loginError'))
                                    <span>{!! Session::get('loginError')!!}</span>
                                    @endif
                                    
                                    @if(Session::has('errors'))
                                    @if(isset($errors) && $errors->count())
                                    @foreach ($errors->all() as $error)
                                    <span>{!! $error !!}</span>		
                                    @endforeach
                                    @endif                            
                                    @endif                            
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <input id="login-chkbox-xs" type="checkbox"  name="remember" value="1"/> 
                        <label id="login-chkbox-label-xs" for="login-chkbox-xs" >Remember Me</label> <br>
                        
                    </form>
                </div>
            </div>