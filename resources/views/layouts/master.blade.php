<!DOCTYPE HTML>
<?php
$current_url = Request::url();
$show_notif = (isset($show_notif) ? false : true);
use \App\Library\Common;  
?>

<html>

<head>
    <meta charset="UTF-8">
    <title> @yield ('title')</title>
    <link rel="shortcut icon" type="image/x-icon" href="{!!asset('favicon.ico')!!}">
    <link href='//fonts.googleapis.com/css?family=Open+Sans:100,200,700,600,800' rel='stylesheet' type='text/css'>
    {!! HTML::style('css/bootstrap.min.css')!!}
    {!!HTML::style('css/fontstyles.css')!!}
    {!!HTML::style('css/jquery-ui.css')!!}
    {!!HTML::style('css/style.css')!!}
    {{-- {!!HTML::style('css/toggle.css')!!} --}}
    {!!HTML::style('css/ladda.min.css')!!}
    {!!HTML::style('css/lightbox.css')!!}
    {!!HTML::style('css/chosen.css') !!}


    {!!HTML::style('css/fontastic/styles.css') !!}
    {!!HTML::style('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css') !!}

    {!!HTML::script('js/jquery.min.js')!!}
    {!!HTML::script('js/jquery-ui.js')!!}
    {!!HTML::script('js/bootstrap.min.js')!!}

    {!!HTML::script('js/spin.min.js')!!}
    {!!HTML::script('js/ladda.min.js')!!}
    {!!HTML::script('js/chosen.jquery.js'); !!}
    {!!HTML::script('js/jquery.cropit.min.js'); !!}
    {!!HTML::script('js/handlebars-v2.0.0.js'); !!}
    {!!HTML::script('js/main.js'); !!}
    {!!HTML::script('js/lightbox.min.js') !!}



    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="shortcut icon" type="image/x-icon" href="{!!asset('favicon.ico')!!}">

    <script>
        $(function () {
          $("#tabs").tabs({
              collapsible: false
          });
      });
    </script>

    {{-- <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> --}}
    <script>
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    (adsbygoogle = window.adsbygoogle || []).push({
      google_ad_client: "ca-pub-5091727005907575",
      enable_page_level_ads: true
  });
    </script>

</head>

<body>

    <script>
        $(document).ready(function () {
          $('#join').slideDown(500);
      });
    </script>
    @if(!Auth::check() && Request::url() == URL::route('publicProfile'))
    <div id="join">
        <a href="/"> {!!trans('translator.join_mangalayojana')!!} </a>
    </div>
    @elseif(Auth::check() && Auth::user()->verified != 1 && $current_url != URL::route('registrationPage') &&
    $current_url != URL::route('matrimonialPage') && $current_url != URL::route('partnerPage') && $current_url !=
    URL::route('thankyouPage'))
    <div id="join">
        <a href="#" id='notverified_msg'> {!!trans('translator.please_verify')!!} </a>
        <button type='button' class='ladda-button ' data-access-url='{{ route('resendConfirmationEmail') }}'
            id='resend_conf_btn'>{!!trans('translator.ResendConfirmationMail')!!}</button>
        <span id='resend_conf_success' style='display:none;'>{!!trans('translator.conf_mail_send_success')!!}</span>
    </div>

    @elseif(!Common::mobileVerified(Auth::id()) && Auth::check() && $current_url != URL::route('mobileVerifPage') &&
    $current_url != URL::route('registrationPage') && $current_url != URL::route('matrimonialPage') && $current_url !=
    URL::route('partnerPage') && $current_url != URL::route('thankyouPage') && $show_notif )
    <div id="join">
        <a href="{!!URL::route('mobileVerifPage')!!}" id='notverified_msg'>
            {!!trans('translator.please_verify_mobile')!!}
            <button type='button' class='ladda-button'
                id='resend_conf_btn'>{!!trans('translator.click_here_to_verifiy')!!}</button>
        </a>
    </div>
    @endif
    @if(Auth::check() && Auth::user()->admin_verified==0)
    <div class="header-message show">
        <strong>{!!trans('translator.account_not_active_till_admin_verification')!!}</strong>
    </div>
    @endif

    <!--Begin of nav bar -->
    <nav class="navbar navbar-default my_nav" role="navigation">
        <div class="container-fluid mncontainer">
            <!-- Brand and toggle get grouped for better mobile display -->

            <div class="navbar-header">
                @if(Auth::check())
                <button type="button" class="navbar-toggle " data-toggle="collapse" data-target="#menu">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                @endif
                <a class="navbar-brand" href="{!!URL::route('homePage')!!}"><img src="{!!asset('img/logo.png')!!}"
                        id="logo" alt="Mangalayojana.lk"></a>
            </div>



            <!-- Collect the nav links, forms, and other content for toggling -->
            @if(!Auth::check())
            <div id="" class="visible-xs">
                @include('layouts.login')
            </div>
            @endif
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                @if(!Auth::check())
                @include('templates.popups.forgotPassword')
                <form class="navbar-form navbar-right navright navLoginForm" name="jjj" id="loginform" method="post"
                    action="{!!URL::route('doLogin')!!}">
                    {!!Form::token()!!}
                    <div class="row">
                        <div class="col-lg-4 col-sm-4">
                            <h4 class="heading4">{!!trans('translator.Memberlogin')!!}</h4>
                            <input type="text" tabindex="1" class="txtbox" name="login-email"
                                value="{!!Input::old('login-email')!!}">
                            <div class="clearfix"></div>
                            <input id="login-chkbox" type="checkbox" class="css-checkbox" name="remember" value="1" />
                            <label id="login-chkbox-label" for="login-chkbox"
                                class="css-label">{!!trans('translator.Remember')!!}</label>
                        </div>
                        <div class="col-lg-4 col-sm-4">
                            <h4 class="heading4">{!!trans('translator.Password')!!}</h4>
                            <input type="password" tabindex="2" class="txtbox" name="login-password">
                            <div class="clearfix"></div>
                            <label><a href="#" data-toggle="modal"
                                    data-target="#forgot-password-modal">{!!trans('translator.ForgotPassword')!!}</a></label>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <button type="submit" tabindex="3"
                                class="btn_login">{!!trans('translator.Login')!!}</button>
                        </div>
                    </div>
                    <div class="row">

                        <div id="navFormErrors">
                            @if(Session::has('loginError'))
                            <span>{!! Session::get('loginError') !!}</span>
                            @endif

                            @if(Session::has('errors'))
                            @if(isset($errors) && $errors->count())
                            @if ($errors->has('login-email'))
                            <span class='formError'>{!! $errors->first('login-email') !!}</span>
                            @endif

                            @if ($errors->has('login-password'))
                            <span class='formError'>{!! $errors->first('login-password') !!}</span>
                            @endif
                            @endif
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="subicons" style="padding-top: 15px;">
                                <ul class="lang_div">
                                    {{-- <li class="offered_in_text">{!!trans('translator.SiteLang')!!}</li> --}}
                                    <li>
                                        <a class="link" href="{!!URL::route('langChange','si')!!}">සිංහල</a> |
                                        <a class="link" href="{!!URL::route('langChange','en')!!}">English</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
                @else
                @include('layouts.sessiondetails')
                @yield ('sessiondetails')
                @endif

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    <!-- End of navbar-->

    @if(Session::has('message'))

    <div class="container bodycontainer">
        <div class="alert {!!Session::get('alert-type')!!} fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span
                    class="sr-only">Close</span></button>
            <strong>{!!Session::get('message')!!}</strong>
        </div>
    </div>
    @endif

    @yield ('content')


    <!--Footer Begin-->
    <div class="footer">
        <div class="container mncontainer">
            <div class="row">
                <div class="col-lg-9 col-md-10 col-sm-8 col-xs-12">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 footerBox">
                        <h3 class="footer_heading3">{!!trans('translator.Membership')!!}</h3>
                        @if(!Auth::check())
                        <a href="{!!URL::route('homePage')!!}">{!!trans('translator.MemberLogin')!!}</a><br />
                        <a href="{!!URL::route('homePage')!!}">{!!trans('translator.RegisterFree')!!}</a><br />
                        @endif
                        <a href="{!!URL::route('searchPage')!!}">{!!trans('translator.PartnerSearch')!!}</a><br />
                        <!--<a href="#">{!!trans('translator.CustomerSupport')!!}</a><br/>-->
                        <!--<a href="#">{!!trans('translator.SuccessStories')!!}</a><br/>-->
                    </div>


                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 footerBox" id="footerOtherServices">
                        <h3 class="footer_heading3">{!!trans('translator.NeedHelpQ')!!}</h3>
                        <!--                            <a href="#">{!!trans('translator.Mobile')!!}</a><br/>
                 <a href="#">{!!trans('translator.WeddingPlans')!!}</a><br/>-->

                        <a href="#">{!!trans('translator.ContactUs')!!}</a><br />
                        <a href="{!!URL::route('faqPage')!!}">{!!trans('translator.faqs')!!}</a><br />

                    </div>


                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 footerBox">
                        <h3 class="footer_heading3">{!!trans('translator.Company')!!}</h3>
                        <a
                            href="{!!URL::route('aboutUsPage')!!}">{!!trans('translator.AboutMangalayojanalk')!!}</a><br />
                        <!--                            <a href="#">{!!trans('translator.Blog')!!}</a><br/>
							<a href="#">{!!trans('translator.AffiliateProgram')!!}</a><br/>
							<a href="{!!URL::route('homePage')!!}">{!!trans('translator.JoinUs')!!}</a><br/>
							<a href="#">{!!trans('translator.ListYourSite')!!}</a><br/>
							<a href="#">{!!trans('translator.AwardsRecognition')!!}</a><br/>
							<a href="#">{!!trans('translator.SiteMap')!!}</a><br/>-->
                    </div>
                    @if(Auth::check())
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 footerBox" id="footerOtherServices">
                        <h3 class="footer_heading3">{!!trans('translator.PrivacyYou')!!}</h3>
                        <a href="#">{!!trans('translator.TermsofUse')!!}</a><br />
                        <!--<a href="#">{!!trans('translator.PrivacyPolicy')!!}</a><br/>-->
                        <!--<a href="#">{!!trans('translator.SecurityTips')!!}</a><br/>-->
                        <!--<a href="#">{!!trans('translator.ReportMisuse')!!}</a><br/>-->
                    </div>
                    @endif
                </div>


                <div class="col-lg-3 col-md-2 col-sm-4 col-xs-12  footer-social">
                    <h3 class="footer_heading3">{!!trans('translator.OnTheMove')!!}</h3>
                    <div class="subicons">
                        <ul class="socialicons">
                            <li class="twicon">
                                <a href="http://twitter.com/mangalayojana" rel="nofollow" target="_blank"
                                    title="Follow Us on Twitter">Twitter</a>
                            </li>
                            <li class="fbicon">
                                <a href="http://facebook.com/mangalayojana.lk" rel="nofollow" target="_blank"
                                    title="Like Us on Facebook">FaceBook</a>
                            </li>
                            <li class="yticon">
                                <a href="https://www.youtube.com/channel/UCiNRrSzbCLSUhYKsddiRY2w" rel="nofollow"
                                    target="_blank" title="Subscribe Us on Youtube">Google+</a>
                            </li>
                            <li class="gpicon">
                                <a href="https://plus.google.com/115043632584817010131" rel="nofollow" target="_blank"
                                    title="Follow Us on Google+">Google+</a>
                            </li>


                        </ul>
                    </div>
                    <div class="subicons">
                        <ul class="lang_div">
                            <li class="offered_in_text">{!!trans('translator.SiteLang')!!}</li>
                            <li>
                                <a class="link" href="{!!URL::route('langChange','si')!!}">සිංහල</a> |
                                <a class="link" href="{!!URL::route('langChange','en')!!}">English</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <div class="container">
            <div class="row">
                <div class="container mncontainer">
                    <span class="footer_copyright">{!!trans('translator.Copyright').' '.trans('translator.ServiceBy')!!}
                        <a href="http://www.accentproductions.info/" target="_blank"
                            style="color:#a4a4a4 !important;">Accent Productions,
                        </a>{!!trans('translator.DevelopedBy')!!}<a href="http://www.ceylonsystems.com" target="_blank"
                            style="color:#a4a4a4 !important;"> {!!trans('translator.CeylonSystems')!!}</a></span>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script>
        $(document).ready(function(e){
            $("#searchform").validate();
            $("#frmSignUp").validate();
        });
    </script>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
            (function(){
                var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
                s1.async=true;
                s1.src='https://embed.tawk.to/5c6ffd403341d22d9ce59df7/default';
                s1.charset='UTF-8';
                s1.setAttribute('crossorigin','*');
                s0.parentNode.insertBefore(s1,s0);
            })();
    </script>
    <!--End of Tawk.to Script-->

</body>

</html>
@include('templates.popups.not_upgraded')