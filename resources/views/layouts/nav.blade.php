<?php 
use \App\Library\FormDetails;    
use \App\Library\Common;    
use \App\Library\Salutation;    
use \App\Models\User;    
use \App\Models\Searchuser;    
use \App\Http\Controllers\FriendController;
use \App\Http\Controllers\ProfileController;
?>
<div class="container-fluid menuBarContainer">
    <nav class="navbar menuBar " role="navigation">
        <div class="container-fluid" >
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="menu" >
                <ul class="nav menuBar-item navbar-nav @if(App::getLocale() == 'si') {!!'lang_sinhala'!!} @else {!!'lang_english'!!} @endif">
                    @if(Auth::check())
                    <li ><a href="{!!URL::route('profilePage')!!}"<?php echo (Request::url() == URL::route('profilePage') ? 'class="active"' : null); ?>>{!!Lang::get('translator.profile')!!}</a></li>

                    @else
                    <li <?php echo (Request::url() == URL::route('homePage') ? 'class="active"' : null); ?>><a href="{!!URL::route('homePage')!!}">Home</a></li>
                    @endif
                    <li><a <?php echo (Request::url() == URL::route('searchPage') ? 'class="active"' : null); ?> href="{!!URL::route('searchPage')!!}">{!!Lang::get('translator.search')!!}</a></li>
                    <li><a <?php echo (Request::url() == URL::route('matchesPage') ? 'class="active"' : null); ?> href="{!!URL::route('matchesPage')!!}">{!!Lang::get('translator.matches')!!}</a></li>
                    <li><a <?php echo (Request::url() == URL::route('dailytenPage') ? 'class="active"' : null); ?> href="{!!URL::route('dailytenPage')!!}">{!!Lang::get('translator.daily10')!!}</a></li>
                    <li><a <?php echo (Request::url() == URL::route('friendsPage') ? 'class="active"' : null); ?> href="{!!URL::route('friendsPage')!!}">{!!Lang::get('translator.Friends')!!}</a></li>

                    @if(Common::hasPermissions(Auth::id(), Common::$permissions_show_inbox))
                    <li><a {!!(Request::url() == URL::route('inboxPage') || Request::url() == URL::route('messagePage') ? 'class="active"' : null)!!} href="{!!URL::route('inboxPage')!!}">{!!Lang::get('translator.inbox')!!}</a></li>
                    @endif

                    @if(Common::hasPermissions(Auth::id(), Common::$permissions_showWhoisOnline))
                    <li>
                        <a 
                            class="border-none <?php echo (Request::url() == URL::route('onlineUsers') ? 'active' : null); ?>"
                            href="<?php echo URL::route('onlineUsers'); ?>" >
                            {!!Lang::get('translator.who_is_online')!!}
                        </a>
                    </li>
                    @else
                    <li>
                        <a class="<?php echo (Request::url() == URL::route('onlineUsers') ? 'active' : null); ?>" data-toggle="modal" data-target="#plz-upgrade-popup" data-title="Ooops! This feature is only enabled for premium users!" data-body="Upgrade now and get many features and find your future partner easily!" href="#" >
                            {!!Lang::get('translator.who_is_online')!!}
                        </a>
                    </li>
                    @endif
                    
                    <li style="padding-left: 5px;"><a class="border-none" href="tel:0775097948"><i class="fa fa-phone"></i> 0775097948</a></li>
                    <li class="visible-xs"><a href="{!!URL::route('doLogout')!!}">{!!trans('translator.Logout')!!}</a></li>

                    

                </ul>
                <ul class="nav navbar-nav menu-icon navbar-right">
                    <?php
                    if (Auth::check()) {
                        $imgchk = (isset($_GET['id']) ? ProfileController::getProfileImageUrl(true, $_GET['id']) : ProfileController::getProfileImageUrl());
                        $user_id = (isset($_GET['id']) ? $_GET['id'] : Auth::user()->id);
                        ?>
                        @if(!ProfileController::userHasProfilePic())
                        <li><a href="{!!URL::route('uploadPage')!!}" data-toggle="tooltip" data-placement="top" title="{!!trans('translator.PendingPhoto')!!}">  <span class="photo-pending danger "><span class="icon-question" /></a></li>

                                        @endif    
                                    <?php } ?>
                                    @if(Auth::check())
                                    <?php $freq_list = FriendController::list_requests(); ?>
                                    @include('layouts.navRightIcons')
                                    @endif
                                    </ul>


                                    <!-- navbar-collapse -->
                                    </div>
                                    </div><!-- /.container-fluid -->
                                    </nav>
                                    </div>
                                    <script>
                                        $(function () {
                                            $('[data-toggle="tooltip"]').tooltip();
                                        });
                                    </script>
                                    @if(Session::has('global'))   
                                    <div class="container bodycontainer">
                                        <div class="alert {!!Session::get('alert-type')!!} fade in" role="alert">
                                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                                            <strong>{!!trans('translator.'.Session::get('global'))!!}</strong>
                                        </div>
                                    </div>
                                    @endif