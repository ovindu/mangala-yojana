<?php

$userid = 149582; 
if(isset($user->user_id)){ 
	$userid = $user->user_id; 
}else if(isset($user->id)){
        $userid = $user->id;
}
$income = $user->an_income;
$auth = Auth::check();
$profilepicture = ProfileController::getProfileImageUrl(true, $userid, 'square');
$lastonline = $ProfileController->getLastOnlineText($userid);
Common::setPrivacyinBorn();
?>
<div id="{!!$userid!!}" gender="{!!$user->gender!!}" income="@if($income===1 || $income===2 || $income===3){!!'basic'!!}@elseif($income===4 || $income===5 || $income===6)
{!!'middle'!!}@elseif($income===7 || $income===8 || $income===9){!!'high'!!}@else{!!'highest'!!}@endif" 
     joined="{!!$searchController->minus_time($userid, 1)!!}" activeIn="{!!$searchController->minus_time($userid, 2)!!}" marital="{!!$user->marital_status!!}"  class="@if($friendController->isLiked($userid)){!!"liked-profile"!!}@endif results @if(str_contains($profilepicture, "default-avatar")){!!"noImage allImage"!!}@else {!!"UserImage allImage"!!} @endif" id='aResult'>
     <div class="row">
        <div id="searchResultName">
            <h4><a href="{!!URL::route("publicProfile",array('id'=>$userid))!!}">{!!Common::getUserPublicName($userid)!!}</a></h4>
            <span>
                <div class="@if(isset($state)){!!"online-icon"!!}@else{!!"online-text"!!}@endif">
                     @if(!isset($state)){!!trans('translator.LastOnline')!!} {!!$lastonline!!}@endif</div></span>
        </div>
    </div>
    <div class="row">
        <div id="searchResultBody" >
            <div class="container-fluid " id="" >

                <div class="col-lg-4 col-md-4 col-sm-4" id="">
                    <div id="searchResultThumbnail">
                        <a href="{!!URL::route("publicProfile",array('id'=>$userid))!!}" >
                        @if(!$auth)   
                        {!!HTML::image($profilepicture,null,array('class'=>'img-responsive blur'))!!}
                        @else
                        {!!HTML::image($profilepicture,null,array('class'=>'img-responsive '))!!}
                        @endif
                        </a>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8" id="" >
                    <div id="searchResultContent">
                        <div class="container-fluid">
                            <div class="row" id="searchcontentRow">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <?php $cityArray = $Form->get_cities();
                                    $cityID = $user->city;
                                    ?>
                                    <div class="row"><span class="myLabel">{!!trans('translator.Age')!!}</span></div>
                                    <div class="row"><span class="myLabel">{!!trans('translator.Height')!!}</span></div>
                                    <div class="row"> <span class="myLabel">{!!trans('translator.Religion')!!}</span></div>
                                    @if(Common::canAccessbyPrivacy($userid, 'location')) <div class="row"><span class="myLabel">{!!trans('translator.CurrentCity')!!}</span></div> @endif
                                    <div class="row"><span class="myLabel">{!!trans('translator.Profile_Created_by')!!}</span> </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="row">: {!!$user->age."yrs"!!}</div>
                                    <div class="row">: @if(isset($user->height)) {!!FormDetails::get_heightList($notGiven)[$user->height]!!} @else {!!"Not Given"!!}@endif</div>
                                    <div class="row">: {!!FormDetails::get_religion($notGiven)[$user->religion]!!}</div>
                                    @if(Common::canAccessbyPrivacy($userid, 'location')) <div class="row">: @if(isset($cityID) && $cityID!=0) {!!Common::getCity($cityID)!!} @else {!!"Not given"!!}@endif</div> @endif
                                    <div class="row">: {!!ProfileController::get_ProfileBy($user->profilefor)!!}</div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row @if(!Auth::check()){!!"hidden"!!}@endif">
                        <div class="container-fluid">
                            <div class="col-lg-12 " >
                                <div id="buttonArea">
                                    <h4><?php echo ($user->gender == 'm' ? trans('translator.likeHisProfile') : trans('translator.likeHerProfile')); ?></h4>
                                    <button  id="btn-{!!$userid!!}" class="@if($friendController->isLiked($userid)){!!'liked'!!}@else{!!'like'!!}@endif searchResult-btn yes 
                                             " data-like-user-id='{!!$userid!!}' liked_id='{!!$friendController->isLiked($userid)!!}'><span class="glyphicon glyphicon-heart"></span></button>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
