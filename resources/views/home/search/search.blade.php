@extends('layouts.master')
@section('title')
    @if(App::getLocale() == 'en')
        {!!Lang::get('translator.title' , array('page' => 'Search'))!!}
    @else
        {!!Lang::get('translator.title' , array('page' => 'සෙවුම්'))!!}
    @endif
@stop
@section('content')
    <?php
    use \App\Library\FormDetails;
    use \App\Library\Common;
    use \App\Library\Salutation;
    use \App\Models\User;
    use \App\Models\Searchuser;
    use \App\Http\Controllers\FriendController;
    use \App\Http\Controllers\ProfileController;
    use \App\Http\Controllers\PrivacyController;
    ?>
    @include('layouts.nav')
    <?php
    $notGiven = '<em>Not Given</em>';
    $ProfileController = new \App\Http\Controllers\ProfileController();
    $searchController = new \App\Http\Controllers\SearchController();
    $Form = new \App\Library\FormDetails;
    ?>
    <div class="container-fluid menucontainer">
        <div class="row">
            <div class="searchResultBar">

                <div class="text-left">
                    <span id="SearchHead">{!!trans('translator.MPartnerSearch')!!}</span>
                </div>
                <div class="text-right">
                    <span id='SearchCriteria'>{!!trans('translator.PartnerSearchTip')!!}</span>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    @if(isset($global))

        {!!"
        <script>
            $(document).ready(function() {
                $('#advancesearchLi').addClass('active');
                $('#basicsearch').removeClass('active in');
                activaTab('advancesearch');
            });
        </script>
        "!!}
    @endif

    <script>
        $(document).on('click', '.switchAS', function () {
            $('#advancesearchLi').addClass('active');
            $('#basicsearch').removeClass('active in');
            $('#whoisonlinetabhead').removeClass('active in');
        });

    </script>
    <div class="container bodycontainer">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 pull-right">
                <div id="searchRefine">
                    <div class="container-fluid">
                        <div class="sideWidget">
                            <div class="row">
                                <div id='searchesHead'>
                                    {!!trans('translator.GoToProfileID')!!}
                                </div>
                            </div>
                            <div class="row">
                                <div id='searchesContent'>
                                    <div class="input-group">
                                        <form id="goto-profile" action="{!!URL::route('gotoProfile')!!}" method="post">
                                            {{ csrf_field() }}
                                            <input id="profileIdBox" name="goid" type="text"
                                                   class="form-control searchProfileID"
                                                   placeholder="{!!trans('translator.EnterProfileID')!!}"
                                                   onkeypress="return IsNumeric(event);" ondrop="return false;"
                                                   onpaste="return false;">
                                            <span class="input-group-btn">
                                            <button class="btn btn-go"
                                                    type="submit">{!!trans('translator.Go')!!}</button>
                                        </span>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sideWidget">
                            <div class="row">
                                <div id='searchesHead'>
                                    {!!trans('translator.RandomProfiles')!!}
                                </div>
                            </div>
                            <div class="row">
                                <div id='searchesContent'>


                                    <?php
                                    $random_matches = Common::getBroaderMatches();
                                    if($random_matches != false){
                                    foreach ($random_matches as $user) {
                                    $id = $user->id;
                                    ?>
                                    <p>
                                        <a href="{!!URL::route('publicProfile', array('id' => $id, 'ref' => 'broader matches'))!!}">{!!HTML::image(\App\Http\Controllers\ProfileController::getProfileImageUrl(true, $id, 'thumb'), null, array('style' => 'height:30px;'))!!} {!!\App\Http\Controllers\ProfileController::get_ProfileName($id)!!}</a>
                                    </p>
                                    <?php } } ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 pull-left">
                <div class="container-fluid">

                    <div class="row">
                        <ul id="myTab" class="nav nav-tabs" role="tablist">
                            <li role="presentation"
                                class=" in col-lg-6 col-md-6 col-sm-6 col-xs-6 active searchTab-head ">
                                <a href="#basicsearch" class="disable_tab " id="basicsearch-tab" role="tab"
                                   data-toggle="tab" aria-controls="basicsearch"
                                   aria-expanded="true">{!!trans('translator.BasicSearch')!!}</a></li>
                            <li role="presentation" class="col-lg-6 col-md-6 col-sm-6 col-xs-6 searchTab-head "
                                id="advancesearchLi"><a href="#advancesearch" role="tab" class="disable_tab"
                                                        id="profile-tab" data-toggle="tab" aria-controls="advancesearch"
                                                        aria-expanded="false">{!!trans('translator.AdvancedSearch')!!}</a>
                            </li>

                        </ul>
                    </div>

                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel"
                             class="active basicsearch tab-pane fade{!!!isset($state) ? ' active in' : ''!!}"
                             id="basicsearch" aria-labelledby="basicsearch-tab">
                            <div class="row">
                                <div class="searchContent">
                                    <div id="basicSearchs">
                                        <form method="get" action="{!!URL::route('searchResults')!!}">
                                            {{ csrf_field() }}
                                            {!!Form::token()!!}
                                            <div class="row frmMat_row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 fmlyDetails_bodyText ">{!!trans('translator.Age')!!}</div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                                    <select name="ageStart" class="agefrom fmlyDetails_txtbox">
                                                        @for($i=18;$i<=75;$i++)
                                                            <option value="{!!$i!!}" <?php echo(Input::old('ageStart') == $i ? 'selected' : '') ?>>{!!$i!!}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 smallcol fmlyDetails_bodyText">
                                                    {!!trans('translator.To')!!}
                                                </div>
                                                <div class="col-lg-3 col-md-3  col-sm-3 col-xs-5">
                                                    <select name="ageEnd" class="ageto fmlyDetails_txtbox">
                                                        @for($i=75;$i>=18;$i--)
                                                            <option value="{!!$i!!}" <?php echo(Input::old('ageEnd') == $i ? 'selected' : '') ?>>{!!$i!!}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row frmMat_row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 fmlyDetails_bodyText ">{!!trans('translator.Height')!!}</div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                                    <select name="heightStart" class="fmlyDetails_txtbox">
                                                        @foreach(FormDetails::get_heightList() as $key=>$val)
                                                            <option value="{!!$key!!}" <?php echo(Input::old('heightStart' == $key ? 'selected' : '')) ?>>{!!$val!!}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 smallcol fmlyDetails_bodyText">
                                                    {!!trans('translator.To')!!}
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                                    <select name="heightEnd" class="fmlyDetails_txtbox">
                                                        <?php $iter = 0; $count = count(FormDetails::get_heightList()); ?>
                                                        @foreach(FormDetails::get_heightList() as $key=>$val)
                                                            <?php $iter++; ?>
                                                            @if(Session::has('heightEnd'))
                                                                <option value="{!!$key!!}" <?php echo(Input::old('heightEnd' == $key) ? 'selected' : '') ?>>{!!$val!!}</option>
                                                            @else
                                                                <option value="{!!$key!!}" <?php echo($count == $iter ? 'selected' : '') ?>>{!!$val!!}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row frmMat_row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText ">{!!trans('translator.MaritalStatus')!!}</div>
                                                <div class="col-lg-8 col-md-8 col-xs-12 col-sm-8">
                                                    {!!Form::select('maritalStatus[]',FormDetails::get_maritalStatus(NULL,TRUE),Input::old('maritalStatus'),array('class'=>'fmlyDetails_txtbox chosen-select','data-placeholder'=>trans('translator.DoesentMatter'),'multiple','style'=>'width: 100% !important;'))!!}
                                                </div>
                                            </div>
                                            <div class="row frmMat_row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText ">{!!trans('translator.Religion')!!}</div>
                                                <div class="col-lg-8 col-md-8 col-xs-12 col-sm-8">
                                                    {!!Form::select('religion[]',FormDetails::get_religion(),Input::old('religion'),array('class'=>'fmlyDetails_txtbox chosen-select','data-placeholder'=>trans('translator.DoesentMatter'),'multiple'=>'multiple','style'=>'width: 100% !important;'))!!}
                                                </div>
                                            </div>
                                            <div class="row frmMat_row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText ">{!!trans('translator.MotherTongue')!!}</div>
                                                <div class="col-lg-8 col-md-8 col-xs-12 col-sm-8">
                                                    {!!Form::select('language[]',FormDetails::get_language(),Input::old('language'),array('class'=>'fmlyDetails_txtbox chosen-select','data-placeholder'=>trans('translator.DoesentMatter'),'multiple'=>'multiple','style'=>'width: 100% !important;'))!!}
                                                </div>
                                            </div>
                                            <div class="row frmMat_row">
                                                <div class="col-lg-offset-4 col-lg-3 col-md-3 col-offset-4 col-sm-3 col-sm-offset-4 col-xs-4 col-xs-offset-4">
                                                    <input type="button" class="formSub btn-SaveSearch" id="formSub"
                                                           value="{!!trans('translator.search')!!}">
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script>
                            $(function () {
                                $('.advancesearch').removeClass('active');
                                $('.advancesearch').removeClass('in');
                            });
                        </script>

                    @if(isset($state))
                        {!!"<script>
                            $(function() {
                                $('.basicsearch').removeClass('active');
                            });
                        </script>"!!}
                    @endif
                    <!--Advance search-->
                        <div role="tabpanel" class="advancesearch tab-pane fade active in" id="advancesearch"
                             aria-labelledby="advancesearch-tab">
                            <div class="row">
                                <div class="searchContent">
                                    <div id="">
                                        <form method="get" action="{!!URL::route('refinesearch2')!!}">
                                            {{ csrf_field() }}
                                            {!!Form::token()!!}
                                            <div class="row frmMat_row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 fmlyDetails_bodyText ">{!!trans('translator.Age')!!}</div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                                    <select name="agesAS" class="agefrom fmlyDetails_txtbox
                                                        {!!isset($global) ? "validateError" : ''!!}">
                                                        @for($i=18;$i<=75;$i++)
                                                            <option value="{!!$i!!}" <?php echo(Input::old('agesAS') == $i ? 'selected' : '') ?>>{!!$i!!}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 smallcol fmlyDetails_bodyText">
                                                    to
                                                </div>
                                                <div class="col-lg-3 col-md-3  col-sm-3 col-xs-5">
                                                    <select name="ageendAS"
                                                            class="ageto fmlyDetails_txtbox {!!isset($global) ? "validateError" : ""!!}">
                                                        @for($i=75;$i>=18;$i--)
                                                            <option value="{!!$i!!}" <?php echo(Input::old('ageendAS') == $i ? 'selected' : '') ?>>{!!$i!!}</option>
                                                        @endfor
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row frmMat_row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 fmlyDetails_bodyText ">{!!trans('translator.Height')!!}</div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                                    <select name="heightS" class="fmlyDetails_txtbox">
                                                        @foreach(FormDetails::get_heightList() as $key=>$val)
                                                            <option value="{!!$key!!}" <?php echo(Input::old('heightStart' == $key ? 'selected' : '')) ?>>{!!$val!!}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 smallcol fmlyDetails_bodyText">
                                                    {!!trans('translator.To')!!}
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5">
                                                    <select name="heightE" class="fmlyDetails_txtbox">
                                                        <?php $iter = 0; $count = count(FormDetails::get_heightList()); ?>
                                                        @foreach(FormDetails::get_heightList() as $key=>$val)
                                                            <?php $iter++; ?>
                                                            @if(Session::has('heightEnd'))
                                                                <option value="{!!$key!!}" <?php echo(Input::old('heightE' == $key) ? 'selected' : '') ?>>{!!$val!!}</option>
                                                            @else
                                                                <option value="{!!$key!!}" <?php echo($count == $iter ? 'selected' : '') ?>>{!!$val!!}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row frmMat_row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText ">{!!trans('translator.MaritalStatus')!!}</div>
                                                <div class="col-lg-8 col-md-8 col-xs-12 col-sm-8">
                                                    {!!Form::select('maritalStatus_ads[]',FormDetails::get_maritalStatus(NULL,TRUE),Input::old('maritalStatus'),array('class'=>'fmlyDetails_txtbox chosen-select','data-placeholder'=>trans('translator.DoesentMatter'),'multiple','style'=>'width: 100% !important;'))!!}
                                                </div>
                                            </div>
                                            <div class="row frmMat_row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText">{!!trans('translator.Religion')!!}</div>
                                                <div class="col-lg-8 col-md-8 col-xs-12 col-sm-8">
                                                    {!!Form::select('religion_ads[]',FormDetails::get_religion(),Input::old('religion'),array('class'=>'fmlyDetails_txtbox chosen-select','data-placeholder'=>trans('translator.DoesentMatter'),'multiple'=>'multiple','style'=>'width: 100% !important;'))!!}
                                                </div>
                                            </div>
                                            <div class="row frmMat_row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText ">{!!trans('translator.MotherTongue')!!}</div>
                                                <div class="col-lg-8 col-md-8 col-xs-12 col-sm-8">
                                                    {!!Form::select('language[]_ads',FormDetails::get_language(),Input::old('language'),array('class'=>'fmlyDetails_txtbox chosen-select','data-placeholder'=>trans('translator.DoesentMatter'),'multiple'=>'multiple','style'=>'width: 100% !important;'))!!}
                                                </div>
                                            </div>
                                            <div class="row frmMat_row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText ">{!!trans('translator.RecentlyJoined')!!}</div>
                                                <div class="col-lg-8 col-md-8 col-xs-12 col-sm-8">
                                                    {!!Form::select('joined[]_ads',FormDetails::getRecentlyJoined(),Input::old('language'),array('class'=>'fmlyDetails_txtbox chosen-select','data-placeholder'=>trans('translator.DoesentMatter'),'multiple'=>'multiple','style'=>'width: 100% !important;'))!!}
                                                </div>
                                            </div>

                                            <div class="row frmMat_row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText ">{!!trans('translator.ActiveMembers')!!}</div>
                                                <div class="col-lg-8 col-md-8 col-xs-12 col-sm-8">
                                                    {!!Form::select('active[]_ads',FormDetails::getActiveMembers(),Input::old('language'),array('class'=>'fmlyDetails_txtbox chosen-select','data-placeholder'=>trans('translator.DoesentMatter'),'multiple'=>'multiple','style'=>'width: 100% !important;'))!!}
                                                </div>
                                            </div>

                                            <div class="row frmMat_row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText ">{!!trans('translator.AnnualIncome')!!}</div>
                                                <div class="col-lg-8 col-md-8 col-xs-12 col-sm-8">
                                                    {!!Form::select('income[]_ads',array("All","Don't want to...","upto $100k","$100k to $200k","$200k or Above"),Input::old('language'),array('class'=>'fmlyDetails_txtbox chosen-select','data-placeholder'=>trans('translator.DoesentMatter'),'multiple'=>'multiple','style'=>'width: 100% !important;'))!!}
                                                </div>
                                            </div>

                                            <div class="row frmMat_row">
                                                <div class="col-lg-offset-4 col-lg-3 col-md-3 col-offset-4 col-sm-3 col-sm-offset-4 col-xs-4 col-xs-offset-4">
                                                    <input type="button" class="btn-SaveSearch formSub" id="formSub"
                                                           value="{!!trans('translator.search')!!}">
                                                </div>
                                            </div>
                                        {!!Form::close()!!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end advance search-->
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var config = {
            '.chosen-select': {},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
        $(function () {
            $('#goto-profile-textbox').keyup(function (e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    $('#goto-profile-textbox').val("");
                } else {
                    $('#goto-profile').attr('action', '<?php echo URL::route('publicProfile'); ?>?id=' + $('#goto-profile-textbox').val());
                }
            });
        });
    </script>
@stop