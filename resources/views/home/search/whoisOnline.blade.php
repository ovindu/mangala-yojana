@extends('layouts.master')
@section('title')
@if(App::getLocale() == 'en')
{!!Lang::get('translator.title' , array('page' => 'Who is online'))!!}
@else
{!!Lang::get('translator.title' , array('page' => 'මාර්ගගත පරිශීලකයින්'))!!}
@endif
@stop
@section('content')
@if(Auth::check())
@include ('layouts.nav')
@endif
<?php
$notGiven = '<em>Not Given</em>';
$ProfileController = new ProfileController();
$searchController = new SearchController();
$friendController = new FriendController();
$Form = new FormDetails;
?>

<div class="container-fluid menucontainer" >
    <div class="row">
        <div class="searchResultBar">

            <div class="text-left">
                <span id="SearchHead">{!!trans('translator.NowOnlineUsers')!!}</span>
            </div>
            <div class="text-right">
                <span id='SearchCriteria'>@if(isset($data)) {!!count($data)!!} @else {!!"0"!!} @endif {!!trans('translator.userCountOnline')!!}.
                </span>
            </div>
            <div class="clearfix"></div>
        </div>     
    </div>
</div>
<div class="container bodycontainer">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 pull-right">
            <div id="searchRefine">
                <div class="container-fluid">
                    <div class="row">
                        <div id='refineSearchHeading'>
                            <h4 class="text-center">{!!trans('translator.RefineUsers')!!}</h4>
                        </div>
                    </div>
                    {!!Form::open(array('url'=>'refine','id'=>'refinesearch'))!!}



                    <div class="row">
                        <div id='refineSearchSubHeading'>
                            {!!trans('translator.PhotoSetting')!!}
                        </div>
                    </div>
                    <div class="row">
                        <div id="refineSearchContent">
                            <div class="col-lg-12">
                                <input type="radio"
                                       class="imageSettings" checked="true" name='photoSetting' id='all' value="all">
                                <label for='all' class="cursor-pointer ">{!!trans('translator.All')!!}</label>
                            </div> 

                            <div class="col-lg-12">
                                <input type="radio"
                                       class="imageSettings" name='photoSetting' id='userImage' value="1">
                                <label for='userImage' class="cursor-pointer ">{!!trans('translator.WithPhoto')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input type="radio" class="imageSettings" id='noImage' name='photoSetting' value="0">
                                <label for='noImage' class="cursor-pointer ">{!!trans('translator.WithoutPhoto')!!}</label>
                            </div> 
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <!--Photo Setting  --> 

                    <!--Recently Joined-->
                    <div class="row">
                        <div id='refineSearchSubHeading'>
                            {!!trans('translator.RecentlyJoined')!!}
                        </div>
                    </div>
                    <div class="row">
                        <div id="refineSearchContent">
                            <div class="col-lg-12">
                                <input type="radio" id='recentyljoinedAll' value="*" name='recentyljoined' maker="all" class="resJoined" checked="@if(Input::old('recentyljoined')==="*") {!!"true"!!} @endif">
                                       <label class="cursor-pointer" for='recentyljoinedAll'>{!!trans('translator.All')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input type="radio" class="resJoined" maker="day"
                                <?php
                                if (Input::old('recentyljoined') == -1) {
                                    echo 'checked="' . 'true"';
                                }
                                ?>
                                       name='recentyljoined' id='recentyljoinedWithinaDay' value="-1">
                                <label class="cursor-pointer" for='recentyljoinedWithinaDay'  class="cursor-pointer">{!!trans('translator.Withinday')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input type="radio" class="resJoined" maker="week"
                                <?php
                                if (Input::old('recentyljoined') == -7) {
                                    echo 'checked="' . 'true"';
                                }
                                ?>
                                       name='recentyljoined' id='recentyljoinedWithinaWeek' value="-7">
                                <label class="cursor-pointer" for='recentyljoinedWithinaWeek'  class="cursor-pointer">{!!trans('translator.Withinweek')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input type="radio" 
                                <?php
                                if (Input::old('recentyljoined') == -30) {
                                    echo 'checked="' . 'true"';
                                }
                                ?>
                                       class="resJoined" name='recentyljoined' maker="month" id='recentyljoinedWithinaMonth' value="-30">
                                <label class="cursor-pointer" for='recentyljoinedWithinaMonth'  class="cursor-pointer">{!!trans('translator.Withinmonth')!!}</label>
                            </div> 

                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <!--Recently Joined-->

                    <div class="row">
                        <div id='refineSearchSubHeading'>
                            {!!trans('translator.ActiveMembers')!!}
                        </div>
                    </div>
                    <div class="row">
                        <div id="refineSearchContent">
                            <div class="col-lg-12">
                                <input
                                <?php
                                if (Input::old('activemembers') == -10000) {
                                    echo 'checked="' . 'true"';
                                }
                                ?> 
                                    type="radio" maker="all" checked="true" class="resActive" name='activemembers' id='activemembersAll' value="-10000">
                                <label class="cursor-pointer" for='activemembersAll'>{!!trans('translator.All')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input type="radio"
                                <?php
                                if (Input::old('activemembers') == -1) {
                                    echo 'checked="' . 'true"';
                                }
                                ?> 
                                       class="resActive" maker="day" name='activemembers' id='activemembersWithinaDay' value="-1">
                                <label class="cursor-pointer" for='activemembersWithinaDay'>{!!trans('translator.Withinday')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input
                                <?php
                                if (Input::old('activemembers') == -7) {
                                    echo 'checked="' . 'true"';
                                }
                                ?> 
                                    type="radio" maker="week" class="resActive" name='activemembers' id='activemembersWithinaWeek' value="-7">
                                <label class="cursor-pointer" for='activemembersWithinaWeek'>{!!trans('translator.Withinweek')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input 
                                <?php
                                if (Input::old('activemembers') == -30) {
                                    echo 'checked="' . 'true"';
                                }
                                ?> 
                                    type="radio" maker="month" class="resActive" name='activemembers' id='activemembersWithinaMonth' value="-30">
                                <label class="cursor-pointer" for='activemembersWithinaMonth'>{!!trans('translator.Withinmonth')!!}</label>
                            </div> 

                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div id='refineSearchSubHeading'>
                            {!!trans('translator.AnnualIncome')!!}
                        </div>
                    </div>
                    <div class="row">
                        <div id="refineSearchContent">
                            <div class="col-lg-12">
                                <input type="radio" class="resIncome" name='annualincome' maker="all" id='noneed' value="1,12">
                                <label class="cursor-pointer" for='noneed'>{!!trans('translator.All')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input type="radio" class="resIncome" maker="basic" name='annualincome' id='annualincome' value="1,11">
                                <label class="cursor-pointer" for='annualincome'>{!!trans('translator.Dontwantto')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input type="radio" class="resIncome" maker="middle" name='annualincome' id='50to100' value="1,4">
                                <label class="cursor-pointer" for='50to100'>{!!trans('translator.Upto100K')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input type="radio" class="resIncome" maker="high" name='annualincome' id='100to200' value="4,8">
                                <label class="cursor-pointer" for='100to200'>{!!trans('translator.Upto100Kto200K')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input type="radio" class="resIncome" name='annualincome' maker="highest" id='200to1000' value="8,11">
                                <label class="cursor-pointer" for='200to1000'>{!!trans('translator.200KAbove')!!}</label>
                            </div> 
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div id='refineSearchSubHeading'>
                           {!!trans('translator.MaritalStatus')!!} 
                        </div>
                    </div>
                    <div class="row">
                        <div id="refineSearchContent">
                            <div class="col-lg-12">
                                <input type="radio" class="marital" maker="1" name='mstatus' id="single" value="1">
                                <label class="cursor-pointer" for='single'>{!!trans('translator.Single')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input type="radio" class="marital" maker="2" name='mstatus' id="widowed" value="2">
                                <label class="cursor-pointer" for='widowed'>{!!trans('translator.Widowed')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input type="radio" class="marital" maker="3" name='mstatus' id="maritalstatus" value="3">
                                <label class="cursor-pointer" for='maritalstatus'>{!!trans('translator.Divorced')!!}</label>
                            </div> 
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!!Form::close()!!}
        <script>

            $('.submitForm').click(function() {
                var value = $('#hiddenValues').attr('value');
                if (value === 'm' | value === 'f') {
                    $('#refinesearch').submit();
                }
            });

        </script>


        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 pull-left">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6 col-md-4 col-sm-4 col-xs-2" style="padding-left: 0px;">
                        <button class="SearchResult-btn text favorite"><i class="icon-heart"></i><span>{!!trans('translator.Interested')!!}</span></button>
                    </div>
                </div>
                <div class="row">
                    <div class="spotlight ">
                        <h4>{!!trans('translator.Spotlight')!!}</h4>
                        <a href="#"><span>{!!trans('translator.featureProfile')!!}.</span></a> 
                    </div>
                </div>
                @foreach($data as $user)
                @include('home.search.searchBody')
                @endforeach
                {!!$data->appends(Input::all())->links();!!}

                @if(isset($empty) && $empty == 1)
                <div class="row">
                    <div class="spotlight">
                        <div class="center position-search-empty">
                            <span>{!!trans('translator.noMatchesOnline')!!} <a class="link" href="{!!URL::route('searchPage')!!}">{!!trans('translator.BasicAndAdvancedSearch')!!}.</a></span> 
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>

@stop