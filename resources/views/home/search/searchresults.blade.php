@extends('layouts.master')
@section('title')
Search Results | MangalaYojana.LK
@stop
@section('content')
@if(Auth::check())
@include ('layouts.nav')
@endif
<?php
$notGiven = '<em>Not Given</em>';
$ProfileController = new ProfileController();
$searchController = new SearchController();
$friendController = new FriendController();
$Form = new FormDetails;
$auth = Auth::check();
?>

<div class="container-fluid menucontainer" >
    <div class="row">
        <div class="searchResultBar">

            <div class="text-left">
                <span id="SearchHead">{!!trans('translator.yourSeachResults')!!}</span>
            </div>
            <div class="text-right">
                <span id='SearchCriteria'>@if(isset($profileCount)) {!!$profileCount!!} @elseif(Session::has('profileCount')) {!!Session::get('profileCount') !!} @else {!!"0"!!} @endif {!!trans('translator.profilesFound')!!}
                    <a @if(!Auth::check()) class="{!!'cursor-not-allow link disabled'!!}" @endif id="modifySearch " href="<?php echo asset("search"); ?>">{!!trans('translator.modifySearch')!!}</a>

                </span>
            </div>
            <div class="clearfix"></div>
        </div>     
    </div>
</div>
<div class="container bodycontainer">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 pull-right">
            <div id="searchRefine">
                <div class="container-fluid">
                    <div class="row">
                        <div id='refineSearchHeading'>
                            <h4 class="text-center">{!!trans('translator.refineSearch')!!}</h4>
                        </div>
                    </div>
                    {!!Form::open(array('url'=>'refine','id'=>'refinesearch'))!!}

                    <!--last searched values-->
                    <div>
                        <input type="hidden" value="<?php
                        if (isset($gender)) {
                            echo $gender;
                        } else if (Session::has('gender')) {
                            echo Session::get('gender');
                        } else {
                            echo Input::old('gender');
                        }
                        ?>" name="gender" id="hiddenValues">

                        <input type="hidden" value="<?php
                        if (isset($agestart)) {
                            echo $agestart;
                        } else if (Session::has('agestart')) {
                            echo Session::get('agestart');
                        } else {
                            echo Input::old('agestart');
                        }
                        ?>" name="agestart" >

                        <input type="hidden" value="<?php
                        if (isset($ageend)) {
                            echo $ageend;
                        } else if (Session::has('ageend')) {
                            echo Session::get('ageend');
                        } else {
                            echo Input::old('ageend');
                        }
                        ?>" name="ageend" >

                        <input type="hidden" value="<?php
                        if (isset($mt)) {
                            echo $mt;
                        } else if (Session::has('mt')) {
                            echo Session::get('mt');
                        } else {
                            echo Input::old('mt');
                        }
                        ?>" name="mt" >

                        <input type="hidden" value="<?php
                        if (isset($relig)) {
                            echo $relig;
                        } else if (Session::has('relig')) {
                            echo Session::get('relig');
                        } else {
                            echo Input::old('relig');
                        }
                        ?>" name="relig">
                    </div>
                    <!--last searched values-->

                    <div class="row">
                        <div id='refineSearchSubHeading'>
                            {!!trans('translator.PhotoSetting')!!}
                        </div>
                    </div>
                    <div class="row">
                        <div id="refineSearchContent">
                            <div class="col-lg-12">
                                <input type="radio"
                                       class="imageSettings" checked="true" name='photoSetting' id='all' value="all">
                                <label for='all' class="cursor-pointer ">{!!trans('translator.All')!!}</label>
                            </div> 

                            <div class="col-lg-12">
                                <input type="radio"
                                       class="imageSettings" name='photoSetting' id='userImage' value="1">
                                <label for='userImage' class="cursor-pointer ">{!!trans('translator.WithPhoto')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input type="radio" class="imageSettings" id='noImage' name='photoSetting' value="0">
                                <label for='noImage' class="cursor-pointer ">{!!trans('translator.WithoutPhoto')!!}</label>
                            </div> 
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <!--Photo Setting  --> 

                    <!--Recently Joined-->
                    <div class="row">
                        <div id='refineSearchSubHeading'>
                            {!!trans('translator.RecentlyJoined')!!}
                        </div>
                    </div>
                    <div class="row">
                        <div id="refineSearchContent">
                            <div class="col-lg-12">
                                <input id="recentyljoinedAll" value="*" type="radio" name='recentyljoined' maker="all" class="resJoined" checked="@if(Input::old('recentyljoined')==="*") {!!"true"!!} @endif">
                                       <label for='recentyljoinedAll'>{!!trans('translator.All')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input type="radio" class="resJoined" maker="day"
                                <?php
                                if (Input::old('recentyljoined') == -1) {
                                    echo 'checked="' . 'true"';
                                }
                                ?>
                                       name='recentyljoined' id='recentyljoinedWithinaDay' value="-1">
                                <label for='recentyljoinedWithinaDay' class="cursor-pointer" >{!!trans('translator.Withinday')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input type="radio" class="resJoined" maker="week"
                                <?php
                                if (Input::old('recentyljoined') == -7) {
                                    echo 'checked="' . 'true"';
                                }
                                ?>
                                       name='recentyljoined' id='recentyljoinedWithinaWeek' value="-7">
                                <label for='recentyljoinedWithinaWeek'  class="cursor-pointer">{!!trans('translator.Withinweek')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input type="radio" 
                                <?php
                                if (Input::old('recentyljoined') == -30) {
                                    echo 'checked="' . 'true"';
                                }
                                ?>
                                       class="resJoined" name='recentyljoined' maker="month" id='recentyljoinedWithinaMonth' value="-30">
                                <label for='recentyljoinedWithinaMonth'  class="cursor-pointer">{!!trans('translator.Withinmonth')!!}</label>
                            </div> 

                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <!--Recently Joined-->

                    <div class="row">
                        <div id='refineSearchSubHeading'>
                            {!!trans('translator.ActiveMembers')!!}
                        </div>
                    </div>
                    <div class="row">
                        <div id="refineSearchContent">
                            <div class="col-lg-12">
                                <input
                                <?php
                                if (Input::old('activemembers') == -10000) {
                                    echo 'checked="' . 'true"';
                                }
                                ?> 
                                    type="radio" maker="all" checked="true" class="resActive cursor-pointer" name='activemembers' id='activemembersAll' value="-10000">
                                <label class="cursor-pointer" for='activemembersAll'>{!!trans('translator.All')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input type="radio" class='cursor-pointer'
                                <?php
                                if (Input::old('activemembers') == -1) {
                                    echo 'checked="' . 'true"';
                                }
                                ?> 
                                       class="resActive cursor-pointer" maker="day" name='activemembers' id='activemembersWithinaDay' value="-1">
                                <label class="cursor-pointer" for='activemembersWithinaDay'>{!!trans('translator.Withinday')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input class="cursor-pointer"
                                <?php
                                if (Input::old('activemembers') == -7) {
                                    echo 'checked="' . 'true"';
                                }
                                ?> 
                                    type="radio" maker="week" class="resActive cursor-pointer" name='activemembers' id='activemembersWithinaWeek' value="-7">
                                <label class="cursor-pointer" for='activemembersWithinaWeek'>{!!trans('translator.Withinweek')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input 
                                <?php
                                if (Input::old('activemembers') == -30) {
                                    echo 'checked="' . 'true"';
                                }
                                ?> 
                                    type="radio" maker="month" class="resActive" name='activemembers' id='activemembersWithinaMonth' value="-30">
                                <label class="cursor-pointer" for='activemembersWithinaMonth'>{!!trans('translator.Withinmonth')!!}</label>
                            </div> 

                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div id='refineSearchSubHeading'>
                            {!!trans('translator.AnnualIncome')!!}
                        </div>
                    </div>
                    <div class="row">
                        <div id="refineSearchContent">
                            <div class="col-lg-12">
                                <input type="radio" class="resIncome" name='annualincome' maker="all" id='noneed' value="1,12">
                                <label class="cursor-pointer"
                                       for='noneed'>{!!trans('translator.All')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input type="radio" class="cursor-pointer resIncome" maker="basic" name='annualincome' id='annualincome' value="1,11">
                                <label class="cursor-pointer" for='annualincome'>{!!trans('translator.Dontwantto')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input type="radio" class="resIncome" maker="middle" name='annualincome' id='50to100' value="1,4">
                                <label class="cursor-pointer" for='50to100'>{!!trans('translator.Upto100K')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input type="radio" class="resIncome" maker="high" name='annualincome' id='100to200' value="4,8">
                                <label class="cursor-pointer" for='100to200'>{!!trans('translator.Upto100Kto200K')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input class="cursor-pointer" type="radio" class="resIncome" name='annualincome' maker="highest" id='200to1000' value="8,11">
                                <label class="cursor-pointer" for='200to1000'>{!!trans('translator.200KAbove')!!}</label>
                            </div> 
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div id='refineSearchSubHeading'>
                            {!!trans('translator.MaritalStatus')!!}
                        </div>
                    </div>
                    <div class="row">
                        <div id="refineSearchContent">
                            <div class="col-lg-12">
                                <input class="cursor-pointer marital" type="radio" class="marital" maker="all" name='mstatus' id="all_ms" value="1">
                                <label class="cursor-pointer" for='all_ms'>{!!trans('translator.All')!!}</label>
                            </div>
                            <div class="col-lg-12">
                                <input class="cursor-pointer marital" type="radio" class="marital" maker="1" name='mstatus' id="single" value="1">
                                <label class="cursor-pointer" for='single'>{!!trans('translator.Single')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input class="cursor-pointer marital" type="radio" class="" maker="2" name='mstatus' id="widowed" value="2">
                                <label class="cursor-pointer" for='widowed'>{!!trans('translator.Widowed')!!}</label>
                            </div> 
                            <div class="col-lg-12">
                                <input type="radio" class="cursor-pointer marital" maker="3" name='mstatus' id="maritalstatus" value="3">
                                <label class="cursor-pointer" for='maritalstatus'>{!!trans('translator.Divorced')!!}</label>
                            </div> 
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!!Form::close()!!}
        <script>

            $('.submitForm').click(function () {
                var value = $('#hiddenValues').attr('value');
                if (value === 'm' | value === 'f') {
                    $('#refinesearch').submit();
                }
            });

        </script>


        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 pull-left">
            <div class="container-fluid">
                @if(Auth::check())
                <div class="row ">
                    <div class="col-lg-6 col-md-4 col-sm-4 col-xs-2" style="padding-left: 0px;">
                        <button class="SearchResult-btn text favorite" data-bool="0"><i class="icon-heart"></i><span>{!!trans('translator.IamInterested')!!}</span></button>
                    </div>
                </div>
                @endif
                <div class="row">
                    <div class="spotlight @if(!Auth::check()) {!!'margin-top-zero'!!} @endif">
                        <h4>{!!trans('translator.Spotlight')!!}</h4>
                        <span><a href="{!!URL::route("upgradePage")!!}">{!!trans('translator.featureProfile')!!}</a></span> 
                    </div>
                </div>

                @if(isset($data) | Session::has('data'))
                <?php
                if (Session::has('data'))
                    $data = Session::get('data');
                $userCheck = Auth::check();
                if ($userCheck)
                    $blockedUsers = $searchController->getBlockedUsers();
                ?>
                @foreach($data as $user)
                @if ($userCheck) 
<?php $userid = $user->id; ?>

                @if(empty($blockedUsers))
                @foreach($blockedUsers as $blockedUser)
                @if($blockedUser->id != $userid)
                @include('home.search.searchBody')
                @endif    
                @endforeach

                @else
                @include('home.search.searchBody')
                @endif

                @else
                @include('home.search.searchBody')
                @endif
                @endforeach
                @endif
                <div id="searchPaginationLinks">
                {!!$data->appends(Input::all())->render();!!}
                </div>
                @if(count($data) <= 0)
                <div class="row">
                    <div class="spotlight">
                        <div class="center position-search-empty">
                            <div id="no-search-dislike-icon" class="center glyphicon icon-dislike"></div>
                            <span>{!!trans('translator.noSearchResults')!!}
                                <a class="link" href="@if(str_contains(URL::current(),'searchmatches')) {!!URL::route('homePage').'/#search-partner'!!} @else {!!URL::route('searchPage')!!} @endif">{!!trans('translator.ClickToSearchAgarin')!!}</a>
                            </span>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>

    </div>
</div>

@stop