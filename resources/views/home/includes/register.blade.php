<style>
    #frmSignUp .frmtexts,
    #frmSignUp .frmdate {
        padding: 10px;
        font-size: 14px;
        border-radius: 5px;
    }

    #frmSignUp .frmRow label.error {
        color: #ff0000 !important;
    }
</style>

{!!Form::token()!!}
<div class="container-fluid ">
    <div class="row frmRow">
        <div class="col-lg-6">
            <strong>{!!trans('translator.Email')!!}</strong> <br>
            <input type="email" name='email' class="frmtexts @if ($errors->has('email')) validateError @endif"
                placeholder="username@example.com" value='{!!Input::old(' email')!!}' required>
        </div>
        <div class="col-lg-6">
            <strong>{!!trans('translator.Password')!!}</strong> <br>
            <input type="password" name='password' class="frmtexts @if($errors->has('password'))validateError @endif"
                placeholder="Password" required>
            @if($errors->has('password'))
            <span class="error-span">{!!trans('translator.PasswordInvalidMessage')!!}</span>
            @endif
        </div>
    </div>

    <div class="row frmRow">
        <div class="col-lg-8">
            <strong>{!!trans('translator.Name')!!}</strong> <br>
            <div class="row frmRow">
                <div class="col-lg-6">
                    <input type="text" class="frmnametexts frmtexts  @if ($errors->has('fname'))validateError @endif"
                        value="<?php echo Input::old('fname') ?>" name='fname'
                        placeholder="{!!trans('translator.FName')!!}" required>

                </div>
                <div class="col-lg-6">
                    <input type="text" class="frmnametexts frmtexts  @if ($errors->has('lname'))validateError @endif"
                        value="<?php echo Input::old('lname') ?>" name='lname'
                        placeholder="{!!trans('translator.LName')!!}" required>

                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <strong>{!!trans('translator.ProfileFor')!!}</strong> <br>
            <?php
                                        $old_profilefor = Input::old('profilefor');
                                        $profilefor_array = $homeForm->get_profileFor();
                                        ?>
            <select class="frmtexts  @if ($errors->has('profilefor')) validateError @endif" name='profilefor' required>
                <option value="" <?php echo empty($old_profilefor) ? 'selected' : ''; ?>>
                    {!!trans('translator.Select')!!}</option>
                @foreach($profilefor_array as $key => $val)
                <option value='{!! $key !!}!!}' {!! $old_profilefor==$key ? 'selected' : '' !!}>
                    {!!$val!!}</option>
                @endforeach
            </select>
        </div>
    </div>

    <hr>

    <div class="row frmRow" style="margin-bottom: 8px; margin-top: 8px;  ">
        <div class="col-lg-8">
            <strong>{!!trans('translator.BDay')!!}</strong> <br>
            <?php $olddobdate = Input::old('dobdate'); ?>
            <select class="frmdate  @if ($errors->has('dobdate'))validateError @endif" name='dobdate' required>
                <option value="">{!!Lang::choice('translator.DayOne', 1)!!}</option>
                @for($d =1 ; $d <32; $d++) <option value="{!!sprintf(" %02s", $d)!!}" {!! $olddobdate==$d ? 'selected'
                    : '' !!}>{!!sprintf("%02s", $d)!!}</option>
                    @endfor
            </select>
            <?php
                                        $dobmonth_array = $homeForm->get_months();
                                        $olddobmonth = Input::old('dobmonth');
                                        ?>

            <select class="frmdate @if ($errors->has('dobmonth'))validateError @endif" name='dobmonth' required>
                <option value="">{!!Lang::choice('translator.MonthOne', 1)!!}</option>
                @foreach($dobmonth_array as $key => $val)
                <option value='{!! $key !!}!!}' {!! $olddobmonth==$key ? 'selected' : '' !!}>{!!$val!!}
                </option>
                @endforeach
            </select>


            <?php $olddobyear = Input::old('dobyear'); ?>
            <select class='frmdate @if ($errors->has(' dobyear'))validateError @endif' name='dobyear'
                style='float:right;' required>
                <option value="">{!!Lang::choice('translator.YearOne', 1)!!}</option>
                @for($tt = date('Y') - Common::$minimumAgeLimit; $tt >= 1934; $tt-- )
                <option value='{!!$tt!!}' {!! $olddobyear==$tt ? 'selected' : '' !!}>{!!$tt!!}
                </option>
                @endfor
            </select>
        </div>
        <div class="col-lg-4 col-xs-12">
            <strong>{!!trans('translator.Gender')!!}</strong> <br> @if($errors->has('gender'))
            <span class="errorIcon icon-alert"></span> @endif
            <?php $oldgender = Input::old('gender'); ?>
            <div class="row frmRow">
                <div class="col-lg-4 col-xs-6"> <input type="radio" <?php echo ($oldgender == 'm' ? 'checked' : '') ?>
                        id="genMale" value="m" name="gender" checked> <label class="optionLabel"
                        for="genMale">{!!trans('translator.Male')!!}</label>
                </div>
                <div class="col-lg-4 col-xs-6"> <input type="radio" <?php echo ($oldgender == 'f' ? 'checked' : '') ?>
                        id="genFemale" value="f" name="gender">
                    <label class="optionLabel" for="genFemale">
                        {!!trans('translator.FeMale')!!}</label></div>
            </div>
        </div>

    </div>
    <hr>

    <div class="row frmRow">
        <div class="col-lg-4">
            <strong>{!!trans('translator.Religion')!!}</strong> <br>
            <?php
                                    $oldreligion = Input::old('religion');
                                    $religionArray = $homeForm->get_religion();
                                    ?>
            <select class="frmtexts    @if ($errors->has('religion')) validateError @endif" name='religion' required>
                <option value="">{!!trans('translator.Select')!!}</option>
                @foreach($religionArray as $key => $val)
                <option value='{!! $key !!}!!}' {!! $oldreligion==$key ? 'selected' : '' !!}>{!!$val!!}
                </option>
                @endforeach
            </select>
        </div>
        <div class="col-lg-4">
            <strong>{!!trans('translator.MotherTongue')!!}</strong> <br>
            <?php
                $oldlang = Input::old('language');
                $langArray = $homeForm->get_language();
                ?>
            <select class="frmtexts @if ($errors->has('language')) validateError @endif" name='language' required>
                <option value="">{!!trans('translator.Select')!!}</option>
                @foreach($langArray as $keylang => $valang)
                <option value='{!! $key !!}lang!!}' {!! $oldlang==$keylang ? 'selected' : '' !!}>
                    {!!$valang!!}</option>
                @endforeach
            </select>
        </div>
        <div class="col-lg-4">
            <strong>{!!trans('translator.LivesIn')!!}</strong> <br>
            <select class="frmtexts" name='country' required>
                @foreach( FormDetails::getCountries() as $cntry => $country)
                <option @if($country['code']=="LK" ) {!!"selected='true'"!!} @endif value=" {!!$country['code']!!}"
                    data-d_code="{!!$country['d_code']!!}">
                    {!!$country['name']!!}</option>
                @endforeach
            </select>
        </div>
    </div>
    <hr>
    <div class="row frmRow">
        <div class="col-lg-6">
            <strong>{!!trans('translator.nic')!!}</strong> <br>
            <?php $nic = Input::old('nic'); ?>
            <input type="text" class="frmtexts @if ($errors->has('nic'))validateError @endif" name='nic' id="nic"
                style="width:100%" placeholder="" value="{!!Input::old('nic')!!}" ondrop="return false;"
                onpaste="return false;" required>
            @if ($errors->has('nic'))<span class='formError'>{!!
                $errors->first('nic') !!}</span>@endif
            <span class="help-block mangalayojana-help-block">{!!trans('translator.nicHint')!!}</span>
        </div>
        <div class="col-lg-6">
            <strong>{!!trans('translator.MobileNo')!!}</strong> <br>
            <?php $mobileNumber = Input::old('mobilenumber'); ?>
            <input type="text" class="frmtexts @if ($errors->has('mobilenumber'))validateError @endif"
                name='mobilenumber' id="number" style="width:100%" placeholder=""
                value="{!!Input::old('mobilenumber')!!}" onkeypress="return IsNumeric(event);" ondrop="return false;"
                onpaste="return false;" required>
            @if ($errors->has('mobilenumber'))<span class='formError'>{!!
                $errors->first('mobilenumber') !!}</span>@endif
            <span class="help-block mangalayojana-help-block">{!!trans('translator.mobileNumberHint')!!}</span>
        </div>
    </div>
</div>