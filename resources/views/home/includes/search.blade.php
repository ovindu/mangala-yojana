<style>
    .age_col .row {
        /* padding-: 1px; */
    }

    #age_col .col-md-5,
    #age_col .col-md-1 {
        padding: 1px 15px;
    }
</style>
{!!Form::open(array('class'=>'frmHome frmSearch form-inline','url'=>'searchmatches', 'id' => 'searchform'))!!}

<div class="row frmRow">
    <div class="col-sm-4 col-xs-12">
        <label class="optionLabel cursor-pointer" for="lookingfor">{!!trans('translator.LookingFor')!!}</label><br>
        <select class="frmtexts cursor-pointer <?php if ($errors->has("lookingfor")) echo ' validateError ' ?>"
            name="lookingfor" required>
            <option class="option" value="f">{!!trans('translator.Bride')!!}</option>
            <option class="option" value="f">{!! trans('translator.Groom')!!}</option>
        </select>
    </div>

    <div class="col-sm-5 col-xs-12" id="age_col">
        <label class="optionLabel cursor-pointer" for="searchLabel">{!!trans('translator.Age')!!}</label><br>
        {{-- <span id="searchLabel" class="searchLabel">{!!trans('translator.Age')!!}</span> --}}
        <div class="row frmRow">
            <div class="col-sm-5 col-xs-5">
                <select class="frmtexts <?php if ($errors->has("ages")) echo ' validateError '; ?> " name="ages"
                    id="agefrom" required>
                    @for($i=18; $i<81; $i++) <option>{!!$i!!}</option>
                        @endfor
                </select>
            </div>
            <div class="col-sm-2 col-xs-2 text-center" style="padding-top: 5px;">
                <span id="to">{!!trans('translator.To')!!}</span>
            </div>
            <div class="col-sm-5 col-xs-5">
                <select class="frmtexts <?php if ($errors->has("ageto")) echo ' validateError '; ?> " name="ageto"
                    id="ageto" required>
                    @for($i=81; $i>=18; $i--)
                    <option>{!!$i!!}</option>
                    @endfor
                </select>
            </div>
        </div>
    </div>
    <div class="col-sm-3 col-xs-12">
        <label class="optionLabel cursor-pointer" for="motherTongue">{!!trans('translator.MotherTongue')!!}</label><br>
        <select class="frmtexts <?php if ($errors->has("mt")) echo ' validateError '; ?>  " name="mt" id="motherTongue" required>
            <?php
                                        $oldlang = Input::old('mt');
                                        $langArray = $homeForm->get_language();
                                        ?>
            <option value="">{!!trans('translator.Select')!!}</option>
            @foreach($langArray as $keylang => $valang)
            <option value='{!! $keylang !!}lang!!}' {!! $oldlang==$keylang ? 'selected' : '' !!}>{!!$valang!!}
            </option>
            @endforeach

        </select>
    </div>
    <div class="col-sm-4 col-xs-12">
        <label class="optionLabel cursor-pointer" for="Country">{!!trans('translator.Country')!!}</label><br>
        <?php
                        $oldc = Input::old("country");
                        $countryAll = $homeForm->get_country();
                        ?>
        <select class="frmtexts <?php if ($errors->has("country")) echo ' validateError '; ?> " name="country"
            id="country" required>
            <option value="">{!!trans('translator.Select')!!}</option>
            @foreach($countryAll as $country => $varCountry)
            <option value="{!!$country!!}">{!!$varCountry!!}</option>
            @endforeach
        </select>
    </div>
    <div class="col-sm-4 col-xs-12">
        <label class="optionLabel cursor-pointer" for="religion">{!!trans('translator.Religion')!!}</label><br>
        <?php
                        $oldRelig = Input::old("relig");
                        $allRelig = $homeForm->get_religion();
                        ?>
        <select class="frmtexts <?php if ($errors->has("relig")) echo ' validateError '; ?>  " name="relig"
            id="religion" required>
            <option value="">{!!trans('translator.Select')!!}</option>
            @foreach($allRelig as $religion => $varRelig)
            <option value="{!!$religion!!}">{!!$varRelig!!}</option>
            @endforeach
        </select>
    </div>
    <div class="col-sm-4 col-xs-12" style="vertical-align: baseline">
        {{-- <input type='checkbox' id='profilewithphoto' value="true" checked="yes" name="photo"> --}}
        {{-- <label for='profilewithphoto' class="smalltext">{!!trans('translator.OnlyProfileWithPhoto')!!}</label> --}}
        <input type="submit" class="btn-Search" style="width: 100%; padding: 10px; height: auto!important; margin-top: 28px;" id="formBtn" value="{!!trans('translator.search')!!}">
    </div>
</div>
{!!Form::close()!!}