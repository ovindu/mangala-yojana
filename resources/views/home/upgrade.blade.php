@extends('layouts.master')
@section('title')
@if(App::getLocale() == 'en')
{!!Lang::get('translator.title' , array('page' => 'Upgrade'))!!}
@else
{!!Lang::get('translator.title' , array('page' => 'ඉහළ ශ්‍රේණියට'))!!}
@endif
@stop
@section('content')
<div class="container bodycontainer" id="upgrade">
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <div id='upgrade-heading'>   
                {!!trans('translator.SwitchtoPremium')!!}
            </div>
            <div id="upgrade-sub-heading">
                {!!trans('translator.andgetExcitingBenefits!')!!}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="pkg" id="free-package">
                            <div class="row">
                                <div class="pkg-head">
                                    <div class='pkg-name'>{!!trans('translator.Basic')!!}</div>
                                    <div class='pkg-price'>
                                        <div class="pkg-amount" style="@if(App::getLocale() == 'si') {!!'font-size: 44px;'!!} @else {!!'font-size: 50px;'!!} @endif">{!!trans('translator.Free')!!}</div> 
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="pkg-body">
                                    <ul>
                                        <li><span class="glyphicon glyphicon-heart bullets" ></span> <span class="liText">{!!trans('translator.LimitedProposing')!!}</span></li>
                                        <li><span class="glyphicon glyphicon-heart bullets" ></span> <span class="liText">{!!trans('translator.LimitedMessaging')!!}</span></li>
                                        <li><span class="glyphicon glyphicon-heart bullets" ></span> <span class="liText">{!!trans('translator.LimitedSupport')!!}</span></li>
                                        <li><span class="glyphicon glyphicon-heart bullets" ></span> <span class="liText">{!!trans('translator.LimitedPrivacy')!!}</span></li>

                                    </ul>
                                    @if(Auth::check())
                                    <a href="{!!URL::route('profilePage')!!}" class="btn-upgrade" id="sign-up">{!!trans('translator.Continue')!!}</a>
                                    @else
                                    <a href="{!!URL::route('homePage')!!}" class="btn-upgrade" id="sign-up">{!!trans('translator.SignUp')!!}</a>
                                    @endif
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="pkg">
                            <div class="row">
                                <div class="pkg-head">
                                    <div class='pkg-name'>{!!trans('translator.PREMIUM')!!}</div>
                                    <div class='pkg-price'>
                                        <div class="pkg-amount">{!!Common::getCommonData('premium_package_price_LKR')!!}</div> 
                                        <div class="pkg-currency">{!!trans('translator.LKR')!!}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="pkg-body">
                                    <ul>
                                        <li><span class="glyphicon glyphicon-heart bullets" ></span> <span class="liText">{!!trans('translator.SendUnlimitedProposals')!!}</span></li>
                                        <li><span class="glyphicon glyphicon-heart bullets" ></span> <span class="liText">{!!trans('translator.InstantMessaging')!!}</span></li>
                                        <li><span class="glyphicon glyphicon-heart bullets" ></span> <span class="liText">{!!trans('translator.ImprovedPrivacy')!!}</span></li>
                                        <li><span class="glyphicon glyphicon-heart bullets" ></span> <span class="liText">{!!trans('translator.DiscountsfromOurPartnersmore')!!}</span></li>

                                    </ul>
                                    <a href="#" data-toggle="modal" data-target="#upgrade-model">
                                        <button class="btn-upgrade">{!!trans('translator.Upgrade')!!}</button>
                                    </a>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div id="paymentmethods">
    <div class="container" id="paymentmethod-container">
        <div class="row">
            @for($z =1; $z <7; $z++)
            <div class="col-md-2 col-sm-4 col-xs-6 text-center">
                {!!HTML::image("img/payment-methods/$z.png",null,array('class'=>'','draggable'=>'false'))!!}
            </div>
            @endfor
        </div>
    </div>
</div>
@include('templates.popups.upgradePopup')
@stop
