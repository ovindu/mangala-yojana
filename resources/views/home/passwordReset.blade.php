@extends('layouts.master')
@section('title')
Reset Password | MangalaYojana.LK
@stop
@section('content')

<div class="container bodycontainer mat_con" >
    <div class="row">
        <div class='col-lg-12 congBar'>
            <span class='congText'>Reset Password</span>
        </div>
    </div>
    <div class="row">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12"><h4 class="fmlyDetails_text">Reset your forgotten Password</h4></div>
            </div>
            {!!Form::open(array('url'=>URL::route('resetPassword')))!!}
            <input name="id" type="hidden" value="{!!Input::get('id')!!}"/>
            <div class="row frmMat_row">
                <div class="col-lg-2 col-md-2 fmlyDetails_bodyText">New Password</div>
                <div class="col-lg-4 col-md-4">
                    {!!Form::password('password-new',array('class' => 'fmlyDetails_txtbox new-password')); !!}
                </div>
                <div class="col-lg-6 fmlyDetails_bodyText new-pwd-message hidden"></div>
            </div>
            <div class="row frmMat_row confirm-pwd">
                <div class="col-lg-2 col-md-2 fmlyDetails_bodyText">Confirm Password</div>
                <div class="col-lg-4 col-md-4">
                    {!!Form::password('password-confirm',array('class' => 'fmlyDetails_txtbox confirm-pwd-text-box')); !!}
                </div>
                <div class="col-lg-6 fmlyDetails_bodyText confirm-pwd-message hidden"></div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row frmMat_row  btn-row">
            <div class="col-lg-2 col-md-2 fmlyDetails_bodyText"></div>
            <div class="col-lg-3 col-md-3">
                <button class="btn-details fmlyDetails_txtbox hidden">Submit</button>
                {!!Form::close()!!}
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(function () {
        $('.new-password').keyup(function () {
            checkShortPwd();
            showSubmit()
        });
        $('.new-password').blur(function () {
            checkShortPwd();
            showSubmit()
        });
        $('.confirm-pwd-text-box').keyup(function () {
            checkSame();
            showSubmit()
        });
        $('.confirm-pwd-text-box').blur(function () {
            checkSame();
            showSubmit()
        });

        function checkShortPwd() {
            if ($('.new-password').val().length <= 7) {
                $('.new-pwd-message').removeClass("hidden");
                $('.new-pwd-message').fadeIn();
                $('.new-pwd-message').css("color", "red");
                $('.new-pwd-message').html("Password must be lengthy than 8 characters.");
            } else {
                $('.new-pwd-message').fadeOut();
            }
        }

        function checkSame() {
            if ($('.confirm-pwd-text-box').val() !== $('.new-password').val()) {
                $('.confirm-pwd-message').removeClass("hidden");
                $('.confirm-pwd-message').fadeIn();
                $('.confirm-pwd-message').css("color", "red");
                $('.confirm-pwd-message').html("Not same with password.");
            } else {
                $('.confirm-pwd-message').fadeOut();
            }
        }
        function showSubmit() {
            if ($('.new-password').val().length >= 8 && $('.confirm-pwd-text-box').val() === $('.new-password').val()) {
                $('.btn-details').removeClass("hidden");
                $('.btn-details').fadeIn();
            } else {
                $('.btn-details').fadeOut();
            }
        }
    });
</script>
@stop