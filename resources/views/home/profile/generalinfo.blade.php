@extends('layouts.master')
@section('title')
Edit General Information | MangalaYojana.LK
@stop
@section('content')
<?php $homeForm = new FormDetails; ?>
<div class="container bodycontainer mat_con" >
    <div class="row">
        <div class='col-lg-12 congBar'>
            <span class='congText'>Edit your General Information</span><a href="{!!URL::route('profilePage')!!}"><span class="dolater">I'll do this later!</span></a>
        </div>
    </div>
    @if($errors->has())
    @foreach ($errors->all() as $error)
    <span class='formError'>{!! $error !!}</span>		
    @endforeach
    @endif 
    <div class="row">
        <form method="post" action="{!!URL::route('generalInfoPage')!!}">
            {!!Form::token()!!}

            <div class="frmMat fmlyDetails">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12"><h4 class="fmlyDetails_text">General Information</h4></div>
                    </div>
                    <div class="row frmMat_row">
                        <div class="col-lg-2 col-md-2 fmlyDetails_bodyText ">Name</div>
                        <div class="col-lg-4 col-md-4">
                            <input type="text" class="fmlyDetails_txtbox" value="<?php echo Input::old('fname') ?>" name="fname" placeholder="First Name">
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <input type="text" class="fmlyDetails_txtbox" value="<?php echo Input::old('lname') ?>" name='lname' placeholder="Last Name">
                        </div>
                    </div>
                    <div class="row frmMat_row">
                        <div class="col-lg-2 col-md-2 fmlyDetails_bodyText ">Date of Birth</div>
                        <div class="col-lg-2 col-md-2">
                            <?php $olddobdate = Input::old('dobdate'); ?>
                            <select class="fmlyDetails_txtbox" name='dobdate' >
                                <option>Date</option>
                                @for($d =1 ; $d <32; $d++)
                                <option value="{!!sprintf("%02s", $d)!!}" {!! $olddobdate == $d ? 'selected' : '' !!}>{!!sprintf("%02s", $d)!!}</option>
                                @endfor
                            </select>
                            @if ($errors->has('dobdate'))<span class='formError'>{!! $errors->first('dobdate') !!}</span>@endif
                            <?php
                            $dobmonth_array = $homeForm->get_months();
                            $olddobmonth = Input::old('dobmonth');
                            ?>
                        </div>
                        <div class="col-lg-2 col-lg-offset-1 col-md-2">
                            <select class="fmlyDetails_txtbox" name='dobmonth' >
                                <option  >Month</option>
                                @foreach($dobmonth_array as $key => $val)
                                <option value='{!!$key!!}' {!! $olddobmonth == $key ? 'selected' : '' !!}>{!!$val!!}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('dobmonth'))<span class='formError'>{!! $errors->first('dobmonth') !!}</span>@endif
                        </div>
                        <div class="col-lg-2 col-lg-offset-1 col-md-2">
                            <?php $olddobyear = Input::old('dobyear'); ?>
                            <select class='fmlyDetails_txtbox' name='dobyear' style='float:right;'>
                                <option>Year</option>
                                @for($tt = date('Y'); $tt >= 1950; $tt-- )
                                <option value='{!!$tt!!}' {!! $olddobyear == $tt ? 'selected' : '' !!}>{!!$tt!!}</option>
                                @endfor
                            </select>
                            @if ($errors->has('dobyear'))<span class='formError'>{!! $errors->first('dobyear') !!}</span>@endif
                        </div>
                    </div>
                    <div class="row frmMat_row">
                        <div class="col-lg-2 col-md-2 col-sm-2 fmlyDetails_bodyText ">Religion</div>
                        <div class="col-lg-4 col-md-4">
                            <?php
                            $oldreligion = Input::old('religion');
                            $religionArray = $homeForm->get_religion();
                            ?>
                            <select class="fmlyDetails_txtbox"  name='religion'>
                                <option >Select</option>
                                @foreach($religionArray as $key => $val)
                                <option value='{!!$key!!}' {!! $oldreligion == $key ? 'selected' : '' !!}>{!!$val!!}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('religion'))<span class='formError'>{!! $errors->first('religion') !!}</span>@endif
                        </div>
                    </div>
                    <div class="row frmMat_row">
                        <div class="col-lg-2 col-md-2 col-sm-2 fmlyDetails_bodyText ">Mother Tongue</div>
                        <div class="col-lg-4 col-md-4">
                            <?php
                            $oldlang = Input::old('language');
                            $langArray = $homeForm->get_language();
                            ?>
                            <select class="fmlyDetails_txtbox"  name='language'>
                                <option >Select</option>
                                @foreach($langArray as $keylang => $valang)
                                <option value='{!!$keylang!!}' {!! $oldlang == $key ? 'selected' : '' !!}>{!!$valang!!}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('language'))<span class='formError'>{!! $errors->first('language') !!}</span>@endif
                        </div>
                    </div>
                    <div class="row frmMat_row">
                        <div class="col-lg-2 col-md-2 col-sm-2 fmlyDetails_bodyText ">Mobile Number</div>
                        <div class="col-lg-4 col-md-4">
                            <input type="text" class="fmlyDetails_txtbox" name='mobilenumber' id="mobilenumber" style="width:100%" placeholder="+94" value="+94">
                            @if ($errors->has('mobilenumber'))<span class='formError'>{!! $errors->first('mobilenumber') !!}</span>@endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="MidDetails" style="margin-bottom: 10px;">
                <div class="container-fluid">

                    <div class = "row frmMat_row  btn-row">
                        <div class = "col-lg-2 col-md-2 fmlyDetails_bodyText"></div>
                        <div class = "col-lg-3 col-md-3">
                            <button class = "btn-details fmlyDetails_txtbox">Submit</button>
                        </div>
                        <div class = "col-lg-7 col-md-7"><a href = "{!!URL::route('profilePage')!!}"><span class = "dolater_blue">I'll do this later!</span></div>
                    </div>

                </div>
            </div>

            {!!Form::close()!!}
    </div>
</div>
@stop