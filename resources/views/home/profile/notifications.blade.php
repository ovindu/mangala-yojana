<?php
foreach ($freq_list as $frq):
    if (Common::isUserVerifiedByAdmin($frq->sender_id)){
     //continue;

       $picture = ProfileController::getProfileImageUrl(true, $frq->users->id, 'thumb');
       ?>
       <li>
        <div class="row freq-{!!$frq->users->id!!}">
            <div class="req col-sm-2">
                <div class="reqPic">
                    {!!HTML::image($picture, null, array('class' => 'img-responsive'))!!}
                </div> 

            </div>
            <div class="req col-sm-6">
                <a href="{!!URL::route('publicProfile')!!}?id={!!$frq->users->id!!}">
                    <span class="reqName">{!!ProfileController::get_ProfileName($frq->users->id, true)!!}</span><br/> 
                    <span class="reqTown">
                        @if(!empty($frq->profiles->city))
                        <?php $user = User::find($frq->users->id); ?>
                        Nearby {!!WorldCities::find($user->profiles->city)->city_name!!} 
                        @else
                        @if($frq->users->religion==3)
                        An @else A {!!FormDetails::get_religion('newbie')[$frq->users->religion]!!}
                        @endif
                        @endif
                    </span>
                </a>  
            </div>
            <div class="req col-sm-4">
                <a class="reqButton accept" data-user-id="{!!$frq->users->id!!}" href="#"><span class="glyphicon glyphicon-ok"></span></a>

                <a class="reqButton cancel" data-user-id="{!!$frq->users->id!!}" href="#"><span class="glyphicon glyphicon-remove"></span></a>
            </div>
        </div>
    </li>

    <?php } endforeach  ?>

    <li class="ifNo <?php echo ($freq_list->isEmpty() ? '' : 'hidden'); ?>">
        <div class="row">
            <div class="req col-sm-4 col-sm-offset-4" style="width: 150px;">
                {!!trans('translator.NoYojana')!!}
            </div>
        </div>
    </li>

    <script type="text/javascript">
        $(document).ready(function () {
           $('.accept').click(function () {
               var userid = $(this).attr('data-user-id');
               $.ajax({
                  url: "{!!URL::route('makeFriends')!!}",
                  dataType: "html",
                  data: {id: userid, 'public-prof-path': '{!!Request::url()!!}'}
              }).done(function () {
                  var req_count = parseInt($('#freq-count').html());
                  req_count--;
                  if (req_count == 0) {
                      $('#freq-count').hide();
                  }
                  $('#freq-count').html(req_count);
                  $('.freq-' + userid).slideUp();
              });
              var numItems = $('.accept').length;
              if (numItems === 1) {
                  $('.ifNo').removeClass('hidden');
              }
          });
           $('.cancel').click(function () {
               var userid = $(this).attr('data-user-id');
               $.ajax({
                  url: "{!!URL::route('cancelFRequest')!!}?cancel-id=" + userid + "&cancel-path={!!Request::url()!!}",
                  dataType: "html",
                  data: {'cancel-id': userid, 'cancel-path': '{!!Request::url()!!}'}
              }).done(function () {
                  $('.freq-' + userid).slideUp();
              });
              var numItems = $('.cancel').length;
              if (numItems === 1) {
                  $('.ifNo').removeClass('hidden');
              }
          });
       });
   </script>