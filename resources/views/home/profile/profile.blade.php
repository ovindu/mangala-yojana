@extends('layouts.master')

@section('title')
<?php
    use \App\Library\Common;
    use \App\Http\Controllers\FriendController;
    ?>

@if(Auth::check())
{!!Lang::get('translator.title' , array('page' => (FriendController::are_friends($theUser->id) || $theUser->id ==
Auth::id() ? Common::getUserPublicName($theUser->id,true) : Common::getUserPublicName($theUser->id,false)) ))!!}
@else
{!!trans('translator.MangalaYojanaUser')!!} || MangalaYojana.LK!!}
@endif
@stop
@section('content')
<?php
    use \App\Library\FormDetails;
    //use \App\Library\Common;
    use \App\Library\Salutation;
    use \App\Models\User;
    use \App\Models\Searchuser;
    ///use \App\Http\Controllers\FriendController;
    use \App\Http\Controllers\ProfileController;
    use \App\Http\Controllers\PrivacyController;
    ?>

@include ('layouts.nav')
@include('layouts.sessiondetails')

<?php
    $gender = ($theUser->gender == 'm' ? 'him' : 'her');
    $UserDetails = new FormDetails;
    $profileCtrl = new ProfileController;
    $friendController = new FriendController();
    $notGiven = '<em> ' . trans('translator.NotGiven') . '</em>';
    $noMatter = '<em>' . trans('translator.NoMatter') . '</em>';
    if(Auth::check()){
        $meAdvertisement = Auth::user()->profiletype == 2;
    }
    
    $theUserAdvertisement = $theUser->profiletype == 2;
    $pendingProfPic = @ProfileController::getProfileImageUrl(false, NULL, 'main', 'pending');
    $newValue = Common::getUserVerifyNewValue('profile picture');
    $profPic = $isSelf && !empty($newValue) ? asset($pendingProfPic) : $profPic;
    if ($isSelf) {
        $theUser->fname = Common::getUserVerifyNewValue('fname') ? Common::getUserVerifyNewValue('fname') : $theUser->fname;
        $theUser->lname = Common::getUserVerifyNewValue('lname') ? Common::getUserVerifyNewValue('lname') : $theUser->lname;
    }
    ?>
{!!\Session::get('verified')!!}
<div class="container bodycontainer">
    <input type="hidden" id="userid" value="{!!Input::get('id')!!}">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="container-fluid">
                <div class="row">
                    <div class="propic-thumbnail main-border">
                        <div class="propic-large">



                            @if(!strpos($profPic, 'default'))
                            <!--Set lightbox-->
                            <a class="example-image-link" href="{!!asset($profPic)!!}" data-lightbox="on-img-click"
                                data-title="Profile picture of {!!$theUser->fname . ' '. $theUser->lname!!}">
                                @endif
                                {!!HTML::image($profPic, null, array('class' =>
                                'img-responsive','draggable'=>'false'))!!}
                                @if(!strpos($profPic, 'default'))
                            </a>
                            <!--End lightbox-->
                            @endif

                        </div>

                        <div class="view-propic text-center">
                            @if(PrivacyController::allowedByPrivacy($theUser->id,Common::$PRIV_COL_PROFILE_PIC))
                            @if(!strpos($profPic, 'default'))
                            <a class="example-image-link" href="{!!asset($profPic)!!}" data-lightbox="openfrom-btn"
                                data-title="Profile pictures of {!!$theUser->fname . ' '. $theUser->lname!!}"><i
                                    class="glyphicon glyphicon-camera"></i> {!!trans('translator.ViewFullPhoto')!!}
                            </a>
                            |
                            @if($isSelf)
                            <a class="example-image-link" href="{!!URL::route('uploadPage')!!}"><i
                                    class="glyphicon glyphicon-pencil"></i> Edit
                            </a>
                            @endif
                            @else
                            {!!trans('translator.NoImage')!!}
                            <a class="example-image-link" href="{!!URL::route('uploadPage')!!}"><i
                                    class="glyphicon glyphicon-pencil"></i> Edit
                            </a>
                            @endif
                            @else
                            @if(!Auth::check())
                            <a href="{!!URL::route('homePage')!!}">{!!trans('translator.RegOrLoginToView')!!}</a>
                            @else
                            {!!trans('translator.NoImage')!!}
                            @endif
                            @endif
                        </div>
                    </div>
                    @if(!$isSelf && Auth::check())
                    <div class="block-report-button">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 btn-blckreport">
                            @if(Auth::check() && !$isSelf && $friends)
                            <a href="#" data-toggle="modal" data-target="#unfriend-confirmation">
                                @elseif(!$friends)
                                <a href="#" data-toggle="modal" data-target="#block-confirmation">
                                    @endif
                                    <button type="submit" id='block-report-button'>
                                        <input type="hidden" name="id" value="{!!Input::get('id')!!}">
                                        <span class="glyphicon glyphicon-cog "></span>
                                        @if(Auth::check() && !$isSelf && $friends)
                                        {!!trans('translator.Unfriend')!!}

                                        @elseif(!$friends)
                                        {!!trans('translator.Block')!!}
                                        @endif
                                    </button>
                                </a>
                            </a>
                        </div>

                        {!!Form::open(array('url'=>URL::route('block')))!!}
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 btn-blckreport"
                            style="padding-left:5px !important;">
                            <div class="btn-group btn-group-unfriend">
                                <a href="#" data-toggle="modal" data-target="#user-report-pop">
                                    <button type="button" class="btn btn-unfriend">
                                        <span class="glyphicon glyphicon-flag"></span>
                                        {!!trans('translator.Report')!!}
                                    </button>
                                </a>
                                {!!Form::close()!!}
                                @if($friends && !$isSelf)
                                <button type="button" class="btn btn-unfriend dropdown-toggle" data-toggle="dropdown"
                                    aria-expanded="false">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>

                                <ul class="dropdown-menu" role="menu">
                                    <input type="hidden" name="id" value="{!!Input::get('id')!!}" />
                                    <a href="#" data-toggle="modal" data-target="#block-confirmation">
                                        <li>
                                            <button type="submit"
                                                class="btn-blk">{!!trans('translator.Block')!!}</button>
                                        </li>
                                    </a>
                                </ul>
                                @endif
                            </div>
                        </div>


                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8">
            <div class="profile-main-shadow">
                <div class="main-border profile-main-out ">
                    <div class="container-fluid profile-main">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="profile-main-Name">
                                    @if(Common::canAccessbyPrivacy($theUser->id, Common::$PRIV_COL_PROF_NAME))
                                    {!!$theUser->fname!!} {!!$theUser->lname!!}
                                    <small class="profilemain-id"> ({!!$theUser->id!!})</small>
                                    @else
                                    {!!trans('translator.MangalaYojanaUser')!!}
                                    @endif
                                    @if(!$isSelf && Auth::check() && !$sameGender)
                                    <button id="btn-{!!$theUser->id!!}" submit="1"
                                        class="pull-right btn-fav {!!($friendController->isLiked($theUser->id)) ? 'liked' : 'like'!!} yes"
                                        data-like-user-id='{!!$theUser->id!!}'
                                        liked_id='{!!$friendController->isLiked($theUser->id)!!}'><span
                                            class="glyphicon glyphicon-heart"></span>
                                    </button>
                                    @endif
                                </h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="profile-status">{!!Lang::get('translator.Profile_Created_by')!!}
                                    {!!ProfileController::get_ProfileBy($theUser->profilefor)!!} @if(!$isSelf)
                                    || {!!trans('translator.LastOnline')!!} , {!!$lastonline!!} @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            @if(!$isSelf && Auth::check() && $friends)
                            <div class="col-lg-9 col-md-9 contactbuttonsContainer">
                                <div class="row">
                                    @if($friends || Common::hasPermissions(Auth::id() , Common::$permissions_messaging))
                                    @if($friends && Common::hasPermissions(Auth::id() , Common::$permissions_messaging)
                                    == false)
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" id='contactButtoncol'>
                                        <button id="contactButton" data-toggle="modal" data-target="#plz-upgrade-popup"
                                            data-title="Ooops! This feature is only enabled for premium users!"
                                            data-body="Upgrade now and get many features and find your future partner easily!">
                                            <span class="glyphicon glyphicon-envelope" id='glyphicon'></span>
                                            <span id="contactbtnText">{!!trans('translator.Message')!!}</span>
                                        </button>
                                    </div>
                                    @else
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" id='contactButtoncol'>
                                        <form action="{!!URL::Route('messagePage', array('userid' => $theUser->id))!!}">
                                            <button id="contactButton">
                                                <span class="glyphicon glyphicon-envelope" id='glyphicon'></span>
                                                <span id="contactbtnText">{!!trans('translator.Message')!!}</span>
                                            </button>
                                        </form>
                                    </div>
                                    @endif
                                    @endif
                                    @if($friends || Common::hasPermissions(Auth::id() , Common::$permissions_chat))
                                    @if(false)
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" id='contactButtoncol'>
                                        <button id="contactButton">
                                            <span class="glyphicon glyphicon-comment" id='glyphicon'></span>
                                            <span id="contactbtnText">{!!trans('translator.ChatNow')!!}</span>
                                        </button>
                                    </div>
                                    @endif
                                    @endif
                                    @if($friends || Common::hasPermissions(Auth::id() , Common::$permissions_call))
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" id='contactButtoncol'>
                                        <button id="contactButton" @if(Common::hasPermissions(Auth::id() ,
                                            Common::$permissions_chat)==false) data-toggle="modal"
                                            data-target="#plz-upgrade-popup"
                                            data-title="Ooops! This feature is only enabled for premium users!"
                                            data-body="Upgrade now and get many features and find your future partner easily!"
                                            @endif>
                                            <span class="glyphicon glyphicon-earphone" id='glyphicon'></span>
                                            <span id="contactbtnText">{!!trans('translator.CallSMS')!!}</span>
                                        </button>
                                    </div>
                                    @endif
                                </div>
                            </div>

                            @elseif(!$isSelf && Auth::check() && !$friends && !$sameGender && (!$meAdvertisement &&
                            !$theUserAdvertisement))
                            <div class="row">
                                <div class="container-fluid">
                                    <div class="col-lg-12">
                                        <h3 class="profile-reminder">
                                            @if(!$friends && !$requested)
                                            Interested?
                                            @elseif(!$friends && $requested && $IsSender)
                                            Request Sent.
                                            @elseif(!$friends && $requested && !$IsSender)
                                            Received a request.
                                            @endif
                                        </h3>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="container-fluid">
                                    <div class="col-lg-8 contactbuttonsContainer">
                                        @if(!$friends && !$requested)
                                        @if(Common::canSendProposal())
                                        <form id="form" action="{!!URL::route('beFriends')!!}" method="get">
                                            <input name="be-id" type="hidden" value="{!!Input::get('id')!!}" />
                                            <input name="be-path" type="hidden"
                                                value="{!!Request::url()!!}?id={!!Input::get('id')!!}" />
                                            <button submit="0"
                                                class="btn_pkg btn-reminder pull-left">{!!trans('translator.sendProposal')!!}</button>
                                        </form>
                                        @else
                                        <!--  show upgrade pop-->

                                        <button data-toggle="modal" data-target="#plz-upgrade-popup"
                                            data-title="{!!trans('translator.requestLimitReach')!!}"
                                            data-body="You have sent 5 proposals, And you can't send new proposals. For send unlimited proposals, Upgrade now!"
                                            submit="0"
                                            class="btn_pkg btn-reminder pull-left">{!!trans('translator.sendProposal')!!}</button>
                                        @endif

                                        @elseif(!$friends && $requested && $IsSender)
                                        <form action="{!!URL::route('cancelFRequest')!!}" method="get">
                                            <input name="cancel-id" type="hidden" value="{!!Input::get('id')!!}" />
                                            <input name="cancel-path" type="hidden"
                                                value="{!!Request::url()!!}?id={!!Input::get('id')!!}" />
                                            <button
                                                class="btn_pkg btn-cancel main-border pull-left">{!!trans('translator.CancelProposal')!!}</button>
                                        </form>

                                        @elseif(!$friends && $requested && !$IsSender)
                                        <form action="{!!URL::route('makeFriends')!!}" method="get">
                                            <input name="id" type="hidden" value="{!!Input::get('id')!!}" />
                                            <input name="public-prof-path" type="hidden"
                                                value="{!!Request::url()!!}?id={!!Input::get('id')!!}" />
                                            <button
                                                class="btn_pkg btn-reminder pull-left">{!!trans('translator.Accept')!!}</button>

                                        </form>

                                        @endif

                                        @if(!$friends && Common::hasPermissions(Auth::id() ,
                                        Common::$permissions_messaging))
                                        <form action="{!!URL::Route('messagePage', array('userid' => $theUser->id))!!}"
                                            method="get">
                                            <button
                                                class="btn_pkg btn-reminder pull-left">{!!trans('translator.Message')!!}</button>
                                        </form>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div id='infosummery1'>
                @include('templates.profile.infosummery',array('theUser'=>$theUser,'UserDetails'=>$UserDetails))
            </div>
            @if(Auth::check() && !Common::isPremiumUser())
            <div id="advert_div">
                @include('templates.profile.advert',array('theUser'=>$theUser,'UserDetails'=>$UserDetails))
            </div>
            @endif
            <div id="user-description-modal">
                @include('templates.popups.editdescript',array('theUser'=>$theUser,'UserDetails'=>$UserDetails))
            </div>

            <div id="user-report-model">
                @include('templates.popups.reportUser')
            </div>
            <div id="">

            </div>
        </div>
    </div>
    <div class="row btn-row profile-edit-container">
        <div class="col-lg-12">
            <div class="profile-info-body infoBox">
                <div class="col-lg-9 col-xs-9 col-sm-10">
                    <h3>
                        @if(Common::canAccessbyPrivacy($theUser->id, Common::$PRIV_COL_PROF_NAME))
                        {!!$theUser->fname!!} {!!trans('translator.is')!!}
                        @else
                        {!!trans('translator.MangalaYojanaUser')!!}
                        @endif
                    </h3>
                </div>
                <div class="col-lg-3">
                    @if($isSelf || Common::isAdmin())
                    <a class="pull-right profile-edit-btn profile-edit-catch" href="#" data-toggle="modal"
                        data-target="#user-description"><span class="glyphicon glyphicon-pencil"></span><span
                            class="hidden-xs">{!!trans('translator.Edit')!!}</span></a>
                    @endif
                </div>
                <div class="clearfix"></div>
                <p class="overflow-hidden">
                    @if(!empty($theUser->profiles->description))
                    {!!$theUser->profiles->description!!}
                    @else
                    @if($isSelf)
                    <em><a href="#" data-toggle="modal"
                            data-target="#user-description">{!!trans('translator.clickToWriteBio')!!}</a></em>
                    @else
                    {!!trans('translator.bitBusyAddsomethingAsap')!!}
                    @endif
                    @endif
                </p>
            </div>
        </div>
    </div>
    <div class="row btn-row">
        <!--Left Column-->
        <div class="col-lg-6 col-md-6">
            <div class="row">
                <div class="col-lg-12">
                    <div class="profile-info-body infoBox profile-edit-container">
                        <div class="row">
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-7">
                                <h3 id="infoBox-Title">{!!trans('translator.Basicinfo')!!}</h3>
                            </div>
                            <div class="col-lg-3 col-lg-offset-4 col-md-7 col-sm-7 col-xs-5">
                                @if($isSelf || Common::isAdmin())
                                @include('templates.popups.basicInfoEdit')
                                <a class="pull-right profile-edit-btn profile-edit-catch" href="#" data-toggle="modal"
                                    data-target="#basic-info-modal"><span
                                        class="glyphicon glyphicon-pencil"></span><span
                                        class="hidden-xs">{!!trans('translator.Edit')!!}</span></a>
                                @endif
                            </div>
                        </div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                    <span class="myLabel">
                                        {!!trans('translator.Age/Height')!!}
                                    </span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    <?php $searchUser = Searchuser::find($theUser->id); ?>
                                    : @if(!empty($searchUser->age)) {!!$searchUser->age!!} @else {!!$notGiven!!} @endif
                                    / {!!(!empty($searchUser->profile->height) ?
                                    FormDetails::get_heightList()[$searchUser->profile->height] : $notGiven )!!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.MaritalStatus')!!}
                                    </span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    @if(!empty($theUser->profiles->marital_status))
                                    : <?php
                                            $status = FormDetails::get_maritalStatus()[$theUser->profiles->marital_status];
                                            echo $status
                                            ?>
                                    @else
                                    : {!!$notGiven!!}
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.SkinTone')!!}
                                    </span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    @if(!empty($theUser->profiles->skin_tone))
                                    <?php $skinT = FormDetails::get_skinTone()[$theUser->profiles->skin_tone]; ?>
                                    : {!!"$skinT"!!}
                                    @else
                                    : {!!$notGiven!!}
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.Bodytype')!!} </span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    @if(!empty($theUser->profiles->body_type))
                                    <?php $bodyTp = FormDetails::get_bodyType()[$theUser->profiles->body_type]; ?>
                                    : {!!$bodyTp!!}
                                    @else
                                    : {!!$notGiven!!}
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.Diet')!!} </span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    @if(!empty($theUser->profiles->diet))
                                    <?php $diat = FormDetails::get_dietInfo('self')[$theUser->profiles->diet]; ?>
                                    : {!!"$diat"!!}
                                    @else
                                    : {!!$notGiven!!}
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.Drink')!!} </span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    @if(!empty($theUser->profiles->drink))
                                    <?php $d = FormDetails::get_drinkInfo()[$theUser->profiles->drink] ?>
                                    : {!!$d!!}
                                    @else
                                    : {!!$notGiven!!}
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.Smoke')!!} </span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    @if(!empty($theUser->profiles->smoke))
                                    <?php $smo = FormDetails::get_smokeInfo()[$theUser->profiles->smoke]; ?>
                                    : {!!$smo!!}
                                    @else
                                    : {!!$notGiven!!}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(!(Common::canAccessbyPrivacy($theUser->id, Common::$PRIV_COL_FAMILY_DETAILS) == false
                & Common::canAccessbyPrivacy($theUser->id, Common::$PRIV_COL_LOCATION) == false
                & Common::canAccessbyPrivacy($theUser->id, Common::$PRIV_COL_EDU_AND_PROF) == false ))
                @include('templates.profile.partnerPreferences')
                @endif

                @if(Common::canAccessbyPrivacy($theUser->id, Common::$PRIV_COL_CONTACT_INFO))
                <!--Edit this for contact info!-->
                <div class="col-lg-12 profile-edit-container">
                    <div class="profile-info-body infoBox">
                        <div class="row">
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                <h3 id="infoBox-Title">{!!trans('translator.ContactDetails')!!}</h3>
                            </div>
                            <div class="col-lg-3 col-lg-offset-4 col-md-7 col-sm-7 col-xs-5">
                                @if($isSelf || Common::isAdmin())
                                @include('templates.popups.contactDetailsEdit')
                                <a id="profile-location-edit" class="pull-right profile-edit-btn profile-edit-catch"
                                    href="#" data-toggle="modal" data-target="#contactinfo-modal"><span
                                        class="glyphicon glyphicon-pencil"></span><span
                                        class="hidden-xs">{!!trans('translator.Edit')!!}</span></a>
                                @endif
                            </div>
                        </div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.MobileNumber')!!} </span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">

                                    @if(strlen($theUser->mobilenumber) >= 10)
                                    : {!!$theUser->mobilenumber!!}
                                    @else
                                    : {!!$notGiven!!}
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.email')!!} </span>
                                </div>


                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    <span class="cursor-pointer" @if(!empty($theUser->email) && strlen($theUser->email)
                                        > 20 ) data-toggle="tooltip"
                                        data-placement="top" title="" data-original-title=" {!!$theUser->email!!}"
                                        @endif>
                                        @if(!empty($theUser->email))
                                        : {!!Common::getEmail($theUser->email)!!}
                                        @else
                                        : {!!$notGiven!!}
                                        @endif
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.facebook')!!} </span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    @if(!empty($theUser->facebook))
                                    : <a href="{!!$theUser->facebook!!}" class="link" target="_blank">Browse</a>
                                    @else
                                    : {!!$notGiven!!}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

            </div>
        </div>
        <!--Right Column-->
        <div class="col-lg-6 col-md-6 <?php if (!Auth::check()) echo 'remove-bt-left-margin' ?>">
            <div class="row">
                @if(Common::canAccessbyPrivacy($theUser->id, Common::$PRIV_COL_FAMILY_DETAILS) == false
                && Common::canAccessbyPrivacy($theUser->id, Common::$PRIV_COL_LOCATION) == false
                && Common::canAccessbyPrivacy($theUser->id, Common::$PRIV_COL_EDU_AND_PROF) == false )
                @include('templates.profile.partnerPreferences')
                @endif

                @if(Common::canAccessbyPrivacy($theUser->id, Common::$PRIV_COL_FAMILY_DETAILS))
                <div id="profile-family" class="col-lg-12">
                    <div class="profile-info-body profile-edit-container infoBox">
                        <div class="row">
                            <div class="col-lg-6 col-md-5 col-sm-5 col-xs-9">
                                <h3 id="infoBox-Title">{!!trans('translator.FamilyDetails')!!}</h3>
                            </div>
                            <div class="col-lg-3 col-lg-offset-3 col-md-7 col-sm-7 col-xs-3">
                                @if($isSelf || Common::isAdmin())
                                @include('templates.popups.familyDetailsEdit')
                                <a id="profile-family-edit" class="pull-right profile-edit-btn profile-edit-catch"
                                    href="#" data-toggle="modal" data-target="#family-details-modal"><span
                                        class="glyphicon glyphicon-pencil"></span><span
                                        class="hidden-xs">{!!trans('translator.Edit')!!}</span></a>
                                @endif
                            </div>
                        </div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.FathersStatus')!!}</span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    @if(!empty($theUser->profiles->fatherStatus))
                                    : {!!FormMatrimonial::get_fatherStatus()[$theUser->profiles->fatherStatus]!!}
                                    @else
                                    : {!!$notGiven!!}
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.MothersStatus')!!} </span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    @if(!empty($theUser->profiles->motherStatus))
                                    : {!!FormMatrimonial::get_motherStatus()[$theUser->profiles->motherStatus]!!}
                                    @else
                                    : {!!$notGiven!!}
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.FamilyValues')!!} </span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    @if(!empty($theUser->profiles->familyValue))
                                    : {!!FormMatrimonial::get_familyValue()[$theUser->profiles->familyValue]!!}
                                    @else
                                    : {!!$notGiven!!}
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.NoofBrothers')!!}</span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    : {!!$theUser->profiles->brotherCount!!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.MarriedBrothers')!!}</span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    : {!!$theUser->profiles->brotherCountMarried!!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.NoofSister')!!}</span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    : {!!$theUser->profiles->sisterCount!!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.MarriedSister')!!}</span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    : {!!$theUser->profiles->sisterCountMarried!!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.Affluence')!!} </span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    @if(!empty($theUser->profiles->affluenceLevel))
                                    : {!!FormMatrimonial::get_affluenceLevel()[$theUser->profiles->affluenceLevel]!!}
                                    @else
                                    : {!!$notGiven!!}
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.AncestralOrigin')!!} </span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    @if(!empty($theUser->profiles->ancestralOrigin))
                                    : {!!HTMLspecialchars($theUser->profiles->ancestralOrigin)!!}
                                    @else
                                    : {!!$notGiven!!}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(Common::canAccessbyPrivacy($theUser->id, Common::$PRIV_COL_EDU_AND_PROF))
                <div class="col-lg-12">
                    <div class="profile-info-body infoBox profile-edit-container">
                        <div class="row">
                            <div class="col-lg-7 col-md-5 col-sm-5 col-xs-8">
                                <h3 id="infoBox-Title">{!!trans('translator.Education')!!} {!!trans('translator.AND')!!}
                                    {!!trans('translator.Profession')!!}</h3>
                            </div>
                            <div class="col-lg-3 col-lg-offset-2 col-md-7 col-sm-7 col-xs-4">
                                @if($isSelf || Common::isAdmin())
                                @include('templates.popups.eduProfEdit')
                                <a id="profile-education-edit" class="pull-right profile-edit-btn profile-edit-catch"
                                    href="#" data-toggle="modal" data-target="#edu-prof-modal"><span
                                        class="glyphicon glyphicon-pencil"></span><span
                                        class="hidden-xs">{!!trans('translator.Edit')!!}</span></a>
                                @endif
                            </div>
                        </div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.Education')!!} </span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    @if(!empty($theUser->profiles->edu_level))
                                    : {!!FormDetails::get_edu_level()[$theUser->profiles->edu_level]!!}
                                    @else
                                    : {!!$notGiven!!}
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.Profession')!!}</span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    <?php
                                            $works = $UserDetails->get_workingAs_option(TRUE);
                                            ?>
                                    @if(!empty($theUser->profiles->working_on))
                                    : {!!$works[$theUser->profiles->working_on]!!}
                                    @else
                                    : {!!$notGiven!!}
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.AnnualIncome')!!} <br> <em>(K = *1000)</em> </span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    @if(!empty($theUser->profiles->an_income))
                                    : {!!FormDetails::get_an_income()[$theUser->profiles->an_income]!!}
                                    @else
                                    : {!!$notGiven!!}
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.Workingwith')!!} </span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    @if(!empty($theUser->profiles->working_at))
                                    : {!!FormDetails::get_workingAt()[$theUser->profiles->working_at]!!}
                                    @else
                                    : {!!$notGiven!!}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(Common::canAccessbyPrivacy($theUser->id, Common::$PRIV_COL_LOCATION))
                <div class="col-lg-12 profile-edit-container">
                    <div class="profile-info-body infoBox">
                        <div class="row">
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                                <h3 id="infoBox-Title">{!!trans('translator.Location')!!}</h3>
                            </div>
                            <div class="col-lg-3 col-lg-offset-4 col-md-7 col-sm-7 col-xs-5">
                                @if($isSelf || Common::isAdmin())
                                {{--                                            @include('templates.popups.locationEdit')--}}
                                <a id="profile-location-edit" class="pull-right profile-edit-btn profile-edit-catch"
                                    href="#" data-toggle="modal" data-target="#location-modal"><span
                                        class="glyphicon glyphicon-pencil"></span><span
                                        class="hidden-xs">{!!trans('translator.Edit')!!}</span></a>
                                @endif
                            </div>
                        </div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.DistrictLiving')!!} </span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    @if(!empty($theUser->profiles->city))
                                    <?php $city = WorldCities::find($theUser->profiles->city); ?>
                                    @if($city->country_code == 'LK')
                                    : {!!trans('translator.'.$city->city_name)!!}
                                    @else
                                    : {!!$city->city_name!!}
                                    @endif
                                    @else
                                    : {!!$notGiven!!}
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.City')!!} </span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    @if(!empty($theUser->profiles->small_city))
                                    : {!!SmallCities::getSmallCities()[$theUser->profiles->small_city]['city']!!}
                                    @else
                                    : {!!$notGiven!!}
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.ResidencyStatus')!!} </span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    @if(!empty($theUser->profiles->residency))
                                    : {!!FormDetails::get_residency()[$theUser->profiles->residency]!!}
                                    @else
                                    : {!!$notGiven!!}
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"> <span class="myLabel">
                                        {!!trans('translator.Country')!!} </span>
                                </div>
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                    @if(!empty($theUser->profiles->residency))
                                    @foreach( FormDetails::getCountries() as $cntry => $country)
                                    @if($country['code'] == "LK")
                                    : {!!trans('translator.'.$country['name'])!!}
                                    @endif
                                    @endforeach
                                    @else
                                    : {!!$notGiven!!}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif


                @if(!Auth::check() )
                <form class="login-temp" name="jjj" id="loginform" method="post" action="{!!URL::route('doLogin')!!}">
                    {!!Form::token()!!}
                    <div class="col-lg-12 profile-edit-container">
                        <div class="profile-info-body infoBox">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <h3 id="infoBox-Title">{!!trans('translator.backLogin')!!}</h3>
                                </div>

                            </div>
                            <div class="container-fluid">
                                @include('templates.popups.forgotPassword')
                                <div class="row">
                                    <div class="col-lg-5 col-sm-5">
                                        <h4 class="heading4">{!!trans('translator.Memberlogin')!!}</h4>
                                        <input type="text" class="txtbox" name="login-email"
                                            value="{!!Input::old('login-email')!!}">
                                        <div class="clearfix"></div>
                                        <input id="login-chkbox" type="checkbox" class="css-checkbox" name="remember"
                                            value="1" />
                                        <label id="login-chkbox-label" for="login-chkbox"
                                            class="css-label">{!!trans('translator.Remember')!!}</label>
                                    </div>
                                    <div class="col-lg-5 col-sm-5">
                                        <h4 class="heading4">{!!trans('translator.Password')!!}</h4>
                                        <input type="password" class="txtbox" name="login-password">
                                        <div class="clearfix"></div>
                                        <label><a href="#" data-toggle="modal"
                                                data-target="#forgot-password-modal">{!!trans('translator.ForgotPassword')!!}</a></label>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-lg-2 col-sm-2">
                                        <button type="submit" class="btn_login">{!!trans('translator.Login')!!}</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </form>
                @endif
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.profile-edit-catch').click(function () {
            // $('.chosen-container').css('width', '100%');
            $('.chosen-container').find('.default').css('width', '100%');
        });
</script>

@include('templates.messageBoxes.unfriendConfirmation')
@include('templates.messageBoxes.blockConfirmation')
@stop