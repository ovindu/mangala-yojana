@extends('layouts.master')
@section('title')
@if(App::getLocale() == 'en')
{!!Lang::get('translator.title' , array('page' => 'Friends'))!!}
@else
{!!Lang::get('translator.title' , array('page' => 'මිතුරන්'))!!}
@endif
@stop
@section('content')
@if(Auth::check())
@include ('layouts.nav')
@endif
<?php
$notGiven = '<em>Not Given</em>';
$searchCtrl = new SearchController;
$friendController = new FriendController;
$authID = Auth::id();
$user_count = 0;
?>
@if($user_count != 0 | count($data) != 0)
<div class="container bodycontainer">
    <div class="row ">
        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-2">
            <button class="SearchResult-btn text friend-favorite" data-bool="0"><i class="icon-heart"></i><span>{!!trans('translator.IamInterested')!!}</span></button>
        </div>
    </div>
</div>
@endif
<div class="container bodycontainer max-width-friends">
    @if(isset($data) & count($data) != 0)
    <div class="row">
        @foreach($data as $user)
        <?php
        $userid = 149582;
        if ($user->sender_id == $authID) {
            $userid = $user->receiver_id;
        } else {
            $userid = $user->sender_id;
        }
        $ProfilePicture = ProfileController::getProfileImageUrl(true, $userid, 'thumb');
        if (Common::isActiveUser($userid)) {
            $user_count++;
            $dataArray = SearchController::getUserData($userid);
            ?>
            <div class="col-sm-6 main-div {!!'friend-'.$userid!!} results allImage {!!($friendController->isLiked($userid) ? 'liked-profile' : '' )!!}" >
                @include('templates.profile.profilebox')
            </div>
        <?php } ?>
        @endforeach
        <div class="row">
            <div class="col-sm-12 text-center"> 
                {!!$data->appends(Input::all())->links();!!}
            </div>
        </div>
    </div>
    @endif
    @if($user_count == 0 | count($data) == 0)
    <div class="empty-friends">
        <div class="container-fluid menucontainer">
            <div class="row">
                <div class="spotlight">
                    <div class="center">
                        <span class="matches-not-found-txt">{!!trans('translator.FriendsPageLonelyTag')!!}
                            <a class="link" href="{!!URL::route('searchPage')!!}">{!!trans('translator.BasicAndAdvancedSearch')!!}</a>
                            {!!trans('translator.FriendsPageLonelyTagMiddle')!!} <a class="link" href="{!!URL::route('matchesPage')!!}">{!!trans('translator.Matches')!!}</a>{!!trans('translator.FriendsPageLonelyTagLast')!!}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    @endif
</div>
@include('templates.messageBoxes.blockConfirmation')
@stop
