@extends('layouts.master')
@section('title')
@if(App::getLocale() == 'en')
{!!Lang::get('translator.title' , array('page' => 'Favorites'))!!}
@else
{!!Lang::get('translator.title' , array('page' => 'මා කැමති පැතිකඩයන්'))!!}
@endif
@stop
@section('content')
@if(Auth::check())
@include ('layouts.nav')
@endif
<?php
$notGiven = '<em>Not Given</em>';
$searchCtrl = new SearchController;
$friendController = new FriendController;
$authID = Auth::id();
$user_count = 0;   
?>
<div class="container bodycontainer max-width-friends">
    @if(isset($data) & count($data) != 0)
    <div class="row">
        @foreach($data as $user)
        <?php $user_ = User::find($user->liked_id); $userid = $user_->id;
        if(Common::isActiveUser($userid)){
        $dataArray = SearchController::getUserData($userid);
        $ProfilePicture = ProfileController::getProfileImageUrl(true, $userid, 'thumb');
        ?>
        <div class="col-sm-6 main-div {!!$user->id!!}" >
            @include('templates.profile.profilebox')
        </div>
        <?php } ?>
        @endforeach
        <div class="row">
            <div class="col-sm-12 text-center"> 
                {!!$data->appends(Input::all())->render();!!}
            </div>
        </div>
    </div>
    @endif
    @if(count($data) == 0)
    <div class="empty-friends">
        <div class="container-fluid menucontainer">
            <div class="row">
                <div class="spotlight">
                    <div class="center">
                        <span class="matches-not-found-txt">{!!trans('translator.FavoritesPageLonelyTag')!!}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    @endif
</div>
@include('templates.messageBoxes.blockConfirmation')
@stop
