@extends('layouts.master')
@section('title')
Upload Profile Picture | MangalaYojana.LK
@stop
@section('content')

<div class="container bodycontainer profileUpload">
    <div class="row">

        <div class='col-lg-12 congBar'>
            <span class='congText'>{!!trans('translator.Uploadyourprofilephoto')!!}</span><a href="{!!URL::route('profilePage')!!}"><span class="dolater">{!!trans('translator.DoThisLater')!!}</span></a>
        </div>

    </div>
    <div class="row uploadRow">
        <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12">
            <div class="profileBox">
                <div id="hiddeninXs">
                    <div class="row">

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div class="profilePhotoThumbnail img-responsive ">

                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <span class="profilePhotoThumbainText">
                                                {!!trans('translator.Getmoreresponsive')!!}
                                            </span>
                                        </div>
                                    </div>
                                    <div id="thumbnailitemBottom">
                                        <div class="row">
                                            <div class="lockIcon text-center">
                                                <span class="glyphicon glyphicon-lock"></span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <span class="profilePhotoThumbainText">
                                                {!!trans('translator.Secured')!!}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding:145px 10px; ">
                            <div class="container-fluid">
                                <div class="row uploadRow">
                                    <div class="col-lg-2 col-md-2 col-sm-2 col xs-2">

                                        <div class="photoUploadBtnIcon pc">
                                            <span class="glyphicon glyphicon-arrow-up"></span>
                                        </div>

                                    </div>
                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-7">                                        
                                        <div class="mobileUpload btn-upload" data-toggle="modal" data-target="#profilePicUploadModal">
                                            <span class="btnText-xs">   
                                                <div><span class="device-sm">{!!trans('translator.UploadFromTablet')!!}</span>
                                                    <span class="device-lgmd">{!!trans('translator.UploadFromComputer')!!} </span>
                                                </div>                                                    
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="ShowinXs">
                    <div class="row">
                        <div class="col-xs-12 uploadRow ">
                            <div class="ProfilePhotoText-xs text-center">{!!trans('translator.Getmoreresponsive')!!}</div>
                            <div class="profilePhotoThumbnail-xs">
                                <img src="{!!URL::route('homePage')!!}/img/profilephotoupload-default.png" class="img-responsive profilePhotoThumbnail-xs-img">
                            </div>
                        </div>

                        <div class="col-xs-12 uploadRow">
                            <div class="mobileUpload mobile" data-toggle="modal" data-target="#profilePicUploadModal">
                                <span class="glyphicon glyphicon-arrow-up"></span>
                                <span class="btnText-xs">{!!trans('translator.MobileUpload')!!}</span>
                                <!--<input type="file" class="btn-upload-xs">-->
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="row uploadRow">
        <div class="col-lg-12">
            <div class="heading3">{!!trans('translator.PhotoGuidelines')!!}</div>
            <div id="photoGuildLines">
                <div class="col-lg-offset-0 col-lg-5 col-md-offset-1 col-md-5">
                    <ul>
                        <li> <span class="glyphicon glyphicon-ok Yes" ></span> <span class="liText">{!!trans('translator.Closeup')!!}</span></li>
                        <li> <span class="glyphicon glyphicon-ok Yes"></span> <span class="liText">{!!trans('translator.FullView')!!}</span></li>
                        <li> <span class="glyphicon glyphicon-ok Yes"></span> <span class="liText">{!!trans('translator.Latest')!!}</span></li>
                        <li> <span class="glyphicon glyphicon-ok Yes"></span> <span class="liText">{!!trans('translator.HighQuality')!!}</span></li>
                        <li> <span class="glyphicon glyphicon-ok Yes"></span> <span class="liText">{!!trans('translator.Selfie')!!}</span></li>
                    </ul>
                </div>
                <div class="col-lg-5 col-lg-offset-1 ">
                    <ul>
                        <li> <span class="glyphicon glyphicon-remove No"></span> <span class="liText">{!!trans('translator.SideFaced')!!}</span></li>
                        <li> <span class="glyphicon glyphicon-remove No"></span> <span class="liText">{!!trans('translator.Blured')!!}</span></li>
                        <li> <span class="glyphicon glyphicon-remove No"></span> <span class="liText">{!!trans('translator.Groups')!!}</span></li>
                        <li> <span class="glyphicon glyphicon-remove No"></span> <span class="liText">{!!trans('translator.Withwatermarks')!!}</span></li>
                        <li> <span class="glyphicon glyphicon-remove No"></span> <span class="liText">{!!trans('translator.Notyours')!!}</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@include('templates.popups.profilePicUpload');

@stop