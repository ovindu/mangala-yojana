@extends('layouts.master')
@section('title')
Welcome to MangalaYojana.LK
@stop
@section('content')
<?php
use \App\Library\FormDetails;    
use \App\Library\Common;    
use \App\Library\Salutation;    
use \App\Models\User;    
use \App\Models\Searchuser;    
use \App\Http\Controllers\AccountController;    
use \App\Http\Controllers\ProfileController;
?>
<?php $homeForm = new FormDetails; ?>
{!!Session::get('verified')!!}

@if(Session::has('deacNotice'))
<div class="container bodycontainer">
    <div class="row">
        @include('templates.confirmationMsgs.greenMessage',array('strong'=>trans('translator.well'),'normal'=>Session::get('deacNotice')))
    </div>
</div>
@endif

<style>
    /* carousel fullscreen */

    .carousel-fullscreen .carousel-inner .item {
        height: 100vh;
        min-height: 600px;
        background-attachment: fixed;
        background-position: top center;
        background-repeat: no-repeat;
        background-size: cover;
    }


    /* carousel fullscreen - vertically centered caption*/

    .carousel-fullscreen .carousel-caption {
        top: 50%;
        bottom: auto;
        -webkit-transform: translate(0, -50%);
        -ms-transform: translate(0, -50%);
        transform: translate(0, -50%);
    }

    /* overlay for better readibility of the caption  */

    .overlay {
        position: absolute;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.5);
        /* opacity: 0.3; */
        transition: all 0.2s ease-out;
    }

    .carousel-caption {

        /* width: 70%; */
        /* left: 15%; */
        /* top: 50% !important; */
    }

    .carousel-caption .content {
        background-color: rgba(0, 0, 0, 0.7);
        border-radius: 5px;
        padding: 20px 10px;
    }

    .carousel-caption .content .frmSearch {
        color: #fff !important;
    }

    .carousel-caption .content .optionLabel {
        color: #fff !important;
        font-size: 18px !important;
        font-weight: 500;
    }

    .carousel-caption .content .frmSearch .frmtexts {
        color: #000000;
        font-size: 15px;
        border-radius: 3px;
        padding: 10px 5px;
    }

    .carousel-caption .content .frmSearch .frmtexts .option {
        padding: 10px 5px !important;
    }

    .carousel-caption .content .frmSearch input {
        border-radius: 3px;
    }

    .carousel-caption .content .super-paragraph a,
    .carousel-caption .content .super-paragraph a:hover {
        color: #fff;
    }

    #carousel-example-generic {
        margin: 40px 0;
    }

    /* carousel fade */
    /* original solution by https://codepen.io/Rowno/pen/Afykb */

    .carousel-fade .carousel-inner .item {
        -webkit-transition-property: opacity;
        transition-property: opacity;
    }

    .carousel-fade .carousel-inner .item,
    .carousel-fade .carousel-inner .active.left,
    .carousel-fade .carousel-inner .active.right {
        opacity: 0;
    }

    .carousel-fade .carousel-inner .active,
    .carousel-fade .carousel-inner .next.left,
    .carousel-fade .carousel-inner .prev.right {
        opacity: 1;
    }

    .carousel-fade .carousel-inner .next,
    .carousel-fade .carousel-inner .prev,
    .carousel-fade .carousel-inner .active.left,
    .carousel-fade .carousel-inner .active.right {
        left: 0;
        -webkit-transform: translate3d(0, 0, 0);
        transform: translate3d(0, 0, 0);
    }

    .carousel-fade .carousel-control {
        z-index: 2;
    }

    .find-title {
        color: #00c0ff;
    }

    #icons_row i {
        font-size: 3em;
        padding: 35px;
        margin-top: 10px;
        margin-bottom: 5px;
        border-radius: 50%;
        color: #ffffff;
        background-color: #00c0ff;
    }

    #icons_row .content_wrap a {
        font-size: 2em;
        color: #00c0ff;
    }

    #registration_modal .modal {
        z-index: 999 !important;
    }

    @media only screen and (max-width: 480px) {
        .carousel-caption {
            width: 100%;
            left: 0%;
            right: 0%;
            top: 60% !important;
            border-radius: 0px !important;
        }

        .carousel-fullscreen .carousel-inner .item {
            min-height: 800px;
        }

        .frmSearch .row .col-xs-12 {
            margin-bottom: 10px;
        }

        .container-title {
            font-size: 18px;
        }
    }
</style>

<div class="container-fluid" style="margin:0; padding:0;">
    <div id="carousel-example-generic2" class="carousel slide carousel-fullscreen carousel-fade" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active" style="background-image: url('img/slider/1.jpg');">
                <div class="overlay"></div>
                <div class="carousel-caption">
                    <h1 class="container-title text-center">Meet someone for keeps</h1>
                    <div class="content">
                        @include('home.includes.search')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container bodycontainer">
    <div class="row" id="icons_row">
        <h2 class="find-title text-center">Find your Special Someone</h2><br>
        <div class="col-sm-4 text-center">
            <a href="#!" class="sign_up" type="button" data-toggle="modal" data-target="#registration_modal"><span><i
                        class="fa fa-edit"></i></span></a>
            <div class="content_wrap">
                <a href="#!" type="button" data-toggle="modal" data-target="#registration_modal">Sign up</a><br><br>
                <div>Register for free &amp; put up your Profile</div>
            </div>
        </div>

        <div class="col-sm-4 text-center">
            <a href="#!" class="interact" type="button" data-toggle="modal" data-target="#registration_modal"><span><i
                        class="fa fa-user-plus"></i></span></a>
            <div class="content_wrap">
                <a href="#!" type="button" data-toggle="modal" data-target="#registration_modal">Connect</a><br><br>
                <div>Select &amp; Connect with Matches you like</div>
            </div>
        </div>
        <div class="col-sm-4 text-center">
            <a href="#!" class="connect" type="button" data-toggle="modal" data-target="#registration_modal"><span><i
                        class="fa fa-comment"></i></span></a>
            <div class="content_wrap">
                <a href="#!" type="button" data-toggle="modal" data-target="#registration_modal">Interact</a><br><br>
                <div>Become a Premium Member &amp; Start a Conversation</div>
            </div>
        </div>
    </div>

    {{-- <div class="row">
        <div class="col-md-10 col-md-offset-2">
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- Mangalayojana -->
            <ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-5091727005907575"
                data-ad-slot="9718065355" data-ad-format="auto" data-full-width-responsive="true"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
    </div> --}}

    {{-- registration model --}}
    <!-- Modal -->
    <div id="registration_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">User Registration</h4>
                </div>
                <form class="frmHome frmSignUp" id="frmSignUp" action='{{ route('doRegister') }}' method='post'>
                    <div class="modal-body">
                        @include('home.includes.register')
                    </div>
                    <div class="modal-footer">
                        <div class="row frmRow">
                            <?php $agree = Input::old('agree'); ?>
                            <div class="col-lg-9 col-xs-8"><input {!!(empty($agree) ? 'checked' : '' )!!} name="agree"
                                    class="cursor-pointer" type="checkbox" id="tandc"><label class="cursor-pointer"
                                    style="font-size: 13px" for='tandc'>{!!trans('translator.AgreeTNC')!!}
                                </label> </div>
                            @if ($errors->has('agree'))<span class='formError'>{!! $errors->first('agree')
                                !!}</span>@endif
                            <div class="col-lg-3 col-xs-4"><button type="submit" class="btn_signup"
                                    style="padding: 10px 15px; height: auto!important; font-size: 14px; border-radius: 5px;">{!!trans('translator.Submit')!!}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

</div>

<hr size="10" align="center" />
<div class="container">
    <div class="row">
        <div class="col-lg-7" style="overflow: hidden;">
            <div class="social">
                <div class="socialfeed">
                    <!--                    <div class="social_buttons">
                        <div class="row">
                            <div class="col-lg-3 col-xs-6" style="padding: 0px; outline: none !important;"><a class="btn_social youtube" href="https://www.youtube.com/channel/UCiNRrSzbCLSUhYKsddiRY2w">Youtube</a></div>
                            <div class="col-lg-3 col-xs-6" style="padding: 0px; outline: none !important;"><a class="btn_social facebook" href="http://facebook.com/mangalayojana.lk">Facebook</a></div>
                            <div class="col-lg-3 col-xs-6" style="padding: 0px; outline: none !important;"><a class="btn_social googleplus" href="https://plus.google.com/115043632584817010131">Google+</a></div>
                            <div class="col-lg-3 col-xs-6" style="padding: 0px;"><a class="btn_social twitter" href="http://twitter.com/mangalayojana">Twitter</a></div>
                        </div>
                    </div>-->

                    <div class="row">
                        <div id="embed">
                            <iframe width="100%" height="300px" src="//www.youtube.com/embed/PXQcpqKstQE"
                                frameborder="0" class="embed-responsive-item"></iframe>
                        </div>
                    </div>
                    <!--                    <div class="row"> <h3 class="heading3" style="margin-left:16px;">Trending Videos</h2></div>
                    <div class="row">
                        <div id="trendingvideos">

                        </div>

                    </div>-->
                </div>
            </div>

        </div>
        <div class="col-lg-5">
            <div class="my_about">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="heading3"><span style="color:#000000;">{!!trans('translator.WelocomeMsg')!!}</span>
                            {!!trans('translator.mangalayojanaLK')!!}</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <br />
                        <p>Mangalayojana is a matrimonial website developed for Sri Lankans who lives in anywhere in the
                            world. You can register with us providing your information and also you can specify
                            preferrences
                            about your future partner. Mangalayojana will match them to find your best match for you.
                        </p>

                        <p>All sensitive information that you provide here is totally secure with us and also we have
                            privacy options that you may chose which information of you to be exposed to public,
                            registered
                            users and also to your Mangalayojana friends.</p>

                        <p>We are trying to provide the best user experience within our service. We have a dedicated
                            support
                            hotline for you which you can call and get support regarding any issues of using our
                            service.
                        </p>


                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<hr size="10" align="center" />
<?php
    $accountCtrl = new AccountController;
    $formDetails = new FormDetails;
    $salutation = new Salutation();
    ?>
<div class="container">
    <div class="row">
        <div id="randomprofiles">
            <h3 class="heading3" style="margin-left:32px; margin-bottom: 20px;">
                {!!trans('translator.RandomProfiles')!!}<small style="text-transform: capitalize; font-size:14px;">
                    {!!trans('translator.FeelingLucky')!!}</small></h3>
            @for($i=0;$i<4;$i++)
                <?php $rand = User::where('active',1)->where('role_id','!=',  Common::$site_admin_user)->with(array('profiles'))->distinct()->orderByRaw("RAND()")->first(); ?>
                @if(!empty($rand)) <div class="col-lg-3 col-md-6 col-sm-6 col-sm-6">
                <?php
                $id = $rand->id;
                $searchView = Searchuser::find($rand->id);
                ?>
                <div class="radpro">
                    <div class="row" style="margin-bottom: 5px;">
                        <div class="col-lg-4 col-xs-4">
                            <div class="propic">
                                {!!HTML::image(ProfileController::getProfileImageUrl(true,$id), null, array('style' =>
                                'height:55px;','class'=> (Common::canAccessbyPrivacy($id, Common::$PRIV_COL_PROFILE_PIC)
                                ?
                                '' : 'blur')))!!}
                            </div>
                        </div>
                        <div class="col-lg-8 col-xs-8">
                            <p class="radpro_name">{!!Common::getUserPublicName($id)!!}</p>
                            <div class="radpro_details">
                                <p>
                                    @if(!empty($searchView['age']))
                                    Age: {!!$searchView['age']!!}
                                    @else
                                    Created by {!!$salutation->salute($rand->profilefor)!!}
                                    @endif
                                </p>
                                <p>
                                    @if(!empty($rand->profiles->city))
                                    {!!Common::getCity($rand->profiles->city)!!}
                                    @else
                                    @if($rand->religion==3)
                                    An @else A {!!$formDetails->get_religion('newbie')[$rand->religion]!!}
                                    @endif
                                    @endif
                                </p>

                            </div>

                        </div>

                    </div>
                    <form action="{!!URL::route('publicProfile')!!}" method="get">
                        <input name="id" type="hidden" value="{!!$id!!}" />
                        <button class="btn_viewprofile">{!!trans('translator.ViewProfile')!!}</button>
                    </form>
                </div>
        </div>
        @endif
        @endfor
    </div>
</div>

</div>


@stop