<div class="col-sm-6">
    <div id="searchResultBody" >
        <div class="container-fluid daily-ten-block">
            <div class="row">
                <div class="col-sm-12">
                    <a href="{!!URL::route("publicProfile",array('id'=>$userid))!!}">
                        <h4 id="daily10Name">{!!$user->fname!!} {!!$user->lname!!} 
                            <span class="@if($searchCtrl->isOnline($userid)){!!"online-icon online-icon-sty"!!}@else{!!"offline-icon online-icon-sty"!!}@endif"></span> 
                        </h4>
                    </a>
                    @if(!Common::isSelf($userid))
                    @include('templates.dropdowns.settingsDropdown')
                    @endif
                </div>

            </div>
            <?php
                $equal_to_daily10 = Request::url() == URL::route('dailytenPage');
            ?>
            <div class="row">
                <div class="@if($equal_to_daily10) col-lg-4 col-md-4 col-sm-4 @else col-lg-3 col-md-3 col-sm-3 @endif" id="">
                    <div id="searchResultThumbnail">
                        <a href="{!!URL::route("publicProfile",array('id'=>$userid))!!}">
                            {!!HTML::image($ProfilePicture,null,array('class'=>'img-responsive'))!!}
                        </a>
                    </div>
                </div>
                <div class="@if($equal_to_daily10) col-lg-8 col-md-8 col-sm-8 @else col-lg-9 col-md-9 col-sm-9 @endif" id="" >
                    <div id="searchResultContent">
                        <div class="container-fluid">
                            <div class="row" id="searchcontentRow">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="row"><span class="myLabel">{!!trans('translator.Age')!!}</span></div>
                                    <div class="row"><span class="myLabel">{!!trans('translator.Height')!!}</span></div>
                                    <div class="row"> <span class="myLabel">{!!trans('translator.Gender')!!}</span></div>
                                    <div class="row"> <span class="myLabel">{!!trans('translator.Religion')!!}</span></div>
                                @if(Common::canAccessbyPrivacy($user->id, 'location')) <div class="row"><span class="myLabel">{!!trans('translator.CurrentCity')!!} </span></div> @endif
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="row">: {!!$user->age!!}yrs</div>
                                    <div class="row">: @if(isset($user->height)) {!!FormDetails::get_heightList($notGiven)[$user->height]!!} @else {!!"Not Given"!!}@endif</div>
                                    <div class="row">: {!!($user->gender == trans('translator.Male')? 'Male' : trans('translator.FeMale'))!!}</div>
                                    <div class="row">: {!!FormDetails::get_religion($notGiven)[$user->religion]!!}</div>
                                @if(Common::canAccessbyPrivacy($user->id, 'location'))    <div class="row">: @if(isset($cityID) && $cityID!=0) {!!Common::getCity($cityID)!!} @else {!!"Not given"!!}@endif</div> @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="container-fluid">
                            <div class="col-lg-12" >
                                @if(!Common::isSelf($userid))
                                @if(!Common::myGender($userid))
                                <div id="buttonArea">
                                    <h4>@if($user->gender == 'm') {!!trans('translator.likeHisProfile')!!} @else {!!trans('translator.likeHerProfile')!!} @endif</h4>
                                    <button  id="btn-{!!$userid!!}" class="@if($friendController->isLiked($userid)){!!'liked'!!}@else{!!'like'!!}@endif searchResult-btn yes 
                                             " data-like-user-id='{!!$userid!!}' liked_id='{!!$friendController->isLiked($userid)!!}'></button>
                                </div>
                                @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>