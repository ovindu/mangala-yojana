@extends('layouts.master')
@section('title')
@if(App::getLocale() == 'en')
{!!Lang::get('translator.title' , array('page' => 'Matches'))!!}
@else
{!!Lang::get('translator.title' , array('page' => 'ගැලපුම්'))!!}
@endif
@stop
@section('content')
@if(Auth::check())
@include ('layouts.nav')
@endif
<?php
$notGiven = '<em>Not Given</em>';
$searchCtrl = new SearchController();
$friendController = new FriendController();
$Form = new FormDetails;

?>
@if(!empty($data) && count($data) != 0)
<?php $DataCount = count($data); ?>
<div class="container-fluid menucontainer">
    <div class="row">
        <div class="searchResultBar">

            <div class="text-left">
                <span id="SearchHead">{!!trans('translator.BestMatchesforyou')!!}</span>
            </div>
            <div class="text-right">
                <span id="SearchCriteria">{!!trans('translator.MatchesPageText')!!} 
                    @if($DataCount==0)
                    {!!trans('translator.NoMatches')!!} 
                    @elseif($DataCount==1)
                    {!!sprintf("%02s", $DataCount)!!} {!!trans('translator.Matchfound')!!}
                    @else 
                    {!!$DataCount!!} {!!trans('translator.Matchfound')!!}
                    @endif
                </span>
            </div>
            <div class="clearfix"></div>
        </div>     
    </div>
</div>


<div class="container bodycontainer">
    <div class="row">
        @foreach($data as $user)
        <?php
        $userid = $user->id;
        $ProfilePicture = ProfileController::getProfileImageUrl(true, $userid, 'thumb');
        $cityArray = $Form->get_cities();
        $cityID = $user->city;
        ?>
        @include('home.matches.matchBody')
        @endforeach
    </div>
    <div class="row">
        <div class="col-sm-12 text-center"> 
            {!!$data->appends(Input::all())->render()!!}
        </div>
    </div>

    @else
    <div class="container-fluid menucontainer">
        <div class="row">
            <div class="spotlight">
                <div class="center">
                    <span class="matches-not-found-txt">{!!trans('translator.noMatches')!!}
                        <a class="link" href="{!!URL::route('searchPage')!!}">{!!trans('translator.BasicAndAdvancedSearch')!!}</a>
                        {!!trans('translator.matchesMsg')!!}</span> 
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    @endif
</div>
</div>
@include('templates.messageBoxes.blockConfirmation')
<script>
    $(document).on('click', '.btn-block-stng-drpwn', function() {
        var userID = $(this).attr('data-uid');
        var headingText = "Block " + $(this).attr('data-user-name');
        $('#hidden-block-id').val(userID);
        $('#block-confirmation-heading').html(headingText);
    });
</script>
@stop


