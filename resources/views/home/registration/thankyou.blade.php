@extends('layouts.master')
@section('title')
End of registration | MangalaYojana.LK
@stop
@section('content')

<style>
    ul {
        list-style-type: none;
        padding: 0px;
        margin: 0px;
    }

    ul li {
        background-image: url('../img/heart-icon.png');
        background-repeat: no-repeat;
        background-position: 0px 5px; 
        padding-left: 24px;
        font-family: 'Open Sans', sans-serif;
        font-size:13px;
        font-weight: 600;
        color:#5C5C5C;
        line-height: 25px;

    }
</style>
<div class="container bodycontainer " id='packages'>
    <div class="row">
        <div class='col-lg-12 congBar'>
            <span class='congText'>{!!trans('translator.Congratulations')!!}</span> {!!trans('translator.MemberThankTag')!!}
        </div>
    </div>
    <div class='row'>
        <div class='col-lg-12 ashBar'>
            {!!trans('translator.MemberThankTag')!!}
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-4 pkgBorder">
            <div class="pkgBox"><h3 class="pkgHeader">{!!trans('translator.BecomeaPremiumMember')!!}</h3>
                <ul class="icon">
                    <li>{!!trans('translator.Senddirectmessages')!!}</li>
                    <li>{!!trans('translator.Chatwithinterestedmembers')!!}</li>
                    <li>{!!trans('translator.Viewverifiedcontact')!!}</li>
                </ul>
                <form action="{!!URL::route('upgradePage')!!}">
                    <button type="submit" class="btn_pkg btn_pkg_pre">{!!trans('translator.BecomeaPremiumMemberBTN')!!}<span id="thankupage_option-button-crown" class="pull-left icon-crown center" ></span></button>
                </form>
            </div>
        </div>
        <div class="col-lg-4 col-md-4  pkgBorder">
            <div class="pkgBox"><h3 class="pkgHeader">{!!trans('translator.ViewYourProfile')!!}</h3>
                <ul>
                    <li>{!!trans('translator.Checkyourdetails')!!}</li>
                    <li>{!!trans('translator.Addifanymissinginformation')!!}</li>
                    <li>{!!trans('translator.Shareprofilelink')!!}</li>
                </ul>
                <form action="{!!URL::route('profilePage')!!}">
                    <button type="submit" class="btn_pkg blue"> {!!trans('translator.ViewYourProfile')!!}<span id="thankupage_option-button" class="pull-left glyphicon glyphicon-user"></span></button>
                </form>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 ">
            <div class="pkgBox"><h3 class="pkgHeader">{!!trans('translator.SearchYourPotentialPartner')!!}</h3>
                <ul>
                    <li>{!!trans('translator.Searchwithourwidecriteria')!!}</li>
                    <li>{!!trans('translator.Usephotosearchoptions')!!}</li>
                    <li>{!!trans('translator.Selectbestmatch')!!}</li>
                </ul>
                <form action="{!!URL::route('searchPage')!!}">
                <button type="submit" class="btn_pkg blue">{!!trans('translator.StartSearchNow')!!}<span id="thankupage_option-button" class="pull-left glyphicon glyphicon-search"></span></button>
            </div>
        </div>
    </div>
</div>
@stop