@extends('layouts.master')
@section('title')
Complete Registration | MangalaYojana.LK
@stop
@section('content')
<?php
$user_id = Auth::user()->id;
$db_query = User::find($user_id);
$whom = $db_query->profilefor;
$showhom = Salutation::salute($whom);
$regForm = new FormDetails;
?>
<div class="container bodycontainer" id="packages registration_last-step">
    <div class="row titleBar">
        <div>
            <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12">
                <div class="titleBarText">
                    {!!trans('translator.Justonemorestep')!!} <span class="glyphicon glyphicon-play" id="more-step-play-icon"></span> {!!trans('translator.CompleteYourRegistration')!!}
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                <div class="pull-right">
                    <span id="mandatory-sign">*</span> <span id="mandatory-text">{!!trans('translator.mandatory')!!}</span>
                </div>
            </div>
        </div>
    </div> 
    <div class="row border" style="background: white;">
        <form method="post" action="{!!URL::route('completeRegistration')!!}">
            {!!Form::token()!!}
            <div class="col-lg-12 col-sm-12">
                <div id="reg-form-container">
                    <div class="secondHeadings border-right border-left">{!!Lang::get('translator.Addlocationdetailsofyour', array('who' => $showhom))!!}</div>

                    <div class="frmMat_row border-right border-left">
                        <div class="row formRow">
                            <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText">

                                {!!trans('translator.DistrictLiving')!!}<span id="mandatory-sign">*</span>

                            </div>


                            <div class="col-lg-6 col-md-6 col-sm-10">
                                <select id="district" class='fmlyDetails_txtbox @if ($errors->has('city')) validateError @endif' name='city'>
                                <option value="">{!!trans('translator.Select')!!}</option>
                                    @foreach (Common::getCitiesForCountry() as $city)
                                    <option class="districts" value='{!!$city->id!!}' data-prov='{!!$city->id!!}' <?php echo (Input::old('city') == $city->id ? 'selected' : '') ?>>@if($city->country_code == 'LK') {!!trans('translator.'.$city->city_name)!!} @else {!!$city->city_name!!}  @endif</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                        @if(Common::getUserCountry(Auth::id()) == 'LK')
                        <div class="row formRow" id='city_living'>
                            <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText">
                                {!!trans('translator.CityLivingIn')!!}
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-10">
                                <?php $dstarray = $regForm->get_cities(); ?>
                                <select id="reg-small-city" name="small-city" class='fmlyDetails_txtbox @if ($errors->has('small-city')) validateError @endif'>
                                        <option value="">{!!trans('translator.Select')!!}</option>
                                    <script> var arrayFromPHP = JSON.parse('{!!json_encode(SmallCities::getSmallCities())!!}');</script>
                                </select>
                            </div>
                        </div>
                        @endif

                        <div class="row formRow">
                            <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText">

                                {!!trans('translator.ResidencyStatus')!!}<span id="mandatory-sign">*</span>

                            </div>


                            <div class="col-lg-6 col-md-6 col-sm-10">
                                <select name="residency" class="fmlyDetails_txtbox @if ($errors->has('residency')) validateError @endif">
                                    @foreach($regForm->get_residency() as $key=>$val)
                                    <option value="{!!$key!!}" <?php echo(Input::old('residency') == $key ? 'selected' : '') ?>>{!!$val!!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="secondHeadings border-right border-left" style="margin-top: -10px;">
                        {!!Lang::get('translator.Tellusalittle', array('who' => $showhom))!!}
                    </div>
                    <div class="frmMat_row border-right border-left">
                        <div class="row formRow"> 
                            <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText">

                                {!!trans('translator.MaritalStatus')!!}  <span id="mandatory-sign">*</span>

                            </div>


                            <div class="col-lg-6 col-md-6 col-sm-10">

                                {!! Form::select('marital_status', $regForm->get_maritalStatus(), Input::old('marital_status'), array('class' => 'fmlyDetails_txtbox maritalStatus')); !!}

                            </div>
                        </div>
                        <div class="row formRow children" style="display: none;"> 
                            <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText">
                                {!!trans('translator.NumberofChildren')!!}


                            </div>


                            <div class="col-lg-6 col-md-6 col-sm-10">

                                <input class="fmlyDetails_txtbox" type="number" name="children" value="0" min="0" max='50'/>

                            </div>
                        </div>
                        <script type="text/javascript">
                            $(function() {
                                $('.maritalStatus').change(function() {
                                    if ($('.maritalStatus').val() !== '1') {
                                        $('.children').show();
                                    } else {
                                        $('.children').hide();
                                    }
                                });
                            });
                        </script>
                        <div class="row formRow"> 
                            <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText">

                                {!!trans('translator.Height')!!} <span id="mandatory-sign">*</span>

                            </div>


                            <div class="col-lg-6 col-md-6 col-sm-10">
                                <?php $height = ""; ?>
                                @if ($errors->has("height")) 
                                <?Php $height = " validateError" ?>
                                @endif

                                {!!Form::select('height',$regForm->get_heightList(), Input::old('height'), array('class' => 'fmlyDetails_txtbox'.$height)); !!}


                            </div>
                        </div>
                        <div class="row formRow"> 
                            <div class="col-lg-4 col-md-4 col-sm-3 fmlyDetails_bodyText">

                                {!!trans('translator.SkinTone')!!} @if ($errors->has('skin_tone')) <span class="errorIcon reg icon-alert" data-toggle="tooltip" data-placement="right" title="Select the skin tone"></span> @endif

                            </div>

                            <?php $skinTone = Input::old('skin_tone') ?>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                <label for="very_fair" class="radio-text radio-inline radio-form"><input type="radio" name="skin_tone" value="1" id='very_fair' <?php echo ($skinTone == '1' ? 'checked' : '') ?>>{!!trans('translator.Veryfair')!!}  </label>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                <label for="fair" class="radio-text radio-inline radio-form"><input type="radio" name="skin_tone" value="2" id='fair' <?php echo ($skinTone == '2' ? 'checked' : '') ?>>{!!trans('translator.Fair')!!}  </label>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                <label for="wheatish" class="radio-text radio-inline radio-form"><input type="radio" name="skin_tone" value="3" id='wheatish' <?php echo ($skinTone == '3' ? 'checked' : '') ?>>{!!trans('translator.Wheatish')!!}  </label>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                <label for="dark" class="radio-text radio-inline radio-form"><input type="radio" name="skin_tone" value="4" id='dark' <?php echo ($skinTone == '4' ? 'checked' : '') ?>> {!!trans('translator.Dark')!!} </label>
                            </div>

                        </div>
                        <div class="row formRow"> 
                            <div class="col-lg-4 col-md-4 col-sm-3 fmlyDetails_bodyText">
                                {!!trans('translator.Bodytype')!!}
                                @if ($errors->has('body_type')) <span class="errorIcon reg icon-alert" data-toggle="tooltip" data-placement="right" title="Select the body"></span> @endif

                            </div>
                            <?php $BodyType = Input::old('body_type') ?>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                <label for="slim" class="radio-text radio-inline radio-form"><input type="radio" name="body_type" value="1" id='slim' <?php echo ($BodyType == '1' ? 'checked' : '') ?> >{!!trans('translator.Slim')!!}  </label>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                <label for="athletic" class="radio-text radio-inline radio-form"><input type="radio" name="body_type" value="2" id='athletic' <?php echo ($BodyType == '2' ? 'checked' : '') ?>> {!!trans('translator.Athletic')!!} </label>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> 
                                <label for="average" class="radio-text radio-inline radio-form"><input type="radio" name="body_type" value="3" id='average' <?php echo ($BodyType == '3' ? 'checked' : '') ?>> {!!trans('translator.Average')!!} </label>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> 
                                <label for="heavy" class="radio-text radio-inline radio-form"><input type="radio" name="body_type" value="4" id='heavy' <?php echo ($BodyType == '4' ? 'checked' : '') ?>> {!!trans('translator.Heavy')!!} </label>
                            </div>
                        </div>
                    </div>
                    <div class="secondHeadings border-right border-left" style="margin-top: -10px;">{!!Lang::get('translator.LifeStyleofyou', array('who' => $showhom))!!}</div>
                    <div class="frmMat_row border-right border-left">
                        <div class="row formRow"> 
                            <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText">
                                {!!trans('translator.Diet')!!} 
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-10">
                                <?php $diet = ""; ?>
                                @if ($errors->has("diet")) 
                                <?Php $diet = " validateError" ?>
                                @endif
                                {!!Form::select('diet', $regForm->get_dietInfo(), Input::old('diet'), array('class' => 'fmlyDetails_txtbox'.$diet))!!}

                            </div>
                        </div>
                        <div class="row formRow">
                            <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText">
                                {!!trans('translator.Drink')!!} @if ($errors->has('drink')) <span class="errorIcon reg icon-alert" data-toggle="tooltip" data-placement="right"></span> @endif
                            </div>
                            <?php $Drinkval = Input::old('drink') ?>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                <label for="yes" class="radio-text radio-inline radio-form"><input type="radio" name="drink" value="1" id='yes' <?php echo ($Drinkval == '1' ? 'checked' : '') ?>>{!!trans('translator.Yes')!!} </label>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                <label for="no" class="radio-text radio-inline radio-form"><input type="radio" name="drink" value="2" id='no' <?php echo ($Drinkval == '2' ? 'checked' : '') ?>> {!!trans('translator.No')!!} </label>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> 
                                <label for="occasionally" class="radio-text radio-inline radio-form"><input type="radio" name="drink" value="3" id='occasionally' <?php echo ($Drinkval == '3' ? 'checked' : '') ?>> {!!trans('translator.Occasionally')!!} </label>
                            </div>

                        </div>
                        <div class="row formRow">
                            <?php $Smokeval = Input::old('smoke') ?>
                            <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText">
                                {!!trans('translator.Smoke')!!} @if ($errors->has('smoke')) <span class="errorIcon reg icon-alert" data-toggle="tooltip" data-placement="right"></span> @endif
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                <label for="smo-yes" class="radio-text radio-inline radio-form"><input type="radio" name="smoke" value="1" id='smo-yes' <?php echo ($Smokeval == '1' ? 'checked' : '') ?>> {!!trans('translator.Yes')!!}</label>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
                                <label for="smo-no" class="radio-text radio-inline radio-form"><input type="radio" name="smoke" value="2" id='smo-no' <?php echo ($Smokeval == '2' ? 'checked' : '') ?>>{!!trans('translator.No')!!}</label>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6"> 
                                <label for="smo-occasionally" class="radio-text radio-inline radio-form"><input type="radio" name="smoke" value="3" id='smo-occasionally' <?php echo ($Smokeval == '3' ? 'checked' : '') ?>>{!!trans('translator.Occasionally')!!} </label>
                            </div>

                        </div>
                    </div>

                    <div class="secondHeadings border-right border-left" style="margin-top: -10px;"> {!!Lang::get('translator.Tellusalittle', array('who' => $showhom))!!}</div>
                    <div class="frmMat_row border-right border-left">
                        <div class="row formRow"> 
                            <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText">

                                {!!trans('translator.EducationLevel')!!} <span id="mandatory-sign">*</span>

                            </div>


                            <div class="col-lg-6 col-md-6 col-sm-10">
                                <?php $edu_level = ""; ?>
                                @if ($errors->has("edu_level")) 
                                <?Php $edu_level = " validateError" ?>
                                @endif
                                <?php
                                echo Form::select('edu_level', $regForm->get_edu_level(), '', array('class' => 'fmlyDetails_txtbox' . $edu_level));
                                ?>

                            </div>
                        </div>
                        <div class="row formRow"> 
                            <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText">

                                {!!trans('translator.JobField')!!} <span id="mandatory-sign">*</span>

                            </div>


                            <div class="col-lg-6 col-md-6 col-sm-10">
                                <?php $edu_field = ""; ?>
                                @if ($errors->has("edu_field")) 
                                <?Php $edu_field = " validateError" ?>
                                @endif
                                <?php
                                echo Form::select('edu_field', $regForm->get_edu_field(), '', array('class' => 'fmlyDetails_txtbox' . $edu_field));
                                ?>

                            </div>
                        </div>
                        <div class="row formRow"> 
                            <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText">
                                {!!trans('translator.WorkingAt')!!} <span id="mandatory-sign">*</span>
                            </div>


                            <div class="col-lg-6 col-md-6 col-sm-10">
                                <?php $working_at = ""; ?>
                                @if ($errors->has("working_at")) 
                                <?Php $working_at = " validateError" ?>
                                @endif
                                <?php
                                echo Form::select('working_at', $regForm->get_workingAt(), Input::old('working_at'), array('class' => 'fmlyDetails_txtbox' . $working_at));
                                ?>

                            </div>
                        </div>
                        <div class="row formRow"> 
                            <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText">
                                {!!trans('translator.WorkingAs')!!}
                            </div>


                            <div class="col-lg-6 col-md-6 col-sm-10">
                                {!!Form::select('working_on', $regForm->get_workingAs_option(), Input::old('working_on'), array('class' => 'fmlyDetails_txtbox'))!!}

                            </div>
                        </div>
                        <div class="row formRow"> 
                            <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText">
                                {!!trans('translator.AnnualIncome')!!} <span id="mandatory-sign">*</span>
                            </div>


                            <div class="col-lg-6 col-md-6 col-sm-10">
                                <?php $an_income = ""; ?>
                                @if ($errors->has("an_income")) 
                                <?Php $an_income = " validateError" ?>
                                @endif
                                <?php
                                echo Form::select('an_income', $regForm->get_an_income(), Input::old('an_income'), array('class' => 'fmlyDetails_txtbox' . $an_income));
                                ?>

                            </div>
                        </div>
                    </div>
                    <div class="secondHeadings border-right border-left" style="margin-top: -10px;">{!!Lang::get('translator.FinallyAboutYouself', array('who' => $showhom))!!}</div>
                    <div class="border-right border-left border-bottom">
                        <div class="row formRow fmlyDetails_bodyText">
                            <div class="text-center" id="lil-abt-bck-desc">{!!trans('translator.YourTextTag')!!}</div>
                            <div class="col-lg-4 col-md-4 col-sm-4 fmlyDetails_bodyText">
                                {!!trans('translator.Inyourownwords')!!}
                                 <span id="mandatory-sign">*</span>

                            </div>


                            <div class="col-lg-6 col-md-6 col-sm-10">
                                <?php $description = ""; ?>
                                @if ($errors->has("description")) 
                                <?Php $description = " validateError" ?>
                                @endif
                                {!!Form::textarea('description', '', array('cols' => '60', 'class' => 'form-control description'.$description))!!}

                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-lg-12 col-sm-12 col-xs-12">
                                        <div class="border" id="character-counter">
                                            <span class="count" style="margin-left: 3px;">Character Count <span id="count" style="color:red;">0</span>
                                                min 50. max 4000
                                            </span>
                                            <script>
                                                $(function() {
                                                    $('.description').keyup(function() {
                                                        var count = $('.description').val().length;
                                                        $('#count').text(count);
                                                        if (count < 50) {
                                                            $('#count').css('color', 'red');
                                                        }
                                                        else if (count < 4000) {
                                                            $('#count').css('color', 'black');
                                                        }
                                                        else if (count > 4000) {
                                                            $('#count').css('color', 'red');
                                                        }
                                                    });
                                                });
                                            </script>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row formRow"> 
                            <div class="col-lg-4 col-md-2 col-sm-2 col-xs-12 fmlyDetails_bodyText">
                                {!!trans('translator.AnyDisability')!!}
                            </div>


                            <div class="col-lg-2 col-md-2 col-sm-3  col-xs-12">
                                <?php $disab = Input::old('disability'); ?>
                                <label for="nodisability" class="radio-text radio-inline radio-form"><input type="radio" name="disability" checked value="1" id='nodisability' <?php echo($disab == '1' ? 'checked' : '') ?>>  {!!trans('translator.None')!!}</label>
                            </div>
                            <div class="col-lg-6 col-md-8 col-sm-7 col-xs-12">
                                <label for="physical" class="radio-text radio-inline radio-form"><input type="radio" name="disability" value="2" id='physical' <?php echo($disab == '2' ? 'checked' : '') ?>> {!!trans('translator.PhysicalDisability')!!}</label>

                            </div>
                        </div>
                    </div>
                    <div class="border-right border-left border-bottom">
                        <div class="row formRow"> 
                            <div class="col-lg-3 col-md-3 col-lg-offset-4 col-md-offset-4 col-sm-5 col-sm-offset-4">

                                <button type="submit" class="btn-details">{!!trans('translator.CreateProfile')!!}</button>
                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>
@endsection