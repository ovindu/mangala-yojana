@extends('layouts.master')
@section('title')
Mobile Verification | MangalaYojana.LK
@stop
@section('content')
<div class="container bodycontainer ">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div id="mobile_ver_box">
                <div class="mobilever-alert">
                    <p></p>
                </div>
                <h2>{!!trans('translator.verifyYourPhoneNumber')!!}</h2>
                <span>{!!trans('translator.YourPhoneNumber')!!} :</span>
                <input {!!(Common::canShowSendSMSbutton() ? '' : 'as' )!!} type="tel" name="mob_no"
                    class="fmlyDetails_txtbox mob_vb" value="{!! str_replace("+", '', Auth::user()->mobilenumber) !!}">
                {{-- @if(!Common::canShowSendSMSbutton()) --}}
                <button data-url="{!!URL::route('ConfirmMobileNumber')!!}" id="CONFNUMBER"
                    class='btn-SaveSearch'>{!!trans('translator.ConfirmAndSendCode')!!}</button>
                {{-- @endif --}}
                <br>
                <div>
                    <span id="note">{!!trans('translator.ConfirmationNoteInMV')!!}</span>
                </div>
                <div>
                    <span id="already_have">{!!trans('translator.AlreadyhaveCode')!!}</span> <br>
                    <input type="tel" name="conf_code" class="fmlyDetails_txtbox mob_vb_conf">
                    <button data-url="{!!URL::route('verifyUserByCode')!!}"
                        redirect-url="{!!URL::route('uploadPage', 'privacy')!!}" id="VERIFYBYCODE"
                        class='btn-SaveSearch verf_btn'>{!!trans('translator.Verify')!!}</button>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="messageBox" data-cannotsendcode="{!!trans('translator.MVCodeSendFailed')!!}"
    data-codesent="{!!trans('translator.MVCodeSendSuccess')!!}"
    data-sendingcode="{!!trans('translator.MVsendingCode')!!}" data-094needed='{!!trans(' translator.MVNumberError')!!}'
    data-invalidnbr="{!!trans('translator.MVinvalidNumber')!!} ">
@stop