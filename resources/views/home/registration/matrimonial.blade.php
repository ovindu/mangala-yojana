@extends('layouts.master')
@section('title')
Complete Profile | MangalaYojana.LK
@stop
@section('content')
<?php
$user_id = Auth::user()->id;
$db_query = User::find($user_id);
$whom = $db_query->profilefor;
$showhom = Salutation::salute($whom);
$matForm = new FormMatrimonial;
?>
<div class="container bodycontainer mat_con" >
    <div class="row">
        <div class='col-lg-12 congBar'>
            <span class='congText'>{!!Lang::get('translator.WriteMoreAbout', array('who' => $showhom))!!}</span><a href="{!!URL::route('thankyouPage')!!}">
                <span class="dolater">{!!trans('translator.DoThisLater')!!}</span></a>
        </div>
    </div>
        <div class="row">
        <form method="post" action="{!!URL::route('familyDetailsPage')!!}">
            {!!Form::token()!!}

            <div class="frmMat fmlyDetails">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12"><h4 class="fmlyDetails_text">{!!trans('translator.FamilyDetails')!!}</h4></div>
                    </div>
                    <div class="row frmMat_row">
                        <div class="col-lg-2 col-md-2 fmlyDetails_bodyText ">{!!trans('translator.FathersStatus')!!}</div>
                        <div class="col-lg-4 col-md-4">
                            <?php $fatherStatus = ""; ?>
                            @if ($errors->has("fatherStatus")) 
                            <?Php $fatherStatus = " validateError" ?>
                            @endif
                            <select name="fatherStatus" class="fmlyDetails_txtbox<?php echo $fatherStatus; ?> ">


                                @foreach($matForm->get_fatherStatus() as $key=>$value)
                                <option value="{!!$key!!}" <?php echo (Input::old('fatherStatus') == $key ? 'selected' : '') ?>>{!!$value!!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row frmMat_row">
                        <div class="col-lg-2 col-md-2 fmlyDetails_bodyText ">{!!trans('translator.MothersStatus')!!}</div>
                        <div class="col-lg-4 col-md-4">
                            <?php $motherStatus = ""; ?>
                            @if ($errors->has("motherStatus")) 
                            <?Php $motherStatus = " validateError" ?>
                            @endif
                            <select name="motherStatus" class="fmlyDetails_txtbox {!!$motherStatus!!}">
                                @foreach($matForm->get_motherStatus() as $key=>$value)
                                <option value="{!!$key!!}" <?php echo (Input::old('motherStatus') == $key ? 'selected' : '') ?>>{!!$value!!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row frmMat_row">
                        <div class="col-lg-2 col-md-2 col-sm-2 fmlyDetails_bodyText ">{!!trans('translator.NoofBrothers')!!} <span id="mandatory-sign">*</span></div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <?php $brotherCount = ""; ?>
                            @if ($errors->has("brotherCount")) 
                            <?Php $brotherCount = " validateError" ?>
                            @endif
                            <input name="brotherCount" class="fmlyDetails_txtbox brother-count {!!$brotherCount!!}" value="{!!Input::old('brotherCount')!!}" type="number" min="0" max="50"/>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 fmlyDetails_bodyText">{!!trans('translator.ofwhichmarried')!!} <span id="mandatory-sign">*</span> </div>
                        <div class="col-lg-2 col-md-2 col-sm-4">
                            <?php $brotherCountMarried = ""; ?>
                            @if ($errors->has("brotherCountMarried")) 
                            <?Php $brotherCountMarried = " validateError" ?>
                            @endif
                            <input name="brotherCountMarried" class="fmlyDetails_txtbox brother-married-count{!!$brotherCountMarried!!}" value="{!!Input::old('brotherCountMarried')!!}" type="number" min="0" max="50" />
                        </div>
                        <script type="text/javascript">
                            $(function () {
                                $('.brother-count').change(function () {
                                    if ($('.brother-count').val() === "0") {
                                       // $('.brother-married-count').prop('disabled', true);
                                        $('.brother-married-count').val("0");
                                    } else {
                                      //  $('.brother-married-count').prop('disabled', false);
                                    }
                                    if ($('.brother-count').val() < $('.brother-married-count').val()) {
                                        $('.brother-married-count').val($('.brother-count').val());
                                    }
                                });
                                $('.brother-married-count').change(function () {
                                    if ($('.brother-married-count').val() > $('.brother-count').val()) {
                                        $('.brother-married-count').val($('.brother-count').val());
                                    }
                                });
                            });
                        </script>
                    </div>
                    <div class="row frmMat_row">
                        <div class="col-lg-2 col-md-2  col-sm-2 fmlyDetails_bodyText ">{!!trans('translator.NoofSister')!!} <span id="mandatory-sign">*</span></div>
                        <div class="col-lg-4 col-md-4 col-sm-4">
                            <?php $sisterCount = ""; ?>
                            @if ($errors->has("sisterCount")) 
                            <?Php $sisterCount = " validateError" ?>
                            @endif
                            <input name="sisterCount" class="sister-count fmlyDetails_txtbox{!!$sisterCount!!}" value="{!!Input::old('sisterCount')!!}" type="number" min="0" max="50"/>
                        </div>
                        <div class="col-lg-2 col-md-2  col-sm-2 fmlyDetails_bodyText">{!!trans('translator.ofwhichmarried')!!} <span id="mandatory-sign">*</span></div>
                        <div class="col-lg-2 col-md-2 col-sm-4">
                            <?php $sisterCountMarried = ""; ?>
                            @if ($errors->has("sisterCountMarried")) 
                            <?Php $sisterCountMarried = " validateError" ?>
                            @endif
                            <input name="sisterCountMarried" class="sister-married-count fmlyDetails_txtbox{!!$sisterCountMarried!!}" value="{!!Input::old('sisterCountMarried')!!}"  type="number" min="0" max="50" />
                        </div>
                        <script type="text/javascript">
                            $(function () {
                                $('.sister-count').change(function () {
                                    if ($('.sister-count').val() === "0") {
                                        //$('.sister-married-count').prop('disabled', true);
                                        $('.sister-married-count').val("0");
                                    } else {
                                        //$('.sister-married-count').prop('disabled', false);
                                    }
                                    if ($('.sister-count').val() < $('.sister-married-count').val()) {
                                        $('.sister-married-count').val($('.sister-count').val());
                                    }
                                });
                                $('.sister-married-count').change(function () {
                                    if ($('.sister-married-count').val() > $('.sister-count').val()) {
                                        $('.sister-married-count').val($('.sister-count').val());
                                    }
                                });
                            });
                        </script>
                    </div>
                    <div class="row frmMat_row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 fmlyDetails_bodyText ">{!!trans('translator.FamilyValues')!!} @if ($errors->has('familyValue')) <span class="errorIcon icon-alert" data-toggle="tooltip" data-placement="right"></span> @endif</div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 fmlyDetails_bodyText">
                            <?php $familyval = Input::old('familyValue'); ?>
                            <label for="familyvalues-Traditional" class="radio-text radio-inline"><input type="radio" name="familyValue" id="familyvalues-Traditional" value="1" class="radio-btn" onBlur="validate_family();" <?php echo ($familyval == '1' ? 'checked' : '') ?> />{!!trans('translator.Traditional')!!} </label>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 fmlyDetails_bodyText">
                            <label for="familyvalues-Moderate" class="radio-text radio-inline"><input type="radio" name="familyValue" id="familyvalues-Moderate" value="2" class="radio-btn" onBlur="validate_family();" <?php echo ($familyval == '2' ? 'checked' : '') ?> /> {!!trans('translator.Moderate')!!}</label> 
                        </div>
                        <div class="col-lg-4 col-md-3 col-sm-3 col-xs-12 fmlyDetails_bodyText">
                            <label for="familyvalues-Liberal" class="radio-text radio-inline"><input type="radio" name="familyValue" id="familyvalues-Liberal" value="3" class="radio-btn" onBlur="validate_family();" <?php echo ($familyval == '3' ? 'checked' : '') ?> /> {!!trans('translator.Liberal')!!}</label>            
                        </div>
                    </div>
                    <div class="row frmMat_row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 fmlyDetails_bodyText ">{!!trans('translator.AffluenceLevel')!!}  @if ($errors->has('affluenceLevel')) <span class="errorIcon icon-alert" data-toggle="tooltip" data-placement="right"></span> @endif</div>
                        <?php $affval = Input::old('affluenceLevel'); ?>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 fmlyDetails_bodyText">
                            <label for="affluencelevel-Affluent" class="radio-text radio-inline"><input type="radio" name="affluenceLevel" id="affluencelevel-Affluent" value="1" class="radio-btn" onBlur="validate_family();" <?php echo ($affval == '1' ? 'checked' : '') ?> /> {!!trans('translator.Affluent')!!}</label>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 fmlyDetails_bodyText">
                            <label for="affluencelevel-UpperMiddleClass" class="radio-text radio-inline"><input type="radio" name="affluenceLevel" id="affluencelevel-UpperMiddleClass" value="2" class="radio-btn" onBlur="validate_family();" <?php echo ($affval == '2' ? 'checked' : '') ?> />{!!trans('translator.UpperMiddleClass')!!} </label> 
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 fmlyDetails_bodyText">
                            <label for="affluencelevel-MiddleClass" class="radio-text radio-inline"><input type="radio" name="affluenceLevel" id="affluencelevel-MiddleClass" value="3" class="radio-btn" onBlur="validate_family();" <?php echo ($affval == '3' ? 'checked' : '') ?> />{!!trans('translator.MiddleClass')!!} </label>            
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 fmlyDetails_bodyText">
                            <label for="affluencelevel-LowerMiddleClass" class="radio-text radio-inline"><input type="radio" name="affluenceLevel" id="affluencelevel-LowerMiddleClass" value="4" class="radio-btn" onBlur="validate_family();" <?php echo ($affval == '4' ? 'checked' : '') ?> />{!!trans('translator.LowerMiddleClass')!!} </label>            
                        </div>
                    </div>
                    <div class="row frmMat_row">
                        <div class="col-lg-2 col-md-2 fmlyDetails_bodyText ">{!!trans('translator.Origin')!!} <span id="mandatory-sign">*</span></div>
                        <div class="col-lg-4 col-md-4">
                            <?php $ancestralOrigin = ""; ?>
                            @if ($errors->has("ancestralOrigin")) 
                            <?Php $ancestralOrigin = " validateError" ?>
                            @endif
                            <input value="{!!Input::old('ancestralOrigin')!!}" name="ancestralOrigin" type="text" class="fmlyDetails_txtbox {!!$ancestralOrigin!!}"/>
                            
                        </div>
                        <div class="col-md-5"><div class="small-text">{!!trans('translator.SpecifOrigin')!!}</div></div>
                    </div>
                </div>
            </div>

            

            <div class="MidDetails">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12"><h4 class="fmlyDetails_text">{!!trans('translator.PhysicalAttributesHealth')!!}</h4></div>
                    </div>
                    <div class="row frmMat_row">
                        <div class="col-lg-2 col-md-2 fmlyDetails_bodyText">{!!trans('translator.BodyWeight')!!} <span id="mandatory-sign">*</span></div>
                        <div class="col-lg-2 col-md-2 col-sm-11 col-xs-10">
                            <input name="weight" type="number" value="{!!Input::old('weight')!!}" class="fmlyDetails_txtbox" min="1" />
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-2 fmlyDetails_bodyText">{!!trans('translator.Kgs')!!}</div>
                    </div>
                    <div class="row frmMat_row">
                        <div class="col-lg-2 col-md-2 fmlyDetails_bodyText ">{!!trans('translator.BloodGroup')!!}</div>
                        <div class="col-lg-3 col-md-3">
                            <select name="bloodGroup" class="fmlyDetails_txtbox">
                                @foreach($matForm->get_blood_group() as $key=>$val)
                                <option value="{!!$key!!}" <?php echo (Input::old('bloodGroup') == $key ? 'selected' : '') ?>>{!!$val!!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class = "row frmMat_row">
                        <div class = "col-lg-2 col-md-2 fmlyDetails_bodyText ">{!!trans('translator.HealthConditions')!!}</div>
                        <div class = "col-lg-3 col-md-3">
                            <select name="health" class="fmlyDetails_txtbox">
                                @foreach($matForm->get_health() as $key=>$val)
                                <option value="{!!$key!!}" <?php echo (Input::old('health') == $key ? 'selected' : '') ?>>{!!$val!!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class = "MidDetails">
                <div class = "container-fluid">

                    <div class = "row frmMat_row  btn-row">
                        <div class = "col-lg-2 col-md-2 fmlyDetails_bodyText"></div>
                        <div class = "col-lg-3 col-md-3">
                            <button class = "btn-details fmlyDetails_txtbox">{!!trans('translator.Submit')!!}</button>
                        </div>
                        <div class = "col-lg-7 col-md-7"><a href = "{!!URL::route('thankyouPage')!!}"><span class = "dolater_blue">{!!trans('translator.DoThisLater')!!}</span></div>
                    </div>

                </div>
            </div>
            {!!Form::close()!!}
    </div>
</div>
@stop