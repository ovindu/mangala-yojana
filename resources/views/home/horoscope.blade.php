@extends('layouts.master')
@section('title')
Upload your Horoscope | MangalaYojana.LK
@stop
@section('content')




<div class="container bodycontainer mat_con" >
    <div class="row">
        <div class='col-lg-12 congBar'>
            <span class='congText'>Upload your Horoscope</span>
        </div>
    </div>
    <div class="row">
        <form class="frmMat">
            <div id="horescope" class="top">
                <div class="container-fluid">
                    {!! Form::open() !!}
                    <div class="row frmMat_row">
                        <h4 class="heading5"> Upload your Horoscope here </h4>
                        <div class="col-lg-8">
                            <input type="file" style="margin-bottom: 10px;">
                            <span class="small upload">Upload your scanned .gif or .jpg or .png format only(Max file 5MB)</span>
                        </div>
                        <div class="col-lg-4">
                            <button class="btn-Search">Submit</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div id="horescope">
                <div class="container-fluid">
                    <div class="row frmMat_row">
                        <h4 class="heading5">Benefits of adding horoscope</h4>
                        <div class="col-lg-6">
                            <div id='uploadHorescope'>
                                <ul>
                                    <li > <span class="glyphicon glyphicon-heart Heart" >   </span><span class="liText">Free matching based on Vedic Astrology</span></li>
                                    <li > <span class="glyphicon glyphicon-heart Heart" >   </span><span class="liText">Check your compatibility with other members</span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div id='uploadHorescope'>
                                <ul>
                                    <li > <span class="glyphicon glyphicon-heart Heart" >   </span><span class="liText">Personality write up based on Moon Sings</span></li>
                                    <li > <span class="glyphicon glyphicon-heart Heart" >   </span><span class="liText">Complete control on Privacy </span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="horescope">
                <div class="container-fluid">
                    <div class="row frmMat_row">
                        <h4 class="text-uppercase">Other ways to upload your horoscope</h4>
                        <div class="col-lg-8">
                            <div class="row">
                                <div class="container-fluid">
                                    <div class="col-lg-1">
                                        <span class="icon-at-sign contactIcon"></span> 
                                    </div>
                                    <div class="col-lg-11">
                                        <h4 class="heading5">Email your horoscope</h4>
                                    </div>
                                    <div class="col-lg-offset-1 col-lg-12">
                                        <p>Email us your horoscope to <a href="">horoscope@mangalayojana.lk</a> You may send us your horoscope by Email mentioning your Profile ID and Name</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="container-fluid">
                                    <div class="col-lg-1">
                                        <span class="icon-mail contactIcon"></span> 
                                    </div>
                                    <div class="col-lg-11">
                                        <h4 class="heading5">Send your horoscope through post</h4>
                                    </div>
                                    <div class="col-lg-offset-1 col-lg-12">
                                        <p>You may send us your horoscope by Email mentioning your Profile ID, Name, Email, and signature on the back of your horoscope to your address</p>
                                        <p>Mangalayojana.lk</p>
                                        <p>No. 48</p>
                                        <p>New Road, Uyanwatte</p>
                                        <p>Matara, Sri Lanka.</p>
                                        <p>Phone: +94 77 3455 678</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@stop