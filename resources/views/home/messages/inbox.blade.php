@extends('layouts.master')
@section('title')
@if(App::getLocale() == 'en')
{!!Lang::get('translator.title' , array('page' => 'Inbox'))!!}
@else
{!!Lang::get('translator.title' , array('page' => 'ලැබෙන පණිවුඩ'))!!}
@endif
@stop
@section('content')
@include('layouts.nav')
<div class="container bodycontainer">
    <div class="row">
        <div class="col-sm-1 col-xs-3">
            <button class="btnInbox">
                <input id="inbox-chkbox" type="checkbox" class="css-checkbox" name="remember" value="1"/> 
                <label id="inbox-chkbox-label" for="inbox-chkbox" class="css-label noLabel"></label>    
            </button>
        </div>
        <div class="col-sm-1 col-xs-3">
            <form action="{!!URL::route('inboxPage')!!}">
                <button class="btnInbox">
                    <span class="glyphicon glyphicon-refresh"></span>
                </button>
            </form>
        </div>
        <div class="col-sm-1 col-xs-3">
            <button type="button" class="btn btn-default dropdown-toggle btnInbox" data-toggle="dropdown">
                {!!trans('translator.More')!!} <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" id="moreActions" role="menu">
                @if(empty($msgs))
                <?php
                $disabled = TRUE;
                $grayed_text = 'style="color:#989898 !important"';
                ?>
                @else
                <?php $disabled = FALSE; ?>
                @endif
                <li><a class="markAsRead" href="#" <?php echo ($disabled ? $grayed_text : '') ?>>{!!trans('translator.MarkAsRead')!!}</a></li>
                <li><a class="markAsUnread" href="#" <?php echo ($disabled ? $grayed_text : '') ?>>{!!trans('translator.MarkAsUnread')!!}</a></li>
                <li><a class="msg-delete" href="#" <?php echo ($disabled ? $grayed_text : '') ?>>{!!trans('translator.Delete')!!}</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="container-fluid">
                <div class="row">
                    <div id="inbox">
                        @if($disabled)
                        <div id="noMsgs" class="text-center">No conversations :(</div>
                        <div class="clearfix"></div>
                        @else
                        <?php
                        $i = 1;
                        foreach ($msgs as $msg):
                            $SenderprofPic = ProfileController::getProfileImageUrl(true, $msg->id, 'thumb');
                            ?>
                            <div class="theMsg <?php echo (($msg->receiver_read == 0 && $msg->sender_read == 0) && ($msg->receiver_id == Auth::user()->id || $msg->sender_id == Auth::user()->id) ? 'active' : '') ?>">
                                <div class="col-sm-1 hidden-xs" id="alignChkBox">
                                    <input data-msgID="{!!$msg->id!!}" id="msg-chkbox-{!!$i!!}" type="checkbox" class="msg-chkbox css-checkbox" name="remember" value="1"/> 
                                    <label id="" for="msg-chkbox-{!!$i!!}" class="msg-chkbox-label css-label noLabel"></label>    
                                </div>
                                <a href="{!!URL::route('messagePage', array('userid' => $msg->id))!!}">
                                    <div class="col-sm-2 col-xs-3"> 
                                        @if (!$msg->blocked)
                                        {!!HTML::image($SenderprofPic,null,array('class'=>'img-responsive inboxThumbnail'))!!}
                                        @else
                                        {!!HTML::image('/img/default-avatar-male-1x1.png',null,array('class'=>'img-responsive inboxThumbnail'))!!}
                                        @endif
                                    </div>
                                    <div class="col-sm-9 col-xs-9">
                                        <div class="row">
                                            <div class="col-sm-9 ">
                                                <div id="msngrName">
                                                    @if (!$msg->blocked)
                                                    {!!$msg->fname!!} {!!$msg->lname!!} <span class="{!!SearchController::isOnline($msg->id) ? "online-icon" : "offline-icon"!!}"></span>
                                                    @else
                                                    MangalaYojana User
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-sm-3 ">
                                                <?php $incomingTime = strtotime($msg->income_time); ?>
                                                <div>{!!Common::getAgoTimeString($incomingTime)!!}</div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div id="msgText" class="visible-xs text-justify">
                                                    {!!Str_Limit(htmlspecialchars($msg->body, ENT_XHTML),50,'...')!!} 
                                                </div>
                                                <div id="msgText" class="hidden-xs text-justify">
                                                    {!!Str_Limit(htmlspecialchars($msg->body, ENT_XHTML),500,'...')!!} 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <div class="clearfix"></div>
                            </div>

                            <?php
                            $i++;
                        endforeach;
                        ?>
                        <div class="clearfix"></div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <script>
            var checkedMessages = [];

            $('#inbox-chkbox').click(function (event) {
                if (this.checked) {
                    $('.msg-chkbox').each(function () {
                        this.checked = true;
                    });
                } else {
                    $('.msg-chkbox').each(function () {
                        this.checked = false;
                        $('.markAsRead').attr('href', '#');
                        $('.markAsUnread').attr('href', '#');
                        $('.msg-delete').attr('href', '#');
                    });
                }
                runCheckBoxSearch()
            });


            $(document).on('click', '.msg-chkbox', function () {
                runCheckBoxSearch();
            });

            function runCheckBoxSearch() {
                checkedMessages = [];
                $('.msg-chkbox').each(function () {
                    if ($(this).is(':checked')) {
                        var msgid = $(this).attr('data-msgID');
                        checkedMessages.push(msgid);
                        $('.markAsRead').attr('href', '<?php echo URL::route('markAsRead'); ?>?id=' + checkedMessages);
                        $('.markAsUnread').attr('href', '<?php echo URL::route('markAsUnread'); ?>?id=' + checkedMessages);
                        $('.msg-delete').attr('href', '<?php echo URL::route('deleteMessage'); ?>?id=' + checkedMessages);
                    }
                });
            }
        </script>
    </div>
</div>
@stop