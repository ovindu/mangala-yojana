@extends('layouts.master')
@section('title')
@if(!$blocked)
{!!Lang::get('translator.ConversationWith', array('fname' => $msg_details->fname, 'lname' => ''))!!} | MangalaYojana.LK 
@else
{!!Lang::get('translator.ConversationWith', array('fname' => 'MangalaYojana', 'lname' => 'User'))!!}   | MangalaYojana.LK
@endif
@stop
@section('content')
@include('layouts.nav')
<?php
$opponent_pic_done = false;
$self_pic_done = false;
$permission_message = Common::hasPermissions(Auth::id(), Common::$permissions_messaging);
?>
<div class="container bodycontainer">
    <div class="row">
        <div class="col-sm-1 col-xs-3">
            <form action="{!!URL::route('inboxPage')!!}">
                <button class="btnInbox">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </button>
            </form>
        </div>
        <div class="col-sm-1 col-xs-3">
            <form action="{!!Request::url()!!}">
                <input name="userid" type="hidden" value="{!!$userid!!}"/>
                <button class="btnInbox">
                    <span class="glyphicon glyphicon-refresh"></span>
                </button>
            </form>
        </div>
        <div class="col-sm-1 col-xs-3">
            <form action="{!!URL::route('deleteMessage')!!}">
                <input type="hidden" name="userid" value="{!!$userid!!}"/>
                <button class="btnInbox">
                    <span class="glyphicon glyphicon-trash"></span>
                </button>
            </form>
        </div>
    </div>
    <div id="Message">
        <div class="row">
            <div class="col-xs-12">
                <div id="msgWith">
                    <?php $accountCtrl = new AccountController; ?>
                    <h4>
                        @if(!$blocked)
                        <a href="{!!URL::route('publicProfile', array('id' => $userid, 'ref' => 'conversation title'))!!}">{!!trans('translator.ConversationWith' , array('fname' => $msg_details->fname , 'lname'=> $msg_details->lname))!!}</a> <span class="{!!(SearchController::isOnline($userid)) ? "online-icon" : "offline-icon"!!}"></span>
                        @else
                        {!!trans('translator.ConversationWith' , array('fname' => 'MangalaYojana', 'lname'=> 'User'))!!}
                        @endif
                    </h4>
                </div>    
            </div>
            <div class="col-xs-12">
                <div id="msgBody">
                    <div class="container-fluid">
                        @if(!$new_msg)
                        @foreach($all_msgs as $msg)
                        @if($msg->sender_id<>Auth::user()->id)
                        <div class="row">
                            <div class="col-sm-2">
                                @if(!$opponent_pic_done)
                                @if(!$blocked)
                                <a href="{!!URL::route('publicProfile')!!}?id={!!$msg->sender_id!!}&ref={!!$accountCtrl->generateRandomString(58)!!}inbox&qc=#{!!$accountCtrl->generateRandomString(20)!!}">{!!HTML::image($SenderprofPic,null,array('class'=>'img-responsive chatThumb pull-right','draggable'=>'false'))!!}</a>
                                @else
                                {!!HTML::image('/img/default-avatar-male-1x1.png',null,array('class'=>'img-responsive chatThumb pull-right','draggable'=>'false'))!!}
                                @endif
                                <?php $self_pic_done = false; ?>
                                <?php $opponent_pic_done = true ?>
                                @endif
                            </div>
                            <div class="col-sm-7">
                                <div class="chatText recive">
                                    <span class="leftTriangle"></span>
                                    <div class="row">
                                        <?php $incomingTime = strtotime($msg->created_at); ?>
                                        <div class="col-xs-12"> <span class="sentTime pull-right">{!!Common::getAgoTimeString($incomingTime)!!}</span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12"> <span>{!!htmlspecialchars($msg->body, ENT_XHTML)!!}</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif

                        @if($msg->sender_id==Auth::user()->id)
                        <div class="row text-right">
                            <div class="col-sm-offset-3 col-sm-7">
                                <div class="chatText send">
                                    <span class="rightTriangle"></span>
                                    <div class="row">
                                        <?php $outgoingTime = strtotime($msg->created_at); ?>
                                        <div class="col-xs-12"> <span class="sentTime pull-left">{!!Common::getAgoTimeString($outgoingTime)!!}</span></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12"> <span>{!!htmlspecialchars($msg->body, ENT_XHTML)!!}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                @if (!$self_pic_done)
                                {!!HTML::image($SelfprofPic,null,array('class'=>'img-responsive chatThumb pull-left','draggable'=>'false'))!!} 
                                <?php $self_pic_done = true; ?>
                                <?php $opponent_pic_done = false; ?>
                                @endif
                            </div>
                        </div>
                        @endif
                        @endforeach
                        @endif
                    </div>    
                </div>
            </div>
            <div class="col-xs-12">
                <div id="msgWrite">
                    <div class="row">
                        <div class="col-xs-12">
                            <form action="{!!URL::route('sendMessage')!!}">
                                <input type="hidden" name="userid" value="{!!$userid!!}"/>                                                           
                                <textarea name="msg-body" type="textarea" class="form-control" id="TypeText" placeholder="{!!trans('translator.TypeMessageHere')!!}." @if($userid == 149582 | $friends==false) @if( $friends==true || $permission_message!=true) {!!"disabled : null"!!} @endif @endif ></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 text-right">
                            @if(($userid != 149582 && $friends) | $permission_message)
                            @if($userid != 149582)
                            <button type="submit" id="btnSendmsg">
                                Send Message
                            </button>
                            @endif
                            @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop