@extends('layouts.master')
@section('title')
@if(App::getLocale() == 'en')
{!!Lang::get('translator.title' , array('page' => 'Daily 10'))!!}
@else
{!!Lang::get('translator.title' , array('page' => 'දෛනික 10'))!!}
@endif
@stop
@section('content')
@if(Auth::check())
@include ('layouts.nav')
@endif
<?php
$notGiven = '<em>Not Given</em>';
$searchCtrl = new SearchController;
$friendController = new FriendController;
$Form = new FormDetails;
?>

<div class="container bodycontainer">
    <div class="row">
        @foreach($data as $user)
        <?php
        $userid = $user->id;
        $ProfilePicture = ProfileController::getProfileImageUrl(true, $userid, 'main');
        $cityArray = $Form->get_cities();
        $cityID = $user->city;
        ?>
        @include('home.matches.matchBody')
        @endforeach
    </div>
</div>
@include('templates.messageBoxes.blockConfirmation')
@stop
