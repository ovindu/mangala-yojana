<?php
$old_inputs = Input::old();
$form_data = (empty($old_inputs) ? User::find(Auth::id()) : Input::old());
?>

@if($errors->count())
@foreach ($errors->all() as $error)
<span class='formError'>{!! $error !!}</span>		
@endforeach
@endif

<form method="post" action="{!!URL::route('editAccountInfo')!!}">
    {!!Form::token()!!}


    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12"><h4 class="fmlyDetails_text">
                    <span class="glyphicon glyphicon-user"></span> 
                    {!!trans('translator.AccountInformation')!!}</h4>
            </div>
        </div>
        <div class="row frmMat_row">
            <div class="col-lg-2 col-md-2 fmlyDetails_bodyText ">{!!trans('translator.FName')!!}</div>
            <div class="col-lg-4 col-md-4">
                {!!Form::text('fname',$form_data['fname'], array('class' => 'fmlyDetails_txtbox')); !!}
            </div>
        </div>
        <div class="row frmMat_row">
            <div class="col-lg-2 col-md-2 fmlyDetails_bodyText ">{!!trans('translator.LName')!!}</div>
            <div class="col-lg-4 col-md-4">
                {!!Form::text('lname',$form_data['lname'], array('class' => 'fmlyDetails_txtbox')); !!}
            </div>
        </div>
        <div class="row frmMat_row">
            <div class="col-lg-2 col-md-2 fmlyDetails_bodyText ">{!!trans('translator.Email')!!}</div>
            <div class="col-lg-4 col-md-4">
                {!!Form::email('email',$form_data['email'], array('class' => 'fmlyDetails_txtbox email')); !!}
            </div>
            <div class="col-lg-4 col-md-4 fmlyDetails_bodyText errorIcon">
                {!!Session::get('email')!!}
            </div>
        </div>
        <script type="text/javascript">
            $(function() {
                if ($('.confirm-mail-text-box').val() !== $('.email').val()) {
                    $('.confirm-mail').show();
                }
                $('.email').change(function() {
                    if ($('.email').val() !== "{!!$form_data['email']!!}") {
                        $('.confirm-mail-text-box').val("");
                        $('.confirm-mail').show();
                        $('.confirm-mail-text-box').focus();
                    } else {
                        $('.confirm-mail').hide();
                    }
                });
            });
        </script>
        <div class="row frmMat_row confirm-mail" style="display: none;">
            <div class="col-lg-2 col-md-2 fmlyDetails_bodyText ">{!!trans('translator.ConfirmEmail')!!}</div>
            <div class="col-lg-4 col-md-4">
                {!!Form::email('email-confirm',$form_data['email'], array('class' => 'fmlyDetails_txtbox confirm-mail-text-box')); !!}
            </div>
        </div>
        <div class="row frmMat_row">
            <div class="col-lg-2 col-md-2 fmlyDetails_bodyText">{!!trans('translator.newPWD')!!}</div>
            <div class="col-lg-4 col-md-4">
                {!!Form::password('password-new',array('class' => 'fmlyDetails_txtbox pwd')); !!}
            </div>
            <div class="col-lg-4 col-md-4 fmlyDetails_bodyText">
                {!!trans('translator.PWDchangeTip')!!}
            </div>
        </div>
        <script type="text/javascript">
            $(function() {
                $('.pwd').change(function() {
                    if ($('.pwd').val() !== "") {
                        $('.confirm-pwd-text-box').val("");
                        $('.confirm-pwd').show();
                        $('.confirm-pwd-text-box').focus();
                    } else {
                        $('.confirm-pwd-text-box').val("");
                        $('.confirm-pwd').hide();
                    }
                });
            });
        </script>
        <div class="row frmMat_row confirm-pwd" style="display: none;">
            <div class="col-lg-2 col-md-2 fmlyDetails_bodyText ">{!!trans('translator.ConfirmPassword')!!}</div>
            <div class="col-lg-4 col-md-4">
                {!!Form::password('password-confirm',array('class' => 'fmlyDetails_txtbox confirm-pwd-text-box')); !!}
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12"><h4 class="fmlyDetails_text"><span class="glyphicon glyphicon-ok"></span>{!!trans('translator.ConfirmIdentity')!!}</h4></div>
        </div>
        <div class="row frmMat_row">
            <div class="col-lg-2 col-md-2 fmlyDetails_bodyText ">{!!trans('translator.CurrentPassword')!!}<span id="mandatory-sign">*</span> </div>
            <div class="col-lg-4 col-md-4">
                {!!Form::password('password-current',array('class' => 'fmlyDetails_txtbox')); !!}
            </div>
        </div>
    </div>


    <div class="container-fluid">

        <div class = "row frmMat_row  btn-row">
            <div class = "col-lg-2 col-md-2 fmlyDetails_bodyText"></div>
            <div class = "col-lg-3 col-md-3">
                <button class = "btn-details fmlyDetails_txtbox">{!!trans('translator.SubmitForm')!!}</button>
            </div>

        </div>
    </div>
    {!!Form::close()!!}
    
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <a class="cursor-pointer" href="#" data-toggle="modal" data-target="#deactivation-popup">
                    <h4 class="deactivate-acc-btn pull-right fmlyDetails_text">
                        <span class="glyphicon glyphicon-remove-sign"></span>
                        {!!trans('translator.DeactivateAccount')!!}
                    </h4>
                </a>
            </div>
        </div>
    </div>
    
    @include('templates.popups.deactivatePopup')



