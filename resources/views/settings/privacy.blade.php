<div class="container-fluid">

    <div class="row">
        <div class="col-lg-12">
            <h4 class="fmlyDetails_text"><span class="glyphicon glyphicon-eye-open"></span> {!!trans('translator.PrivacySettings')!!}</h4>
        </div>
    </div>
    <div class="alert alert-success" style="display:none;" role="alert">{!!trans('translator.SettingsUpdated')!!}</div>

    <div class="row frmMat_row">
        <div class="col-sm-3 fmlyDetails_bodyText">
            <div class="fmlyDetails_bodyText">{!!trans('translator.ProfileName')!!} </div>
        </div>
        <div class="col-sm-3 fmlyDetails_bodyText">
            <?php $field="profile_name" ?>
            @include('templates.dropdowns.privacySettings')
        </div>
    </div>
    <div class="row frmMat_row">
        <div class="col-sm-3 fmlyDetails_bodyText">
            <div class="fmlyDetails_bodyText">{!!trans('translator.ProfilePicture')!!} </div>
        </div>
        <div class="col-sm-3 fmlyDetails_bodyText">
            <?php $field="profile_picture" ?>
            @include('templates.dropdowns.privacySettings')
        </div>
    </div>
    <div class="row frmMat_row">
        <div class="col-sm-3 fmlyDetails_bodyText">
            <div class="fmlyDetails_bodyText">{!!trans('translator.FamilyDetails')!!}</div>
        </div>
        <div class="col-sm-3 fmlyDetails_bodyText">
            <?php $field="family_details" ?>
            @include('templates.dropdowns.privacySettings')
        </div>
    </div>
    <div class="row frmMat_row">
        <div class="col-sm-3 fmlyDetails_bodyText">
            <div class="fmlyDetails_bodyText">{!!trans('translator.Location')!!}</div>
        </div>
        <div class="col-sm-3 fmlyDetails_bodyText">
            <?php $field="location" ?>
            @include('templates.dropdowns.privacySettings')
        </div>
    </div>
    <div class="row frmMat_row">
        <div class="col-sm-3 fmlyDetails_bodyText">
            <div class="fmlyDetails_bodyText">{!!trans('translator.Education')!!} {!!trans('translator.AND')!!} {!!trans('translator.Profession')!!}</div>
        </div>
        <div class="col-sm-3 fmlyDetails_bodyText">
            <?php $field="edu_and_prof" ?>
            @include('templates.dropdowns.privacySettings')
        </div>
    </div>
    <div class="row frmMat_row">
        <div class="col-sm-3 fmlyDetails_bodyText">
            <div class="fmlyDetails_bodyText">{!!trans('translator.ContactDetails')!!} </div>
        </div>
        <div class="col-sm-3 fmlyDetails_bodyText">
            <?php $field="contact_info" ?>
            @include('templates.dropdowns.privacySettings')
        </div>
    </div>

    
    <div class="row">
        <div class="col-lg-12">
            <h4 class="fmlyDetails_text"><h4 class="fmlyDetails_text"><span class="glyphicon glyphicon-remove-circle"></span>{!!trans('translator.BlockedUsers')!!}</h4>
        </div>
    </div>

    <div class="row frmMat_row ifNo <?php echo ($blocked_list->isEmpty() ? '' : 'hidden'); ?>">
        <div class="col-sm-6 fmlyDetails_bodyText">
             {!!trans('translator.NoBlockedUsers')!!}
        </div>
    </div>

    @foreach($blocked_list as $blocked_item)

    <?php
    $id = $blocked_item->user->id;
    $fullname = ProfileController::get_ProfileName($id, true);
    $name = str_limit($fullname, 50);
    ?>

    <div class="row frmMat_row user-row-{!!$id!!}">
        <div class="col-sm-4 fmlyDetails_bodyText">
            <label for="block-user-{!!$id!!}">{!!$name!!}</label>
        </div>
        <div class="col-sm-2">
            <input type="button" id="block-user-{!!$id!!}" class="btn-toggle blocked-user" value="{!!trans('translator.Unblock')!!}" data-blocked-user-id="{!!$id!!}">
        </div>
    </div>
    @endforeach

    <script>
        $(function () {
            $('.blocked-user').click(function () {
                var user_id = $(this).attr('data-blocked-user-id');
                $.ajax({
                    url: "{!!URL::route('unblock')!!}",
                    dataType: 'html',
                    data: {id: user_id},
                    cache: false
                }).done(function (obs) {
                    $('.user-row-' + user_id).fadeOut();
                });
                var numItems = $('.blocked-user').length;
                if (numItems === 1) {
                    $('.ifNo').removeClass('hidden');
                }
            });
        });
    </script>
</div>