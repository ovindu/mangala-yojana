@extends('layouts.master')
@section('title')
@if(App::getLocale() == 'en')
{!!Lang::get('translator.title' , array('page' => 'Account Settings'))!!}
@else
{!!Lang::get('translator.title' , array('page' => 'කට්ටල අංග'))!!}
@endif
@stop
@section('content')
@include('layouts.nav')
<?php $payment_id = (Session::has('payment_id') ? Session::get('payment_id') : null ); ?>
<div class="container bodycontainer">
    <div  class="row">
        <div  class="col-lg-12">
            <div role="tabpanel">
                
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="{!!$gen!!}"><a href="general" >{!!trans('translator.General')!!}</a></li>
                    <li role="presentation" class="{!!$pri!!}"><a href="privacy">{!!trans('translator.Privacy')!!}</a></li>
                    <li role="presentation" class="{!!$noti!!}"><a href="notifications">{!!trans('translator.Notifications')!!}</a></li>
                    <li role="presentation" class="{!!$pymnt!!}">
                        <a href="payments">{!!trans('translator.Payments')!!}</a>
                    </li>
                    @if(isset($pkgUp) && $pkgUp == 'active' && isset($payment_id) && Common::getTxnID($payment_id))
                    <li role="presentation" class="{!!$pkgUp!!}"><a href="package-upgrade">{!!trans('translator.ThankYouPage')!!}</a></li>
                    @endif
                    
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tabPane tab-pane {!!$gen!!}" id="home">
                        @include('settings.account')
                    </div>
                    <div role="tabpanel" class="tabPane tab-pane {!!$pri!!}" id="profile">
                        @include('settings.privacy')
                    </div>
                    <div role="tabpanel" class="tabPane tab-pane {!!$noti!!}" id="messages">
                        @include('settings.notifications')
                    </div>
                    @if(isset($pkgUp) && $pkgUp == 'active' && Common::getTxnID($payment_id))
                    <div role="tabpanel" class="tabPane tab-pane {!!$pkgUp!!}" id="pkg-upgrade">
                        @include('settings.upgradeThankYou')
                    </div>
                    @endif
                    <div role="tabpanel" class="tabPane tab-pane {!!$pymnt!!}" id="payments">
                        @include('settings.payments')
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
</div>

@stop
