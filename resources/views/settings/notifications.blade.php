<form method="post" action="{!!URL::route('emailSettings')!!}">
    {!!Form::token()!!}

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12"><h4 class="fmlyDetails_text"><span class="glyphicon glyphicon-bullhorn"></span>{!!trans('translator.EmailNotifications')!!}</h4></div>
            <div class="col-lg-12"><h6 class="fmlyDetails_text">{!!trans('translator.SendMailSoon')!!}</h6></div>
        </div>
        <div class="row frmMat_row">
            <label for="new-request" class="cursor-pointer col-sm-5 fmlyDetails_bodyText">{!!trans('translator.SomeoneSendYojana')!!}</label>
            <div class="col-sm-1">
                {!!Form::checkbox('new-request', $mail_settings['new_request'], $mail_settings['new_request'],array('id'=>'new-request','class'=>'mail-settings-chkbox cursor-pointer'));!!}
            </div>
        </div>
        <div class="row frmMat_row">
            <label for="new-msg" class="cursor-pointer col-sm-5 fmlyDetails_bodyText">{!!trans('translator.newMessageReceived')!!}</label>
            <div class="col-sm-1">
                {!!Form::checkbox('new-msg', $mail_settings['new_msg'], $mail_settings['new_msg'],array('id'=>'new-msg','class'=>'mail-settings-chkbox cursor-pointer'));!!}
            </div>
        </div>
        <div class="row frmMat_row">
            <label for="became-friends" class="cursor-pointer col-sm-5 fmlyDetails_bodyText">{!!trans('translator.AcceptYojana')!!}</label>
            <div class="col-sm-1">
                {!!Form::checkbox('became-friends', $mail_settings['became_friends'], $mail_settings['became_friends'],array('id'=>'became-friends','class'=>'mail-settings-chkbox cursor-pointer'));!!}
            </div>
        </div>
        <div class="row frmMat_row">
            <label for="profile_changed" class="cursor-pointer col-sm-5 fmlyDetails_bodyText">{!!trans('translator.ProfileChange')!!}</label>
            <div class="col-sm-1">
                {!!Form::checkbox('profile-changed', $mail_settings['profile_changed'], $mail_settings['profile_changed'],array('id'=>'profile_changed','class'=>'mail-settings-chkbox cursor-pointer'));!!}
            </div>
        </div>



        <div class="container-fluid">

            <div class="row frmMat_row  btn-row">
                <div class="col-lg-3 col-md-3 col-lg-offset-2 col-md-offset-2">
                    <button type="submit" class="btn-details fmlyDetails_txtbox">{!!trans('translator.SubmitForm')!!}</button>
                </div>

            </div>

        </div>

        <script>
            $(function () {
                $('.mail-settings-chkbox').change(function () {
                    if ($(this).prop('checked')) {
                        $(this).val('1');
                    } else {
                        $(this).val('0');
                    }
                });
            });
        </script>
    </div>
        {!!Form::close()!!}

