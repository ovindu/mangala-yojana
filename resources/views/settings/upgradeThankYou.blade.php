<div class="container-fluid">
    
    
    <div class="row">
        <div class="col-lg-8">
            <h4 style="line-height: 1.2;">{!!trans('translator.upgradeThankyouTitle')!!} </h4>
            <ul>
                <li>
                    {!!trans('translator.nowYouCan')!!} {!!trans('translator.SendUnlimitedProposals')!!}
                </li>
                <li>
                    {!!trans('translator.InstantMessaging')!!}
                </li>
                <li>
                   {!!trans('translator.ImprovedPrivacy')!!}
                </li>
                <li>
                   {!!trans('translator.DiscountsfromOurPartnersmore')!!}
                </li>
            </ul>
            <span class="help-block">{!!trans('translator.yourTransID')!!} : {!!Common::getTxnID($payment_id)!!}</span>
        </div>
        <div class="col-lg-4">
            <img class="center-block img-responsive img-thankyou" src="{!!asset('img/thankyou.png')!!}" alt="Mangalayojana.lk">
        </div>
    </div>
    <div class="col-lg-12">
        <div class="">
            
            
        </div>
    </div>
</div>