@section('head')
<?php
$sender_flname = explode(' ', $sender_name);
$accountCtrl = new AccountController;
?>
@stop
@extends('emails.master')
@section('title')
Marriage Proposal from {!!$sender_flname[0]!!}
@stop

@section('body')
<p style="text-align: justify; font-size: 15px; line-height: 1.4; color:#363636; letter-spacing: -0.5;">
    {!!$sender_name!!} has sent you a Mangalayojana! Click the button below to respond.
</p>
<a href="{!!URL::route('makeFriends', array('utm' => $accountCtrl->generateRandomString(), 'id' => $id, 'retro' => $accountCtrl->generateRandomString(25), 'public-prof-path' => URL::route('publicProfile',array('id'=>$id))))!!}" style="text-decoration: none; margin:0 auto; text-align: center; display: table; padding:10px 45px; color:#fff; background: #00C0FE; border: transparent; font-size: 18px; font-weight: 600;">Respond</a>
@stop
@section('footer')
<p style="padding-top: 10px; text-align: center;">
    <span style="font-size: 12px; color:#7E7E7E;">We always value your privacy:</span>
    <span style="font-size: 12px; color:#909090;">If someone spams or bothers you in any way, simply go to the corresponding profile and report us by clicking Report button.</span>
</p>
@stop