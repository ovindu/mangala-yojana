@extends('emails.master')
@section('title')
Message from {!!$sender_name!!}
@stop
@section('head')
<?php
$sender_flname = explode(' ', $sender_name);
$accountCtrl = new AccountController;
?>
@stop
@section('body')
<p style=" text-align: justify; font-size: 15px; line-height: 1.4; color:#363636; letter-spacing: -0.5;">
    A message from {!!$sender_flname[0]!!} is waiting for your reply.
</p>
<p style="margin-left: 30px; margin-bottom: 30px; text-align: justify; font-size: 15px; line-height: 1.4; color:#626262; letter-spacing: -0.5; background: #EFEFEF; padding: 13px;
   font-style: italic;">
    {!!str_limit($msg_body,150)!!}
</p>
<a href="{!!URL::route('messagePage', array('utm_mj' => $accountCtrl->generateRandomString(), 'wqcert' => $accountCtrl->generateRandomString(45), 'btn' => $accountCtrl->generateRandomString(25), 'retro' => $accountCtrl->generateRandomString(30), 'userid' => $sender_id, 'oop' => $accountCtrl->generateRandomString(45)))!!}" style="text-decoration: none; margin:0 auto; text-align: center; display: table; padding:10px 45px; color:#fff; background: #00C0FE; border: transparent; font-size: 18px; font-weight: 600;">View and Reply</a>
@stop
@section('footer')
<!--<p style="padding-top: 20px;">
    <span style="font-size: 12px; color:#7E7E7E;">This message has sent to {!!$receiver_email!!}</span>
</p>-->
<p style="padding-top: 10px; text-align: center;">
    <span style="font-size: 12px; color:#7E7E7E;">We always value your privacy:</span>
    <span style="font-size: 12px; color:#909090;">If someone spams or bothers you in any way, simply go to the corresponding profile and report us by clicking Report button.</span>
</p>
@stop