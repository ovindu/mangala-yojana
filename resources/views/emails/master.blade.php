<html>
    <head>
        
    </head>
    <body style="font-family: 'Verdana';">
        @yield ('head')
        <table border="0" width="100%">
            <tr>
                <td>
                    <a href="{!!URL::route('homePage')!!}" target="_blank" tabindex="1">
                    <img style="margin:0 auto; display: table; margin-bottom: 15px;" alt="Mangalayojana" src="{!!asset('img/logo.png')!!}">
                    </a>
                </td>
            </tr>
            <tr style="background: #fafafa">

            </tr>
            <tr style="background: #fafafa; ">
                <td style="text-align: center;">
                    <table border="0" style="width: 93%; max-width: 565px; margin: 0 auto;">
                        <tr>
                            <td><div style="font-weight: 600; text-align: center; font-size:20px; color: #656565; margin: 20px;" >@yield ('title')</div></td>
                        </tr>
                        <tr>
                            <td>
                                <p style=" text-align: justify; font-size: 16px; line-height: 1.4; color:#363636; letter-spacing: -0.5;">
                                    Hi {!!$fname!!},
                                </p>
                                @yield ('body')
                                <br/>

                            </td>
                        </tr>
                        <tr>
                            <td>

                            </td>
                        </tr>
                    </table>
                </td>
            </tr> 
            <tr style="background: #ebeaea;">
                <td style="">
                    <div style="max-width: 565px; width: 93%; margin: 0 auto;">
                        @yield ('footer')

                        <div id='socialIcons' style="margin: 0 auto; display: table; margin-top: 20px;">
                            <a style="text-decoration: none;" href="http://twitter.com/mangalayojana"> <img src='{!!asset('img/icons/tw-icon.png')!!}'></a>
                            <a style="text-decoration: none;" href="http://facebook.com/mangalayojana"> <img src='{!!asset('img/icons/fb-icon.png')!!}'></a>
                            <a style="text-decoration: none;" href="http://youtube.com/mangalayojana"> <img src='{!!asset('img/icons/yt-icon.png')!!}'></a>
                            <a style="text-decoration: none;" href="http://plus.google.com/+mangalayojana"> <img src='{!!asset('img/icons/gp-icon.png')!!}'></a>

                        </div>
                        <p style="text-align: center; font-size: 12px; line-height: 0.5; color:#242424; letter-spacing: -0.5;">Have a question? <a href="#" style="color:#242424;">Read our FAQ</a>
                        <p style="text-align: center; font-size: 12px; line-height: 0.5; color:#242424; letter-spacing: -0.5;">If you don't want these mails, you can <a href="{!! URL::route('showSettings', array('tab' => 'notifications' ,'ref' => 'email', 'action' => 'unsubscribe'))!!}">unsubscribe</a> here.</p>
                        <p style="text-align: center; font-size: 12px; line-height: 0.5; color:#242424; letter-spacing: -0.5;padding-bottom: 10px;">Copyright &copy; <a href="{!! URL::route('homePage', array('ref' => 'email', 'action' => 'unsubscribe'))!!}">Mangalayojana.lk</a>, All rights reserved.</p>
                    </div>
                </td>
            </tr>
        </table>
    </body>
</html>