@extends('emails.master')
@section('head')
<?php
$accountCtrl = new AccountController;
?>
@stop

@section('title')
Attention: Your Account is Deactivated!
@stop

@section('body')
<p style=" text-align: justify; font-size: 15px; line-height: 1.4; color:#363636; letter-spacing: -0.5;">{!!$reason!!}</p>

<p style=" text-align: justify; font-size: 15px; line-height: 1.4; color:#363636; letter-spacing: -0.5;">
    Please click the button below to verify your email address and continue on finding your life partner.
</p>

<a href="@if(MailController::$unverified_email_deactivation == $mail_type) {!!URL::route('Mail_confirmUser', array('cen' => $accountCtrl->generateRandomString(), 'id' => $id, 'haben' => $accountCtrl->generateRandomString(25), 'confirm' => $accountCtrl->generateRandomString(25), 'code' => $code, 'source' => $accountCtrl->generateRandomString(45)))!!} @elseif(MailController::$unverified_mobile_number_deactivation == $mail_type) {!!URL::Route('Verify_from_mail' , array('rand' =>$accountCtrl->generateRandomString(3),'id' => $id , 'code' => $code ))!!}  @endif" style="text-decoration: none; margin:0 auto; text-align: center; display: table; padding:10px 45px; color:#fff; background: #00C0FE; border: transparent; font-size: 18px; font-weight: 600;">Confirm</a>
<br/>
<p style="font-size: 12px; color:#adadad;">
    or copy, paste and go to the link below if your browser or email client doesn't supports. <br/> @if(MailController::$unverified_email_deactivation == $mail_type) {!!URL::route('Mail_confirmUser', array('do' => $accountCtrl->generateRandomString(5), 'id' => $id, 'guid' => $accountCtrl->generateRandomString(3), 'code' => $code, 'certify' => $accountCtrl->generateRandomString(12)))!!} @elseif(MailController::$unverified_mobile_number_deactivation == $mail_type) {!!URL::Route('Verify_from_mail' , array('rand' =>$accountCtrl->generateRandomString(3),'id' => $id , 'code' => $code ))!!} @endif
</p>
@stop

@section('footer')
<p style="padding-top: 10px; text-align: center;">
    <span style="font-size: 12px; color:#7E7E7E;">We always value your privacy:</span>
    <span style="font-size: 12px; color:#909090;">Please contact our support team for any information via <a href="mailto:support@mangalayojana.lk">support@mangalayojana.lk</a></span>
</p>
@stop