@extends('emails.master')
@section('head')
<?php
$accountCtrl = new AccountController;
?>
@stop

@section('title')
Attention: Your Package Expires Soon!
@stop
@section('body')
<p style=" text-align: justify; font-size: 15px; line-height: 1.4; color:#363636; letter-spacing: -0.5;">
    Please note that your mangalayojana.lk premium package going to expire. to stop suspending your account, please settle the unpaid
    invoices.</p>
<p style=" text-align: justify; font-size: 15px; line-height: 1.4; color:#363636; letter-spacing: -0.5;">
    Please click the button below to show your payment details.
</p>
<a href="{!!URL::route('showSettings', array('tab' => 'payments'))!!}" style="text-decoration: none; margin:0 auto; text-align: center; display: table; padding:10px 45px; color:#fff; background: #00C0FE; border: transparent; font-size: 18px; font-weight: 600;">Show Invoice</a>
<br/>
<p style="font-size: 12px; color:#adadad;">	
	Please note, if you click the above button and it appears to be broken, please copy and paste <a href="{!!URL::route('showSettings', array('payments'))!!}">{!!URL::route('showSettings', array('payments'))!!}</a> into a new browser window. If you did not create this profile, please ignore this message.
</p>
@stop

@section('footer')
<p style="padding-top: 10px; text-align: center;">
    <span style="font-size: 12px; color:#7E7E7E;">We always value your privacy:</span>
    <span style="font-size: 12px; color:#909090;">Please contact our support team for any information via <a href="mailto:support@mangalayojana.lk">support@mangalayojana.lk</a></span>
</p>
@stop