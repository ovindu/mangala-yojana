@section('head')
<?php
$sender_flname = explode(' ', $name);
$accountCtrl = new AccountController;
?>
@stop
@extends('emails.master')
@section('title')
Reset your Password
@stop

@section('body')
<p style="text-align: justify; font-size: 15px; line-height: 1.4; color:#363636; letter-spacing: -0.5;">
    We've received a password reset request. Please follow the button below to get your account access back.
</p>
<a href="{!!URL::route('checkPasswordResetToken', array('utm' => $accountCtrl->generateRandomString(), 'source' => $accountCtrl->generateRandomString(45), 'btn' => $accountCtrl->generateRandomString(25), 'id' => $id, 're' => $accountCtrl->generateRandomString(30), 'token' => $code, 'oop' => $accountCtrl->generateRandomString(45)))!!}" style="text-decoration: none; margin:0 auto; text-align: center; display: table; padding:10px 45px; color:#fff; background: #00C0FE; border: transparent; font-size: 18px; font-weight: 600;">Reset Password</a>
@stop
@section('footer')
<p style="padding-top: 1px;">
    <span style="font-size: 12px; color:#7E7E7E;">We always value your privacy:</span>
    <span style="font-size: 12px; color:#909090;">If you feel your account has compromised, change and protect account details. Never handover them to any third-party. If something is wrong don't hesitate to let us know via <a href="mailto:info@mangalayojana.lk">support@mangalayojana.lk</a>.</span>
</p>
@stop