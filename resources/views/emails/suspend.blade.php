@extends('emails.master')
@section('head')
<?php
$accountCtrl = new AccountController;
?>
@stop

@section('title')
Attention: Your Account is Suspended!
@stop
@section('body')
<p style=" text-align: justify; font-size: 15px; line-height: 1.4; color:#363636; letter-spacing: -0.5;">
    Please note that your account has been suspended due to your package renewing. You are no longer able to
    log-in to mangalayojana.lk account. Please settle your payments to re-activate your account.</p>
@stop

@section('footer')
<p style="padding-top: 10px; text-align: center;">
    <span style="font-size: 12px; color:#7E7E7E;">We always value your privacy:</span>
    <span style="font-size: 12px; color:#909090;">Please contact our support team for any information via <a href="mailto:support@mangalayojana.lk">support@mangalayojana.lk</a></span>
</p>
@stop