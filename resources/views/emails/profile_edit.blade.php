@extends('emails.master')

@section('head')
<?php
$accountCtrl = new AccountController;
?>
@stop

@section('title')
Profile Details Changed
@stop

@section('body')
<p style=" text-align: justify; font-size: 15px; line-height: 1.4; color:#363636; letter-spacing: -0.5;">
    Your profile details has changed right now. If it wasn't you, get an action soon to protect your account by changing password. Never give your sensitive information to a third-party person next time.</p>
<p style=" text-align: justify; font-size: 15px; line-height: 1.4; color:#363636; letter-spacing: -0.5;">
    Click the button below to view your profile.
</p>
<a href="{!!URL::route('profilePage',array('utm_mj'=>$accountCtrl->generateRandomString(),'wqcert'=>$accountCtrl->generateRandomString(45),'id'=>$id,'confirm'=>$accountCtrl->generateRandomString(25),'haben'=>$accountCtrl->generateRandomString(50)))!!}" style="text-decoration: none; margin:0 auto; text-align: center; display: table; padding:10px 45px; color:#fff; background: #00C0FE; border: transparent; font-size: 18px; font-weight: 600;">View Profile</a>
<br/>
<p style="font-size: 12px; color:#adadad;">
    Please ignore this message if you are ok with the details changed by you/parent/sibling or friend.
</p>
@stop

@section('footer')
<p style="padding-top: 10px; text-align: center;">
    <span style="font-size: 12px; color:#7E7E7E;">We always value your privacy:</span>
    <span style="font-size: 12px; color:#909090;">If you feel your account has compromised, change and protect account details. Never handover them to any third-party. If something is wrong don't hesitate to let us know via <a href="mailto:info@mangalayojana.lk">support@mangalayojana.lk</a>.</span>
</p>
@stop