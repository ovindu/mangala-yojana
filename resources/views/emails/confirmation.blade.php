@extends('emails.master')

@section('head')
<?php
$accountCtrl = new AccountController;
?>
@stop

@section('title')
Welcome to MangalaYojana.lk
@stop

@section('body')
<p style=" text-align: justify; font-size: 15px; line-height: 1.4; color:#363636; letter-spacing: -0.5;">
    You've successfuly registered with Mangalayojana.lk. Thank you for signing in! Start finding your life partner by completing your profile and then using our search with a wide criteria. But one more step ahead.</p>
<p style=" text-align: justify; font-size: 15px; line-height: 1.4; color:#363636; letter-spacing: -0.5;">
    Click the verify button to verify your email address.
</p>
<a href="{!!URL::route('Mail_confirmUser', array('do' => $accountCtrl->generateRandomString(5), 'id' => $id, 'guid' => $accountCtrl->generateRandomString(3), 'code' => $code, 'certify' => $accountCtrl->generateRandomString(12)))!!}" style="text-decoration: none; margin:0 auto; text-align: center; display: table; padding:10px 45px; color:#fff; background: #00C0FE; border: transparent; font-size: 18px; font-weight: 600;">Verify</a>
<br/>
<p style="font-size: 12px; color:#adadad;">
	Please note, if you click the above button and it appears to be broken, please copy and paste <a href="{!!URL::route('Mail_confirmUser', array('do' => $accountCtrl->generateRandomString(5), 'id' => $id, 'guid' => $accountCtrl->generateRandomString(3), 'code' => $code, 'certify' => $accountCtrl->generateRandomString(12)))!!}">this verification link</a> into a new browser window. If you did not create this profile, please ignore this message.
</p>
@stop

@section('footer')
<p style="padding-top: 10px; text-align: center;">
    <span style="font-size: 12px; color:#7E7E7E;">We always value your privacy:</span>
    <span style="font-size: 12px; color:#909090;">Your{!!$salute!!} has created this account and if you don't want to have this account or if you don't need this account, please contact our support team via <a href="mailto:support@mangalayojana.lk">support@mangalayojana.lk</a></span>
</p>
@stop