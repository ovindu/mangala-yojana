@extends('emails.master')

@section('head')
<?php
$accountCtrl = new AccountController;
?>
@stop

@section('title')
Your payment has been completed!
@stop
@section('body')
<?php $payment = Payment::find($payment_id); ?>

<p style=" text-align: justify; font-size: 15px; line-height: 1.4; color:#363636; letter-spacing: -0.5;">
    You've just completed a payment on MangalaYojana.lk and Your MangalaYojana.lk package has been updated to Premium. Hope you enjoy the Premium Package. Now you are free for send Unlimited messages, Instant Messaging, Choose Improved Privacy and get discounts from Our Partners.</p>
<p style=" text-align: justify; font-size: 15px; line-height: 1.4; color:#363636; letter-spacing: -0.5;">
    Here are the summery :
</p>
<ol style=" text-align: justify; font-size: 15px; line-height: 1.4; color:#363636; letter-spacing: -0.5;">
    <li>Payment Method : {!!$payment->pay_method!!}</li>
    <li>Payment Transaction ID : {!!$payment->txn_id!!}</li>
    <li>Timestamp : {!!$payment->created_at!!}</li>
</ol>
@stop
@section('footer')
<p style="padding-top: 10px; text-align: center;">
    <span style="font-size: 12px; color:#7E7E7E;">We Always Value Your Privacy:</span>
    <span style="font-size: 12px; color:#909090;">Your{!!$salute!!} has created this account and if you don't want to have this account, please contact our Support Team via <a href="mailto:support@mangalayojana.lk">support@mangalayojana.lk</a></span>
</p>
@stop