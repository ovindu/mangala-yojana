@extends('layouts.master')
@section('title')
{!!trans('translator.faq')!!} | MangalaYojana.LK
@stop
@section('content')

<div class='container bodycontainer'> 
    <div class='row'>
        <div class='col-lg-12'>
            <h1 id="faq-title">{!!trans('translator.faq')!!}</h1>
        </div>
    </div>

    <div class='row'>
        <div class='col-lg-12'>


            <div class="panel-group " id="faq1" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default custom-panel custom-panel-group border-blue" data-id='c1' >
                    <div class="panel-heading custom-panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title custom-panel-title">
                            <a class="custom-panel-anc" role="button" child-id='c1' data-toggle="collapse" data-parent="#faq1" href="#c1" aria-expanded="true" aria-controls="collapseOne">
                                How I send a request?
                            </a>
                        </h4>
                    </div>
                    <div id="c1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body custom-panel-body">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                        </div>
                    </div>
                </div>
            </div>



            <div class="panel panel-default custom-panel custom-panel-group border-brown" data-id='c2'  id="faq2" >
                <div class="panel-heading custom-panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title custom-panel-title">
                        <a class="custom-panel-anc" role="button" child-id='c2' data-toggle="collapse" data-parent="#faq2" href="#c2" aria-expanded="true" aria-controls="CollapseTwo">
                            How many request can I send per day?
                        </a>
                    </h4>
                </div>
                <div id="c2" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body custom-panel-body">
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </div>
                </div>
            </div>


        </div>


    </div>
</div>

</div>
<script>
    $(function () {
        $("#accordion").accordion({
            collapsible: true,
            heightStyle: "content",
            icons: false
        });
    });
</script>
@stop