@extends('layouts.master')
@section('title')
404 Not found | MangalaYojana.LK
@stop
@section('content')
<div class="container bodycontainer">
    <div class="row">
        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
            {!!HTML::image(asset('img/plastered heart.png'), null, array('class' => 'img-responsive','draggable'=>'false'))!!}
        </div>
		<div class='col-lg-7 col-md-7 col-sm-7 col-xs-12 ' id='not_found_styles'>
			<h1>{!!trans('translator.Uhoh')!!}</h1>
			<h2>{!!trans('translator.pageCouldnotFind')!!}</h2>
			<h4>
				@if(Auth::check())
				{!!trans('translator.Auth404Note')!!}	
				@else
				{!!trans('translator.notAuth404Note')!!}
				@endif
			</h4>
			@if(Auth::check())
			<a href='{!!URL::previous()!!}'><button type='button' >{!!trans('translator.GoBack')!!}</button></a>
			@else
			<a href='{!!URL::route('homePage')!!}'><button type='button' >{!!trans('translator.SignUp')!!}</button></a>
			
			@endif
		</div>
    </div>
</div>
@stop