@extends('layouts.master')
@section('title')
Unpaid Invoices | MangalaYojana.LK
@stop
@section('content')
<div class="container">
    @if(Common::getUserPaidInvoices() != null | Common::getUserUnPaidInvoices() != null)
    <div class="row">
        <div class="col-lg-12">
            @if($unpaid_invoices)
            <h4 style="line-height: 1.2;">{!!trans('translator.invoicesToPaid')!!} </h4>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>{!!trans('translator.InvoiceNo')!!}</th>
                        <th>{!!trans('translator.Amount')!!}</th>
                        <th>{!!trans('translator.Description')!!}</th>
                        <th class="hidden-xs">{!!trans('translator.InvoiceDate')!!}</th>
                        <th>{!!trans('translator.pay')!!}</th>
                    </tr>

                </thead>
                <tbody>
                    @foreach($unpaid_invoices as $invoice)
                    <?php $payment = Payment::where('invoice_id', $invoice->id)->first(); ?>
                    <tr>
                        <th scope="row">{!!'INV'.$invoice->id!!}</th>
                        <td>{!!$invoice->amount.' ('.trans('translator.LKR').''!!})</td>
                        <td>{!!trans('translator.renewCurrentPackage')!!}</td>
                        <td class="hidden-xs">{!!$invoice->created_at!!}</td>
                        <td>
                            <a href="#" data-pp-url='{!!URL::route('PaymentActorController',array("type"=>'paypal','invoice_id' => $invoice->id,'userid'=>Auth::id()))!!}' data-toggle="modal" data-target="#upgrade-model">
                            <button class="formSub btn-paynow center">{!!trans('translator.payNowBtn')!!}</button>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
            @if($paid_invoices)
            <h4 style="line-height: 1.2;">{!!trans('translator.paidInvoices')!!} </h4>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>{!!trans('translator.InvoiceNo')!!}</th>
                        <th class="hidden-xs">{!!trans('translator.PaymentType')!!}</th>
                        <th>{!!trans('translator.Amount')!!}</th>
                        <th>{!!trans('translator.ValidUntil')!!}</th>
                        <th class="hidden-xs">{!!trans('translator.TransactionID')!!}</th>
                        <th class="hidden-xs">{!!trans('translator.InvoiceDate')!!}</th>
                        <th class="hidden-xs">{!!trans('translator.PaidDate')!!}</th>
                    </tr>

                </thead>
                <tbody>
                    @foreach($paid_invoices as $invoice)
                    <?php $payment = Payment::where('invoice_id', $invoice->id)->first(); ?>
                    <tr>
                        <th scope="row">{!!'INV'.$invoice->id!!}</th>
                        <td class="hidden-xs">{!!$payment->pay_method!!}</td>
                        <td>{!! ($payment->pay_method == 'paypal' ? trans('translator.USD').' '.$invoice->amount : $invoice->amount )!!}</td>
                        <td>{!!$invoice->due_date!!}</td>
                        <td class="hidden-xs">{!!$payment->txn_id!!}</td>
                        <td class="hidden-xs">{!!$invoice->created_at!!}</td>
                        <td class="hidden-xs">{!!$payment->created_at!!}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
        </div>
    </div>
    @else
    <div class="row">
        <div class="col-lg-12">
            <p class="help-block">{!!trans('translator.noInvoicesNotice')!!} </p>
        </div>
    </div>
    @endif
    @include('templates.popups.upgradePopup');
    <script>
        $("a[data-target='#upgrade-model']").click(function(){
            var url = $(this).attr('data-pp-url');
            $("input[name='return']").val(url);
            $("input[name='notify_url']").val(url);
        });
        
    </script>
</div>
@stop