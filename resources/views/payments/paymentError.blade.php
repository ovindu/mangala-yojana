@extends('layouts.master')
@section('title')
Payment Error | Mangalayojana.lk
@stop
@section('content')
@include('layouts.nav')
<div class="container-fluid bodycontainer">
    <div class="row ">
        <div class="col-lg-12">
            <div class="border" style="background-color: white;">
                <div class="errorpage-padding">
                    <div class="row">
                        <div class="col-lg-8">
                            <h4>Something wrong with your payment informations.</h4>
                            <ul>
                                <li>
                                    Double check your account information.
                                </li>
                                <li>
                                    Enter your correct details.
                                </li>
                                <li>
                                    Make sure your account have enough balance for payment.
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-4">
                            <img class="center-block img-responsive img-brokenHeart" src="{!!asset('img/brockenheart.png')!!}" alt="Mangalayojana.lk">
                        </div>
                    </div>
                    <div class="center">
                        <span style="color: #737373;">Accidentally if you have a message for Mangalayojana Team leave it here.</span><br>
                        <button class="btn btn-default margin-btm-15 msgbtn"><i class="glyphicon glyphicon-envelope"></i> Leave a Message</button>
                    </div>
                    <div class="display-none messageArea ">
                        {!!Form::open(['url'=>URL::route('submitUserReport')])!!}
                        <div>
                            <textarea placeholder="Enter message for Team Mangalayojana.lk" rows="4" class="@if(Session::has('class')) {!!Session::get('class')!!} @endif form-control resize-none margin-btm-5" name="message"></textarea>
                        </div>
                        <button type="submit" class="btn btn-success margin-btm-15 ">Submit Message</button>
                        {!!Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    $('.msgbtn').click(function() {
        $('.messageArea').slideDown({duration: 600, queue: false});
    });

</script>
@if(Session::has('class'))
{!!"
<script>
    $('.messageArea').slideDown({duration: 600, queue: false});
</script> 
"!!}
@endif

@stop