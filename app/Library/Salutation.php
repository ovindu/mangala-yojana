<?php

namespace App\Library;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Salutation
 *
 * @author Thambaru Wijesekara
 */
class Salutation
{

    public static function salute($whom)
    {
        switch ($whom) {
            case 1:
                return $showhom = trans('translator.self');
                break;
            case 2:
                return $showhom = trans('translator.son');
                break;
            case 3:
                return $showhom = trans('translator.daughter');
                break;
            case 4:
                return $showhom = trans('translator.sibling');
                break;
            case 5:
                return $showhom = trans('translator.friend');
                break;
            case 6:
                return $showhom = trans('translator.relative');
                break;
            case null:
                return $showhom = ' null';
                break;
        }
    }
    
    public static function mail_salutation($whom)
    {
        switch ($whom) {
            case 1:
                return $showhom = 'self';
                break;
            case 2:
                return $showhom = ' Parent';
                break;
            case 3:
                return $showhom = ' Parent';
                break;
            case 4:
                return $showhom = ' Sibling';
                break;
            case 5:
                return $showhom = ' Friend';
                break;
            case 6:
                return $showhom = ' Relative';
                break;
            case null:
                return $showhom = ' null';
                break;
        }
    }
}
