<?php
namespace App\Library;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Form_Matrimonial
 *
 * @author Thambaru Wijesekara
 */
class FormMatrimonial
{

    public static function get_fatherStatus()
    {
        return [
            '' => trans('translator.Select'),
            '1' => trans('translator.Employed'),
            '2' => trans('translator.Business'),
            '3' => trans('translator.Professional'),
            '4' => trans('translator.Retired'),
            '5' => trans('translator.NotEmployed'),
            '6' => trans('translator.PassedAway')
        ];
    }

    public static function get_motherStatus()
    {
        return [
            '' => trans('translator.Select'),
            '1' => trans('translator.Employed'),
            '2' => trans('translator.Business'),
            '3' => trans('translator.Professional'),
            '4' => trans('translator.Retired'),
            '5' => trans('translator.NotEmployed'),
            '6' => trans('translator.PassedAway')
        ];
    }

    public static function get_familyValue()
    {
        return [
            '1' => trans('translator.Traditional'),
            '2' => trans('translator.Moderate'),
            '3' => trans('translator.Liberal')
        ];
    }

    public static function get_affluenceLevel()
    {
        return [
            '1' => trans('translator.Affluent'),
            '2' => trans('translator.UpperMiddleClass'),
            '3' => trans('translator.MiddleClass'),
            '4' => trans('translator.LowerMiddleClass'),
        ];
    }

    public static function get_blood_group()
    {
        return [
            '1' => trans('translator.DontKnow'),
            '2' => 'A+',
            '3' => 'A-',
            '4' => 'B+',
            '5' => 'B-',
            '6' => 'AB+',
            '7' => 'AB-',
            '8' => 'O+',
            '9' => 'O-',
        ];
    }

    public static function get_health()
    {
        return [
            '' => trans('translator.Select'),
            '1' => trans('translator.NoHealthProblems'),
            '2' => trans('translator.HIVpositive'),
            '3' => trans('translator.Diabetes'),
            '4' => trans('translator.LowBP'),
            '5' => trans('translator.HighBP'),
            '6' => trans('translator.HeartAilments'),
            '7' => trans('translator.OtherHelth')
        ];
    }
}
