<?php
namespace App\Library;

use App\Models\CommonSetting;
use App\Models\Searchuser;
use App\Models\PrivacySetting;
use App\Models\User;
use App\Models\Profile;
use App\Models\UserSetting;
use App\Models\Report;
use App\Models\Matches;
use App\Models\Permissions;
use App\Models\WorldCities;
use App\Models\AdminReviews;
use App\Models\MobileVerifications;
use App\Http\Controllers\PrivacyController;
use App\Http\Controllers\FriendController;
use App\Http\Controllers\ProfileController;
use Lang;
use Auth;
use DB;
use Schema;
use App\Models\Invoice;
use DateTime;

class Common
{
    /*
     * Controll permissions.
     * Do not change values.
     */

    public static $permissions_messaging = 'Messaging';
    public static $permissions_show_inbox = 'ShowInbox';
    public static $permissions_showWhoisOnline = 'WhoIsOnline';
    public static $permissions_chat = 'Chat';
    public static $permissions_call = 'Call';
    public static $basic_user = 1; // Related with user roles DB.roles
    public static $site_admin_user = 2; // Related with user roles DB.roles
    public static $premium_user = 3; // Related with user roles DB.roles
    public static $minimumAgeLimit = 18;
    public static $DEFAULT_PACKAGE_FOR_PREMIUM_USER = 2;
    public static $PRIV_COL_PROFILE_PIC = 'profile_picture', $PRIV_COL_FAMILY_DETAILS = 'family_details', $PRIV_COL_LOCATION = 'location', $PRIV_COL_CONTACT_INFO = 'contact_info', $PRIV_COL_PROF_NAME = 'profile_name', $PRIV_COL_EDU_AND_PROF = 'edu_and_prof';
    public static $PRIV_STP_REGISTERED_USERS_ONLY = 0, $PRIV_STP_FRIENDS_ONLY = 1, $PRIV_STP_PUBLIC = 2, $PRIV_STP_ONLY_ME = 3;

    public static function setPrivacyinBorn($user_id = null)
    {
        if ($user_id == null) {
            if (Auth::check()) {
                $user_id = Auth::id();
            } else {
                return false;
            }
        }
        $columns_ar = [self::$PRIV_COL_FAMILY_DETAILS, self::$PRIV_COL_LOCATION, self::$PRIV_COL_CONTACT_INFO, self::$PRIV_COL_PROF_NAME, self::$PRIV_COL_PROFILE_PIC, self::$PRIV_COL_EDU_AND_PROF];
        $p_values_r = [self::$PRIV_STP_REGISTERED_USERS_ONLY, self::$PRIV_STP_FRIENDS_ONLY, self::$PRIV_STP_FRIENDS_ONLY, self::$PRIV_STP_REGISTERED_USERS_ONLY, self::$PRIV_STP_PUBLIC, self::$PRIV_STP_REGISTERED_USERS_ONLY];
        $priv_settings = PrivacySetting::where('user_id', $user_id)->first();
        $determine = $priv_settings;
        $priv_settings = (empty($priv_settings) ? new PrivacySetting() : $priv_settings);
        $priv_settings->user_id = $user_id;
        $do = (empty($determine) ? 'save' : 'update');
        $count_each = 0;
        foreach ($columns_ar as $column) {
            $priv_settings->$column = $p_values_r[$count_each];
            $count_each++;
        }
        ($do == 'save' ? $priv_settings->save() : $priv_settings->update());
        return true;
    }

    public static function getUserPublicName($user_id, $self = false, $get_both_names = true)
    {
        $name = '';
        $privacy = PrivacySetting::where('user_id', $user_id)->first();
        if (Auth::check()) {
            if (!empty($privacy) && $privacy->profile_name == 0 | $self) {
                $user = User::find($user_id);
                $name = $user->fname . ' ' . ($get_both_names ? $user->lname : '');
            } else if (!empty($privacy) && $privacy->profile_name != 0) {
                $name = trans('translator.userPublicName');
            }
        } else {
            if ($privacy->profile_name == 2) {
                $user = User::find($user_id);
                $name = $user->fname . ' ' . ($get_both_names ? $user->lname : '');
            } else {
                $name = trans('translator.userPublicName');
            }
        }
        return $name;
    }

    public static function getAgoTimeString($ptime)
    {
        $etime = time() - $ptime;

        if ($etime < 1) {
            return '0 seconds';
        }

        $a = [365 * 24 * 60 * 60 => Lang::choice('translator.Year', 1),
            30 * 24 * 60 * 60 => Lang::choice('translator.Month', 1),
            24 * 60 * 60 => Lang::choice('translator.Day', 1),
            60 * 60 => Lang::choice('translator.Hour', 1),
            60 => Lang::choice('translator.Minute', 1),
            1 => Lang::choice('translator.Second', 1)
        ];
        $a_plural = [Lang::choice('translator.Year', 1) => Lang::choice('translator.Year', 10),
            Lang::choice('translator.Month', 1) => Lang::choice('translator.Month', 10),
            Lang::choice('translator.Day', 1) => Lang::choice('translator.Day', 10),
            Lang::choice('translator.Hour', 1) => Lang::choice('translator.Hour', 10),
            Lang::choice('translator.Minute', 1) => Lang::choice('translator.Minute', 10),
            Lang::choice('translator.Second', 1) => Lang::choice('translator.Second', 10)
        ];

        foreach ($a as $secs => $str) {
            $d = $etime / $secs;
            if ($d >= 1) {
                $r = round($d);
                return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ' . trans('translator.Ago');
            }
        }
    }

    public static function myGender($theId)
    {
        return (User::find($theId)->gender == Auth::user()->gender ? true : false );
    }

    public static function getCitiesForCountry()
    {
        $profile = Profile::where('user_id', '=', Auth::id())->first();
        $country = $profile->country;
        $city_list = WorldCities::where('country_code', '=', $country)->orderby('city_name', 'ASC')->get();
        return $city_list;
    }

    public static function getCity($city_id)
    {
        if (isset($city_id)) {
            $city_row = WorldCities::find($city_id);
            if (!empty($city_row)) {
                $city = ($city_row->country_code == 'LK') ? trans('translator.' . $city_row->city_name) : $city_row->city_name;
                return $city;
            } else {
                return "<em>Not Given</em>";
            }
        }
    }

    public static function canAccessbyPrivacy($third_user_id, $column)
    {
        $privacy = PrivacySetting::where('user_id', $third_user_id)->first();
        if (empty($privacy)) {
            self::setPrivacyinBorn($third_user_id);
            $privacy = PrivacySetting::where('user_id', $third_user_id)->first();
        }
        if (User::find($third_user_id)['profiletype'] == 2) { //Advertisement Profile
            return true;
        }
        if (Auth::check()) {
            $cur_user = Auth::id();
            if (Auth::check() && Auth::id() == $third_user_id) {
                return true;
            } else {
                if (!empty($column)) {
                    if (!empty($privacy)) {
                        $privacy_status = $privacy->$column;
                        if ($privacy_status == self::$PRIV_STP_REGISTERED_USERS_ONLY || $privacy_status == self::$PRIV_STP_PUBLIC) {
                            return true;
                        } else if ($privacy_status == self::$PRIV_STP_ONLY_ME) {
                            return false;
                        } else if ($privacy_status == self::$PRIV_STP_FRIENDS_ONLY) {
                            return (FriendController::are_friends($third_user_id) ? true : false);
                        }
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            }
        } else {
            return (!empty($privacy->$column) && $privacy->$column == 2 ? true : false);
        }
    }

    public static function getUserCountry($user_id)
    {
        if (isset($user_id)) {
            $profile = Profile::where('user_id', '=', $user_id)->first();
            $country = $profile->country;
            return $country;
        }
    }

    public static function createDaily10()
    {
        $user_timestamp = CommonSetting::where('option_name', '=', 'daily10_male')->first()['updated_at'];
        $mysql_to_unix = strtotime($user_timestamp);
        $lastRunDate = date('Y-m-d', $mysql_to_unix);
        $current_date = date('Y-m-d', time());
        if ($lastRunDate !== $current_date) {
            $i = 0;
            $userListforTodayFemaleIdArray = [];

            $dailyTenMaleList = Searchuser::where('id', '<>', '149582')
                    ->where('gender', '=', 'm')
                    ->whereNotIn('role_id', [self::$site_admin_user])
                    ->where('active', '=', 1)
                    ->orderByRaw("RAND()")
                    ->take(10)
                    ->get();
            $dailyTenFemaleList = Searchuser::where('id', '<>', '149582')
                    ->where('gender', '=', 'f')
                    ->whereNotIn('role_id', [self::$site_admin_user])
                    ->where('active', '=', 1)
                    ->orderByRaw("RAND()")
                    ->take(10)
                    ->get();
            $userListforTodayMaleIdArray = [];
            $userListforTodayMaleIdArray = [];
            foreach ($dailyTenMaleList as $dailyTenMale) {
                $userListforTodayMaleIdArray[$i] = $dailyTenMale->id;
                $i++;
            }
            foreach ($dailyTenFemaleList as $dailyTenFemale) {
                $userListforTodayFemaleIdArray[$i] = $dailyTenFemale->id;
                $i++;
            }
            $maleListforToday = implode(',', $userListforTodayMaleIdArray);
            $femaleListforToday = implode(',', $userListforTodayFemaleIdArray);

            CommonSetting::where('option_name', '=', 'daily10_male')->update(['option_value' => $maleListforToday]);
            CommonSetting::where('option_name', '=', 'daily10_female')->update(['option_value' => $femaleListforToday]);
        }
    }

    public static function isSelf($id)
    {
        return (Auth::id() == $id ? true : false);
    }

    public static function grabException($ex)
    {
        echo 'Exception!';
    }

    public static function createMatchesRow()
    {
        if (is_null(Matches::where('user_id', '=', Auth::id())->first())) {
            $matchesColumns = Schema::getColumnListing('matches');
            $matches = new Matches;
            $matches->user_id = Auth::id();
            foreach ($matchesColumns as $matchesColumn) {
                switch ($matchesColumn) {
                    case 'height'://20,75
                        $matches->$matchesColumn = "1,32";
                        break;
                    case 'age':
                        $matches->$matchesColumn = "20,75";
                        break;
                    case 'user_id':
                    case 'id':
                        break;
                    default:
                        $matches->$matchesColumn = 0;
                }
            }
            $matches->save();
        }
    }

    public static function getBroaderMatches()
    {
        $broader_matches = [];
        $cur_user_gender = Auth::user()->gender;
        $matches = User::where('gender', '<>', $cur_user_gender)->where('id', '<>', Auth::id())->orderByRaw("RAND()")->where('active', 1)->take(4)->get();
        foreach ($matches as $user) {
            if (PrivacyController::allowedByPrivacy($user->id, self::$PRIV_COL_PROF_NAME)) {
                array_push($broader_matches, $user);
            }
        }
        return (empty($broader_matches) ? false : $broader_matches);
    }

    public static function getUsersCount()
    {
        if (Auth::check()) {
            $user = User::where(function ($query) {
                        $query->where('role_id', '=', '1')
                                ->orWhere('role_id', '=', '3');
            })->where('active', '=', '1')->get()->count();
            return $user;
        } else {
            return 0;
        }
    }

    public static function getDeactivatedCount()
    {
        if (Auth::check()) {
            $user = User::where('active', '=', 0)->where('role_id', '=', 1)->where('id', '!=', 149582)
                            ->where('id', '!=', Auth::id())->get()->count();
            return $user;
        } else {
            return 0;
        }
    }

    public static function getOnlineCount()
    {
        if (Auth::check()) {
            $timeNow = date('Y-m-d h:i:s');
            $date = new DateTime();
            $nowTime = $date->format("Y-m-d h:i:s");
            $date = new DateTime($timeNow);
            date_sub($date, date_interval_create_from_date_string('10 minutes'));
            $befTenMins = date_format($date, 'Y-m-d h:i:s');
            $users = Searchuser::where('gender', '!=', Auth::user()->gender)
                            ->whereBetween('lastvisit', [$befTenMins, $nowTime])
                            ->where('active', '=', 1)
                            ->get()->count();
            return $users;
        } else {
            return 0;
        }
    }

    public static function getReprtsCount()
    {
        if (Auth::check()) {
            $reports = Report::where('read', '!=', '1')->count();
            return $reports;
        } else {
            return 0;
        }
    }

    public static function getTxnID($payment_id)
    {
        $payment = Payment::find($payment_id);
        if (empty($payment)) {
            return false;
        } else {
            return $payment->txn_id;
        }
    }

    public static function getUserPaidInvoices()
    {
        $invoices = Invoice::where('user_id', Auth::id())->where('paid', '1')->get();
        return ($invoices->isEmpty() ? null : $invoices);
    }

    public static function getUserUnPaidInvoices()
    {
        $invoices = Invoice::where('user_id', Auth::id())->where('paid', '0')->get();
        return ($invoices->isEmpty() ? null : $invoices);
    }

    public static function hasPermissions($user_id, $type)
    {
        if (Auth::check()) {
            $user = User::find($user_id);
            $user_role = $user->role_id;
            $permissions = Permissions::where('role', '=', $user_role)->where('permissions', '=', $type)->first();
            if (!empty($permissions)) {
                $has_permission = $permissions->has_permission;
                if ($has_permission == 1) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    public static function isPremiumUser()
    {
        $cur_user_role = User::find(Auth::id())->role_id;
        if ($cur_user_role == self::$premium_user | $cur_user_role == self::$site_admin_user) {
            return true;
        } else {
            return false;
        }
    }

    public static function isAdministrator($user_id)
    {
        if (!empty($user_id)) {
            $cur_user_role = User::find($user_id)->role_id;
            if ($cur_user_role == self::$site_admin_user) {
                return true;
            } else {
                return false;
            }
        } else {
            $cur_user_role = User::find(Auth::id())->role_id;
            if ($cur_user_role == self::$site_admin_user) {
                return true;
            } else {
                return false;
            }
        }
    }

    public static function canSendProposal()
    {
        if (self::isPremiumUser()) {
            return true;
        } else {
            $user_settings = UserSetting::where('user_id', '=', Auth::id())->first();
            $sended_reqs = (!empty($user_settings) ? $user_settings->frequest_count : 1);
            if ($sended_reqs < 5) {
                return true;
            } else {
                return false;
            }
        }
    }

    public static function getEmail($email)
    {
        if (strlen($email) > 15) {
            $email_ar = explode('@', $email);
            $email_user = $email_ar[0];
            $email_domain = $email_ar[1];
            if (strlen($email_user) > 10 | strlen($email_domain) > 10) {
                $parse = "";
                $email_parse = str_limit($email_user, '8', '...');
                if (strlen($email_domain) > 10) {
                    $email_parse_domain = str_limit($email_domain, '8', '...');
                    $parse = $email_parse . '@' . $email_parse_domain;
                } else {
                    $parse = $email_parse . '@' . $email_domain;
                }
                return $parse;
            } else {
                return $email;
            }
        } else {
            return $email;
        }
    }

    public static function isActiveUser($user_id)
    {
        return (isset($user_id) & User::find($user_id)->active == 1 ? true : false);
    }

    public static function mobileVerified($user_id)
    {
        if (Auth::check()) {
            $user_id = (isset($user_id) ? $user_id : Auth::id());
            return (User::find($user_id)->mobile_verified == 1) ? true : false;
        }
    }

    public static function isCodeLive($code_row)
    {
        $row_id = $code_row->id;
        $search = DB::select("SELECT * FROM `mobile_verifications` WHERE `updated_at` BETWEEN '" . date("Y-m-d H:i:s", strtotime("-1 day")) . "' AND '" . date("Y-m-d H:i:s") . "' AND id='" . $row_id . "' LIMIT 1");
        return (empty($search) ? false : true);
    }

    public static function canShowSendSMSbutton()
    {
        $find_row = MobileVerifications::where('user_id', Auth::id())->get()->first();
        if (!empty($find_row)) {
            $row = DB::select("SELECT * FROM `mobile_verifications` WHERE `updated_at` BETWEEN '" . date("Y-m-d H:i:s", strtotime("-15 minutes")) . "' AND '" . date("Y-m-d H:i:s") . "' LIMIT 1");
            return (!empty($row) ? true : false);
        } else {
            // New User
            return false;
        }
    }

    public static function getCountryInfo($request)
    {
        $returner = '';
        $array = FormDetails::getCountries();
        foreach ($array as $value) {
            if ($value['code'] == Common::getUserCountry(Auth::id())) {
                $returner = $value[$request];
            }
        }
        return $returner;
    }

    public static function isMobileVerified($user_id)
    {
        if (Auth::check()) {
            $user = User::find($user_id);
            return ($user->mobile_verified == 1) ? true : false;
        }
    }

    public static function getCommonData($field)
    {
        $common_settings = CommonSetting::where('option_name', '=', $field)->first();
        return (!empty($common_settings) ? $common_settings->option_value : 'Invalid data field' );
    }

    public static function setCommonData($option_name, $option_value)
    {
        $option = CommonSetting::where('option_name', $option_name)->first();
        if (empty($option)) {
            $option = new CommonSetting();
            $option->option_name = $option_name;
            $option->option_value = $option_value;
            $option->save();
        } else {
            $option->option_name = $option_name;
            $option->option_value = $option_value;
            $option->update();
        }
    }

    public static function setForAdminApproval($key, $value, $userId = null)
    {
        if (empty($userId)) {
            $userId = Auth::id();
        }

        if (self::isAdmin($userId)) {
            return;
        }

        $adminReviews = AdminReviews::where('user_id', $userId)->where('key', $key);

        if ($adminReviews->get()->isEmpty()) {
            $item = new AdminReviews();
            $item->user_id = $userId;
        } else {
            $item = $adminReviews->first();
        }
        $item->key = $key;
        $item->value = $value;
        $item->save();
    }

    public static function getUserVerifyNewValue($key, $userId = null)
    {
        $userId = empty($userId) ? Auth::id() : $userId;
        $value = AdminReviews::where(['user_id' => $userId, 'key' => $key])->first();
        return empty($value) ? false : $value->value;
    }

    public static function getUserVerifyOldValue($adminReviewsId)
    {
        $adminReviews = AdminReviews::find($adminReviewsId);
        $user = User::find($adminReviews->user_id);
        $key = $adminReviews->key;

        switch ($key) {
            case 'mobilenumber':
            case 'fname':
            case 'lname':
                return $user->{$key};
            case 'profile picture':
                return '<img src="' . asset(ProfileController::getProfileImageUrl(true, $user->id)) . '" height="100">';
        }

        return false;
    }

    public static function isUserVerifiedByAdmin($userId = null)
    {
        $id = empty($userId) ? Auth::id() : $userId;
        $user = User::find($id);

        if (!empty($user)) {
            return $user->admin_verified == 1;
        }

        return false;
    }

    public static function isAdmin($userId = null)
    {
        $userId = empty($userId) ? Auth::id() : $userId;
        $user = User::find($userId);
        return empty($user) ? false : $user->role_id == 2;
    }

    public static function getProfileAge($user_id)
    {
        $user = DB::select("SELECT id,email,timestampdiff(DAY,`users`.`created_at`,curdate()) AS `account_age` FROM users WHERE id='$user_id'");
        return (int) $user[0]->account_age;
    }
}
