<?php

namespace App\Library;

use Lang;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FormDetails
 *
 * @author Thambaru Wijesekara
 */
class FormDetails
{

    public static function get_profileFor()
    {
        return [
            '1' => trans('translator.BySelf'),
            '2' => trans('translator.BySon'),
            '3' => trans('translator.ByDaughter'),
            '4' => trans('translator.BySibling'),
            '5' => trans('translator.ByFriend'),
            '6' => trans('translator.ByRelative'),
        ];
    }

    public static function getCountries()
    {
        $countries = [];
        $countries[] = ["code" => "AF", "name" => "Afghanistan", "d_code" => "+93"];
        $countries[] = ["code" => "AL", "name" => "Albania", "d_code" => "+355"];
        $countries[] = ["code" => "DZ", "name" => "Algeria", "d_code" => "+213"];
        $countries[] = ["code" => "AS", "name" => "American Samoa", "d_code" => "+1"];
        $countries[] = ["code" => "AD", "name" => "Andorra", "d_code" => "+376"];
        $countries[] = ["code" => "AO", "name" => "Angola", "d_code" => "+244"];
        $countries[] = ["code" => "AI", "name" => "Anguilla", "d_code" => "+1"];
        $countries[] = ["code" => "AG", "name" => "Antigua", "d_code" => "+1"];
        $countries[] = ["code" => "AR", "name" => "Argentina", "d_code" => "+54"];
        $countries[] = ["code" => "AM", "name" => "Armenia", "d_code" => "+374"];
        $countries[] = ["code" => "AW", "name" => "Aruba", "d_code" => "+297"];
        $countries[] = ["code" => "AU", "name" => "Australia", "d_code" => "+61"];
        $countries[] = ["code" => "AT", "name" => "Austria", "d_code" => "+43"];
        $countries[] = ["code" => "AZ", "name" => "Azerbaijan", "d_code" => "+994"];
        $countries[] = ["code" => "BH", "name" => "Bahrain", "d_code" => "+973"];
        $countries[] = ["code" => "BD", "name" => "Bangladesh", "d_code" => "+880"];
        $countries[] = ["code" => "BB", "name" => "Barbados", "d_code" => "+1"];
        $countries[] = ["code" => "BY", "name" => "Belarus", "d_code" => "+375"];
        $countries[] = ["code" => "BE", "name" => "Belgium", "d_code" => "+32"];
        $countries[] = ["code" => "BZ", "name" => "Belize", "d_code" => "+501"];
        $countries[] = ["code" => "BJ", "name" => "Benin", "d_code" => "+229"];
        $countries[] = ["code" => "BM", "name" => "Bermuda", "d_code" => "+1"];
        $countries[] = ["code" => "BT", "name" => "Bhutan", "d_code" => "+975"];
        $countries[] = ["code" => "BO", "name" => "Bolivia", "d_code" => "+591"];
        $countries[] = ["code" => "BA", "name" => "Bosnia and Herzegovina", "d_code" => "+387"];
        $countries[] = ["code" => "BW", "name" => "Botswana", "d_code" => "+267"];
        $countries[] = ["code" => "BR", "name" => "Brazil", "d_code" => "+55"];
        $countries[] = ["code" => "IO", "name" => "British Indian Ocean Territory", "d_code" => "+246"];
        $countries[] = ["code" => "VG", "name" => "British Virgin Islands", "d_code" => "+1"];
        $countries[] = ["code" => "BN", "name" => "Brunei", "d_code" => "+673"];
        $countries[] = ["code" => "BG", "name" => "Bulgaria", "d_code" => "+359"];
        $countries[] = ["code" => "BF", "name" => "Burkina Faso", "d_code" => "+226"];
        $countries[] = ["code" => "MM", "name" => "Burma Myanmar", "d_code" => "+95"];
        $countries[] = ["code" => "BI", "name" => "Burundi", "d_code" => "+257"];
        $countries[] = ["code" => "KH", "name" => "Cambodia", "d_code" => "+855"];
        $countries[] = ["code" => "CM", "name" => "Cameroon", "d_code" => "+237"];
        $countries[] = ["code" => "CA", "name" => "Canada", "d_code" => "+1"];
        $countries[] = ["code" => "CV", "name" => "Cape Verde", "d_code" => "+238"];
        $countries[] = ["code" => "KY", "name" => "Cayman Islands", "d_code" => "+1"];
        $countries[] = ["code" => "CF", "name" => "Central African Republic", "d_code" => "+236"];
        $countries[] = ["code" => "TD", "name" => "Chad", "d_code" => "+235"];
        $countries[] = ["code" => "CL", "name" => "Chile", "d_code" => "+56"];
        $countries[] = ["code" => "CN", "name" => "China", "d_code" => "+86"];
        $countries[] = ["code" => "CO", "name" => "Colombia", "d_code" => "+57"];
        $countries[] = ["code" => "KM", "name" => "Comoros", "d_code" => "+269"];
        $countries[] = ["code" => "CK", "name" => "Cook Islands", "d_code" => "+682"];
        $countries[] = ["code" => "CR", "name" => "Costa Rica", "d_code" => "+506"];
        $countries[] = ["code" => "CI", "name" => "Côte d'Ivoire", "d_code" => "+225"];
        $countries[] = ["code" => "HR", "name" => "Croatia", "d_code" => "+385"];
        $countries[] = ["code" => "CU", "name" => "Cuba", "d_code" => "+53"];
        $countries[] = ["code" => "CY", "name" => "Cyprus", "d_code" => "+357"];
        $countries[] = ["code" => "CZ", "name" => "Czech Republic", "d_code" => "+420"];
        $countries[] = ["code" => "CD", "name" => "Democratic Republic of Congo", "d_code" => "+243"];
        $countries[] = ["code" => "DK", "name" => "Denmark", "d_code" => "+45"];
        $countries[] = ["code" => "DJ", "name" => "Djibouti", "d_code" => "+253"];
        $countries[] = ["code" => "DM", "name" => "Dominica", "d_code" => "+1"];
        $countries[] = ["code" => "DO", "name" => "Dominican Republic", "d_code" => "+1"];
        $countries[] = ["code" => "EC", "name" => "Ecuador", "d_code" => "+593"];
        $countries[] = ["code" => "EG", "name" => "Egypt", "d_code" => "+20"];
        $countries[] = ["code" => "SV", "name" => "El Salvador", "d_code" => "+503"];
        $countries[] = ["code" => "GQ", "name" => "Equatorial Guinea", "d_code" => "+240"];
        $countries[] = ["code" => "ER", "name" => "Eritrea", "d_code" => "+291"];
        $countries[] = ["code" => "EE", "name" => "Estonia", "d_code" => "+372"];
        $countries[] = ["code" => "ET", "name" => "Ethiopia", "d_code" => "+251"];
        $countries[] = ["code" => "FK", "name" => "Falkland Islands", "d_code" => "+500"];
        $countries[] = ["code" => "FO", "name" => "Faroe Islands", "d_code" => "+298"];
        $countries[] = ["code" => "FM", "name" => "Federated States of Micronesia", "d_code" => "+691"];
        $countries[] = ["code" => "FJ", "name" => "Fiji", "d_code" => "+679"];
        $countries[] = ["code" => "FI", "name" => "Finland", "d_code" => "+358"];
        $countries[] = ["code" => "FR", "name" => "France", "d_code" => "+33"];
        $countries[] = ["code" => "GF", "name" => "French Guiana", "d_code" => "+594"];
        $countries[] = ["code" => "PF", "name" => "French Polynesia", "d_code" => "+689"];
        $countries[] = ["code" => "GA", "name" => "Gabon", "d_code" => "+241"];
        $countries[] = ["code" => "GE", "name" => "Georgia", "d_code" => "+995"];
        $countries[] = ["code" => "DE", "name" => "Germany", "d_code" => "+49"];
        $countries[] = ["code" => "GH", "name" => "Ghana", "d_code" => "+233"];
        $countries[] = ["code" => "GI", "name" => "Gibraltar", "d_code" => "+350"];
        $countries[] = ["code" => "GR", "name" => "Greece", "d_code" => "+30"];
        $countries[] = ["code" => "GL", "name" => "Greenland", "d_code" => "+299"];
        $countries[] = ["code" => "GD", "name" => "Grenada", "d_code" => "+1"];
        $countries[] = ["code" => "GP", "name" => "Guadeloupe", "d_code" => "+590"];
        $countries[] = ["code" => "GU", "name" => "Guam", "d_code" => "+1"];
        $countries[] = ["code" => "GT", "name" => "Guatemala", "d_code" => "+502"];
        $countries[] = ["code" => "GN", "name" => "Guinea", "d_code" => "+224"];
        $countries[] = ["code" => "GW", "name" => "Guinea-Bissau", "d_code" => "+245"];
        $countries[] = ["code" => "GY", "name" => "Guyana", "d_code" => "+592"];
        $countries[] = ["code" => "HT", "name" => "Haiti", "d_code" => "+509"];
        $countries[] = ["code" => "HN", "name" => "Honduras", "d_code" => "+504"];
        $countries[] = ["code" => "HK", "name" => "Hong Kong", "d_code" => "+852"];
        $countries[] = ["code" => "HU", "name" => "Hungary", "d_code" => "+36"];
        $countries[] = ["code" => "IS", "name" => "Iceland", "d_code" => "+354"];
        $countries[] = ["code" => "IN", "name" => "India", "d_code" => "+91"];
        $countries[] = ["code" => "ID", "name" => "Indonesia", "d_code" => "+62"];
        $countries[] = ["code" => "IR", "name" => "Iran", "d_code" => "+98"];
        $countries[] = ["code" => "IQ", "name" => "Iraq", "d_code" => "+964"];
        $countries[] = ["code" => "IE", "name" => "Ireland", "d_code" => "+353"];
        $countries[] = ["code" => "IL", "name" => "Israel", "d_code" => "+972"];
        $countries[] = ["code" => "IT", "name" => "Italy", "d_code" => "+39"];
        $countries[] = ["code" => "JM", "name" => "Jamaica", "d_code" => "+1"];
        $countries[] = ["code" => "JP", "name" => "Japan", "d_code" => "+81"];
        $countries[] = ["code" => "JO", "name" => "Jordan", "d_code" => "+962"];
        $countries[] = ["code" => "KZ", "name" => "Kazakhstan", "d_code" => "+7"];
        $countries[] = ["code" => "KE", "name" => "Kenya", "d_code" => "+254"];
        $countries[] = ["code" => "KI", "name" => "Kiribati", "d_code" => "+686"];
        $countries[] = ["code" => "XK", "name" => "Kosovo", "d_code" => "+381"];
        $countries[] = ["code" => "KW", "name" => "Kuwait", "d_code" => "+965"];
        $countries[] = ["code" => "KG", "name" => "Kyrgyzstan", "d_code" => "+996"];
        $countries[] = ["code" => "LA", "name" => "Laos", "d_code" => "+856"];
        $countries[] = ["code" => "LV", "name" => "Latvia", "d_code" => "+371"];
        $countries[] = ["code" => "LB", "name" => "Lebanon", "d_code" => "+961"];
        $countries[] = ["code" => "LS", "name" => "Lesotho", "d_code" => "+266"];
        $countries[] = ["code" => "LR", "name" => "Liberia", "d_code" => "+231"];
        $countries[] = ["code" => "LY", "name" => "Libya", "d_code" => "+218"];
        $countries[] = ["code" => "LI", "name" => "Liechtenstein", "d_code" => "+423"];
        $countries[] = ["code" => "LT", "name" => "Lithuania", "d_code" => "+370"];
        $countries[] = ["code" => "LU", "name" => "Luxembourg", "d_code" => "+352"];
        $countries[] = ["code" => "MO", "name" => "Macau", "d_code" => "+853"];
        $countries[] = ["code" => "MK", "name" => "Macedonia", "d_code" => "+389"];
        $countries[] = ["code" => "MG", "name" => "Madagascar", "d_code" => "+261"];
        $countries[] = ["code" => "MW", "name" => "Malawi", "d_code" => "+265"];
        $countries[] = ["code" => "MY", "name" => "Malaysia", "d_code" => "+60"];
        $countries[] = ["code" => "MV", "name" => "Maldives", "d_code" => "+960"];
        $countries[] = ["code" => "ML", "name" => "Mali", "d_code" => "+223"];
        $countries[] = ["code" => "MT", "name" => "Malta", "d_code" => "+356"];
        $countries[] = ["code" => "MH", "name" => "Marshall Islands", "d_code" => "+692"];
        $countries[] = ["code" => "MQ", "name" => "Martinique", "d_code" => "+596"];
        $countries[] = ["code" => "MR", "name" => "Mauritania", "d_code" => "+222"];
        $countries[] = ["code" => "MU", "name" => "Mauritius", "d_code" => "+230"];
        $countries[] = ["code" => "YT", "name" => "Mayotte", "d_code" => "+262"];
        $countries[] = ["code" => "MX", "name" => "Mexico", "d_code" => "+52"];
        $countries[] = ["code" => "MD", "name" => "Moldova", "d_code" => "+373"];
        $countries[] = ["code" => "MC", "name" => "Monaco", "d_code" => "+377"];
        $countries[] = ["code" => "MN", "name" => "Mongolia", "d_code" => "+976"];
        $countries[] = ["code" => "ME", "name" => "Montenegro", "d_code" => "+382"];
        $countries[] = ["code" => "MS", "name" => "Montserrat", "d_code" => "+1"];
        $countries[] = ["code" => "MA", "name" => "Morocco", "d_code" => "+212"];
        $countries[] = ["code" => "MZ", "name" => "Mozambique", "d_code" => "+258"];
        $countries[] = ["code" => "NA", "name" => "Namibia", "d_code" => "+264"];
        $countries[] = ["code" => "NR", "name" => "Nauru", "d_code" => "+674"];
        $countries[] = ["code" => "NP", "name" => "Nepal", "d_code" => "+977"];
        $countries[] = ["code" => "NL", "name" => "Netherlands", "d_code" => "+31"];
        $countries[] = ["code" => "AN", "name" => "Netherlands Antilles", "d_code" => "+599"];
        $countries[] = ["code" => "NC", "name" => "New Caledonia", "d_code" => "+687"];
        $countries[] = ["code" => "NZ", "name" => "New Zealand", "d_code" => "+64"];
        $countries[] = ["code" => "NI", "name" => "Nicaragua", "d_code" => "+505"];
        $countries[] = ["code" => "NE", "name" => "Niger", "d_code" => "+227"];
        $countries[] = ["code" => "NG", "name" => "Nigeria", "d_code" => "+234"];
        $countries[] = ["code" => "NU", "name" => "Niue", "d_code" => "+683"];
        $countries[] = ["code" => "NF", "name" => "Norfolk Island", "d_code" => "+672"];
        $countries[] = ["code" => "KP", "name" => "North Korea", "d_code" => "+850"];
        $countries[] = ["code" => "MP", "name" => "Northern Mariana Islands", "d_code" => "+1"];
        $countries[] = ["code" => "NO", "name" => "Norway", "d_code" => "+47"];
        $countries[] = ["code" => "OM", "name" => "Oman", "d_code" => "+968"];
        $countries[] = ["code" => "PK", "name" => "Pakistan", "d_code" => "+92"];
        $countries[] = ["code" => "PW", "name" => "Palau", "d_code" => "+680"];
        $countries[] = ["code" => "PS", "name" => "Palestine", "d_code" => "+970"];
        $countries[] = ["code" => "PA", "name" => "Panama", "d_code" => "+507"];
        $countries[] = ["code" => "PG", "name" => "Papua New Guinea", "d_code" => "+675"];
        $countries[] = ["code" => "PY", "name" => "Paraguay", "d_code" => "+595"];
        $countries[] = ["code" => "PE", "name" => "Peru", "d_code" => "+51"];
        $countries[] = ["code" => "PH", "name" => "Philippines", "d_code" => "+63"];
        $countries[] = ["code" => "PL", "name" => "Poland", "d_code" => "+48"];
        $countries[] = ["code" => "PT", "name" => "Portugal", "d_code" => "+351"];
        $countries[] = ["code" => "PR", "name" => "Puerto Rico", "d_code" => "+1"];
        $countries[] = ["code" => "QA", "name" => "Qatar", "d_code" => "+974"];
        $countries[] = ["code" => "CG", "name" => "Republic of the Congo", "d_code" => "+242"];
        $countries[] = ["code" => "RE", "name" => "Réunion", "d_code" => "+262"];
        $countries[] = ["code" => "RO", "name" => "Romania", "d_code" => "+40"];
        $countries[] = ["code" => "RU", "name" => "Russia", "d_code" => "+7"];
        $countries[] = ["code" => "RW", "name" => "Rwanda", "d_code" => "+250"];
        $countries[] = ["code" => "BL", "name" => "Saint Barthélemy", "d_code" => "+590"];
        $countries[] = ["code" => "SH", "name" => "Saint Helena", "d_code" => "+290"];
        $countries[] = ["code" => "KN", "name" => "Saint Kitts and Nevis", "d_code" => "+1"];
        $countries[] = ["code" => "MF", "name" => "Saint Martin", "d_code" => "+590"];
        $countries[] = ["code" => "PM", "name" => "Saint Pierre and Miquelon", "d_code" => "+508"];
        $countries[] = ["code" => "VC", "name" => "Saint Vincent and the Grenadines", "d_code" => "+1"];
        $countries[] = ["code" => "WS", "name" => "Samoa", "d_code" => "+685"];
        $countries[] = ["code" => "SM", "name" => "San Marino", "d_code" => "+378"];
        $countries[] = ["code" => "ST", "name" => "São Tomé and Príncipe", "d_code" => "+239"];
        $countries[] = ["code" => "SA", "name" => "Saudi Arabia", "d_code" => "+966"];
        $countries[] = ["code" => "SN", "name" => "Senegal", "d_code" => "+221"];
        $countries[] = ["code" => "RS", "name" => "Serbia", "d_code" => "+381"];
        $countries[] = ["code" => "SC", "name" => "Seychelles", "d_code" => "+248"];
        $countries[] = ["code" => "SL", "name" => "Sierra Leone", "d_code" => "+232"];
        $countries[] = ["code" => "SG", "name" => "Singapore", "d_code" => "+65"];
        $countries[] = ["code" => "SK", "name" => "Slovakia", "d_code" => "+421"];
        $countries[] = ["code" => "SI", "name" => "Slovenia", "d_code" => "+386"];
        $countries[] = ["code" => "SB", "name" => "Solomon Islands", "d_code" => "+677"];
        $countries[] = ["code" => "SO", "name" => "Somalia", "d_code" => "+252"];
        $countries[] = ["code" => "ZA", "name" => "South Africa", "d_code" => "+27"];
        $countries[] = ["code" => "KR", "name" => "South Korea", "d_code" => "+82"];
        $countries[] = ["code" => "ES", "name" => "Spain", "d_code" => "+34"];
        $countries[] = ["code" => "LK", "name" => "Sri Lanka", "d_code" => "+94"];
        $countries[] = ["code" => "LC", "name" => "St. Lucia", "d_code" => "+1"];
        $countries[] = ["code" => "SD", "name" => "Sudan", "d_code" => "+249"];
        $countries[] = ["code" => "SR", "name" => "Suriname", "d_code" => "+597"];
        $countries[] = ["code" => "SZ", "name" => "Swaziland", "d_code" => "+268"];
        $countries[] = ["code" => "SE", "name" => "Sweden", "d_code" => "+46"];
        $countries[] = ["code" => "CH", "name" => "Switzerland", "d_code" => "+41"];
        $countries[] = ["code" => "SY", "name" => "Syria", "d_code" => "+963"];
        $countries[] = ["code" => "TW", "name" => "Taiwan", "d_code" => "+886"];
        $countries[] = ["code" => "TJ", "name" => "Tajikistan", "d_code" => "+992"];
        $countries[] = ["code" => "TZ", "name" => "Tanzania", "d_code" => "+255"];
        $countries[] = ["code" => "TH", "name" => "Thailand", "d_code" => "+66"];
        $countries[] = ["code" => "BS", "name" => "The Bahamas", "d_code" => "+1"];
        $countries[] = ["code" => "GM", "name" => "The Gambia", "d_code" => "+220"];
        $countries[] = ["code" => "TL", "name" => "Timor-Leste", "d_code" => "+670"];
        $countries[] = ["code" => "TG", "name" => "Togo", "d_code" => "+228"];
        $countries[] = ["code" => "TK", "name" => "Tokelau", "d_code" => "+690"];
        $countries[] = ["code" => "TO", "name" => "Tonga", "d_code" => "+676"];
        $countries[] = ["code" => "TT", "name" => "Trinidad and Tobago", "d_code" => "+1"];
        $countries[] = ["code" => "TN", "name" => "Tunisia", "d_code" => "+216"];
        $countries[] = ["code" => "TR", "name" => "Turkey", "d_code" => "+90"];
        $countries[] = ["code" => "TM", "name" => "Turkmenistan", "d_code" => "+993"];
        $countries[] = ["code" => "TC", "name" => "Turks and Caicos Islands", "d_code" => "+1"];
        $countries[] = ["code" => "TV", "name" => "Tuvalu", "d_code" => "+688"];
        $countries[] = ["code" => "UG", "name" => "Uganda", "d_code" => "+256"];
        $countries[] = ["code" => "UA", "name" => "Ukraine", "d_code" => "+380"];
        $countries[] = ["code" => "AE", "name" => "United Arab Emirates", "d_code" => "+971"];
        $countries[] = ["code" => "GB", "name" => "United Kingdom", "d_code" => "+44"];
        $countries[] = ["code" => "US", "name" => "United States", "d_code" => "+1"];
        $countries[] = ["code" => "UY", "name" => "Uruguay", "d_code" => "+598"];
        $countries[] = ["code" => "VI", "name" => "US Virgin Islands", "d_code" => "+1"];
        $countries[] = ["code" => "UZ", "name" => "Uzbekistan", "d_code" => "+998"];
        $countries[] = ["code" => "VU", "name" => "Vanuatu", "d_code" => "+678"];
        $countries[] = ["code" => "VA", "name" => "Vatican City", "d_code" => "+39"];
        $countries[] = ["code" => "VE", "name" => "Venezuela", "d_code" => "+58"];
        $countries[] = ["code" => "VN", "name" => "Vietnam", "d_code" => "+84"];
        $countries[] = ["code" => "WF", "name" => "Wallis and Futuna", "d_code" => "+681"];
        $countries[] = ["code" => "YE", "name" => "Yemen", "d_code" => "+967"];
        $countries[] = ["code" => "ZM", "name" => "Zambia", "d_code" => "+260"];
        $countries[] = ["code" => "ZW", "name" => "Zimbabwe", "d_code" => "+263"];
        return $countries;
    }

    public static function getRecentlyJoined()
    {
        return [
            '1' => trans('translator.Withinday'),
            '2' => trans('translator.Withinweek'),
            '3' => trans('translator.Withinmonth'),
        ];
    }

    public static function getActiveMembers()
    {
        return [
            '0' => trans('translator.All'),
            '1' => trans('translator.Withinday'),
            '2' => trans('translator.Withinweek'),
            '3' => trans('translator.Withinmonth'),
        ];
    }

    public static function get_months()
    {
        return [
            '1' => trans('translator.January'),
            '2' => trans('translator.Febuary'),
            '3' => trans('translator.March'),
            '4' => trans('translator.April'),
            '5' => trans('translator.May'),
            '6' => trans('translator.June'),
            '7' => trans('translator.July'),
            '8' => trans('translator.August'),
            '9' => trans('translator.September'),
            '10' => trans('translator.October'),
            '11' => trans('translator.November'),
            '12' => trans('translator.December'),
        ];
    }

    public static function get_religion($atBegin = null)
    {
        $ret = [
            '1' => trans('translator.Buddhist'),
            '2' => trans('translator.Hindu'),
            '3' => trans('translator.Islam'),
            '4' => trans('translator.Christian'),
        ];
        if ($atBegin) {
            $ret[0] = $atBegin;
        }
        return $ret;
    }

    public static function get_language($atBegin = null)
    {
        $ret = [
            '1' => trans('translator.Sinhala'),
            '2' => trans('translator.Tamil'),
            '3' => trans('translator.English'),
            '4' => trans('translator.AnyOther'),
        ];
        if ($atBegin) {
            $ret[0] = $atBegin;
        }
        return $ret;
    }

    public static function get_provinces()
    {
        return [
            '' => trans('translator.Select'),
            '1' => trans('translator.Central'),
            '2' => trans('translator.Eastern'),
            '3' => trans('translator.Northern'),
            '4' => trans('translator.NorthCentral'),
            '5' => trans('translator.NorthWestern'),
            '6' => trans('translator.Sabaragamuwa'),
            '7' => trans('translator.Southern'),
            '8' => trans('translator.Uva'),
            '9' => trans('translator.Western')];
    }

    public static function get_country()
    {
        return [
            '1' => trans('translator.SriLanka'),
            '2' => trans('translator.Other'),
        ];
    }

    public static function get_cities($atBegin = null)
    {
        $ret = [
            '1' => ['prov' => 'east', 'dist' => trans('translator.Ampara')],
            '2' => ['prov' => 'nrthcnt', 'dist' => trans('translator.Anuradhapura')],
            '3' => ['prov' => 'uva', 'dist' => trans('translator.Badulla')],
            '4' => ['prov' => 'east', 'dist' => trans('translator.Batticaloa')],
            '5' => ['prov' => 'west', 'dist' => trans('translator.Colombo')],
            '6' => ['prov' => 'south', 'dist' => trans('translator.Galle')],
            '7' => ['prov' => 'west', 'dist' => trans('translator.Gampaha')],
            '8' => ['prov' => 'south', 'dist' => trans('translator.Hambantota')],
            '9' => ['prov' => 'north', 'dist' => trans('translator.Jaffna')],
            '10' => ['prov' => 'west', 'dist' => trans('translator.Kalutara')],
            '11' => ['prov' => 'cnt', 'dist' => trans('translator.Kandy')],
            '12' => ['prov' => 'sbrgmw', 'dist' => trans('translator.Kegalle')],
            '13' => ['prov' => 'north', 'dist' => trans('translator.Kilinochchi')],
            '14' => ['prov' => 'northwstn', 'dist' => trans('translator.Kurunegala')],
            '15' => ['prov' => 'north', 'dist' => trans('translator.Mannar')],
            '16' => ['prov' => 'cnt', 'dist' => trans('translator.Matale')],
            '17' => ['prov' => 'south', 'dist' => trans('translator.Matara')],
            '18' => ['prov' => 'uva', 'dist' => trans('translator.Moneragala')],
            '19' => ['prov' => 'north', 'dist' => trans('translator.Mullaitiv')],
            '20' => ['prov' => 'cnt', 'dist' => trans('translator.Nuwara Eliya')],
            '21' => ['prov' => 'northcnt', 'dist' => trans('translator.Polonnaruwa')],
            '22' => ['prov' => 'northcnt', 'dist' => trans('translator.Puttalam')],
            '23' => ['prov' => 'sbrgmw', 'dist' => trans('translator.Rathnapura')],
            '24' => ['prov' => 'east', 'dist' => trans('translator.Trincomalee')],
            '25' => ['prov' => 'north', 'dist' => trans('translator.Vavuniya')],
        ];
        if ($atBegin) {
            $ret[0] = $atBegin;
        }
        return $ret;
    }

    public static function get_residency($atBegin = null, $multiselect = false)
    {
        if ($multiselect) {
            $ret = [
                '' => trans('translator.Select'),
                '1' => trans('translator.Citizen'),
                '2' => trans('translator.PermanentResident'),
                '3' => trans('translator.StudentVisa'),
                '4' => trans('translator.TemporaryVisa'),
                '5' => trans('translator.WorkPermit'),
            ];
        } else {
            $ret = [
                '1' => trans('translator.Citizen'),
                '2' => trans('translator.PermanentResident'),
                '3' => trans('translator.StudentVisa'),
                '4' => trans('translator.TemporaryVisa'),
                '5' => trans('translator.WorkPermit'),
            ];
        }
        if ($atBegin && !$multiselect) {
            $ret[0] = $atBegin;
        }
        return $ret;
    }

    public static function get_maritalStatus($atBegin = null, $multiselect = false)
    {
        if ($multiselect) {
            $ret = [
                '1' => trans('translator.Single'),
                '2' => trans('translator.Widowed'),
                '3' => trans('translator.Divorced'),
            ];
        } else {
            $ret = [
                '' => trans('translator.Select'),
                '1' => trans('translator.Single'),
                '2' => trans('translator.Widowed'),
                '3' => trans('translator.Divorced'),
            ];
        }

        if ($atBegin && !$multiselect) {
            $ret[0] = $atBegin;
        }
        return $ret;
    }

    public static function get_heightList($atBegin = null)
    {
        $ret = [
            '1' => Lang::get('translator.HeightTrans', ['ft' => '4', 'in' => '5', 'cm' => '134']),
            '2' => Lang::get('translator.HeightTrans', ['ft' => '4', 'in' => '6', 'cm' => '134']),
            '3' => Lang::get('translator.HeightTrans', ['ft' => '4', 'in' => '7', 'cm' => '139']),
            '4' => Lang::get('translator.HeightTrans', ['ft' => '4', 'in' => '8', 'cm' => '142']),
            '5' => Lang::get('translator.HeightTrans', ['ft' => '4', 'in' => '9', 'cm' => '144']),
            '6' => Lang::get('translator.HeightTrans', ['ft' => '4', 'in' => '10', 'cm' => '147']),
            '7' => Lang::get('translator.HeightTrans', ['ft' => '4', 'in' => '11', 'cm' => '149']),
            '8' => Lang::get('translator.HeightTrans', ['ft' => '5', 'in' => '0', 'cm' => '152']),
            '9' => Lang::get('translator.HeightTrans', ['ft' => '5', 'in' => '1', 'cm' => '154']),
            '10' => Lang::get('translator.HeightTrans', ['ft' => '5', 'in' => '2', 'cm' => '157']),
            '11' => Lang::get('translator.HeightTrans', ['ft' => '5', 'in' => '3', 'cm' => '160']),
            '12' => Lang::get('translator.HeightTrans', ['ft' => '5', 'in' => '4', 'cm' => '162']),
            '13' => Lang::get('translator.HeightTrans', ['ft' => '5', 'in' => '5', 'cm' => '165']),
            '14' => Lang::get('translator.HeightTrans', ['ft' => '5', 'in' => '6', 'cm' => '167']),
            '15' => Lang::get('translator.HeightTrans', ['ft' => '5', 'in' => '7', 'cm' => '170']),
            '16' => Lang::get('translator.HeightTrans', ['ft' => '5', 'in' => '8', 'cm' => '172']),
            '17' => Lang::get('translator.HeightTrans', ['ft' => '5', 'in' => '9', 'cm' => '175']),
            '18' => Lang::get('translator.HeightTrans', ['ft' => '5', 'in' => '10', 'cm' => '177']),
            '19' => Lang::get('translator.HeightTrans', ['ft' => '5', 'in' => '11', 'cm' => '180']),
            '20' => Lang::get('translator.HeightTrans', ['ft' => '6', 'in' => '0', 'cm' => '182']),
            '21' => Lang::get('translator.HeightTrans', ['ft' => '6', 'in' => '1', 'cm' => '185']),
            '22' => Lang::get('translator.HeightTrans', ['ft' => '6', 'in' => '2', 'cm' => '187']),
            '23' => Lang::get('translator.HeightTrans', ['ft' => '6', 'in' => '3', 'cm' => '190']),
            '24' => Lang::get('translator.HeightTrans', ['ft' => '6', 'in' => '4', 'cm' => '193']),
            '25' => Lang::get('translator.HeightTrans', ['ft' => '6', 'in' => '5', 'cm' => '195']),
            '26' => Lang::get('translator.HeightTrans', ['ft' => '6', 'in' => '6', 'cm' => '198']),
            '27' => Lang::get('translator.HeightTrans', ['ft' => '6', 'in' => '7', 'cm' => '200']),
            '28' => Lang::get('translator.HeightTrans', ['ft' => '6', 'in' => '8', 'cm' => '203']),
            '29' => Lang::get('translator.HeightTrans', ['ft' => '6', 'in' => '9', 'cm' => '205']),
            '30' => Lang::get('translator.HeightTrans', ['ft' => '6', 'in' => '10', 'cm' => '208']),
            '31' => Lang::get('translator.HeightTrans', ['ft' => '6', 'in' => '11', 'cm' => '210']),
            '32' => Lang::get('translator.HeightTrans', ['ft' => '7', 'in' => '0', 'cm' => '213']),
        ];
        if ($atBegin) {
            $ret[0] = $atBegin;
        }
        return $ret;
    }

    public static function get_skinTone($atBegin = null)
    {
        $ret = [
            '1' => trans('translator.Veryfair'),
            '2' => trans('translator.Fair'),
            '3' => trans('translator.Wheatish'),
            '4' => trans('translator.Dark'),
        ];
        if ($atBegin) {
            $ret[0] = $atBegin;
        }
        return $ret;
    }

    public static function get_bodyType($atBegin = null)
    {
        $ret = [
            '1' => trans('translator.Slim'),
            '2' => trans('translator.Athletic'),
            '3' => trans('translator.Average'),
            '4' => trans('translator.Heavy'),
        ];
        if ($atBegin) {
            $ret[0] = $atBegin;
        }
        return $ret;
    }

    public static function get_dietInfo($for = 'self', $atBegin = null)
    {
        switch ($for) :
            case 'self':
                return [
                    '' => trans('translator.Select'),
                    '1' => trans('translator.Vegetarian'),
                    '2' => trans('translator.NonVegetarian'),
                    '3' => trans('translator.OccNonVeg'),
                    '4' => trans('translator.Eggetarian'),
                    '5' => trans('translator.Vegan')];
            case 'partner':
                return [
                    '1' => trans('translator.Vegetarian'),
                    '2' => trans('translator.NonVegetarian'),
                    '3' => trans('translator.OccNonVeg'),
                    '4' => trans('translator.Eggetarian'),
                    '5' => trans('translator.Vegan')];
        endswitch;
    }

    public static function get_drinkInfo($atBegin = null)
    {
        $ret = [
            '1' => trans('translator.Yes'),
            '2' => trans('translator.No'),
            '3' => trans('translator.Occasionally'),
        ];
        if ($atBegin) {
            $ret[0] = $atBegin;
        }
        return $ret;
    }

    public static function get_smokeInfo($atBegin = null)
    {
        $ret = [
            '1' => trans('translator.Yes'),
            '2' => trans('translator.No'),
            '3' => trans('translator.Occasionally'),
        ];
        if ($atBegin) {
            $ret[0] = $atBegin;
        }
        return $ret;
    }

    public static function get_edu_level($for = 'self', $atBegin = null)
    {
        switch ($for) :
            case 'self':
                return [
                    '' => trans('translator.Select'),
                    '1' => trans('translator.Bachelors'),
                    '2' => trans('translator.Masters'),
                    '3' => trans('translator.Doctorate'),
                    '4' => trans('translator.Diploma'),
                    '5' => trans('translator.Undergraduate'),
                    '6' => trans('translator.AssociatesDegree'),
                    '7' => trans('translator.HonoursDegree'),
                    '8' => trans('translator.TradeSchool'),
                    '9' => trans('translator.HighSchool'),
                    '10' => trans('translator.LessThanHighSchool'),
                ];
            case 'partner':
                return [
                    '1' => trans('translator.Bachelors'),
                    '2' => trans('translator.Masters'),
                    '3' => trans('translator.Doctorate'),
                    '4' => trans('translator.Diploma'),
                    '5' => trans('translator.Undergraduate'),
                    '6' => trans('translator.AssociatesDegree'),
                    '7' => trans('translator.HonoursDegree'),
                    '8' => trans('translator.TradeSchool'),
                    '9' => trans('translator.HighSchool'),
                    '10' => trans('translator.LessThanHighSchool'),
                ];
        endswitch;
    }

    public static function get_edu_field()
    {
        return [
            '' => trans('translator.Select'),
            '1' => trans('translator.AdvertisingMarketing'),
            '2' => trans('translator.AdministrativeServices'),
            '3' => trans('translator.Architecture'),
            '3' => trans('translator.ArmedForces'),
            '4' => trans('translator.Arts'),
            '5' => trans('translator.Commerce'),
            '6' => trans('translator.ComputersIT'),
            '7' => trans('translator.Education'),
            '8' => trans('translator.EngineeringTechnology'),
            '9' => trans('translator.Fashion'),
            '10' => trans('translator.Finance'),
            '11' => trans('translator.FineArts'),
            '12' => trans('translator.HomeScience'),
            '13' => trans('translator.Law'),
            '14' => trans('translator.Management'),
            '15' => trans('translator.Medicine'),
            '16' => trans('translator.NursingHealthSciences'),
            '17' => trans('translator.OfficeAdministration'),
            '18' => trans('translator.Science'),
            '19' => trans('translator.Shipping'),
            '20' => trans('translator.TravelTourism'),
        ];
    }

    public static function get_workingAt($for = 'self')
    {
        switch ($for) :
            case 'self':
                return [
                    '' => trans('translator.Select'),
                    '1' => trans('translator.PrivateCompany'),
                    '2' => trans('translator.GovernmentPublicSector'),
                    '3' => trans('translator.DefenseCivilServices'),
                    '4' => trans('translator.BusinessSelfEmployed'),
                    '5' => trans('translator.Overseas'),
                    '6' => trans('translator.NonWorking'),
                ];
            case 'partner':
                return [
                    '1' => trans('translator.PrivateCompany'),
                    '2' => trans('translator.GovernmentPublicSector'),
                    '3' => trans('translator.DefenseCivilServices'),
                    '4' => trans('translator.BusinessSelfEmployed'),
                    '5' => trans('translator.Overseas'),
                    '6' => trans('translator.NonWorking'),
                ];
        endswitch;
    }

    public static function get_workingAs_partner()
    {
        return [
            '1' => trans('translator.AccountingBankingFinance'),
            '2' => trans('translator.AdministrationHR'),
            '3' => trans('translator.AdvertisingMediaEntertainment'),
            '4' => trans('translator.Agriculture'),
            '5' => trans('translator.AirlineAviation'),
            '6' => trans('translator.ArchitectureDesign'),
            '7' => trans('translator.ArtistsAnimatorsWebDesigners'),
            '8' => trans('translator.BeautyFashionJewelleryDesigners'),
            '9' => trans('translator.BPOKPOCustomerSupport'),
            '10' => trans('translator.CivilServicesLawEnforcement'),
            '11' => trans('translator.Defence'),
            '12' => trans('translator.EducationTraining'),
            '13' => trans('translator.Engineering'),
            '14' => trans('translator.HotelHospitality'),
            '15' => trans('translator.Legal'),
            '16' => trans('translator.MedicalHealthcare'),
            '17' => trans('translator.MerchantNavy'),
            '18' => trans('translator.SalesMarketing'),
            '19' => trans('translator.Science'),
            '20' => trans('translator.CorporateProfessionals'),
            '21' => trans('translator.NonWorking'),
        ];
    }

    public static function get_workingAs_option($onlyData = false)
    {
        $workingArray = [
            '' => ['0' => trans('translator.Select')],
            trans('translator.AccountingBankingFinance') => [
                '1' => trans('translator.BankingProfessional'),
                '2' => trans('translator.CharteredAccountant'),
                '3' => trans('translator.CompanySecretary'),
                '4' => trans('translator.FinanceProfessional'),
                '5' => trans('translator.InvestmentProfessional'),
                '6' => trans('translator.AccountingProfessional'),
            ],
            trans('translator.AdministrationHR') => [
                '7' => trans('translator.AdminProfessional'),
                '8' => trans('translator.HumanResourcesProfessional'),
            ],
            trans('translator.AdvertisingMediaEntertainment') => [
                '9' => trans('translator.Actor'),
                '10' => trans('translator.AdvertisingProfessional'),
                '11' => trans('translator.EntertainmentProfessional'),
                '12' => trans('translator.EventManager'),
                '13' => trans('translator.Journalist'),
                '14' => trans('translator.MediaProfessional'),
                '15' => trans('translator.PublicRelationsProfessional'),
            ],
            trans('translator.Agriculture') => [
                '16' => trans('translator.Farming'),
                '17' => trans('translator.Horticulturist'),
                '18' => trans('translator.AgriculturalProfessional'),
            ],
            trans('translator.AirlineAviation') => [
                '19' => trans('translator.AirHostessFlightAttendant'),
                '20' => trans('translator.PilotCoPilot'),
                '21' => trans('translator.OtherAirlineProfessional'),
            ],
            trans('translator.ArchitectureDesign') => [
                '23' => trans('translator.Architect'),
                '24' => trans('translator.InteriorDesigner'),
                '25' => trans('translator.LandscapeArchitect'),
            ],
            trans('translator.ArtistsAnimatorsWebDesigners') => [
                '26' => trans('translator.Animator'),
                '27' => trans('translator.CommercialArtist'),
                '28' => trans('translator.WebUXDesigners'),
                '29' => trans('translator.ArtistOthers'),
            ],
            trans('translator.BeautyFashionJewelleryDesigners') => [
                '30' => trans('translator.Beautician'),
                '31' => trans('translator.FashionDesigner'),
                '32' => trans('translator.Hairstylist'),
                '33' => trans('translator.JewelleryDesigner'),
                '34' => trans('translator.DesignerOther'),
            ],
            trans('translator.BPOKPOCustomerSupport') => [
                '35' => trans('translator.BPOKPOCustomerSupport'),
            ],
            trans('translator.CivilServicesLawEnforcement') => [
                '36' => trans('translator.IASIRSIESIFS'),
                '37' => trans('translator.SLPS'),
                '38' => trans('translator.LawEnforcementEmployee'),
            ],
            trans('translator.Defence') => [
                '39' => trans('translator.Airforce'),
                '40' => trans('translator.Army'),
                '41' => trans('translator.Navy'),
                '42' => trans('translator.DefenseServicesOther'),
            ],
            trans('translator.EducationTraining') => [
                '43' => trans('translator.Lecturer'),
                '44' => trans('translator.Professor'),
                '45' => trans('translator.ResearchAssistant'),
                '46' => trans('translator.ResearchScholar'),
                '47' => trans('translator.Teacher'),
                '48' => trans('translator.TrainingProfessionalOthers'),
            ],
            trans('translator.Engineering') => [
                '49' => trans('translator.CivilEngineer'),
                '50' => trans('translator.ElectronicsTelecomEngineer'),
                '51' => trans('translator.MechanicalProductionEngineer'),
                '52' => trans('translator.NonITEngineerOthers'),
            ],
            trans('translator.HotelHospitality') => [
                '53' => trans('translator.ChefSommelierFoodCritic'),
                '54' => trans('translator.CateringProfessional'),
                '55' => trans('translator.HotelHospitalityProfessionalOthers'),
            ],
            trans('translator.ITSoftwareEngineering') => [
                '56' => trans('translator.SoftwareDeveloperProgrammer'),
                '57' => trans('translator.SoftwareConsultant'),
                '58' => trans('translator.HardwareNetworkingprofessional'),
                '59' => trans('translator.SoftwareProfessionalOthers'),
            ],
            trans('translator.Legal') => [
                '60' => trans('translator.Lawyer'),
                '61' => trans('translator.LegalAssistant'),
                '62' => trans('translator.LegalProfessionalOthers'),
            ],
            trans('translator.MedicalHealthcare') => [
                '63' => trans('translator.Dentist'),
                '64' => trans('translator.Doctor'),
                '65' => trans('translator.MedicalTranscriptionist'),
                '66' => trans('translator.Nurse'),
                '67' => trans('translator.Pharmacist'),
                '68' => trans('translator.PhysicianAssistant'),
                '69' => trans('translator.PhysiotherapistOccupationalTherapist'),
                '70' => trans('translator.Psychologist'),
                '71' => trans('translator.Surgeon'),
                '72' => trans('translator.VeterinaryDoctor'),
                '73' => trans('translator.Therapist'),
                '74' => trans('translator.MedicalHealthcareProfessionalOthers'),
            ],
            trans('translator.MerchantNavy') => [
                '75' => trans('translator.MerchantNavalOfficer'),
                '76' => trans('translator.Mariner'),
            ],
            trans('translator.SalesMarketing') => [
                '77' => trans('translator.MarketingProfessional'),
                '78' => trans('translator.SalesProfessional'),
            ],
            trans('translator.Science') => [
                '79' => trans('translator.BiologistBotanist'),
                '80' => trans('translator.Physicist'),
                '81' => trans('translator.ScienceProfessional'),
            ],
            trans('translator.CorporateProfessionals') => [
                '82' => trans('translator.CxOChairmanDirectorPresident'),
                '83' => trans('translator.VPAVPGMDGM'),
                '84' => trans('translator.SrManagerManager'),
                '85' => trans('translator.ConsultantSupervisorTeam Leads'),
                '86' => trans('translator.TeamMemberStaff'),
            ],
            trans('translator.Others') => [
                '87' => trans('translator.AgentBrokerTraderContractor'),
                '88' => trans('translator.BusinessOwnerEntrepreneur'),
                '89' => trans('translator.Politician'),
                '90' => trans('translator.SocialWorkerVolunteerNGO'),
                '91' => trans('translator.Sportsman'),
                '92' => trans('translator.TravelTransportProfessional'),
                '93' => trans('translator.Writer'),
                '97' => trans('translator.CompanyOwner'),
                '98' => trans('translator.Other')
            ],
            trans('translator.NonWorkingForm') => [
                '94' => trans('translator.Student'),
                '95' => trans('translator.Retired'),
                '96' => trans('translator.Notworking'),
            ]
        ];

        if ($onlyData) {
            $dataArray = [];
            foreach ($workingArray as $arr) {
                foreach ($arr as $key => $val) {
                    $dataArray[$key] = $val;
                }
            }
            return $dataArray;
        }

        return $workingArray;
    }

    public static function get_an_income()
    {
        return [
            '' => trans('translator.Select'),
            '1' => Lang::get('translator.priceUpto', ['val' => '25K']),
            '2' => Lang::get('translator.priceRange', ['fval' => '25K', 'lval' => '50K']),
            '3' => Lang::get('translator.priceRange', ['fval' => '50K', 'lval' => '70K']),
            '4' => Lang::get('translator.priceRange', ['fval' => '70K', 'lval' => '100K']),
            '5' => Lang::get('translator.priceRange', ['fval' => '100K', 'lval' => '125K']),
            '6' => Lang::get('translator.priceRange', ['fval' => '125K', 'lval' => '150K']),
            '7' => Lang::get('translator.priceRange', ['fval' => '150K', 'lval' => '200K']),
            '8' => Lang::get('translator.priceRange', ['fval' => '200K', 'lval' => '250K']),
            '9' => Lang::get('translator.priceRange', ['fval' => '250K', 'lval' => '350K']),
            '10' => Lang::get('translator.priceRange', ['fval' => '350K', 'lval' => '500K']),
            '11' => Lang::get('translator.AbovePrice', ['val' => '500K']),
            '12' => trans('translator.Notapplicable'),
            '13' => trans('translator.Dontwanttospecify'),
        ];
    }

    public static function get_disability()
    {
        return [
            '1' => trans('translator.NoneDis'),
            '2' => trans('translator.PhysicalDisability'),
        ];
    }

    public static function get_skin_tone($form = true)
    {
        if ($form) {
            return ['' => trans('translator.Select'), '1' => trans('translator.Veryfair'), '2' => trans('translator.Fair'), '3' => trans('translator.Wheatish'), '4' => trans('translator.Dark')];
        } else {
            return ['1' => trans('translator.Veryfair'), '2' => trans('translator.Fair'), '3' => trans('translator.Wheatish'), '4' => trans('translator.Dark')];
        }
    }

    public static function get_body_type($form = true)
    {
        if ($form) {
            return ['' => trans('translator.Select'), '1' => trans('translator.Slim'), '2' => trans('translator.Athletic'), '3' => trans('translator.Average'), '4' => trans('translator.Heavy')];
        } else {
            return ['' => trans('translator.Select'), '1' => trans('translator.Slim'), '2' => trans('translator.Athletic'), '3' => trans('translator.Average'), '4' => trans('translator.Heavy')];
        }
    }
}
