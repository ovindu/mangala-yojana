<?php

namespace App\Library;

class NoticeBag
{
    // All notices moved to translators.
    public static function getLoginError()
    {
        return trans('translator.loginCredentialErro');
    }
    
    public static $loginNotice = 'loginNotice',
            $logoutNotice = 'logoutNotice',
            $profileUpdatedNotice = 'profileUpdatedNotice';
    public static $successAlert = 'alert-success';
    // Inbox controller - (Messages).
    public static $deleteMessageNotice = 'Messages has been deleted successfully!';
    // Email Controller
    public static $confirmRegSubjectNotce = 'Please verify your email address',

            $confirmMobileRegSubjectNotce = 'Please verify your phone number',

            $settlePaymentNotice = 'Please settle your payments',
            $newInvoice = 'Your package expires soon',
            $newProposalSubjectNotice = 'You have a new Proposal',

            $acceptProposalSubjectNotice = 'Your Proposal Accepted',
            $resetPwdSubjectNotice = 'Reset Your Password',
            $paymentComplete = 'Your payment is completed';
    //Password Controller
    public static $codeErrorNotice = 'Invalid or Expired code.',
            $pwdResetNotice = 'Your password has reset.';
    //Profile Controller.
    public static $noUserNotice = 'No User Available';
    public static $deactivateNotice = 'deactivateNotice',
            $profileReactivationNotice = 'welcomeBack';
    // Payment Received
    public static $RECEIVED_PAYMENT_ACTIVATE_AFTER = 'hnbPaymentReceived';
}
