<?php

namespace App\Http\Middleware;

use Closure;

class LocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // \App::setLocale(\Session::get('my.locale', \Config::get('app.locale')))
        if (session()->has('my.locale')) {

            /*
             * Set the Laravel locale
             */
            
            app()->setLocale(\Session::get('my.locale', \Config::get('app.locale')));

        }

        return $next($request);
    }
}
