<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use DB;
use URL;
use Request;
use Redirect;

class MobileVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        $account_age = DB::select("SELECT timestampdiff(DAY,`users`.`created_at`,curdate()) AS `account_age` FROM users WHERE id=" . $user->id . "")[0]->account_age;
        if ($user->mobile_verified == 0 && $account_age > 14) {
        // Redirect to mobile verification page.
            if (Request::url() != URL::route('mobileVerifPage'))
                return Redirect::route('mobileVerifPage');
        }

        return $next($request);
    }
}
