<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class SkipSuspends
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->suspended == 1) {
            return Redirect::to('unpaid-invoices');
        }

        return $next($request);
    }
}
