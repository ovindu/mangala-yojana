<?php

namespace App\Http\Controllers;

use App\Models\Block;
use App\Models\CommonSetting;
use App\Library\Common;
use App\Library\FormDetails;
use App\Models\Like;
use App\Models\Matches;
use App\Models\Profile;
use App\Models\Searchuser;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class SearchController extends Controller
{

    public function ShowSearchResults()
    {

        $inputs = Input::all();
        $validator = Validator::make($inputs, [
            'lookingfor' => 'required|max:1',
            'ages' => 'required|integer',
            'ageto' => 'required|integer',
            // 'mt' => 'required|max:1',
            'country' => 'required|integer|max:2',
            'relig' => 'required|integer|max:4'
        ]);
        if ($validator->fails()) {
            return Redirect::route("homePage")->with(['errors' => $validator->errors(), 'invalid' => 'true']);
        } else {
            $lookingFor = Input::get("lookingfor");
            $ageStart = Input::get("ages");
            $ageEnd = Input::get("ageto");
            $mt = Input::get("mt");
            $religion = Input::get("relig");

            $matched = Searchuser::where('gender', '=', $lookingFor)
            ->whereBetween('age', [$ageStart, $ageEnd])
            ->where('language', '=', $mt)
            ->where('religion', '=', $religion)
            ->where('id', '!=', 149582)
            ->where('hidden', 0)
            ->where('role_id', '!=', Common::$site_admin_user)
            ->paginate(5);

            if ($matched->isEmpty()) {
                return view('home/search/searchresults', ['data' => $matched,
                'profileCount' => count($matched),
                'gender' => $lookingFor,
                'agestart' => $ageStart,
                'ageend' => $ageEnd,
                'mt' => $mt,
                'relig' => $religion,
                ]);
            } else {
                return view('home/search/searchresults', ['data' => $matched,
                'profileCount' => count($matched),
                'gender' => $lookingFor,
                'agestart' => $ageStart,
                'ageend' => $ageEnd,
                'mt' => $mt,
                'relig' => $religion,
                ]);
            }
        }
    }

    public function refineSearch()
    {
        return Redirect::route("searchPage");
    }

    public function showResults()
    {
        try {
            $input = Input::all();
            if (Auth::check()) {
                $userid = Auth::user()->id;
            } else {
                $userid = 0;
            }

            $user = DB::table('searchusers')
            ->join('profiles', 'searchusers.id', '=', 'profiles.user_id')
            ->join('users', 'searchusers.id', '=', 'users.id')
            ->select('*')
            ->where('users.hidden', 0)
            ->where('searchusers.gender', '!=', Auth::user()->gender)
            ->where(function ($query) use ($input) {
                if (!empty($input['ageStart']) && !empty($input['ageEnd'])) {
                    $query->whereBetween('searchusers.age', [$input['ageStart'], $input['ageEnd']]);
                }
                if (!empty($input['heightStart']) && !empty($input['heightEnd'])) {
                    $query->whereBetween('profiles.height', [$input['heightStart'], $input['heightEnd']]);
                }
                if (!empty($input['maritalStatus'])) {
                    $query->where('profiles.marital_status', $this->GetValMultipleInput($input['maritalStatus']));
                }
                if (!empty($input['religion'])) {
                    $query->where('searchusers.religion', $this->GetValMultipleInput($input['religion']));
                }
                if (!empty($input['language'])) {
                    $query->where('searchusers.language', $this->GetValMultipleInput($input['language']));
                }
            })->where('profiles.user_id', '!=', $userid)
                ->whereNotIn('users.role_id', [Common::$site_admin_user])
                ->where('users.active', '=', '1')
                ->where('users.admin_verified', 1)
                ->orderBy('user_id', 'DESC')
                ->paginate(10);

                $gender = Auth::user()->gender;
                $ageStart = $input['ageStart'];
                $ageEnd = $input['ageEnd'];
                $profile = Profile::where('user_id', '=', Auth::user()->id)->first();
                $religion = Auth::user()->religion;

            if (!empty($user)) {
                return view('home/search/searchresults', [
                    'data' => $user,
                    'gender' => $gender,
                    'agestart' => $ageStart,
                    'ageend' => $ageEnd,
                    'mt' => $profile->language,
                    'relig' => $religion,
                    'profileCount' => count($user)
                ]);
            } else {
                return Redirect::route('searchPage');
            }
        } catch (Exception $exc) {
            return Redirect::back();
        }
    }

    private function GetValMultipleInput($fieldname)
    {
        if (is_array($fieldname)) {
            return implode(" OR ", $fieldname);
        } else {
            return $fieldname;
        }
    }

    public static function getBlockedUsers()
    {
        if (Auth::check()) {
            $userid = Auth::user()->id;
            $blocked_query = Block::where(function ($query) use ($userid) {
                $query->where('blocked_id', '=', $userid)
                ->orwhere('blocker_id', '=', $userid);
            })->get();
                return $blocked_query;
        }
    //return "User not logged in";
    }

    public static function getBlockerUsers()
    {
        if (Auth::check()) {
            $userid = Auth::id();
            $query = Block::where('blocked_id', '=', $userid)
            ->get();

            $blockers = [149582];
            foreach ($query as $blockerIDS) {
                array_push($blockers, $blockerIDS->blocker_id);
            }
            return $blockers;
        }
    }

    public function get_onlineUsers()
    {
        try {
            if (Auth::check()) {
                // $timeNow = date('Y-m-d h:i:s');
                // $date = new DateTime();
                // $nowTime = $date->format("Y-m-d h:i:s");
                // $date = new DateTime($timeNow);
                $date = Carbon::now()->format('Y-m-d h:i:s');
                date_sub($date, date_interval_create_from_date_string('10 minutes'));
                $befTenMins = date_format($date, 'Y-m-d h:i:s');
                $blockedList = [149582]; //Default mangalayojana user (Kapuwa Bot)
                $blocked_set = $this->getBlockedUsers();
                foreach ($blocked_set as $key) {
                    array_push($blockedList, $key->blocked_id);
                }
                $blocker_list = $this->getBlockerUsers();
                $users = Searchuser::join('users', 'searchusers.id', '=', 'users.id')
                ->where('users.gender', '!=', Auth::user()->gender)
                ->whereBetween('users.lastvisit', [$befTenMins, $nowTime])
                ->where('users.active', '=', 1)
                ->whereNotIn('users.id', $blockedList)
                ->whereNotIn('users.id', $blocker_list)
                ->where('users.hidden', 0)
                ->where('admin_verified', 1)
                ->paginate(5);
                $empty = 0;
                if ($users->isEmpty()) {
                    $empty = 1;
                }

                return view("home.search.whoisOnline")->with(['count' => count($users), 'data' => $users, 'state' => 1, 'empty' => $empty]);
            } else {
                return Redirect::route('searchPage');
            }
        } catch (Exception $ex) {
        }
    }

    public function getTimeIntervals($DataArray)
    {
        try {
            $dayCount = 0;
            foreach ($DataArray as $key) {
                if ($key == 0) {
                    $dayCount = $dayCount + 10000;
                } else if ($key == 1) {
                    $dayCount = $dayCount + 1;
                } else if ($key == 2) {
                    $dayCount = $dayCount + 7;
                } else if ($key == 3) {
                    $dayCount = $dayCount + 30;
                }
            }
            $timeNow = date('Y-m-d h:i:s');
            $date = new DateTime();
            $nowTime = $date->format("Y-m-d h:i:s");
            $date = new DateTime($timeNow);
            date_sub($date, date_interval_create_from_date_string($dayCount . ' days'));
            $befTenMins = date_format($date, 'Y-m-d h:i:s');
            return $befTenMins;
        } catch (Exception $ex) {
        }
    }

    public function advanceSearch()
    {
        try {
            $input = Input::all();
            $userid = 0;
            if (Auth::check()) {
                $userid = Auth::id();
            }
            $validator = Validator::make($input, [
            'agesAS' => 'required|max:3',
            'ageendAS' => 'required|max:3'
            ]);
            if ($validator->passes()) {
                $user = DB::table('searchusers')
                ->join('profiles', 'searchusers.id', '=', 'profiles.user_id')
                ->join('users', 'searchusers.id', '=', 'users.id')
                ->select('*')
                ->where(function ($query) use ($input) {
                    if (!empty($input['agesAS']) && !empty($input['ageendAS'])) {
                        $query->whereBetween('searchusers.age', [$input['agesAS'], $input['ageendAS']]);
                    }
                    if (!empty($input['heightStart']) && !empty($input['heightEnd'])) {
                        $query->whereBetween('profiles.height', [$input['heightStart'], $input['heightEnd']]);
                    }
                    if (!empty($input['maritalStatus'])) {
                        $query->orWhere('profiles.marital_status', $this->GetValMultipleInput($input['maritalStatus']));
                    }
                    if (!empty($input['religion'])) {
                        $query->orWhere('searchusers.religion', $this->GetValMultipleInput($input['religion']));
                    }
                    if (!empty($input['language'])) {
                        $query->orWhere('searchusers.language', $this->GetValMultipleInput($input['language']));
                    }
                    if (!empty($input['income'])) {
                        $query->orWhere('searchusers.an_income', $this->GetValMultipleInput($input['income']));
                    }
                    if (isset($input['joined'])) {
                        $date = new DateTime();
                        $query->whereBetween('searchusers.joined_date', [$this->getTimeIntervals($input['joined']), $date->format("Y-m-d h:i:s")]);
                    }
                    if (isset($input['active'])) {
                        $date = new DateTime();
                        $query->whereBetween('searchusers.lastvisit', [$this->getTimeIntervals($input['active']), $date->format("Y-m-d h:i:s")]);
                    }
                })->where('profiles.user_id', '!=', $userid)
                    ->where('users.gender', '!=', Auth::user()->gender)
                    ->where('users.hidden', 0)
                    ->where('admin_verified', 1)
                    ->paginate(5);

                $gender = Auth::user()->gender;
                $ageStart = $input['agesAS'];
                $ageEnd = $input['ageendAS'];
                $profile = Profile::where('user_id', '=', Auth::user()->id)->first();
                $religion = Auth::user()->religion;

                if (!empty($user)) {
                            return view('home/search/searchresults', [
                                'data' => $user,
                                'gender' => $gender,
                                'agestart' => $ageStart,
                                'ageend' => $ageEnd,
                                'mt' => $profile->language,
                                'relig' => $religion,
                                'profileCount' => count($user)
                            ]);
                } else {
                            return Redirect::route('searchPage');
                }
            } else {
                return view('home/search/search')->withErrors($validator)->with('global', 'Please select a valid date period to search!');
            }
        } catch (Exception $ex) {
        }
    }

    public function minus_time($userid, $make)
    {
    //State = 1 (Cal by joined date).
    //State = 2 (Cal by recently loggedTime).
        $curtime = time();
        $dbtime = $curtime;

        if ($make == 1) {
            $dbtime = strtotime(User::find($userid)->created_at);
        } else {
            $dbtime = strtotime(User::find($userid)->lastvisit);
        }

        $timeDiff = $curtime - $dbtime;

        $a = ['month' => 30 * 24 * 60 * 60,
        'day' => 24 * 60 * 60,
        'week' => 7 * 24 * 60 * 60,
        '10min' => 60 * 10
        ];

        if ($a['week'] < $timeDiff && $timeDiff < $a['month']) {
            return 'month';
        } elseif ($a['day'] < $timeDiff && $timeDiff < $a['week']) {
            return 'week';
        } elseif ($timeDiff < $a['day']) {
            return 'day';
        }
    }

    public function showDailyten()
    {
        $blockedList = [149582]; //Default mangalayojana user (Kapuwa Bot)
        $blocked_set = $this->getBlockedUsers();
        foreach ($blocked_set as $key) {
            array_push($blockedList, $key->blocked_id);
        }
        $blocker_list = $this->getBlockerUsers();

        $myGender = Auth::user()->gender;
        $dailyTenByGender = ($myGender == 'm' ? 'daily10_female' : 'daily10_male');
        $daily10Today = CommonSetting::where('option_name', '=', $dailyTenByGender)->first()->option_value;
        $daily10IdArray = explode(',', $daily10Today);

        $dailyten = DB::table('searchusers')
        ->whereIn('id', $daily10IdArray)
        ->whereNotIn('id', $blockedList)
        ->whereNotIn('id', $blocker_list)
        ->get();
        return view('home.dailyTen.dailyten')->with('data', $dailyten);
    }

    public function goToProfile()
    {
        $id = Input::get('goid');
        $path = URL::route(!empty($id) ? 'publicProfile' : 'searchPage');
        return redirect($path . '?id=' . $id);
    }

    public static function isOnline($userid)
    {
        $curtime = time();
        $dbtime = strtotime(User::find($userid)->lastvisit);

        $timeDiff = $curtime - $dbtime;
        $a = ['month' => 30 * 24 * 60 * 60,
        'day' => 24 * 60 * 60,
        'week' => 7 * 24 * 60 * 60,
        '10min' => 60 * 10
        ];

        if ($timeDiff > $a['10min']) {
            return 0;
        } else {
            return 1;
        }
    }

    public function getMatches()
    {
        $blockedList = [149582]; //Default mangalayojana user (Kapuwa Bot)
        $blocked_set = $this->getBlockedUsers();
        foreach ($blocked_set as $key) {
            array_push($blockedList, $key->blocked_id);
        }
        $blocker_list = $this->getBlockerUsers();

        Common::createMatchesRow();
        $cur_user_reqs = Matches::where('user_id', '=', Auth::id())->first();

        if (!empty($cur_user_reqs)) {
            $age_arr = $cur_user_reqs->age; //age
            $height_arr = $cur_user_reqs->height; // height
            $age_start = explode(',', $age_arr)[0];
            $age_end = explode(',', $age_arr)[1];
            $height_start = explode(',', $height_arr)[0];
            $height_end = explode(',', $height_arr)[1];

            $matches = Searchuser::where('gender', '!=', Auth::user()->gender)
            ->whereBetween('age', [$age_start, $age_end])
            ->whereBetween('height', [$height_start, $height_end])

        //                    ->where(function($query) use($cur_user_reqs) {
        //                        $smoke = explode(',', $cur_user_reqs->smoke);
        //                        if ($cur_user_reqs->smoke != 0)
        //                            $query->wherein('smoke', $smoke);
        //                    })
        //                    ->where(function($query) use($cur_user_reqs) {
        //                        $drink = explode(',', $cur_user_reqs->drink);
        //                        if ($cur_user_reqs->drink != 0){
        //                            $query->whereOr('drink', '=', $drink);
        //                        }
        //                    })
        //                    ->where(function($query) use($cur_user_reqs) {
        //                        $religion = explode(',', $cur_user_reqs->religion);
        //                        if ($cur_user_reqs->religion != 0)
        //                            $query->where('religion', '=', $religion);
        //                    })
        //                    ->where(function($query) use($cur_user_reqs) {
        //                        $language = explode(',', $cur_user_reqs->language);
        //                        if ($cur_user_reqs->language != 0)
        //                            $query->where('language', '=', $language);
        //                    })
        //                    ->where(function($query) use($cur_user_reqs) {
        //                        $maritalStatus = explode(',', $cur_user_reqs->marital_status);
        //                        if ($cur_user_reqs->marital_status != 0)
        //                            $query->where('marital_status', '=', $maritalStatus);
        //                    })
            ->whereNotIn('id', $blockedList)
            ->whereNotIn('id', $blocker_list)
            ->paginate(6);
            return view('home.matches.matches')->with(['data' => $matches]);
        } else {
            return view('home.matches.matches');
        }
    }

    public static function getUserData($userid)
    {
        $notGiven = '<em>Not Given</em>';
        $Form = new FormDetails;
        $user = Searchuser::find($userid);
        $profile = Profile::where('user_id', '=', $userid)->first();
        $religion = FormDetails::get_religion($notGiven)[$user->religion];
        $height = "Not Given";
        if (isset($profile->height) && $profile->height != 0) {
            $height = FormDetails::get_heightList($notGiven)[$profile->height];
        }
        $age = $user->age;
        $cityArray = $Form->get_cities();
        $cityID = $user->city;
        $city = "Not Given";
        if (isset($user->city) && $cityID != 0) {
            $city = Common::getCity($cityID);
        }
        $name = $user->fname . " " . $user->lname;
        $gender = $user->gender;
        return ['relig' => $religion, 'height' => $height, 'age' => $age, 'city' => $city, 'name' => $name, 'gender' => $gender];
    }

    public function showFavorites()
    {
        $userid = Auth::id();
        $favorites = Like::where('liker_id', '=', Auth::id())->paginate(4);
        return view('home/profile/favorites')->with('data', $favorites);
    }
}
