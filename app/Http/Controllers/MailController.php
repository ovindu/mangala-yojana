<?php

namespace App\Http\Controllers;

use App\Models\CommonSetting;
use App\Library\Common;
use App\Library\NoticeBag;
use App\Library\Salutation;
use App\Models\User;
use App\Models\UserSetting;
use Illuminate\Support\Facades\Auth;
use Mail;

class MailController
{

    public static $unverified_email_deactivation = 1;
    public static $unverified_mobile_number_deactivation = 2;
    private static $defaultEmail,
        $defaultEmailName,
        $defaultSubjectSuffix;

    public static function getDefaultEmail()
    {
        if (empty(self::$defaultEmail)) {
            self::$defaultEmail = CommonSetting::where('option_name', '=', 'defaultEmail')->first()->option_value;
        }
        return self::$defaultEmail;
    }

    public static function getDefaultEmailName()
    {
        if (empty(self::$defaultEmailName)) {
            self::$defaultEmailName = CommonSetting::where('option_name', '=', 'defaultEmailName')->first()->option_value;
        }
        return self::$defaultEmailName;
    }

    public static function getDefaultEmailSubjectSuffix()
    {
        if (empty(self::$defaultSubjectSuffix)) {
            self::$defaultSubjectSuffix = CommonSetting::where('option_name', '=', 'defaultEmailSubjectSuffix')->first()->option_value;
        }
        return self::$defaultSubjectSuffix;
    }

    public static function confirm_registration($id, $email, $fname, $profilefor)
    {
        try {
            $salute = Salutation::mail_salutation($profilefor);
            $code = User::find($id)->code;
            Mail::send('emails.confirmation', ['id' => $id, 'salute' => $salute, 'fname' => $fname, 'code' => $code], function ($message) use ($email, $fname) {
                $message->to($email, $fname)
                ->subject(NoticeBag::$confirmRegSubjectNotce . self::getDefaultEmailSubjectSuffix())
                ->from(self::getDefaultEmail(), self::getDefaultEmailName());
            });
        } catch (Exception $exc) {
        }
    }

    public static function new_msg($receiver_email, $receiver_fname, $sender_name, $sender_id, $msg_body)
    {
        try {
            $user_setting = UserSetting::where('user_id', '=', Auth::user()->id)->first()['new_msg'];
            if ($user_setting == 1) {
                $default_email = self::getDefaultEmail();
                $default_name = self::getDefaultEmailName();
                $default_email_subject_suffix = self::getDefaultEmailSubjectSuffix();
                Mail::send('emails.new_msg', ['receiver_email' => $receiver_email, 'sender_name' => $sender_name, 'fname' => $receiver_fname, 'sender_id' => $sender_id, 'msg_body' => $msg_body], function ($message) use ($sender_name, $default_email, $default_name, $receiver_email, $receiver_fname, $default_email_subject_suffix) {
                    $message->to($receiver_email, $receiver_fname)
                        ->subject('New message from ' . $sender_name . $default_email_subject_suffix)
                        ->from($default_email, $default_name);
                });
            }
        } catch (Exception $exc) {
        }
    }

    public static function profile_changed($id, $email, $fname)
    {
        try {
            $user_setting = UserSetting::where('user_id', '=', Auth::user()->id)->first()->profile_changed;
            if ($user_setting == 1) {
                Mail::send('emails.profile_edit', ['id' => $id, 'fname' => $fname], function ($message) {
                    $message->to(Auth::user()->email, Auth::user()->fname)->subject('Your Details Changed' . self::getDefaultEmailSubjectSuffix())->from(self::getDefaultEmail(), self::getDefaultEmailName());
                });
            }
        } catch (Exception $exc) {
        }
    }

    public static function new_request($id)
    {
        try {
            $user_setting = UserSetting::where('user_id', '=', Auth::user()->id)->first()->new_request;
            if ($user_setting != 1 || !Common::isUserVerifiedByAdmin()) {
                return;
            }
            $receiver = User::find($id);
            $email = $receiver->email;
            $fname = $receiver->fname;
            $sender_name = ProfileController::get_ProfileName(Auth::user()->id, true);
            Mail::send('emails.frequest', ['fname' => $fname, 'sender_name' => $sender_name, 'receiver_email' => $email, 'id' => Auth::id()], function ($message) use ($email, $fname) {
                $message->to($email, $fname)->subject(NoticeBag::$newProposalSubjectNotice . self::getDefaultEmailSubjectSuffix())->from(self::getDefaultEmail(), self::getDefaultEmailName());
            });
        } catch (Exception $exc) {
        }
    }

    public static function became_friends($id, $frqsender_fname, $email)
    {
        try {
            $user_setting = UserSetting::where('user_id', '=', $id)->first()->became_friends;
            if ($user_setting == 1) {
                $frqreceiver_name = ProfileController::get_ProfileName(Auth::user()->id, true);

                Mail::send('emails.frequest_accepted', ['fname' => $frqsender_fname, 'id' => $id, 'frqreceiver_name' => $frqreceiver_name, 'frqreceiver_email' => Auth::user()->email], function ($message) use ($frqsender_fname, $email) {
                    $message->to($email, $frqsender_fname)->subject(NoticeBag::$acceptProposalSubjectNotice . self::getDefaultEmailSubjectSuffix())->from(self::getDefaultEmail(), self::getDefaultEmailName());
                });
            }
        } catch (Exception $exc) {
        }
    }

    public static function forgot_password($id, $email, $code)
    {
        try {
            $name = ProfileController::get_ProfileName($id, true);

            Mail::send('emails.forgotPassword', ['id' => $id, 'fname' => $name, 'name' => $name, 'email' => $email, 'code' => $code], function ($message) use ($email, $name) {
                $message->to($email, $name)->subject(NoticeBag::$resetPwdSubjectNotice . self::getDefaultEmailSubjectSuffix())->from(self::getDefaultEmail(), self::getDefaultEmailName());
            });
        } catch (Exception $exc) {
        }
    }

    public static function paymentComplete($profilefor, $price, $payment_comp_id)
    {
        $salute = Salutation::mail_salutation($profilefor);
        $user = Auth::user();
        Mail::send('emails.paymentSuccess', ['salute' => $salute, 'fname' => $user->fname, 'lname' => $user->lname, 'price' => $price, 'payment_id' => $payment_comp_id], function ($message) use ($user) {
            $message->to($user->email, $user->fname)->subject(NoticeBag::$paymentComplete . self::getDefaultEmailSubjectSuffix(), self::getDefaultEmailName());
        });
    }

    public static function accountDeactivation($user_id, $mail_type)
    {
        try {
            if (isset($user_id)) {
                $user = User::find($user_id);
                $email = $user->email;
                $fname = $user->fname;
                $salute = Salutation::mail_salutation($user->profilefor);

                if ($mail_type == self::$unverified_email_deactivation) {
                    $reason = 'Please note that your account has been temporally deactivated due to pending email verifications.';
                    Mail::send('emails.deactivation', ['id' => $user->id, 'salute' => $salute, 'fname' => $user->fname, 'code' => $user->code, 'reason' => $reason, 'mail_type' => self::$unverified_email_deactivation], function ($message) use ($email, $fname) {
                        $message->to($email, $fname)
                        ->subject(NoticeBag::$confirmRegSubjectNotce)
                        ->from(self::getDefaultEmail(), self::getDefaultEmailName());
                    });
                } else if ($mail_type == self::$unverified_mobile_number_deactivation) {
                    $reason = 'Please note that your account has been temporally deactivated due to pending mobile phone verifications.';
                    Mail::send('emails.deactivation', ['id' => $user->id, 'salute' => $salute, 'fname' => $user->fname, 'code' => $user->code, 'reason' => $reason, 'mail_type' => self::$unverified_mobile_number_deactivation], function ($message) use ($email, $fname) {
                        $message->to($email, $fname)
                        ->subject(NoticeBag::$confirmMobileRegSubjectNotce)
                        ->from(self::getDefaultEmail(), self::getDefaultEmailName());
                    });
                }
            }
        } catch (Exception $exc) {
            throw new Exception("Something error in account deactivation email sending", '450', 'previous');
        }
    }

    public static function suspendAccount($user_id)
    {
        try {
            if (isset($user_id)) {
                $user = User::find($user_id);
                $email = $user->email;
                $fname = $user->fname;
                $salute = Salutation::mail_salutation($user->profilefor);

                Mail::send('emails.suspend', ['id' => $user->id, 'salute' => $salute, 'fname' => $user->fname, 'code' => $user->code], function ($message) use ($email, $fname) {
                    $message->to($email, $fname)
                        ->subject(NoticeBag::$settlePaymentNotice)
                        ->from(self::getDefaultEmail(), self::getDefaultEmailName());
                });
            }
        } catch (Exception $exc) {
            throw new Exception("Something error in account suspend email sending", '450', 'previous');
        }
    }

    public static function informPackageRenewel($user_id)
    {
        if (isset($user_id)) {
            $user = User::find($user_id);
            $email = $user->email;
            $fname = $user->fname;
            $salute = Salutation::mail_salutation($user->profilefor);

            Mail::send('emails.remindInvoice', ['id' => $user->id, 'salute' => $salute, 'fname' => $user->fname], function ($message) use ($email, $fname) {
                $message->to($email, $fname)
                ->subject(NoticeBag::$newInvoice)
                ->from(self::getDefaultEmail(), self::getDefaultEmailName());
            });
        }
    }
}
