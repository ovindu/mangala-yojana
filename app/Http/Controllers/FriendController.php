<?php

namespace App\Http\Controllers;

use App\Models\FRequests;
use App\Library\Common;
use App\Models\Like;
use App\Models\User;
use App\Models\UserSetting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class FriendController extends Controller
{

    public function send_request()
    {
        try {
            $id = Input::get('be-id');
            $path = Input::get('be-path');
            if (!(Common::isPremiumUser())) {
                $user_settings = UserSetting::where('user_id', '=', Auth::id())->first();
                if (empty($user_settings)) {
                    $user_settings = new UserSetting();
                    $user_settings->user_id = Auth::id();
                    $user_settings->frequest_count = 1;
                    $user_settings->save();
                    $this->send_request_save($id);
                } else {
                    $cur_sended_reqs = $user_settings->frequest_count;
                    if ($cur_sended_reqs < 5) {
                        $user_settings->frequest_count = ++$cur_sended_reqs;
                        $user_settings->update();
                        $this->send_request_save($id);
                    }
                }
            } else {
                $this->send_request_save($id, $path);
            }
                return redirect($path);
        } catch (Exception $ex) {
        }
    }

    public function send_request_save($id)
    {
        $frequests = new FRequests;
        $frequests->sender_id = Auth::user()->id;
        $frequests->receiver_id = $id;
        $frequests->save();
        MailController::new_request($id);
    }

    public function make_friends()
    {
        try {
            $id = Input::get('id');
            FRequests::where('sender_id', '=', $id)
            ->where('receiver_id', '=', Auth::user()->id)
            ->update(['friended' => 1]);

            $sender_details = User::find($id);
            NotificationController::setupNewNotification($id, '2', ['user_id' => Auth::user()->id]);
            MailController::became_friends($id, $sender_details->fname, $sender_details->email);
            $publicProfPath = Input::get('public-prof-path');
            if (!empty($publicProfPath)) {
                $path = Input::get('public-prof-path');
                return redirect($path);
            } else {
                return Redirect::route('profilePage');
            }
        } catch (Exception $ex) {
        }
    }

    public function ignore_request()
    {
        try {
            $id = Input::get('id');
            FRequests::where('sender_id', '=', $id)->where('receiver_id', '=', Auth::user()->id)->update(['ignored' => 1]);
            return Redirect::route('profilePage');
        } catch (Exception $ex) {
        }
    }

    public function del_request()
    {
        try {
            $id = Input::get('cancel-id');
            $path = Input::get('cancel-path');
            FRequests::where(function ($query) use ($id) {
                $query->where('sender_id', '=', Auth::user()->id)->where('receiver_id', '=', $id);
            })->Orwhere(function ($query) use ($id) {
                $query->where('sender_id', '=', $id)->where('receiver_id', '=', Auth::user()->id);
            })->delete();
                return Redirect::back();
        } catch (Exception $ex) {
        }
    }

    public function remind_request()
    {
        try {
            $id = Input::get('remind-id');
            $path = Input::get('remind-path');
            FRequests::where('sender_id', '=', Auth::user()->id)->where('receiver_id', '=', $id)->update(['ignored' => 0]);
            return redirect($path);
        } catch (Exception $ex) {
        }
    }

    public static function are_friends($id)
    {
        try {
            $table = null;
            if (Auth::check()) {
                $table = DB::table('frequests')
                ->where('friended', '=', '1')
                ->where(function ($query) use ($id) {
                    $query->Orwhere(function ($query) use ($id) {
                        $query->where('sender_id', '=', Auth::user()->id)->where('receiver_id', '=', $id);
                    });
                    $query->Orwhere(function ($query) use ($id) {
                        $query->where('sender_id', '=', $id)->where('receiver_id', '=', Auth::user()->id);
                    });
                })->first();
            }
            return (empty($table) ? false : true);
        } catch (Exception $ex) {
        }
    }

    public static function has_ignored($id)
    {
        try {
            $table = null;
            if (Auth::check()) {
                $table = FRequests::where('sender_id', '=', Auth::user()->id)->where('receiver_id', '=', $id)->where('ignored', '=', '1')->first();
            }
            return (empty($table) ? false : true);
        } catch (Exception $ex) {
        }
    }

    public static function has_requested($id)
    {
        try {
            $table = null;
            if (Auth::check()) {
                $table = FRequests::where(function ($query) use ($id) {
                    $query->where('sender_id', '=', Auth::user()->id)->where('receiver_id', '=', $id);
                })
                    ->Orwhere(function ($query) use ($id) {
                        $query->where('sender_id', '=', $id)->where('receiver_id', '=', Auth::user()->id);
                    })
                    ->where('friended', '<>', 1)->first();
            }
            return (empty($table) ? false : true);
        } catch (Exception $ex) {
        }
    }

    public static function list_requests()
    {
        try {
            $friendRequests = FRequests::with('users', 'profiles')->where('receiver_id', '=', Auth::user()->id)->where('friended', '=', 0)->where('ignored', '=', 0)->orderBy('id', 'desc')->get();
            $friendRequests->each(function ($item, $key) {
                if (!Common::isUserVerifiedByAdmin($item->sender_id)) {
                    $friendRequests->forget($key);
                }
            });
                return $friendRequests;
        } catch (Exception $ex) {
        }
    }

    public static function request_info($id)
    {
        try {
            if (Auth::check()) {
                return FRequests::with('users')->where('sender_id', '=', Auth::user()->id)->where('receiver_id', '=', $id)->first();
            }
        } catch (Exception $ex) {
        }
    }

    public static function if_sender($id)
    {
        try {
            $frq_table = null;
            if (Auth::check()) {
                $frq_table = FRequests::where('sender_id', '=', Auth::user()->id)->where('receiver_id', '=', $id)->first();
            }
            return !empty($frq_table);
        } catch (Exception $ex) {
        }
    }

    public function chkFrequests()
    {
        try {
            $val = 0;
            $frequests = FRequests::where('receiver_id', '=', Auth::user()->id)->where('ignored', '=', 0)->where('friended', '=', 0)->get();
            foreach ($frequests as $request) {
                if (Common::isUserVerifiedByAdmin($request->sender_id)) {
                    $val++;
                }
            }

            if ($val > 0) {
                echo '<div id="freq-count">' . $val . '</div>';
            }
        } catch (Exception $ex) {
        }
    }

    public function fetchFrqs()
    {
        try {
            $profileCtrl = new ProfileController();
            $freq_list = FriendController::list_requests();
            $goto = Input::get('path');
            return view('home.profile/notifications', ['profileCtrl' => $profileCtrl, 'freq_list' => $freq_list, 'goto' => $goto]);
        } catch (Exception $ex) {
        }
    }

    public function showFriends()
    {
        $userid = Auth::id();
        $friends = DB::table('frequests')
        ->where('friended', '=', 1)
        ->Where(function ($query) {
            $query->where('sender_id', '=', Auth::id())
            ->orWhere('receiver_id', '=', Auth::id());
        })
        ->paginate(4);
        return view('home.profile.friends')->with('data', $friends);
    }

    public static function setLike($liker_id, $liked_id)
    {
        try {
            $like = new Like();
            $like->liker_id = $liker_id;
            $like->liked_id = $liked_id;
            $like->save();
            NotificationController::setupNewNotification($liked_id, '1', ['user_id' => $liker_id]);
            return $like->id;
        } catch (Exception $ex) {
        }
    }

    public static function isLiked($userid)
    {
        try {
            $currUID = Auth::id();
            $like_bool = Like::where('liker_id', '=', $currUID)
                ->where('liked_id', '=', $userid)->first();
            if (empty($like_bool)) {
                return 0;
            } else {
                return $like_bool->id;
            }
        } catch (Exception $ex) {
        }
    }

    public function unfriendUser()
    {
        $friendID = Input::get('id');
        $authID = Auth::id();
        if ($this->are_friends($friendID)) {
            $friend = FRequests::where('sender_id', '=', $authID)->where('receiver_id', '=', $friendID)->first();
            if (empty($friend)) {
                $friend = FRequests::where('receiver_id', '=', $authID)->where('sender_id', '=', $friendID)->first();
            }
            $friend->delete();
        }
        return Redirect::route('publicProfile', ['id' => $friendID]);
    }

    public static function getFriendsList()
    {
        $flist = [];
        $fid = [];
        $userid = Auth::id();
        $friends = DB::table('frequests')
        ->where('friended', '=', 1)
        ->Where(function ($query) {
            $query->where('sender_id', '=', Auth::id())
            ->orWhere('receiver_id', '=', Auth::id());
        })
        ->get();
        foreach ($friends as $user) {
            if ($user->sender_id == Auth::id()) {
                $userid = $user->receiver_id;
            } else {
                $userid = $user->sender_id;
            }
                $user = User::find($userid);
                $friendName = $user->fname . ' ' . $user->lname;
                array_push($flist, $friendName);
                array_push($fid, $userid);
        }
        $mainArray = [$flist, $fid];
        return $mainArray;
    }
}
