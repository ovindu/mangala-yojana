<?php

namespace App\Http\Controllers;

use App\Models\CommonSetting;
use App\Library\Common;
use App\Library\NoticeBag;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Models\Invoice;
use App\Models\Payment;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentsController
 *
 * @author Sahan Dissanayake HP-PC
 */
class PaymentsController extends Controller
{

    public function doPayment()
    {
        //Common::setCommonData('some_testse', json_encode(Input::all()));
        $hnbPayment;
        $pamentType = Input::get('type');
        switch ($pamentType) {
            case 'paypal':
                $data = $this->doPaypalPayment();
                if ($data['done']) {
                    return Redirect::route($data['route'], $data['route_para'])->with($data['with']);
                } else {
                    return Redirect::route($data['route']);
                }
                break;
            case 'hnb':
                $hnbPayment = $this->doHNBpayment();
                break;
            default:
                echo 'No payment method received';
        }
        if (isset($hnbPayment)) {
            if ($hnbPayment) {
                return Redirect::route('profilePage')->with(['global' => NoticeBag::$RECEIVED_PAYMENT_ACTIVATE_AFTER, 'alert-type' => 'alert-success']);
            } else {
                return Redirect::route('payErrorPage');
            }
        }
    }

    public function redirectToPaypal()
    {
    }

    public function doPaypalPayment()
    {
        $payerID = Input::get('payer_id');
        $tax = Input::get('tax');
        $adrs_strt = Input::get('address_street');
        $payDate = Input::get('payment_date');
        $ccode = Input::get('address_country_code');
        $vsign = Input::get('verify_sign');
        $auth = Input::get('auth');
        $description = "payer_id : $payerID , Tax : $tax , Street Address : $adrs_strt , Pay Date : $payDate , Country code : $ccode , Verify sign : $vsign , Auth : $auth ";


        if (Input::has('txn_id')) {
            $package_price = CommonSetting::where('option_name', 'premium_package_price_USD')->first()->option_value;
            $day_count = Common::getCommonData('package_expire_after');
            $date = strtotime("$day_count day");
            $after_day = date('Y-m-d', $date);
            $invoice_id = null;
            $unpaid_invoices = Invoice::where('user_id', Input::get('userid'))->where('paid', 0)->get();
            if ($unpaid_invoices->isEmpty()) {
                $invoice_id = InvoiceController::createInvoice(Common::$DEFAULT_PACKAGE_FOR_PREMIUM_USER, Auth::id(), $package_price, $after_day, true);
            }
            $invid = ($invoice_id == null ? Input::get('invoice_id') : $invoice_id);
            $pymt = new Payment();
            $pymt->invoice_id = $invid;
            $pymt->user_id = Auth::id();
            $pymt->pay_method = Input::get('type');
            $pymt->description = $description;
            $pymt->txn_id = Input::get('txn_id');
            $pymt->save();

            $invoice_upd = Invoice::find($invid);
            if (!empty($invoice_upd)) {
                $invoice_upd->paid = 1;
                $invoice_upd->due_date = $after_day;
                $invoice_upd->update();
                $usr = User::find(Input::get('userid'));
                $usr->suspended = 0;
                $usr->update();
            }

            $user = Auth::user();
            $user->role_id = Common::$premium_user;
            $user->update();
            MailController::paymentComplete(Auth::user()->profilefor, Common::getCommonData('premium_package_price_USD'), $pymt->id);
            return ['done' => true, 'route' => 'showSettings', 'route_para' => 'package-upgrade', 'with' => ['payment_id' => $pymt->id]];
        } else {
            return ['done' => false, 'route' => 'payErrorPage'];
        }
    }

    public function doHNBpayment()
    {
        $trans_id = Input::get('transaction_id');
        if (!empty($trans_id)) {
            // Create the invoice;
            $day_count = Common::getCommonData('package_expire_after');
            $date = strtotime("$day_count day");
            $after_day = date('Y-m-d', $date);

            $invoice = new Invoice();
            $invoice->package_id = Common::$DEFAULT_PACKAGE_FOR_PREMIUM_USER;
            $invoice->user_id = Auth::id();
            $invoice->amount = CommonSetting::where('option_name', 'premium_package_price_LKR')->first()->option_value;
            $invoice->due_date = $after_day;
            $invoice->paid = 1;
            $invoice->save();

            $payment = new Payment();
            $payment->invoice_id = $invoice->id;
            $payment->pay_method = 'hnb';
            $payment->description = 'Manual Payment';
            $payment->txn_id = $trans_id;
            $payment->completed = 0;
            $payment->save();
            return true;
        } else {
            return false;
        }
    }

    public function completePayment()
    {
        $paymentType = Input::get('payment_type');
        $paymentID = Input::get('payment_id');
        $pay_user = Input::get('payment_user');
        if ($paymentType == 'hnb' && !empty($paymentID) && !empty($pay_user)) {
            $day_count = Common::getCommonData('package_expire_after');
            $date = strtotime("$day_count day");
            $after_day = date('Y-m-d', $date);

            $paymnt = Payment::find($paymentID);

            $invoice = Invoice::find($paymnt->invoice_id);
            $invoice->due_date = $after_day;
            $invoice->paid = 1;
            $invoice->update();

            $paymnt->completed = 1;
            $paymnt->update();
            
            $user = User::find($pay_user);
            if (!empty($user)) {
                $user->role_id = Common::$premium_user;
                $user->update();
            }
        }
        return Redirect::back();
    }
}
