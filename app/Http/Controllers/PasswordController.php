<?php

namespace App\Http\Controllers;

use App\Library\NoticeBag;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

/**
 * Description of PasswordController
 *
 * @author Thambaru Wijesekara
 */
class PasswordController extends Controller
{

    public function handleForgotPassword()
    {
        $email = Input::get('email');
        $user = User::where('email', '=', $email)->first();
        if (empty($user)) {
            return null;
        } else {
            $code = str_random(40);
            User::where('email', '=', $email)->update(['passwordResetToken' => $code]);
            MailController::forgot_password($user->id, $email, $code);
            return "possitive";
        }
    }

    public function checkToken()
    {
        $id = Input::get('id');
        $code = Input::get('token');
        $codeInDB = User::where('id', '=', $id)->first()->passwordResetToken;
        if ($codeInDB == $code) {
            return view('home.passwordReset');
        } else {
            return view('home.passwordReset')->with('codeError', NoticeBag::$codeErrorNotice);
        }
    }

    public function resetPassword()
    {
        $inputs = Input::all();
        $validator = Validator::make($inputs, [
                    'password-new' => 'min:8',
                    'password-confirm' => 'same:password-new',
                        ]);
        if ($validator->passes()) {
            $hashed_new_pwd = Hash::make($inputs['password-new']);
            User::where('id', '=', $inputs['id'])->update(['passwordResetToken' => '', 'password' => $hashed_new_pwd]);
            return Redirect::route('homePage')->with('global', NoticeBag::$pwdResetNotice);
        } else {
            return Redirect::route('passwordReset')->withErrors($validator);
        }
    }
}
