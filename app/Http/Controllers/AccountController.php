<?php

namespace App\Http\Controllers;

use App\Models\Deactivation;
use App\Library\Common;
use App\Library\NoticeBag;
use App\Models\Matches;
use App\Models\Message;
use App\Models\Profile;
use App\Models\User;
use App\Models\UserSetting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{
    /*
     * 
     * Manage the login request
     * 
     */

    public static $ACCOUNT_SYSTEM_DEACTIVATION = 1;
    public static $ACCOUNT_USER_DEACTIVATION = 2;
    public static $ACCOUNT_SITE_ADMIN_DEACTIVATION = 3;

    public function doLogin()
    {
        try {
            $input = Input::all();
            $validator = Validator::make($input, [
                'login-email' => 'required|email',
                'login-password' => 'required'
            ]);

            $remember = empty($input['remember']) ? false : true;

            if ($validator->fails()) {
                return Redirect::route('homePage')->withErrors($validator)->withInput();
            } else {
                $auth = Auth::attempt(['email' => $input['login-email'], 'password' => $input['login-password'], 'active' => 1], $remember);
                if (!$auth) {
                    $recheck = Auth::attempt(['email' => $input['login-email'], 'password' => $input['login-password'], 'inactive_status' => self::$ACCOUNT_USER_DEACTIVATION]);
                }
                if ($auth | (isset($recheck) && $recheck)) {
                    date_default_timezone_set('UTC');
                    $user = User::find(Auth::id(Auth::id()));
                    $user->lastvisit = date("Y-m-d h:i:s");
                    $user->active = 1;
                    $user->inactive_status = 1;
                    $user->save();
                    Common::createMatchesRow();
                    if (isset($recheck) && $recheck) {
                        return Redirect::intended(URL::route('profilePage', [], false))->with(['global' => NoticeBag::$profileReactivationNotice, 'alert-type' => NoticeBag::$successAlert]);
                    } else {
                        return Redirect::intended(URL::route('profilePage', [], false));
                    }
                } else {
                    return Redirect::route('homePage')->withInput()->with('loginError', NoticeBag::getLoginError());
                }
            }
        } catch (Exception $ex) { }
    }

    public static function editContactInfo()
    {
        if (Auth::check()) {
            $userId = Common::isAdmin() ? Input::get('userId') : Auth::user()->id;
            $user = User::find($userId);
            $edited_pno = Input::get('mobilenumber');
            $edited_email = Input::get('emailedit');
            $edited_facebook = Input::get('facebookedit');
            if ($user->mobilenumber != $edited_pno) {
                Common::setForAdminApproval('mobilenumber', strip_tags($edited_pno));
                $user->mobile_verified = 0;
            }

            if ($user->email != $edited_email) {
                $user->email = $edited_email;
                $user->verified = 0;
            }

            $user->facebook = strip_tags($edited_facebook);
            $user->update();
            return Redirect::back();
        }
    }

    public function doLogout()
    {
        Auth::logout();
        return Redirect::route('homePage')->with('logoutMsg', NoticeBag::$logoutNotice);
    }

    /*
     * 
     * Handles the registration
     * 
     */

    public function doRegister()
    {
        // dd(Input::all());
        $input = Input::all();
        $code = str_random(40);
        $validator = Validator::make($input, [
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'fname' => 'required|min:3',
            'lname' => 'required|min:3',
            'dobyear' => 'required',
            'dobmonth' => 'required',
            'dobdate' => 'required',
            'gender' => 'required',
            'profilefor' => 'required',
            'profiletype' => 'required',
            'religion' => 'required',
            'language' => 'required',
            'country' => 'required',
            'agree' => 'required',
            'mobilenumber' => 'required'
        ]);

        $user = new User;
        $profile = new Profile;

        if ($validator->fails()) {
            return Redirect::route('homePage')->withErrors($validator)->withInput();
        } else {
            $user->fname = strip_tags($input['fname']);
            $user->lname = strip_tags($input['lname']);
            $user->email = $input['email'];
            $user->password = Hash::make($input['password']);
            $user->bday = $input['dobyear'] . "-" . $input['dobmonth'] . "-" . $input['dobdate'];
            $user->gender = $input['gender'];
            $user->profilefor = $input['profilefor'];
            $user->profiletype = $input['profiletype'];
            $user->role_id = 1;
            $user->active = 1;
            $user->verified = 0;
            $user->religion = $input['religion'];
            $user->code = $code;
            $user->mobilenumber = $input['mobilenumber'];
            $user->mobile_verified = ($input['country'] == 'LK' ? 0 : 1);
            $user->save();
            $profile->user_id = $user->id;
            $profile->language = $input['language'];
            $profile->country = $input['country'];
            $profile->save();
            MailController::confirm_registration($user->id, $input['email'], $input['fname'], $input['profilefor']);

            $msgs_table = new Message;
            $msgs_table->receiver_id = $user->id;
            $msgs_table->sender_id = 149582;
            $msgs_table->body = "Hi " . $input['fname'] . ",\r\n We mostly welcome you to MangalaYojana.LK! You can Upgrade your account features with a Premium Account, Upload your own profile with an handy image, Search for Best Matches, Start a conversation and Yes, finally write your eternity yourself!\r\nWe wish you best of luck! \r\nRegards, Team MangalaYojana.";
            $msgs_table->save();

            $userSettings = new UserSetting;
            $userSettings->user_id = $user->id;
            $userSettings->new_msg = 1;
            $userSettings->profile_changed = 1;
            $userSettings->new_request = 1;
            $userSettings->became_friends = 1;
            $userSettings->save();

            if (Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password')], true)) {
                Common::createMatchesRow();
                Common::setPrivacyinBorn($user->id);
                return Redirect::route('mobileVerifPage')->with('newUser', "Welcome {$input['fname']} to mangalayojana.lk! Complete your registration.");
            }
        }
    }

    public function completeRegistration()
    {
        try {
            $input = Input::all();
            $validator = Validator::make($input, [
                'city' => 'required',
                'residency' => 'required',
                'marital_status' => 'required',
                'children' => 'required',
                'height' => 'required',
                'skin_tone' => 'required',
                'body_type' => 'required',
                'edu_level' => 'required',
                'edu_field' => 'required',
                'working_on' => 'required',
                'working_at' => 'required',
                'an_income' => 'required',
                'description' => 'required|between:50,4000',
                'skin_tone' => 'required',
                'body_type' => 'required',
                'diet' => 'required',
                'smoke' => 'required',
                'drink' => 'required',
                'disability' => 'required'
            ]);

            if ($validator->fails()) :
                return Redirect::route('registrationPage')->withErrors($validator)->withInput()->with('msg', 'The values you entered are invalid, please fill out all required fields.');
            else :
                $row = Profile::where('user_id', '=', Auth::user()->id)->first();
                if (empty($row)) {
                    $profile = new Profile;
                    $profile->province = $input['province'];
                    $profile->city = $input['city'];
                    $profile->small_city = $input['small-city'];
                    $profile->residency = $input['residency'];
                    $profile->marital_status = $input['marital_status'];
                    $profile->children = $input['children'];
                    $profile->height = $input['height'];
                    $profile->skin_tone = $input['skin_tone'];
                    $profile->body_type = $input['body_type'];
                    $profile->edu_level = $input['edu_level'];
                    $profile->edu_field = $input['edu_field'];
                    $profile->working_on = $input['working_on'];
                    $profile->working_at = $input['working_at'];
                    $profile->an_income = $input['an_income'];
                    $profile->description = strip_tags($input['description']);
                    $profile->diet = $input['diet'];
                    $profile->drink = $input['drink'];
                    $profile->smoke = $input['smoke'];
                    $profile->disability = $input['disability'];

                    $profile->user_id = Auth::user()->id;
                    $profile->save();
                } else {
                    Profile::where('user_id', '=', Auth::user()->id)
                        ->update([
                            'city' => $input['city'],
                            'residency' => $input['residency'],
                            'marital_status' => $input['marital_status'],
                            'children' => $input['children'],
                            'height' => $input['height'],
                            'skin_tone' => $input['skin_tone'],
                            'body_type' => $input['body_type'],
                            'edu_level' => $input['edu_level'],
                            'edu_field' => $input['edu_field'],
                            'working_on' => $input['working_on'],
                            'working_at' => $input['working_at'],
                            'an_income' => $input['an_income'],
                            'description' => strip_tags($input['description']),
                            'diet' => $input['diet'],
                            'drink' => $input['drink'],
                            'smoke' => $input['smoke'],
                            'disability' => $input['disability']
                        ]);
                }
                $userName = Auth::user()->fname;
                if (!$this->created_today()) {
                    MailController::profile_changed(Auth::user()->id, Auth::user()->email, Auth::user()->fname);
                }

                return Redirect::route('matrimonialPage')->with("msg", "Hello" . $userName . "Last step for complete registration.");
            endif;
        } catch (Exception $ex) { }
    }

    public function doFamilyDetailsRegistration()
    {
        try {
            $input = Input::all();
            $validator = Validator::make($input, [
                'fatherStatus' => 'required',
                'motherStatus' => 'required',
                'brotherCount' => 'required',
                'sisterCount' => 'required',
                'brotherCountMarried' => 'required',
                'sisterCountMarried' => 'required',
                'familyValue' => 'required',
                'ancestralOrigin' => 'required'
            ]);

            if ($validator->passes()) :
                $row = Profile::where('user_id', '=', Auth::user()->id)->first();
                if (empty($row)) {
                    $profile = new Profile;
                    $profile->fatherStatus = $input['fatherStatus'];
                    $profile->motherStatus = $input['motherStatus'];
                    $profile->brotherCount = $input['brotherCount'];
                    $profile->sisterCount = $input['sisterCount'];
                    $profile->brotherCountMarried = $input['brotherCountMarried'];
                    $profile->sisterCountMarried = $input['sisterCountMarried'];
                    $profile->familyValue = $input['familyValue'];
                    $profile->ancestralOrigin = strip_tags($input['ancestralOrigin']);
                    $profile->weight = $input['weight'];
                    $profile->bloodGroup = $input['bloodGroup'];
                    $profile->health = $input['health'];

                    $profile->user_id = Auth::user()->id;
                    $profile->save();
                } else {
                    Profile::where('user_id', '=', Auth::user()->id)
                        ->update([
                            'fatherStatus' => $input['fatherStatus'],
                            'motherStatus' => $input['motherStatus'],
                            'brotherCount' => $input['brotherCount'],
                            'sisterCount' => $input['sisterCount'],
                            'brotherCountMarried' => $input['brotherCountMarried'],
                            'sisterCountMarried' => $input['sisterCountMarried'],
                            'familyValue' => $input['familyValue'],
                            'ancestralOrigin' => $input['ancestralOrigin'],
                            'weight' => $input['weight'],
                            'bloodGroup' => $input['bloodGroup'],
                            'health' => $input['health']
                        ]);
                }

                if (!$this->created_today()) {
                    MailController::profile_changed(Auth::user()->id, Auth::user()->email, Auth::user()->fname);
                }

                return Redirect::route('partnerPage')->with('msg', 'You make it!');
            else :
                return Redirect::route('matrimonialPage')->withErrors($validator)->withInput()->with('msg', 'The values you entered are invalid, please fill out all required fields.');
            endif;
        } catch (Exception $ex) { }
    }

    public function doAskPartnerDetails()
    {
        try {
            $input = Input::all();
            $validator = Validator::make($input, [
                'ageStart' => 'required',
                'ageEnd' => 'required',
                'heightStart' => 'required',
                'heightEnd' => 'required',
            ]);

            if ($validator->passes()) :
                $Fields = ['ageStart', 'ageEnd', 'heightStart', 'maritalStatus', 'religion', 'language', 'residency', 'edu_level', 'workingAt', 'workingWith', 'body', 'skin', 'smoke', 'diet', 'drink'];

                foreach ($Fields as $keys) {
                    if (!isset($input[$keys])) {
                        $input[$keys] = 0;
                    }
                }

                $row = Matches::where('user_id', '=', Auth::user()->id)->first();

                if (empty($row)) {
                    $matches = new Matches;
                    $matches->age = $input['ageStart'] . ',' . $input['ageEnd'];
                    $matches->height = $input['heightStart'] . ',' . $input['heightEnd'];
                    $matches->marital_status = $input['maritalStatus'];
                    $matches->religion = $input['religion'];
                    $matches->language = $input['language'];
                    $matches->residency = $input['residency'];
                    $matches->edu_level = $input['edu_level'];
                    $matches->working_on = $input['workingAt'];
                    $matches->position = $input['workingWith'];
                    $matches->body_type = $input['body'];
                    $matches->skin_tone = $input['skin'];
                    $matches->drink = $input['drink'];
                    $matches->smoke = $input['smoke'];
                    $matches->diet = $input['diet'];
                    $matches->user_id = Auth::user()->id;
                    $matches->save();
                } else {
                    Matches::where('user_id', '=', Auth::user()->id)->update([
                        'age' => $input['ageStart'] . ',' . $input['ageEnd'],
                        'height' => $input['heightStart'] . ',' . $input['heightEnd'],
                        'marital_status' => $input['maritalStatus'],
                        'religion' => $input['religion'],
                        'language' => $input['language'],
                        'residency' => $input['residency'],
                        'edu_level' => $input['edu_level'],
                        'working_on' => $input['workingAt'],
                        'position' => $input['workingWith'],
                        'body_type' => $input['body'],
                        'skin_tone' => $input['skin'],
                        'drink' => $input['drink'],
                        'smoke' => $input['smoke'],
                        'diet' => $input['diet'],
                    ]);
                }

                if (!$this->created_today()) {
                    MailController::profile_changed(Auth::user()->id, Auth::user()->email, Auth::user()->fname);
                }

                return Redirect::route('thankyouPage');
            else :
                return Redirect::route('partnerPage')->withErrors($validator)->withInput();
            endif;
        } catch (Exception $ex) { }
    }

    public function doBasicInfoEdit()
    {
        try {
            $inputs = input::all();
            $Fields = ['height', 'marital_status', 'skin_tone', 'body_type', 'diet', 'drink', 'smoke'];

            foreach ($Fields as $field) {
                foreach ($inputs as $input) {
                    if (!isset($inputs[$field])) {
                        $inputs[$field] = 0;
                    }
                }
            }

            $userId = Common::isAdmin() ? $inputs['userId'] : Auth::user()->id;

            Profile::where('user_id', '=', $userId)->update([
                'height' => $inputs['height'],
                'marital_status' => $inputs['marital_status'],
                'skin_tone' => $inputs['skin_tone'],
                'body_type' => $inputs['body_type'],
                'diet' => $inputs['diet'],
                'drink' => $inputs['drink'],
                'smoke' => $inputs['smoke']
            ]);
            if (!$this->created_today()) {
                MailController::profile_changed(Auth::user()->id, Auth::user()->email, Auth::user()->fname);
            }
        } catch (Exception $exc) { }
        return Redirect::back()->with(['global' => NoticeBag::$profileUpdatedNotice, 'alert-type' => NoticeBag::$successAlert]);
    }

    public function showgeneralInfoPage()
    {
        try {
            $input = Input::all();
            $validator = Validator::make($input, [
                'fname' => 'required|min:3',
                'lname' => 'required|min:3',
                'dobyear' => 'required',
                'dobmonth' => 'required',
                'dobdate' => 'required',
                'religion' => 'required',
                'language' => 'required',
            ]);

            if ($validator->passes()) {
                $user = User::where('id', '=', Auth::user()->id)->first();
                $profile_row = Profile::where('user_id', '=', Auth::user()->id)->first();
                if (empty($profile_row)) {
                    $profile = new Profile;
                    $user->fname = $input['fname'];
                    $user->lname = $input['lname'];
                    $user->bday = $input['dobyear'] . "-" . $input['dobmonth'] . "-" . $input['dobdate'];
                    $user->religion = $input['religion'];
                    $profile->language = $input['language'];

                    $profile->user_id = Auth::user()->id;
                    $user->save();
                    $profile->save();
                } else {
                    User::where('id', '=', Auth::user()->id)->update([
                        'fname' => $input['fname'],
                        'lname' => $input['lname'],
                        'bday' => $input['dobyear'] . "-" . $input['dobmonth'] . "-" . $input['dobdate'],
                        'religion' => $input['religion'],
                    ]);
                    Profile::where('user_id', '=', Auth::user()->id)->update([
                        'language' => $input['language'],
                    ]);
                }
                if (!$this->created_today()) {
                    MailController::profile_changed(Auth::user()->id, Auth::user()->email, Auth::user()->fname);
                }
                return Redirect::route('profilePage')->with('success', 'alert-success');
            } else {
                return Redirect::route('basicInfoPage')->withErrors($validator)->withInput();
            }
        } catch (Exception $ex) { }
    }

    public function editAccountInfo()
    {
        try {
            $inputs = Input::all();
            $validator = Validator::make($inputs, [
                'email' => 'required|email',
                'email-confirm' => 'same:email',
                'password-new' => 'min:8',
                'password-confirm' => 'same:password-new',
                'password-current' => 'required',
                'fname' => 'required',
                'lname' => 'required',
            ]);

            $pwds_same = Auth::attempt(['email' => Auth::user()->email, 'password' => $inputs['password-current']]);

            if ($validator->passes() && $pwds_same) {
                if (!empty($inputs['fname']) && $inputs['fname'] <> Auth::user()->fname) { //New fname
                    Common::setForAdminApproval('fname', $inputs['fname']);
                }

                if (!empty($inputs['lname']) && $inputs['lname'] <> Auth::user()->lname) { //New lname
                    Common::setForAdminApproval('lname', $inputs['lname']);
                }


                if (!empty($inputs['password-new']) && $inputs['password-confirm'] == $inputs['password-new']) {
                    $hashed_new_pwd = Hash::make($inputs['password-new']);
                    User::where('id', '=', Auth::id())->update(['password' => $hashed_new_pwd]);
                }

                $same_emails = User::where('email', '=', Auth::user()->email)->first();
                if ($inputs['email'] <> Auth::user()->email && empty($same_emails)) {
                    User::where('id', '=', Auth::id())->update(['email' => $inputs['email']]);
                }
                if (!$this->created_today()) {
                    MailController::profile_changed(Auth::user()->id, Auth::user()->email, Auth::user()->fname);
                }
                return Redirect::route('profilePage')->with(['global' => NoticeBag::$profileUpdatedNotice, 'alert-type' => NoticeBag::$successAlert]);
            } else {
                return Redirect::back()->withErrors($validator)->withInput();
            }
        } catch (Exception $ex) { }
    }

    public function dofamilyDetailsEdit()
    {
        try {
            $userId = Common::isAdmin() ? Input::get('userId') : Auth::user()->id;
            Profile::where('user_id', '=', $userId)->update([
                'fatherStatus' => Input::get('fatherStatus'),
                'motherStatus' => Input::get('motherStatus'),
                'brotherCount' => Input::get('brotherCount'),
                'brotherCountMarried' => Input::get('brotherCountMarried'),
                'sisterCount' => Input::get('sisterCount'),
                'sisterCountMarried' => Input::get('sisterCountMarried'),
                'familyValue' => Input::get('familyValue'),
                'affluenceLevel' => Input::get('affluenceLevel'),
                'ancestralOrigin' => Input::get('ancestralOrigin')
            ]);
            if (!$this->created_today()) {
                MailController::profile_changed(Auth::user()->id, Auth::user()->email, Auth::user()->fname);
            }
        } catch (Exception $ex) { }
        return Redirect::back()->with(['global' => NoticeBag::$profileUpdatedNotice, 'alert-type' => NoticeBag::$successAlert]);;
    }

    public function doeduProfEdit()
    {
        try {
            $userId = Common::isAdmin() ? Input::get('userId') : Auth::user()->id;
            Profile::where('user_id', '=', $userId)->update([
                'edu_level' => Input::get('edu_level'),
                'working_on' => Input::get('working_on'),
                'an_income' => Input::get('an_income'),
                'working_at' => Input::get('working_at')
            ]);
            if (!$this->created_today()) {
                MailController::profile_changed(Auth::user()->id, Auth::user()->email, Auth::user()->fname);
            }
        } catch (Exception $ex) { }
        return Redirect::back()->with(['global' => NoticeBag::$profileUpdatedNotice, 'alert-type' => NoticeBag::$successAlert]);
    }

    public function doLocationEdit()
    {
        try {
            Profile::where('user_id', '=', Auth::id())->update([
                'city' => Input::get('city'),
                'small_city' => Input::get('small-city'),
                'residency' => Input::get('residency')
            ]);



            if (!$this->created_today()) {
                MailController::profile_changed(Auth::user()->id, Auth::user()->email, Auth::user()->fname);
            }
        } catch (Exception $ex) { }
        return Redirect::route('profilePage')->with(['global' => NoticeBag::$profileUpdatedNotice, 'alert-type' => NoticeBag::$successAlert]);
    }

    public function doPartnerPreferencesEdit()
    {
        try {
            $inputs = Input::all();
            $Fields = ['ageStart', 'ageEnd', 'heightStart', 'marital_status', 'religion', 'language', 'edu_level', 'body_type', 'skin_tone', 'smoke', 'diet', 'drink'];

            foreach ($Fields as $field) {
                foreach ($inputs as $input) {
                    if (!isset($inputs[$field])) {
                        $inputs[$field] = 0;
                    }
                    if (is_array($inputs[$field])) {
                        $inputs[$field] = implode(',', $inputs[$field]);
                    }
                }
            }
            $userId = Common::isAdmin() ? $inputs['userId'] : Auth::user()->id;
            Matches::where('user_id', '=', $userId)->update([
                'age' => $inputs['ageStart'] . ',' . $inputs['ageEnd'],
                'height' => $inputs['heightStart'] . ',' . $inputs['heightEnd'],
                'marital_status' => $inputs['marital_status'],
                'religion' => $inputs['religion'],
                'language' => $inputs['language'],
                'edu_level' => $inputs['edu_level'],
                'diet' => $inputs['diet'],
                'smoke' => $inputs['smoke'],
                'drink' => $inputs['drink'],
                'body_type' => $inputs['body_type'],
                'skin_tone' => $inputs['skin_tone']
            ]);
            if (!$this->created_today()) {
                MailController::profile_changed(Auth::user()->id, Auth::user()->email, Auth::user()->fname);
            }
        } catch (Exception $ex) { }
        return Redirect::back()->with(['global' => NoticeBag::$profileUpdatedNotice, 'alert-type' => NoticeBag::$successAlert]);
    }

    public function confirmUserRegistration()
    {
        try {
            $id = Input::get('id');
            $code = Input::get('code');
            if (!empty($id)) {
                $code_db = User::find($id)->code;
                if (!empty($code) && $code == $code_db) {
                    if (User::find($id)->verified == 0) {
                        User::where('id', '=', $id)->update(['verified' => 1, 'active' => 1, 'hidden' => 0]);
                        return Redirect::route('homePage')->with(['message' => trans('translator.Successfulverified'), 'alert-type' => 'alert-success']);
                    } else {
                        return Redirect::route('homePage')->with(['message' => trans('translator.AlreadyVerified'), 'alert-type' => 'alert-success']);
                    }
                } else {
                    return Redirect::route('homePage')->with(['message' => trans('translator.InvalidVerifCode'), 'alert-type' => 'alert-danger']);
                }
            } else {
                return Redirect::route('homePage')->with(['message' => trans('translator.VerifiyCodeBrocken'), 'alert-type' => 'alert-danger']);
            }
        } catch (Exception $ex) { }
    }

    public static function generateRandomString($length = 10)
    {
        try {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, strlen($characters) - 1)];
            }
            return $randomString;
        } catch (Exception $ex) { }
    }

    public static function created_today()
    {
        try {
            $user_timestamp = User::find(Auth::user()->id)->created_at;
            $mysql_to_unix = strtotime($user_timestamp);
            $user_created_date = date('Y-m-d', $mysql_to_unix);
            $current_date = date('Y-m-d', time());
            if ($user_created_date == $current_date) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $ex) { }
    }

    public function doEditAboutMe()
    {
        try {
            $finalText = strip_tags(Input::get('desc'));
            $str_count = strlen($finalText);
            if ($str_count >= 50) {
                Common::setForAdminApproval('description', $finalText);
            }
            return Redirect::route('profilePage')->with(['global' => NoticeBag::$profileUpdatedNotice, 'alert-type' => NoticeBag::$successAlert]);
        } catch (Exception $ex) { }
    }

    public function doDeactivation()
    {
        if (Input::has('password')) {
            $pwds_same = Auth::attempt(['email' => Auth::user()->email, 'password' => Input::get('password')]);
            if (Auth::check() & Input::has('partner') & Input::has('reason') & $pwds_same) {
                $authid = Auth::id();
                $partnerId = Input::get('partner');
                $reason = Input::get('reason');
                $user = User::find(Auth::id());
                $user->active = 0;
                $user->inactive_status = self::$ACCOUNT_USER_DEACTIVATION;
                $user->update();
                Auth::logout();

                $deactivation = new Deactivation();
                $deactivation->user_id = $authid;
                $deactivation->partner_id = Input::get('partner');
                $deactivation->description = Input::get('reason');
                $deactivation->save();
                return Redirect::route('homePage')->with('deacNotice', trans('translator.' . NoticeBag::$deactivateNotice));
            } else {
                return Redirect::back()->with('global', trans('translator.invalidPassword'));
            }
        }
    }

    public function checkInactiveAccounts()
    {
        $inactive_user = 0;
        $active_users = DB::select("SELECT *,timestampdiff(DAY,`users`.`created_at`,curdate()) AS `account_age` FROM users WHERE created_at BETWEEN DATE_SUB(NOW(), INTERVAL 10 DAY) AND NOW() AND verified='0' AND active=1");
        foreach ($active_users as $active_user) {
            if ($active_user->account_age > 7) {
                if (!Common::isAdministrator($active_user->id)) {
                    $user = User::find($active_user->id);
                    $user->active = $inactive_user;
                    $user->remember_token = " ";
                    $user->update();
                    MailController::accountDeactivation($active_user->id, MailController::$unverified_email_deactivation);
                    echo $active_user->id . ' Has been deactivated, Period Expired. = ' . $active_user->account_age . '<br>';
                } else {
                    echo 'Unverified user found (' . $active_user->id . ') , Days for deactivation : ' . (14 - $active_user->account_age) . ' <br>';
                }
            }
        }

        $mobile_unverified_users = DB::select("SELECT *,timestampdiff(DAY,`users`.`created_at`,curdate()) AS `account_age` FROM users WHERE created_at BETWEEN DATE_SUB(NOW(), INTERVAL 18 DAY) AND NOW() AND mobile_verified='0' AND active=1");
        foreach ($mobile_unverified_users as $muv_user) {
            if (!Common::isAdministrator($muv_user->id)) {
                if ($muv_user->account_age > 14) {
                    $user = User::find($muv_user->id);
                    $user->remember_token = " ";
                    $user->update();
                    MailController::accountDeactivation($user->id, MailController::$unverified_mobile_number_deactivation);
                    echo $muv_user->id . ' Has been deactivated, Period Expired. = ' . $muv_user->account_age . '<br>';
                } else {
                    echo 'Unverified user found (' . $muv_user->id . ') , Days for deactivation : ' . (14 - $muv_user->account_age) . ' <br>';
                }
            }
        }
        InvoiceController::createRenewInvoice();
    }

    public static function showInvoicesForSuspends()
    {
        return view('payments.invoicesForSuspends')->with(['paid_invoices' => Common::getUserPaidInvoices(), 'unpaid_invoices' => Common::getUserUnPaidInvoices()]);
    }
}
