<?php

namespace App\Http\Controllers;

use App\Library\Common;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Notification;

class NotificationController extends Controller
{

    public static function getNotificationsForUser()
    {
        $not = [];
        $notifications = Notification::where('read', '=', '0')
        ->where('user_id', '=', Auth::id())
        ->orderBy('id', 'DESC')
        ->get();
        foreach ($notifications as $notification) {
            $data = base64_decode($notification->data);
            if (Common::isUserVerifiedByAdmin($notification['user_id'])) {
                continue;
            }
            array_push($not, $notification);
        }
        return $not;
    }

    public static function getNotificationCountForUser()
    {
        return count(self::getNotificationsForUser());
    }

    public static function setupNewNotification($user_id, $layout_id, $data = [])
    {
        $data_ = base64_encode(json_encode($data));

        if ($layout_id == '1') {
            self::deleteOldLikeNotifications($user_id, $layout_id, $data_);
        }

        $not = new Notification();
        $not->user_id = $user_id;
        $not->layout_id = $layout_id;
        $not->read = 0;
        $not->data = $data_;
        $not->save();
        if (!empty($notif)) {
            return true;
        }
        return false;
    }

    private static function deleteOldLikeNotifications($user_id, $layout_id, $data_)
    {
        DB::table("notifications")->where('user_id', '=', $user_id)->where('layout_id', '=', $layout_id)->where('data', '=', $data_)->delete();
    }

    public static function notificationBuild($notification)
    {
        $lay_id = $notification->layout_id;
        $data = json_decode(base64_decode($notification->data), true);
        if (Common::isUserVerifiedByAdmin($notification['user_id'])) {
            return;
        }
        $not_text = null;
        $img = null;
        $url = null;
        switch ($lay_id) {
            case '1': // user_id
                $text = '<b>[[name]]</b> ' . trans('translator.likedYourProfile');
                $user = User::find($data['user_id']);
                $not_text = str_replace('[[name]]', $user->fname . ' ' . $user->lname, $text);
                $img = ProfileController::getProfileImageUrl(true, $user->id, 'thumb');
                $url = ProfileController::getProfileUrl($user->id);
                break;

            case '2': // user_id
                $text = '<b>[[name]]</b> ' . trans('translator.AcceptedYourRequest');
                $user = User::find($data['user_id']);
                $not_text = str_replace('[[name]]', $user->fname . ' ' . $user->lname, $text);
                $img = ProfileController::getProfileImageUrl(true, $user->id, 'thumb');
                $url = ProfileController::getProfileUrl($user->id);
                break;

            default:
                break;
        }

        return [
        'id' => $notification->id,
        'text' => $not_text,
        'img' => asset($img),
        'ago' => Common::getAgoTimeString(strtotime($notification->created_at)),
        'url' => $url
        ];
    }
}
