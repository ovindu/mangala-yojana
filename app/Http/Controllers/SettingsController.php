<?php

namespace App\Http\Controllers;

use App\Library\Common;
use App\Models\UserSetting;
use App\Models\Invoice;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

/**
 * Description of SettingsController
 *
 * @author Sashen Pasindu
 */
class SettingsController extends Controller
{

    public function showSettings($tab)
    {
        $general = '';
        $notifications = '';
        $privacy = '';
        $packageUpgrade = '';
        $payments = '';
        switch ($tab) {
            case 'general':
                $general = 'active';
                break;
            case 'privacy':
                $privacy = 'active';
                break;
            case 'notifications';
                $notifications = 'active';
                break;
            case 'package-upgrade':
                $packageUpgrade = 'active';
                break;
            case 'payments':
                $payments = 'active';
                break;
            case '':
                $general = 'active';
                break;
        }
        $blocked_list = BlockController::unblock_list();
        $mailSettings = UserSetting::where('user_id', '=', Auth::id())->first();
        return view('settings.settings', ['noti' => $notifications, 'pkgUp' => $packageUpgrade, 'gen' => $general, 'pri' => $privacy, 'pymnt' => $payments, 'blocked_list' => $blocked_list, 'paid_invoices' => Common::getUserPaidInvoices(), 'unpaid_invoices' => Common::getUserUnPaidInvoices(), 'mail_settings' => $mailSettings]);
    }

    public function showGeneralSettings()
    {
        return Redirect::route('showGenral');
    }

    public function emailSettings()
    {
        $newReq = Input::get('new-request');
        $newMsg = Input::get('new-msg');
        $becameFriends = Input::get('became-friends');
        $profChanged = Input::get('profile-changed');

        UserSetting::where('user_id', '=', Auth::user()->id)->update(['new_request' => $newReq, 'new_msg' => $newMsg, 'became_friends' => $becameFriends, 'profile_changed' => $profChanged]);
        $settingsRoute = URL::route('showGeneralSettings');
        return redirect($settingsRoute . '/notifications');
    }
}
