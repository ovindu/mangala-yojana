<?php

namespace App\Http\Controllers;

use App\Models\Block;
use App\Library\Common;
use App\Library\NoticeBag;
use App\Models\Message;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

/**
 * Description of InboxController
 *
 * @author Sashen Pasindu
 */
class InboxController extends Controller
{

    public function showInbox()
    {
        $id = Auth::user()->id;

        $sent = DB::select('SELECT distinct sender_id FROM messages WHERE receiver_id = ' . $id . ' AND receiver_del = 0 ORDER BY created_at DESC');


        $msgs = [];
        $ig = 1;
        $alreadyLoadedThreads = [0 => 1];
        foreach ($sent as $sender) {
            $singleThread = DB::table('messages')->join('users', 'messages.sender_id', '=', 'users.id')->select('*', 'messages.created_at as income_time')->where('messages.sender_id', '=', $sender->sender_id)->where('messages.receiver_id', '=', $id)->orderBy('messages.id', 'desc')->first();
            $msgs[$ig] = $singleThread;
            $alreadyLoadedThreads [$ig] = $sender->sender_id;

            $ig++;
        }

        $received = DB::select('SELECT distinct receiver_id FROM messages WHERE sender_id = ' . $id . ' AND sender_del = 0 AND receiver_id NOT IN (' . implode(",", $alreadyLoadedThreads) . ') ORDER BY created_at DESC');

        foreach ($received as $receiver) {
            $singleThread = DB::table('messages')->join('users', 'messages.receiver_id', '=', 'users.id')->select('*', 'messages.created_at as income_time')->where('messages.receiver_id', '=', $receiver->receiver_id)->where('messages.sender_id', '=', $id)->orderBy('messages.id', 'desc')->first();
            $msgs[$ig] = $singleThread;
            $ig++;
        }
        $blockers = BlockController::unblock_list();
        $msg_arr_count = 1;

        foreach ($msgs as $msg) {
            if (!$blockers->isEmpty()) {
                foreach ($blockers as $blocker) {
                    if (($msg->receiver_id == $blocker->blocker_id || $msg->sender_id == $blocker->blocker_id) && ($msg->receiver_id == $blocker->blocked_id || $msg->sender_id == $blocker->blocked_id)) {
                        $msgs[$msg_arr_count]->blocked = true;
                    } else {
                        $msgs[$msg_arr_count]->blocked = false;
                    }
                }
            } else {
                $msgs[$msg_arr_count]->blocked = false;
            }
            $msg_arr_count++;
        }
        return view('home.messages.inbox', ['msgs' => $msgs]);
    }

    public function showMsg($userid)
    {
        $last_user = User::orderBy('id', 'desc')->first();
        $friends = ($userid == 149582 ? true : FriendController::are_friends($userid));
        if (empty($userid) || $userid == Auth::user()->id || $userid > $last_user->id) {
            return Redirect::route('inboxPage')->with('someError', true);
        } else {
            Message::with('user')
                    ->where('receiver_id', '=', Auth::user()->id)
                    ->where('sender_id', '=', $userid)
                    ->where('receiver_del', '=', '0')
                    ->update(['receiver_read' => 1]);

            Message::with('user')
                    ->where('sender_id', '=', Auth::user()->id)
                    ->where('receiver_id', '=', $userid)
                    ->where('sender_del', '=', '0')
                    ->update(['sender_read' => 1]);

            $all_msgs = DB::table('messages')
                    ->join('users', 'messages.sender_id', '=', 'users.id')
                    ->select('messages.body', 'messages.created_at', 'messages.receiver_id', 'messages.sender_id', 'messages.receiver_read', 'users.id', 'users.fname', 'users.lname')
                    ->orWhere(function ($query) use ($userid) {
                        $query->where('sender_id', '=', Auth::user()->id)
                        ->where('receiver_id', '=', $userid)
                        ->where('sender_del', '=', '0');
                    })->orWhere(function ($query) use ($userid) {
                        $query->where('receiver_id', '=', Auth::user()->id)
                        ->where('sender_id', '=', $userid)
                        ->where('receiver_del', '=', '0');
                    })
                    ->orderBy('created_at', 'asc')
                    ->get()
                    ->toArray();

            if (empty($all_msgs)) {
                $all_msgs = User::find($userid);
                $msg_details = $all_msgs;
                $new_msg = true;
            } else {
                $new_msg = false;
                if ($all_msgs[0]->sender_id == Auth::user()->id) {
                    $msg_details = User::with('messages')->where('id', '=', $all_msgs[0]->receiver_id)->first();
                } else {
                    $msg_details = $all_msgs[0];
                }
            }

            $blocked_query = Block::where(function ($query) use ($userid) {
                        $query->where('blocked_id', '=', $userid)
                        ->orwhere('blocker_id', '=', $userid);
            })
                    ->where(function ($query) use ($userid) {
                        $query->where('blocked_id', '=', Auth::user()->id)
                        ->orwhere('blocker_id', '=', Auth::user()->id);
                    })
                    ->first();
            if (!empty($blocked_query)) {
                $blocked = true;
            } else {
                $blocked = false;
            }

            return view('home.messages.message', ['all_msgs' => $all_msgs, 'msg_details' => $msg_details, 'SelfprofPic' => ProfileController::getProfileImageUrl(false, null, 'thumb'), 'SenderprofPic' => ProfileController::getProfileImageUrl(true, $userid, 'thumb'), 'new_msg' => $new_msg, 'blocked' => $blocked, 'friends' => $friends, 'userid' => $userid]);
        }
    }

    public function doReceiver_delete()
    {
        $userid = Input::get('userid');
        $chkboxString = Input::get('id');
        if (!empty($userid)) {
            DB::table("messages")
                    ->orWhere(function ($query) use ($userid) {
                        $query->where('sender_id', '=', Auth::user()->id)
                        ->where('receiver_id', '=', $userid);
                    })->update(['sender_del' => 1]);

            DB::table("messages")
                    ->orWhere(function ($query) use ($userid) {
                        $query->where('sender_id', '=', $userid)
                        ->where('receiver_id', '=', Auth::user()->id);
                    })->update(['receiver_del' => 1]);
        } elseif (!empty($chkboxString)) {
            $idArray = explode(',', $chkboxString);

            $loggedUserID = Auth::user()->id;

            foreach ($idArray as $id) {
                Message::where('sender_id', '=', $loggedUserID)->where('receiver_id', '=', $id)->update(['sender_del' => 1]);
                Message::where('receiver_id', '=', $loggedUserID)->where('sender_id', '=', $id)->update(['receiver_del' => 1]);
            }
        }
        return Redirect::route('inboxPage')->with(['delete' => true, 'msg' => NoticeBag::$deleteMessageNotice]);
    }

    public function sendMsg()
    {
        $messageData = Input::all();
        $msg = new Message;
        $msg->body = $messageData['msg-body'];
        $msg->sender_id = Auth::user()->id;
        $msg->receiver_id = $messageData['userid'];
        $msg->save();

        $msg_table = User::find($messageData['userid']);

        $sender_name = $msg_table->fname . ' ' . $msg_table->lname;

        MailController::new_msg($msg_table->email, $sender_name, Auth::user()->fname, Auth::user()->id, $messageData['msg-body']);

        return Redirect::route('messagePage', ['userid' => $messageData['userid'], 'send' => true]);
    }

    public function doMarkAsRead()
    {
        $chkboxString = Input::get('id');
        $msgArray = explode(',', $chkboxString);
        $loggedUserID = Auth::user()->id;
        foreach ($msgArray as $msg) {
            Message::where('sender_id', '=', $loggedUserID)->where('receiver_id', '=', $msg)->update(['sender_read' => 1]);
            Message::where('receiver_id', '=', $loggedUserID)->where('sender_id', '=', $msg)->update(['receiver_read' => 1]);
        }
        return Redirect::route('inboxPage')->with('setSeen', true);
    }

    public function doMarkAsUnread()
    {
        $chkboxString = Input::get('id');
        $msgArray = explode(',', $chkboxString);
        $loggedUserID = Auth::user()->id;
        foreach ($msgArray as $msg) {
            Message::where('sender_id', '=', $loggedUserID)->where('receiver_id', '=', $msg)->update(['sender_read' => 0]);
            Message::where('receiver_id', '=', $loggedUserID)->where('sender_id', '=', $msg)->update(['receiver_read' => 0]);
        }
        return Redirect::route('inboxPage')->with('markAsUnead', true);
    }

    public function chkMsgs()
    {
        $msgs = DB::table('messages')->where('receiver_id', '=', Auth::user()->id)->where('receiver_read', '=', 0)->groupBy('sender_id')->get();
        $val = count($msgs);
        if ($val !== 0) {
            echo '<div id="msg-count">' . $val . '</div>';
        }
    }

    public static function notifyMessages()
    {
        return DB::table('messages')->join('users', 'users.id', '=', 'messages.sender_id')->select('*', 'messages.created_at as income_time')->where('receiver_id', '=', Auth::user()->id)->where('receiver_read', '=', '0')->orderBy('messages.id', 'desc')->groupBy('sender_id')->get();
    }

    public static function fetchMsgList()
    {
        $newMsg_list = InboxController::notifyMessages();
        $profileCtrl = new ProfileController();
        if (empty($newMsg_list)) :
            echo'
            
                <div class = "row">
                    <div class = "req col-sm-6 col-sm-offset-4"> 
                        '.trans('translator.NoNewMessages').'
                    </div>
                </div>
                ';
        else :
            foreach ($newMsg_list as $msg) :
                $incomingTime = strtotime($msg->income_time);
                echo '<a href = "' . URL::route('messagePage', ['userid' => $msg->sender_id]).'">
                        <div class = "row">
                            <div class = "col-sm-7">
                                <span class = "msgName">' . $profileCtrl->get_ProfileName($msg->sender_id, true) . '</span>
                            </div>
                            <div class = "col-sm-5">
                                <span class = "msgTime">' . Common::getAgoTimeString($incomingTime) . '</span>
                            </div>
                        </div>
                        <div class = "row">
                            <div class = "col-sm-12">
                                <div class = "msgMsg">
                                    ' . Str_Limit($msg->body, 70) . '
                                </div>
                            </div>
                        </div>
                    </a>';
            endforeach;
        endif;
    }
}
