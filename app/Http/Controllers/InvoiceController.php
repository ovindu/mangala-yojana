<?php

namespace App\Http\Controllers;

use App\Library\Common;
use App\Models\User;
use App\Library\PayPal\Api\Invoice;

class InvoiceController extends Controller
{

    public static function createInvoice($package_id, $user_id, $amount, $due_date, $paid = 0)
    {
        $invoice = new Invoice();
        $invoice->package_id = $package_id;
        $invoice->user_id = $user_id;
        $invoice->amount = $amount;
        $invoice->due_date = $due_date;
        $invoice->paid = $paid;
        $invoice->save();
        return $invoice->id;
    }

    public static function createRenewInvoice()
    {
        $premium_users = User::where('role_id', Common::$premium_user)->where('id', '!=', '149582')->where('suspended', 0)->where('active', 1)->get();
        foreach ($premium_users as $user) {
            $last_paid_invoice = Invoice::where('user_id', $user->id)->where('paid', 1)->orderBy('created_at', 'DESC')->first();
            if (!empty($last_paid_invoice)) {
                $today = date('Y-m-d');
                $invoice_date = $last_paid_invoice->created_at;
                $days = (strtotime($today) - strtotime($invoice_date)) / (60 * 60 * 24);
                echo $days;
                $sys_remind_end = Common::getCommonData('package_expire_after');
                $sys_remind_date = $sys_remind_end - 7;

                if ($days >= $sys_remind_date & $days <= $sys_remind_end) {
                    $unpaid_invoices = Invoice::where('user_id', $user->id)->where('paid', 0)->get();
                    if ($unpaid_invoices->isEmpty()) {
                        MailController::informPackageRenewel($user->id);
                        self::createInvoice($last_paid_invoice->package_id, $last_paid_invoice->user_id, Common::getCommonData('premium_package_price_LKR'), '0000-00-00', false);
                        echo 'unpaid invoice created and email sent';
                    }
                } else if ($sys_remind_end < $days) {
                    //Send email to inform deactivation.
                    MailController::suspendAccount($user->id);

                    $usr = User::find($user->id);
                    $usr->suspended = 1;
                    $usr->update();
                }
            }
        }
    }
}
