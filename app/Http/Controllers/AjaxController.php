<?php

namespace App\Http\Controllers;

use App\Models\FRequests;
use App\Library\Common;
use App\Models\Like;
use App\Models\MobileVerifications;
use App\Models\Report;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Models\Notification;
use Illuminate\Http\Request;
use Carbon\Carbon;

class AjaxController extends Controller
{

    public function hitTheLastOnlineRecord()
    {
        $theUser = User::find(Auth::id());
        date_default_timezone_set("UTC");
        $theUser->lastvisit = date("Y-m-d H:i:s");
        $theUser->save();
        return Response::json(true);
    }

    public function doUnblock()
    {
        $id = Input::get('id');
        BlockController::doUnblock($id);
    }

    public function hitLike()
    {
        $userid = Auth::id();
        $likedUser = Input::get('id');
        $insertID = FriendController::setLike($userid, $likedUser);
        return $insertID;
    }

    public function removeLike()
    {
        $liked_row_id = Input::get('id');
        $like = Like::find($liked_row_id);
        if ($like->liker_id == Auth::id()) {
            $like->delete();
        } else {
            return Response::json(['msg' => "you don't have permission to delete this!"]);
        }
    }

    /*
     * 
     * Notifications
     * 
     */

    public function getNotifications()
    {
        $nots = Notification::where('user_id', '=', Auth::id())
            ->orderBy('id', 'DESC')
            ->get();
        $unread = Notification::where('user_id', '=', Auth::id())
            ->where('read', '=', '0')
            ->count();
        $arr = [];
        foreach ($nots as $not) {
            $built = NotificationController::notificationBuild($not);
            $arr[] = $built;
        }
        return Response::json(['unread_count' => $unread, 'notifications' => $arr]);
    }

    public function closeNotifications()
    {
        $notif_id = Input::get('not_id');
        $stat = DB::table("notifications")
            ->where('id', '=', $notif_id)
            ->where('user_id', '=', Auth::user()->id)
            ->delete();
        if (!$stat) {
            return Response::json(false);
        }
        return Response::json(true);
    }

    public function readNotifications()
    {
        $ids = Input::get('ids');
        if ($ids !== null) {
            foreach ($ids as $id) {
                DB::table("notifications")
                    ->where('id', '=', $id)
                    ->where('user_id', '=', Auth::user()->id)
                    ->update(['read' => 1]);
            }
            return Response::json(true);
        } else {
            return Response::json(false);
        }
    }

    /*
     *  Friend Controllers
     * 
     */

    public function unfriend_user()
    {
        $fid = Input::get('fid');
        $friend = Input::get('friend');
        if (FriendController::are_friends($friend)) {
            $row = FRequests::find($fid);
            $row->delete();
        }
        return Response::json(true);
    }

    /*
     * Report Controllers
     */

    public function reportUser()
    {
        $fid = Input::get('fid');
        $message = Input::get('message');
        if (Auth::check()) {
            $report = new Report();
            $report->reporter_id = Auth::id();
            $report->reported_id = $fid;
            $report->description = strip_tags($message);
            $report->save();
        }
        return Response::json(true);
    }

    public function resend_confirmation_email()
    {
        $user = Auth::user();
        $id = Auth::id();
        $email = $user->email;
        $fname = $user->fname;
        $profilefor = $user->profilefor;
        if (isset($user) && !empty($user)) {
            MailController::confirm_registration($id, $email, $fname, $profilefor);
        }
    }

    public function sendMobileVerificationCode($mob_no)
    {
        $mob_no = $mob_no;
        if (str_contains('+', $mob_no)) {
            $mob_no = str_replace('+', '', $mob_no);
        }

        // Genarate verification code
        $mob_no = preg_replace('/\s+/', '', $mob_no);
        $verification_code = 0;
        $rand_nbr = rand(100000, 999999);

        // Check code already sent
        $mobile_verif = MobileVerifications::where('user_id', '=', Auth::id())->first();
        if (empty($mobile_verif)) {
            $is_unique_code = !$this->codeIsUnique($rand_nbr);
            while ($is_unique_code) {
                $new_rand_nbr = rand(100000, 999999);
                $rand_nbr = $new_rand_nbr;
                $is_unique_code = !$this->codeIsUnique($new_rand_nbr);
            }

            $new_verif = new MobileVerifications();
            $new_verif->user_id = Auth::id();
            $new_verif->code = $rand_nbr;
            $new_verif->save();
            $verification_code = $rand_nbr;
        } else {
            $is_unique_code = !$this->codeIsUnique($rand_nbr);
            while ($is_unique_code) {
                $new_rand_nbr = rand(100000, 999999);
                $rand_nbr = $new_rand_nbr;
                $is_unique_code = !$this->codeIsUnique($new_rand_nbr);
            }

            $new_verif = $mobile_verif;
            $new_verif->user_id = Auth::id();
            $new_verif->code = $rand_nbr;
            $new_verif->update();
            $verification_code = $rand_nbr;
        }

        /*
         * Send the code
         */
        try {
            // requesting auth token sms
            $url        = 'https://digitalreachapi.dialog.lk/refresh_token.php';

            // DATA JASON ENCODED
            $data       = array("u_name" => "Accent_api", "passwd" => "0770112470");
            $data_json  = json_encode($data);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);

            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Accept: application/json'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // DATA ARRAY
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response  = curl_exec($ch);

            if ($response === false)
                $response = curl_error($ch);
            $accesstoken = json_decode($response, true)['access_token'];

            curl_close($ch);

            $url        = 'https://digitalreachapi.dialog.lk/camp_req.php';

            $data = array(
                "msisdn" => $mob_no,
                "channel" => "1",
                "mt_port" => "Mangalayoja",
                "s_time" => Carbon::today()->format('Y-m-d H:i:s'),
                "e_time" => Carbon::today()->addDays(3)->format('Y-m-d H:i:s'),
                "msg" => "Thank you for joining with Mangalayojana.lk. Your code is " . $verification_code . "."
            );

            $params = json_encode($data);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization:' . $accesstoken));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);

            $phpAr = json_decode($response, true);

            if ($phpAr['error'] == 0) {
                return json_encode([
                    'error' => false,
                    'number' => $mob_no,
                    'success' => true,
                ]);
            } else {
                return json_encode([
                    'error' => true,
                    'number' => $mob_no,
                    'success' => false
                ]);
            }
        } catch (Exception $ex) {
            echo $ex;
        }
    }

    public function codeIsUnique($random_code)
    {
        $old_codes = MobileVerifications::where('code', '=', $random_code)->get();
        return ($old_codes->isEmpty() ? true : false);
    }

    public function updateMobileNumber()
    {
        $mob_no = Input::get('mob_num');
        if (strlen($mob_no) > 7) {
            $user = Auth::user();
            $user->mobilenumber = $mob_no;
            $user->update();
            $returnJson = $this->sendMobileVerificationCode($mob_no);
            return Response::json(json_decode($returnJson));
        } else {
            return Response::json([
                'error' => true,
                'number' => $mob_no,
                'code' => '100',
                'text' => "Phone number is required.",
                'success' => false
            ]);
        }
    }

    public function verifyMobileNumber()
    {
        $return_obj;
        $user_received_code = Input::get('conf_code');
        $system_sent_code = MobileVerifications::where('user_id', '=', Auth::id())->first();
        $system_code = $system_sent_code->code;
        if ($system_code == $user_received_code) {
            if (Common::isCodeLive($system_sent_code)) {
                $auth_user = Auth::user();
                $auth_user->mobile_verified = 1;
                $auth_user->update();
                $return_obj = ['error' => false, 'data' => trans('translator.MVSuccess')];
            } else {
                $return_obj = ['error' => true, 'data' => trans('translator.MVFailed')];
            }
        } else {
            $return_obj = ['error' => true, 'data' => 'Code is invalid. Please check and try again.'];
        }
        echo json_encode($return_obj);
    }
}
