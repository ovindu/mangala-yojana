<?php

namespace App\Http\Controllers;

use App\Models\AdminReviews;
use App\Models\Advertisements;
use App\Models\CommonSetting;
use App\Library\Common;
use App\Library\ImageLib;
use App\Library\NoticeBag;
use App\Models\Profile;
use App\Models\Report;
use App\Models\Searchuser;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use App\Models\Invoice;
use App\Models\Payment;
use Illuminate\Http\Request;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BackendController
 *
 * @author Sahan Dissanayake HP-PC
 */
class BackendController extends Controller
{

    public static $profilePicUploadDir = 'data/uploads/profile';

    public function showBackendLogin()
    {
        self::saveMe();
        if (Auth::check()) {
            if (Auth::user()->role_id == 2) {
                return Redirect::route('Backend_Dashboard');
            }
        } else {
            return Redirect::route('Backend_Login');
        }
    }

    public function editUser($id)
    {
        if (Auth::check()) {
            $user = User::find($id);
            $data = Searchuser::where('id', '=', $id)->get();
            return view('backend.editUser')->with(['user' => $user, 'data' => $data]);
        }
    }

    public function doLogin()
    {
        $input = Input::all();
        $validator = Validator::make($input, [
            'login-email' => 'required|email',
            'login-password' => 'required'
        ]);

        $remember = empty($input['remember']) ? false : true;

        if ($validator->fails()) {
            return view('backend.login')->withErrors($validator)->withInput()->with('global', "You have an error in your credentials");
        } else {
            $auth = Auth::attempt(['email' => $input['login-email'], 'password' => $input['login-password'], 'role_id' => 2], $remember);
            if ($auth) {
                date_default_timezone_set('UTC');
                $user = User::find(Auth::id(Auth::id()));
                $user->lastvisit = date("Y-m-d h:i:s");
                $user->save();
                Common::createMatchesRow();
                return Redirect::intended(URL::route('Backend_Dashboard', [], false))->with('global', NoticeBag::$loginNotice);
            } else {
                return Redirect::route('Backend_Login')->withInput()->with('loginError', NoticeBag::getLoginError());
            }
        }
    }

    public function doLogout()
    {
        if (Auth::check()) {
            Auth::logout();
        }
        return Redirect::route('Backend_Login');
    }

    public function getNewestUsers()
    {
        $users = User::where('id', '!=', 149582)
        ->where('mobilenumber', '!=', '987145632')
        ->orderBy('id', 'DESC')->take(10)->get();
        return Redirect::route('users_Dashboard')->with(['users' => $users, 'default', true]);
    }

    public function showUsersPage()
    {
        $input = Input::all();
        if (Input::has('profile_created') && Input::has('gender')) {
            $date = \Carbon\Carbon::today();
            $nowTime = $date->format("Y-m-d h:i:s");

            if (Input::get('profile_created') == 1) { // Before week
                date_sub($date, date_interval_create_from_date_string('7 days'));
            } else if (Input::get('profile_created') == 30) { // Before month
                date_sub($date, date_interval_create_from_date_string('30 days'));
            } else if (Input::get('profile_created') == 90) { //Three month
                date_sub($date, date_interval_create_from_date_string('90 days'));
            } else {
                date_sub($date, date_interval_create_from_date_string('5000 months'));
            }
            $finalBefDate = date_format($date, 'Y-m-d h:i:s');
            $users = DB::table('users')
            ->select('*')
            ->where('admin_verified', '!=', 0)
            ->where('id', '!=', 149582)
            ->where('id', '!=', Auth::id())
            ->where('mobilenumber', '!=', '987145632')
            ->where(function ($query) use ($input, $nowTime, $finalBefDate) {
                $query->whereBetween('created_at', [$finalBefDate, $nowTime]);
            })
            ->where(function ($query) {
                if (Input::get('gender') != '0') {
                    $query->where('gender', '=', Input::get('gender'));
                }
            })
            ->where(function ($query) {
                if (Input::get('user_state') != 'a') {
                    $query->where('active', '=', Input::get('user_state'));
                }
            })
            ->where(function ($query) {
                if (Input::get('user_package') != 'a') {
                    $query->where('role_id', '=', Input::get('user_package'));
                }
            })
            ->orderBy('id', 'DESC')
            ->paginate(10);
            return view("backend.users")->with(['users' => $users, 'showLinks' => true, 'inputsOLD' => $input]);
        } else {
            $users = User::where('id', '!=', 149582)
            ->where('id', '!=', Auth::id())
            ->where('role_id', '!=', 2)
            ->where('admin_verified', '!=', 0)
            ->orderBy('id', 'DESC')->paginate(10);
            return view("backend.users")->with(['users' => $users, 'inputsOLD' => $input]);
        }
    }

    public static function getUserDescription($userid)
    {
        return Profile::where('user_id', '=', $userid)->first()->description;
    }

    public function updateUser()
    {
        if (Auth::check() && Input::has('userid')) {
            $user = User::find(Input::get('userid'));
            $user->fname = Input::get('first_name');
            $user->lname = Input::get('last_name');
            $user->role_id = Input::get('user_package');
            $user->update();

            $profile = Profile::where('user_id', '=', Input::get('userid'))->first();
            $profile->description = Input::get('description');
            $profile->update();
            return Redirect::back()->withInput()->with('success', "Changes updated successfully.");
        } else {
            return Redirect::back()->withInput()->with('error', "Something wrong with updating please retry!");
        }
    }

    public function updateUserProPic(Request $request){

        $user = $request->input('user');
        // dd($user);
        $sizes = explode(',', $request->input('cropPosition'));
        if (!$request->hasFile('theProfilePic')) {
            return redirect()->back();
        }

        if (!$request->file('theProfilePic')->isValid()) {
            return redirect()->back();
        }

        $theImage = $request->file('theProfilePic');

        $existingFiles = glob(self::$profilePicUploadDir . '/*_c' . $user . '_*');
        if (is_array($existingFiles)) {
            foreach ($existingFiles as $file) {
                unlink($file);
            }
        }
// dd($existingFiles);
        $isPending = '';

        $isEmptyCurrentImg = glob(self::$profilePicUploadDir . '/*_c' . $user . '_*');

        if (!empty($isEmptyCurrentImg)) {
        // $isPending = 'pending';
        // Common::setForAdminApproval('profile picture', 'has');
        }


        $dir = self::$profilePicUploadDir;
        $extension = strtolower($theImage->getClientOriginalExtension());
        $name_without_ext = '_c' . $user . $isPending . '_' . time() . '_' . strtolower(str_random(15));
        $name = $name_without_ext . '.' . $theImage->getClientOriginalExtension();
// dd(file_exists($dir . '/main' . $name), $dir . '/main' . $name, $dir . '/square' . $name, $dir . '/zoom' . $name, $dir . '/thumb' . $name);
        $unlink_imgs = [];
        $theImage->move($dir, 'ori' . $name);

        $theImgPath = public_path() . '/' . $dir . '/ori' . $name;
        $magicianObj = new imageLib($theImgPath);
        $magicianObj2 = new imageLib($theImgPath);

        list($curwidth, $curheight) = getimagesize($theImgPath);

        $newHeight = $curheight;
        $newWidth = $curwidth;

        if (!array_key_exists(1, $sizes))
            $sizes[1] = 1;
        if (!array_key_exists(2, $sizes))
            $sizes[2] = 1;

        if ($sizes[2] !== '1') {
            $zoom = $sizes[2];

            $newHeight = (int) ((($curheight + 10) * $zoom ) * 4);
            $newWidth = (int) ((($curwidth + 10) * $zoom ) * 4);
            if ($newHeight != 0)
                $magicianObj->resizeImage($newWidth, $newHeight, 0);
            if ($extension == 'png') {
                $magicianObj->saveImage($dir . '/zoom' . $name, 30);

                $this->convertImage($dir . '/zoom' . $name, $dir . '/zoom' . $name_without_ext . '.jpg', 30);
                if (file_exists($dir . '/zoom' . $name)) {
                    array_push($unlink_imgs, $dir . '/zoom' . $name);
                }
            } else {
                $magicianObj->saveImage($dir . '/zoom' . $name, 30);
            }
            $offset_x = (int) ((int)$sizes[0] * 4);
            $offset_y = (int) ((int)$sizes[1] * 4);

            $magicianObj_ = new imageLib(public_path() . '/' . $dir . '/zoom' . $name);
            $magicianObj_2 = new imageLib(public_path() . '/' . $dir . '/zoom' . $name);

            if ($offset_x < 0)
                $offset_x *= -1;
            if ($offset_y < 0)
                $offset_y *= -1;

            $magicianObj_->cropImage(700, 1050, ($offset_x) . 'x' . ($offset_y));
            $magicianObj_2->cropImage(700, 700, ($offset_x) . 'x' . ($offset_y));
            if ($extension == 'png') {
                $magicianObj_->saveImage($dir . '/main' . $name, 100);
                $this->convertImage($dir . '/main' . $name, $dir . '/main' . $name_without_ext . '.jpg', 30);
                if (file_exists($dir . '/main' . $name))
                    array_push($unlink_imgs, $dir . '/main' . $name);

                $magicianObj_2->saveImage($dir . '/square' . $name, 100);
                $this->convertImage($dir . '/square' . $name, $dir . '/square' . $name_without_ext . '.jpg', 30);
                if (file_exists($dir . '/square' . $name))
                    array_push($unlink_imgs, $dir . '/square' . $name);
            }else {
                $magicianObj_->saveImage($dir . '/main' . $name, 100);
                $magicianObj_2->saveImage($dir . '/square' . $name, 100);
            }
        } else {
            $offset_x = $sizes[0];
            $offset_y = $sizes[1];
            $magicianObj->cropImage(700, 1050, $offset_x . 'x' . $offset_y);
            $magicianObj2->cropImage(700, 700, $offset_x . 'x' . $offset_y);
            if ($extension == 'png') {
                $magicianObj->saveImage($dir . '/main' . $name, 100);
                $this->convertImage($dir . '/main' . $name, $dir . '/main' . $name_without_ext . '.jpg', 30);
                if (file_exists($dir . '/main' . $name))
                    array_push($unlink_imgs, $dir . '/main' . $name);

                $magicianObj2->saveImage($dir . '/square' . $name, 100);
                $this->convertImage($dir . '/square' . $name, $dir . '/square' . $name_without_ext . '.jpg', 30);
                if (file_exists($dir . '/square' . $name))
                    array_push($unlink_imgs, $dir . '/square' . $name);
            }else {
                $magicianObj->saveImage($dir . '/main' . $name, 100);
                $magicianObj2->saveImage($dir . '/square' . $name, 100);
            }
        }
        $magicianObj3 = new imageLib(public_path() . '/' . $dir . '/square' . $name);
        $magicianObj3->resizeImage(60, 60, 0);


        if ($extension == 'png') {
            $magicianObj3->saveImage($dir . '/thumb' . $name, 70);
            $this->convertImage($dir . '/thumb' . $name, $dir . '/thumb' . $name_without_ext . '.jpg', 70);
            if (file_exists($dir . '/thumb' . $name))
                array_push($unlink_imgs, $dir . '/thumb' . $name);
        }else {
            $magicianObj3->saveImage($dir . '/thumb' . $name, 70);
        }

        if (!empty($unlink_imgs)) {
            foreach ($unlink_imgs as $del_img) {
                unlink($del_img);
            }
        }

        return Redirect::back();
    }

    public function convertImage($originalImage, $outputImage, $quality)
    {
    // jpg, png, gif or bmp?
        $exploded = explode('.', $originalImage);
        $ext = $exploded[count($exploded) - 1];

        if (preg_match('/jpg|jpeg/i', $ext)) {
            $imageTmp = imagecreatefromjpeg($originalImage);
        } else if (preg_match('/png/i', $ext)) {
            $imageTmp = imagecreatefrompng($originalImage);
        } else if (preg_match('/gif/i', $ext)) {
            $imageTmp = imagecreatefromgif($originalImage);
        } else if (preg_match('/bmp/i', $ext)) {
            $imageTmp = imagecreatefrombmp($originalImage);
        } else {
            return 0;
        }

    // quality is a value from 0 (worst) to 100 (best)
        imagejpeg($imageTmp, $outputImage, $quality);
        imagedestroy($imageTmp);

        return 1;
    }

    public function deletePic()
    {
        $userid = Input::get('id');
        $size = 'main';

        $files = glob(ProfileController::$profilePicUploadDir . '/' . $size . '_c' . $userid . '_*');
        $files_alt = glob(ProfileController::$profilePicUploadDir . '/main_c' . $userid . '_*');

        if (!empty($files[0])) {
            unlink($files[0]);
        } elseif (!empty($files_alt[0])) {
            unlink($files_alt[0]);
        }

        return Redirect::back();
    }

    public function setAvailability()
    {
        $userid = Input::get('id');
        $user = User::find($userid);
        if ($user->active == '1') {
            $user->active = 0;
            $user->inactive_status = AccountController::$ACCOUNT_SITE_ADMIN_DEACTIVATION;
        } else {
            $user->active = 1;
        }
        $user->update();
        return Redirect::back()->with('user', $user);
    }

    public function showReports()
    {
        if (Input::has('read_not')) {
            if (Input::get('read_not') == 0) {
                $reports = Report::where('read', '=', 1)->paginate(10);
            //echo "throw read";
            } else {
                $reports = Report::where('read', '=', 0)->paginate(10);
                //echo "unread";
            }
            return view('backend.userReports')->with('reports', $reports);
        } else {
            $reports = Report::where('read', '=', '0')->paginate(10);
            return view('backend.userReports')->with('reports', $reports);
        }
    }

    public static function getUserName($userid)
    {
        $user = User::find($userid);
        return $user->fname . ' ' . $user->lname;
    }

    public static function markAsRead($id)
    {
        $report = Report::find($id);
        if ($report->read == 1) {
            $report->read = '0';
        } else {
            $report->read = '1';
        }
        $report->update();
        return Redirect::back();
    }

    public function searchUsersByName()
    {
        if (Input::has('query')) {
            $key = Input::get('query');
            if (preg_match('/[^a-z]/i', $key)) {
                $user = User::where('id', '=', $key)->where('id', '!=', Auth::id())->get();
            } else {
                $user = User::where('fname', 'LIKE', '%' . $key . '%')
                ->orWhere('lname', 'LIKE', '%' . $key . '%')
                ->where('id', '!=', Auth::id())
                ->paginate(10);
            }

            $url = route('users_Dashboard', ['query' => Input::get('query')], false);

            return Redirect($url)->with(['users' => $user, 'showLinks' => true])->withInput();
        } else {
            return Redirect::back()->with('searchError', true);
        }
    }

    public function showAdsPage()
    {

        if (Input::has('newAdd')) {
            $input = Input::all();
            $validator = Validator::make($input, [
                'title' => 'required',
                'content' => 'required'
            ]);
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator)->withInput();
            } else {
                $advert = new Advertisements();
                $advert->title = $input['title'];
                $advert->content = $input['content'];
                $advert->save();
                return Redirect::back();
            }
        } else if (Input::has('adid')) {
            $input = Input::all();
            $validator = Validator::make($input, [
                'edit_title' => 'required',
                'edit_content' => 'required',
            ]);
            if ($validator->passes()) {
                $adver = Advertisements::find(Input::get('adid'));
                $adver->title = Input::get('edit_title');
                $adver->content = Input::get('edit_content');
                $adver->update();
                return Redirect::back();
            } else {
                return Redirect::back()->withInput()->withErrors($validator);
            }
        } else if (Input::has('delete_id')) {
            $advert = Advertisements::find(Input::get('delete_id'));
            if (!empty($advert)) {
                if ($advert->active == 1) {
                    $advert->active = 0;
                } else {
                    $advert->active = 1;
                }
                $advert->update();
            }
            return Redirect::back();
        } else {
            $adverts = Advertisements::where('active', '!=', 3)->get();
            return view('backend.adsManage')->with(['ads' => $adverts]);
        }
    }

    public static function getUserData($userid)
    {
        $user = Searchuser::find($userid);
        return $user;
    }

    public static function getProfile($userid)
    {
        $profile = Profile::where('user_id', '=', $userid)->first();
        return $profile;
    }

    public static function getAge($id)
    {
        $age = DB::select("SELECT timestampdiff(YEAR,`bday`,curdate()) AS `age` from users where id='$id'");
        $ageVal = 0;
        foreach ($age as $s) {
            $ageVal = $s->age;
        }
        return $ageVal;
    }

    public function updateSettings()
    {
        $inputs = Input::all();
        $option_names = ['HNB_account_no', 'HNB_account_name', 'premium_package_price_LKR', 'premium_package_price_USD', 'paypal_package_name', 'merchant_button_link', 'package_expire_after'];
        $validator = Validator::make($inputs, [
            $option_names[0] => 'required',
            $option_names[1] => 'required',
            $option_names[2] => 'required',
            $option_names[3] => 'required',
            $option_names[4] => 'required',
            $option_names[5] => 'required',
            $option_names[6] => 'required'
        ]);
        if ($validator->passes()) {
            $count = 0;
            foreach ($inputs as $input) {
                if(isset($option_names[$count])){
                    $setting = CommonSetting::where('option_name', $option_names[$count])->first();
                    $setting->option_value = Input::get($option_names[$count]);
                    $setting->update();
                    $count++;
                }
            }
            return Redirect::back()->with('message', ['type' => 'info', 'message' => 'Successully updated the system!']);
        } else {
            //var_dump($validator->errors()); exit();
            return Redirect::back()->with('message', ['type' => 'error', 'message' => 'All inputs are required!']);
        }
    }

    public function showPayments()
    {
        $paymentsView = [];
        $payments = Payment::paginate(20);
        foreach ($payments as $payment) {
            $invoice = Invoice::find($payment->invoice_id);
            array_push($paymentsView, [
                'invoice_id' => $payment->invoice_id,
                'user_id' => $invoice->user_id,
                'pay_method' => $payment->pay_method,
                'txn_id' => $payment->txn_id,
                'amount' => $invoice->amount,
                'due_date' => $invoice->due_date,
                'cleared' => $payment->completed,
                'payment_id' => $payment->id
            ]);
        }
        return view('backend.payments')->with(['payments' => $paymentsView, 'data' => $payments]);
    }

    public function showVerify()
    {
        $input = Input::all();
        if (Input::has('profile_created') && Input::has('gender')) {
            $date = new DateTime();
            $nowTime = $date->format("Y-m-d h:i:s");

            if (Input::get('profile_created') == 1) { // Before week
                date_sub($date, date_interval_create_from_date_string('7 days'));
            } else if (Input::get('profile_created') == 30) { // Before month
                date_sub($date, date_interval_create_from_date_string('30 days'));
            } else if (Input::get('profile_created') == 90) { //Three month
                date_sub($date, date_interval_create_from_date_string('90 days'));
            } else {
                date_sub($date, date_interval_create_from_date_string('5000 months'));
            }
            $finalBefDate = date_format($date, 'Y-m-d h:i:s');
            $users = DB::table('users')
            ->select('*')
            ->where('id', '!=', 149582)
            ->where('id', '!=', Auth::id())
            ->where('mobilenumber', '!=', '987145632')
            ->where('admin_verified', '=', 0)
            ->where(function ($query) use ($input, $nowTime, $finalBefDate) {
                $query->whereBetween('created_at', [$finalBefDate, $nowTime]);
            })
            ->where(function ($query) {
                if (Input::get('gender') != '0') {
                    $query->where('gender', '=', Input::get('gender'));
                }
            })
            ->where(function ($query) {
                if (Input::get('user_state') != 'a') {
                    $query->where('active', '=', Input::get('user_state'));
                }
            })
            ->where(function ($query) {
                if (Input::get('user_package') != 'a') {
                    $query->where('role_id', '=', Input::get('user_package'));
                }
            })
            ->orderBy('id', 'DESC')
            ->paginate(10);
            return view("backend.userVerify")->with(['users' => $users, 'showLinks' => true, 'inputsOLD' => $input]);
        } else {
            $users = User::where('id', '!=', 149582)
            ->where('id', '!=', Auth::id())
            ->where('role_id', '!=', 2)
            ->where('admin_verified', '=', 0)
            ->orderBy('id', 'DESC')->paginate(10);
            return view("backend.userVerify")->with(['users' => $users, 'inputsOLD' => $input]);
        }
    }

    /*
     * New user verification
     */

    public function setVerify()
    {
        $user = User::find(Input::get('id'));
        $user->admin_verified = 1;
        $user->save();
    }

    public function cancelVerify()
    {
        $user = User::find(Input::get('id'));
        $user->admin_verified = 0;
        $user->save();
    }

    /*
     * End of New user verification
     */

    /*
     * User details edit verification
     */

    public function showUserProfileEdits()
    {
        $changes = AdminReviews::paginate(10);
        return view("backend.verifyEdits", [
            'changes' => $changes
        ]);
    }

    public function approveUserProfileEdit($id)
    {
        $adminReview = AdminReviews::find($id);

        if (empty($adminReview)) {
            return;
        }

        $user = User::find($adminReview->user_id);
        $key = $adminReview->key;

        switch ($key) {
            case 'mobilenumber':
            $user->mobile_verified = 1;
            case 'fname':
            case 'lname':
            $user->{$key} = $adminReview->value;
            break;
            case 'profile picture':
            ProfileController::replaceOldImagesWithApproved($user->id);
            break;
        }

        $user->save();
        $adminReview->delete();
    }

    public function ignoreUserProfileEdit($id)
    {
        $adminReview = AdminReviews::find($id);

        if (empty($adminReview)) {
            return;
        }

        $adminReview->delete();
    }

    /*
     * End of User details edit verification
     */
}
