<?php

namespace App\Http\Controllers;

use App\Library\Common;
use App\Library\ImageLib;
use App\Library\NoticeBag;
use App\Models\Searchuser;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProfileController
 *
 * @author Thambaru Wijesekara
 */
class ProfileController extends Controller
{

    public static $profilePicUploadDir = 'data/uploads/profile';

    public function ShowSelfProfileInfo()
    {
        Common::createMatchesRow();
        $profileDetails = User::with(['profiles', 'matches'])->find(Auth::user()->id);
        $serchUsersView = Searchuser::with('users')->find(Auth::user()->id);
        $last_online = $this->getLastOnlineText(Auth::user()->id);
        $formData = User::with('profiles')->find(Auth::id());
        return view('home.profile/profile', ['theUser' => $profileDetails, 'profPic' => $this::getProfileImageUrl(true, Auth::id()), 'isSelf' => true, 'userDetails' => $serchUsersView, 'lastonline' => $last_online, 'formData' => $formData]);
    }

    public function showPublicProfile()
    {
        $theId = Input::get('id');
        $th_user = User::find($theId);
        if (empty($theId) || empty($th_user)) {
            return Redirect::route('homePage');
        }
        $last_user_in_db = User::orderBy('id', 'desc')->first();
        $profileDetails = User::with(['profiles', 'matches'])->find($theId);
        $formData = User::with('profiles')->find($theId);
        if (Auth::check() && $theId == Auth::id()) {
            return Redirect::route('profilePage');
        }
        if (Common::isActiveUser($theId) && !Common::isAdministrator($theId) && $th_user->admin_verified == 1) {
            if (!empty($theId) && $theId <= $last_user_in_db->id && $theId > 149582) {
                $last_online = $this->getLastOnlineText($theId);
                if (Auth::check()) {
                    $block_query = BlockController::unblock_list();
                    $friends = FriendController::are_friends($theId);
                    $ignored = FriendController::has_ignored($theId);
                    $requested = FriendController::has_requested($theId);
                    $request_info = FriendController::request_info($theId);
                    $if_sender = FriendController::if_sender($theId);
                    $sameGender = Common::myGender($theId);
                    foreach ($block_query as $blocked) {
                        if ($blocked->blocker_id == $theId || $blocked->blocked_id == $theId) {
                            $hasBlocked = true;
                        } else {
                            $hasBlocked = false;
                        }
                    }
                    if (!$block_query->isEmpty() && $hasBlocked) {
                        return Redirect::route('profilePage')->with('global', NoticeBag::$noUserNotice);
                    } else {
                        return view('home.profile/profile', ['formData' => $formData, 'theUser' => $profileDetails, 'profPic' => self::getProfileImageUrl(true, $theId), 'isSelf' => false, 'lastonline' => $last_online, 'friends' => $friends, 'ignored' => $ignored, 'requested' => $requested, 'requestInfo' => $request_info, 'IsSender' => $if_sender, 'sameGender' => $sameGender]);
                    }
                } else {
                    $profile_pic = self::getProfileImageUrl(true, $theId);
                    return view('home.profile/profile', ['theUser' => $profileDetails, 'profPic' => $profile_pic, 'isSelf' => false, 'lastonline' => $last_online, 'friends' => false, 'ignored' => false, 'requested' => false, 'requestInfo' => null, 'IsSender' => false, 'sameGender' => false]);
                }
            } else {
                if (Auth::check()) {
                    return Redirect::route('profilePage');
                } else {
                    return Redirect::route('homePage');
                }
            }
        } else {
            return Redirect::route("404page");
        }
    }

    public static function getProfileImageUrl($notSelf = false, $id = null, $size = 'main', $suffix = '')
    {
        $id_ = NULL;
        if (!$notSelf) {
            $id_ = Auth::User()->id;
        } elseif (isset($id)) {
            $id_ = $id;
        }
        $isSelf = (Auth::check() && Auth::id() == $id ? true : false);
        $can_show = Common::canAccessbyPrivacy($id_, Common::$PRIV_COL_PROFILE_PIC);
        $user = User::find($id_);
        if (isset($user->gender)) {
            $gender = $user->gender;
        } else {
            $gender = "m";
        }
        if ($can_show || $isSelf) {

            $files = glob(self::$profilePicUploadDir . '/' . $size . '_c' . $id_ . $suffix . '_*');
            $files_alt = glob(self::$profilePicUploadDir . '/main_c' . $id_ . '_*');
            if (!empty($files[0])) {
                return end($files);
            } elseif (!empty(end($files_alt))) {
                return end($files_alt);
            } else {
                return ($gender == 'm' ? "/img/default-avatar-male-1x1.png" : "/img/default-avatar-female-1x1.png");
            }
        } else {
            return ($gender == 'm' ? "/img/default-avatar-male-1x1.png" : "/img/default-avatar-female-1x1.png");
        }
    }

    public static function userHasProfilePic()
    {
        if (Auth::check()) {
            $user_id = Auth::id();
            $size = 'main';
            $files = glob(self::$profilePicUploadDir . '/' . $size . '_c' . $user_id . '_*');
            if (!empty($files[0])) {
                return true;
            } else {
                return false;
            }
        }
    }

    public static function getDefaultProfilePic($gender)
    {
        if (isset($gender)) {
            return ($gender == 'm' ? "/img/default-avatar-male-1x1.png" : "/img/default-avatar-female-1x1.png");
        } else {
            return false;
        }
    }

    public function profilePictureUpload(Request $request)
    {

        $sizes = explode(',', $request->input('cropPosition'));
        if (!$request->has('theProfilePic')) {
            return redirect()->back();
        }

        // if (!$request->file('theProfilePic')->isValid()) {
        //     return redirect()->back();
        // }

        $theImage = $request->input('theProfilePicString');
        @list($type, $theImage) = explode(';', $theImage);
        @list(, $theImage) = explode(',', $theImage);
        $theImage = base64_decode($theImage);

        $existingFiles = glob(self::$profilePicUploadDir . '/*_c' . Auth::User()->id . 'pending_*');
        if (is_array($existingFiles)) {
            foreach ($existingFiles as $file) {
                unlink($file);
            }
        }

        $isPending = '';

        $isEmptyCurrentImg = glob(self::$profilePicUploadDir . '/*_c' . Auth::User()->id . '_*');

        if (!empty($isEmptyCurrentImg)) {
            // $isPending = 'pending';
            Common::setForAdminApproval('profile picture', 'has');
        }

        $dir = self::$profilePicUploadDir;
        $extension = strtolower('png');
        $name_without_ext = '_c' . Auth::User()->id . $isPending . '_' . time() . '_' . strtolower(str_random(15));
        $name = $name_without_ext . '.' . 'png';

        $unlink_imgs = [];
        // $theImage->move(public_path() . '/' .$dir, 'ori' . $name);

        $theImage = file_put_contents(public_path() . '/' . $dir . '/' . 'ori' . $name, $theImage);

        $theImgPath = public_path() . '/' . $dir . '/ori' . $name;

        $magicianObj = new ImageLib($theImgPath);
        $magicianObj2 = new ImageLib($theImgPath);

        list($curwidth, $curheight) = getimagesize($theImgPath);

        $newHeight = $curheight;
        $newWidth = $curwidth;
        // dd($newHeight, $newWidth, getimagesize($theImgPath));

        if (!array_key_exists(1, $sizes)) {
            $sizes[1] = 1;
        }
        if (!array_key_exists(2, $sizes)) {
            $sizes[2] = 1;
        }

        if ($sizes[2] !== '1') {
            $zoom = $sizes[2];

            $newHeight = (int) ((($curheight + 10) * $zoom) * 4);
            $newWidth = (int) ((($curwidth + 10) * $zoom) * 4);
            if ($newHeight != 0) {
                $magicianObj->resizeImage($newWidth, $newHeight, 0);
            }

            if ($extension == 'png') {
                $magicianObj->saveImage(public_path() . '/' . $dir . '/zoom' . $name, 30);

                $this->convertImage(public_path() . '/' . $dir . '/zoom' . $name, public_path() . '/' . $dir . '/zoom' . $name_without_ext . '.jpg', 30);
                if (file_exists(public_path() . '/' . public_path() . '/' . $dir . '/zoom' . $name)) {
                    array_push($unlink_imgs, public_path() . '/' . $dir . '/zoom' . $name);
                }
            } else {
                $magicianObj->saveImage(public_path() . '/' . $dir . '/zoom' . $name, 30);
            }
            $offset_x = (int) ((int) $sizes[0] * 4);
            $offset_y = (int) ((int) $sizes[1] * 4);

            $magicianObj_ = new imageLib(public_path() . '/' . $dir . '/zoom' . $name);
            // dd($dir, $name);
            $magicianObj_2 = $magicianObj_;
            // dd($sizes);

            if ($offset_x < 0) {
                $offset_x *= -1;
            }
            if ($offset_y < 0) {
                $offset_y *= -1;
            }

            // dd('d');
            // $magicianObj_->cropImage(700, 1050, ($offset_x) . 'x' . ($offset_y));
            // $magicianObj_2->cropImage(700, 700, ($offset_x) . 'x' . ($offset_y));
            if ($extension == 'png') {
                $magicianObj_->saveImage(public_path() . '/' . $dir . '/main' . $name, 100);
                $this->convertImage(public_path() . '/' . $dir . '/main' . $name, $dir . '/main' . $name_without_ext . '.jpg', 30);
                if (file_exists(public_path() . '/' . $dir . '/main' . $name)) {
                    array_push($unlink_imgs, public_path() . '/' . $dir . '/main' . $name);
                }

                $magicianObj_2->saveImage(public_path() . '/' . $dir . '/square' . $name, 100);
                $this->convertImage(public_path() . '/' . $dir . '/square' . $name, public_path() . '/' . $dir . '/square' . $name_without_ext . '.jpg', 30);
                if (file_exists(public_path() . '/' . $dir . '/square' . $name)) {
                    array_push($unlink_imgs, public_path() . '/' . $dir . '/square' . $name);
                }
            } else {
                $magicianObj_->saveImage(public_path() . '/' . $dir . '/main' . $name, 100);
                $magicianObj_2->saveImage(public_path() . '/' . $dir . '/square' . $name, 100);
            }
        } else {
            $offset_x = $sizes[0];
            $offset_y = $sizes[1];
            // $magicianObj->cropImage(700, 1050, $offset_x . 'x' . $offset_y);
            // $magicianObj2->cropImage(700, 700, $offset_x . 'x' . $offset_y);
            if ($extension == 'png') {
                $magicianObj->saveImage(public_path() . '/' . $dir . '/main' . $name, 100);
                $this->convertImage(public_path() . '/' . $dir . '/main' . $name, public_path() . '/' . $dir . '/main' . $name_without_ext . '.jpg', 30);
                if (file_exists(public_path() . '/' . $dir . '/main' . $name)) {
                    array_push($unlink_imgs, public_path() . '/' . $dir . '/main' . $name);
                }

                $magicianObj2->saveImage(public_path() . '/' . $dir . '/square' . $name, 100);
                $this->convertImage(public_path() . '/' . $dir . '/square' . $name, public_path() . '/' . $dir . '/square' . $name_without_ext . '.jpg', 30);
                if (file_exists(public_path() . '/' . $dir . '/square' . $name)) {
                    array_push($unlink_imgs, public_path() . '/' . $dir . '/square' . $name);
                }
            } else {
                $magicianObj->saveImage(public_path() . '/' . $dir . '/main' . $name, 100);
                $magicianObj2->saveImage(public_path() . '/' . $dir . '/square' . $name, 100);
            }
        }
        $magicianObj3 = new imageLib(public_path() . '/' . $dir . '/square' . $name);
        $magicianObj3->resizeImage(60, 60, 0);


        if ($extension == 'png') {
            $magicianObj3->saveImage(public_path() . '/' . $dir . '/thumb' . $name, 70);
            $this->convertImage(public_path() . '/' . $dir . '/thumb' . $name, public_path() . '/' . $dir . '/thumb' . $name_without_ext . '.jpg', 70);
            if (file_exists(public_path() . '/' . $dir . '/thumb' . $name)) {
                array_push($unlink_imgs, public_path() . '/' . $dir . '/thumb' . $name);
            }
        } else {
            $magicianObj3->saveImage(public_path() . '/' . $dir . '/thumb' . $name, 70);
        }

        if (!empty($unlink_imgs)) {
            foreach ($unlink_imgs as $del_img) {
                unlink($del_img);
            }
        }

        return Redirect::route('showSettings', 'privacy');
    }

    public function convertImage($originalImage, $outputImage, $quality)
    {
        // jpg, png, gif or bmp?
        $exploded = explode('.', $originalImage);
        $ext = $exploded[count($exploded) - 1];

        if (preg_match('/jpg|jpeg/i', $ext)) {
            $imageTmp = imagecreatefromjpeg($originalImage);
        } else if (preg_match('/png/i', $ext)) {
            $imageTmp = imagecreatefrompng($originalImage);
        } else if (preg_match('/gif/i', $ext)) {
            $imageTmp = imagecreatefromgif($originalImage);
        } else if (preg_match('/bmp/i', $ext)) {
            $imageTmp = imagecreatefrombmp($originalImage);
        } else {
            return 0;
        }

        // quality is a value from 0 (worst) to 100 (best)
        imagejpeg($imageTmp, $outputImage, $quality);
        imagedestroy($imageTmp);

        return 1;
    }

    public static function get_ProfileBy($value)
    {
        switch ($value): case 1:
                echo trans('translator.BySelf');
                break;
            case 2:
                echo trans('translator.ByParent');
                break;
            case 3:
                echo trans('translator.ByParent');
                break;
            case 4:
                echo trans('translator.BySibling');
                break;
            case 5:
                echo trans('translator.ByFriend');
                break;
            case 6:
                echo trans('translator.ByRelative');
                break;
        endswitch;
    }

    public static function get_ProfileName($id, $return = false)
    {
        if (PrivacyController::allowedByPrivacy($id, Common::$PRIV_COL_PROF_NAME)) {
            $name = User::with(['profiles'])->find($id);
            if (!$return) {
                echo $name['fname'] . ' ' . $name['lname'];
            } else {
                return $name['fname'] . ' ' . $name['lname'];
            }
        } else {
            return trans('translator.MangalaYojanaUser');
        }
    }

    public function getLastOnlineText($Userid)
    {
        $user = User::find($Userid);

        $lasv = $user->lastvisit;
        $string = Common::getAgoTimeString(strtotime($lasv));
        return $string;
    }

    public static function getProfileUrl($user_id)
    {
        return URL::route('publicProfile', ['id' => $user_id]);
    }

    public static function replaceOldImagesWithApproved($userId)
    {
        $existingFiles = glob(self::$profilePicUploadDir . '/*_c' . $userId . '_*');
        if (is_array($existingFiles)) {
            foreach ($existingFiles as $file) {
                unlink($file);
            }
        }
        $newFiles = glob(self::$profilePicUploadDir . '/*_c' . $userId . 'pending_*');
        if (is_array($newFiles)) {
            foreach ($newFiles as $fileName) {
                $newName = str_replace('pending', '', $fileName);
                rename($fileName, $newName);
            }
        }
    }
}
