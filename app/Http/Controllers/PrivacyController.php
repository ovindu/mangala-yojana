<?php

namespace App\Http\Controllers;

use App\Library\Common;
use App\Models\PrivacySetting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PrivacyController
 *
 * @author Thambaru Wijesekara
 */
class PrivacyController extends Controller
{

    public static function setPrivacy()
    {
        $inputs = Input::all();
        $field = $inputs['field'];
        $privacyStep = $inputs['privacy-step'];
        $update = true;
        $psettings = PrivacySetting::where('user_id', '=', Auth::id())->first();
        if (empty($psettings)) {
            $update = false;
            $psettings = new PrivacySetting();
        }
        switch ($field) {
            case "profile_picture":
                $psettings->profile_picture = $privacyStep;
                echo 'profile pic <br>';
                break;
            case "family_details":
                $psettings->family_details = $privacyStep;
                break;
            case "location":
                //return PrivacySetting::firstOrCreate(array('user_id' => Auth::id()))->update(array('location' => $privacyStep));
                $psettings->location = $privacyStep;
                break;
            case "contact_info":
                $psettings->contact_info = $privacyStep;
                break;
            case "profile_name":
                $psettings->profile_name = $privacyStep;
            case "edu_and_prof":
                $psettings->edu_and_prof = $privacyStep;
        }
        if ($update) {
            $psettings->update();
        } else {
            $psettings->save();
        }
    }

    public static function allowedByPrivacy($id, $field)
    {
        $step = PrivacySetting::firstOrCreate(['user_id' => $id])->$field;
        $isSelf = Common::isSelf($id);
        /*
         * Registered user only : 0
         * Friends only : 1
         * Public : 2
         */
        if ($isSelf) {
            return true;
        } elseif ($step == 0 && Auth::check()) {
            return true;
        } elseif ($step == 1 && FriendController::are_friends($id)) {
            return true;
        } elseif ($step == 2) {
            return true;
        } else {
            return false;
        }
    }

    public static function displayPrivacyIcon($field)
    {
        $step = PrivacySetting::firstOrCreate(['user_id' => Auth::id()])->$field;
        switch ($step) {
            case 0:
                echo "glyphicon-resize-small";
                break;
            case 1:
                echo "glyphicon-resize-horizontal";
                break;
            case 2:
                echo "glyphicon-globe";
                break;
            case 3:
                echo "glyphicon-lock";
                break;
            default:
                echo "glyphicon-eye-open";
                break;
        }
    }

    public static function getPrivacyName($field)
    {
        $step = PrivacySetting::firstOrCreate(['user_id' => Auth::id()])->$field;
        switch ($step) {
            case 0:
                echo trans('translator.RegisteredUsersOnly');
                break;
            case 1:
                echo trans('translator.FriendsOnly');
                break;
            case 2:
                echo trans('translator.premiumPublic');
                break;
            case 3:
                echo trans('translator.OnlyMe');
                break;
            default:
                echo trans('translator.FriendsOnly');
                break;
        }
    }
}
