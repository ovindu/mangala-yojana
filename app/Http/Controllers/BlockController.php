<?php

namespace App\Http\Controllers;

use App\Models\Block;
use App\Models\FRequests;
use App\Models\Report;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class BlockController extends Controller
{

    public function doBlock()
    {
        try {
            $id = Input::get('id');
            if (!empty($id) && $id <> Auth::user()->id) {
                $block_table = new Block;
                $block_table->blocker_id = Auth::user()->id;
                $block_table->blocked_id = $id;
                $block_table->save();
                FRequests::where(function ($query) use ($id) {
                            $query->where('sender_id', '=', Auth::user()->id)->where('receiver_id', '=', $id);
                })
                        ->Orwhere(function ($query) use ($id) {
                            $query->where('sender_id', '=', $id)->where('receiver_id', '=', Auth::user()->id);
                        })
                        ->delete();
            }

            return Redirect::back();
        } catch (Exception $ex) {
        }
    }

    public static function unblock_list()
    {
        try {
            return Block::where(function ($query) {
                                $query->where('blocker_id', '=', Auth::id())->orWhere('blocked_id', '=', Auth::id());
            })
                            ->get();
        } catch (Exception $ex) {
        }
    }

    public static function doUnblock($id)
    {
        try {
            Block::where('blocker_id', '=', Auth::id())->where('blocked_id', '=', $id)->delete();
        } catch (Exception $ex) {
        }
    }
    
    public static function submitUserComplain()
    {
        if (Input::has('message')) {
            $report = new Report();
            $report->reporter_id = Auth::id();
            $report->reported_id = 149582;
            $report->description = 'Payment failed : message -> '.Input::get('message');
            $report->read = 0;
            $report->save();
            return Redirect::route('publicProfile');
        } else {
            return Redirect::back()->with('class', 'validateError');
        }
    }
}
