<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Package
 *
 * @author Sahan
 */
class MyojanaPackage extends Model
{
    protected $table = 'packages';
    protected $fillable = ['title'];
    public $timestamps = false;
}
