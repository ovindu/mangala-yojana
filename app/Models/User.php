<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    protected $connection = 'mysql';
    protected $fillable = ['verified'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function profiles()
    {

        return $this->hasOne('App\Models\Profile');
    }

    public function matches()
    {

        return $this->hasOne('App\Models\Matches');
    }

    public function searchusers()
    {
        return $this->hasOne('App\Models\Searchuser', 'id', 'id');
    }

    public function messages()
    {
        return $this->hasOne('App\Models\Message', 'id', 'sender_id');
    }
    
    public function blockers()
    {
        return $this->hasMany('App\Models\Block', 'blocked_id', 'id');
    }
    
    public function role()
    {
        return $this->hasOne('App\Models\UserRoles', 'id', 'role_id');
    }
}
