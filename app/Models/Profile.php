<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{

    protected $table = 'profiles';

    public $timestamps = false;
    
    public function users()
    {
        return $this->hasOne('App\Models\User');
    }
}
