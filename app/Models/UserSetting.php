<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UserSettings
 *
 * @author Thambaru Wijesekara
 */
class UserSetting extends Model
{

    protected $table = 'user_settings';
    protected $fillable = ['user_id', 'new_msg', 'profile_changed', 'new_request', 'became_friends'];
    public $timestamps = false;

    public function users()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}
