<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Block
 *
 * @author Thambaru Wijesekara
 */
class Block extends Model
{
    protected $table = 'blocks';
    public $timestamps = false;
    
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'blocked_id', 'id');
    }
}
