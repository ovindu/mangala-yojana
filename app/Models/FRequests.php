<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FRequests
 *
 * @author Thambaru Wijesekara
 */
class FRequests extends Model
{

    protected $table = 'frequests';
    protected $fillable = ['sender_id', 'receiver_id', 'friended'];

    public function users()
    {
        return $this->hasOne('App\Models\User', 'id', 'sender_id');
    }

    public function profiles()
    {
        return $this->hasOne('App\Models\Profile', 'user_id', 'sender_id');
    }
}
