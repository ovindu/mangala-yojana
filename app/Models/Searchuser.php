<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Searchuser
 *
 * @author Thambaru Wijesekara
 */
class Searchuser extends Model
{

    protected $table = 'searchusers';
    protected $connection = 'mysql';

    public function profile()
    {

        return $this->hasOne('App\Models\Profile', 'user_id', 'id');
    }
    public function blocks()
    {
        return $this->hasMany('App\Models\Block', 'blocker_id', 'id');
    }

    public function matches()
    {

        return $this->hasOne('App\Models\Matches', 'user_id', 'id');
    }
    public function users()
    {

        return $this->hasOne('App\Models\User', 'id', 'id');
    }
}
