<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Messages
 *
 * @author Thambaru Wijesekara
 */
class Message extends Model
{

    protected $table = 'messages';

    public function user()
    {

        return $this->belongsTo('App\Models\User', 'sender_id', 'id');
    }
}
