<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of matches
 *
 * @author Thambaru Wijesekara
 */
class Matches extends Model
{

    protected $table = 'matches';
    protected $fillable = ['*'];
    public $timestamps = false;

    public function users()
    {
        return $this->hasOne('App\Models\user');
    }

    public function searchusers()
    {

        return $this->hasOne('App\Models\Searchuser', 'user_id', 'id');
    }

    public function profiles()
    {

        return $this->hasOne('App\Models\Profile', 'user_id', 'user_id');
    }
}
