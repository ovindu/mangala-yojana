<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PrivacySettings
 *
 * @author Thambaru Wijesekara
 */
class PrivacySetting extends Model
{

    protected $table = 'privacy_settings';
    protected $fillable = ['user_id', 'profile_picture', 'family_details', 'location'];
    public $timestamps = false;

    public function user()
    {
        return $this->hasOne('App\Models\users');
    }
}
